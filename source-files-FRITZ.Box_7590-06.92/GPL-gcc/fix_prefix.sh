#! /bin/sh

mypath=`dirname $0`
mypath=`realpath ${mypath}`
. ${mypath}/post_install.cfg

if [ -z ${ORIG_BUILD_PATH} ]
then
    echo "Original build path unknown! Aborting."
    exit 1
fi

all_files=0
skipped_files=0
echo -n "Adjusting paths in tool chain... "
while read i;
do
    i="${mypath}/${i}"
    
    chmod +w "${i}"
    ftype=`file --brief --mime-type "${i}"`
    if [ ${ftype##text/} != ${ftype} ]
    then
        sed -i "s|${ORIG_BUILD_PATH}|${mypath}/usr|g" "${i}"
    elif [ ${ftype##application/} != ${ftype} ]
    then
        ${mypath}/patch_binary_strings "${i}"  "${ORIG_BUILD_PATH}" "${mypath}/usr"
    else
        skipped_file=$((${skipped_files} + 1))
    fi
    all_files=$((${all_files} + 1))
done < ${mypath}/files_to_patch.cfg

echo "Done. ${all_files} checked, ${skipped_files} skipped"

