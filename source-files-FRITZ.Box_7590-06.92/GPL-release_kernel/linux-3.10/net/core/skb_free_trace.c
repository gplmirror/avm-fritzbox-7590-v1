#include <linux/avm_debug.h>
#include <linux/hashtable.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/netdevice.h>
#include <linux/simple_proc.h>
#include <linux/stacktrace.h>
#include <linux/types.h>

struct skb_free_stack {
	unsigned long trace_entries[64];
	struct stack_trace trace;
};

struct skb_log_record {
	struct hlist_node hlist;
	struct sk_buff *skb;
	uint64_t jiffies;
	atomic_t extralife;
	struct skb_free_stack stack;
	unsigned int cpu;
};

void save_skb_free_stack(struct skb_free_stack *fstack);
void show_skb_free_stack(void);

DEFINE_HASHTABLE(skb_log, 10);
static spinlock_t skb_log_lock[1 << 10];
static bool skb_stack_disabled;

static uint32_t skb_log_limit(void)
{
	return 10;
}

void skb_log_add(struct sk_buff *skb)
{
	uint32_t cap;
	struct skb_log_record *cur;
	struct hlist_node *tmp;
	struct hlist_head disposal = HLIST_HEAD_INIT;
	unsigned long flags;
	unsigned long bkt;

	if(likely(skb_stack_disabled)) return;

	cap = skb_log_limit() - 1;

	/* create new record */
	cur = kmalloc(sizeof(*cur), GFP_ATOMIC);
	memset(cur, 0, sizeof(*cur));
	cur->jiffies = jiffies;
	atomic_set(&cur->extralife, 4);
	cur->skb = skb;
	cur->cpu = smp_processor_id();
	save_skb_free_stack(&cur->stack);

	bkt = hash_min((u32)cur->skb, HASH_BITS(skb_log));
	spin_lock_irqsave(&skb_log_lock[bkt], flags);
	{
		hash_add(skb_log, &cur->hlist, (unsigned long)cur->skb);

		/* destroy old entries */
		hlist_for_each_entry_safe(cur, tmp, &skb_log[bkt], hlist)
		{
			if(cap == 0 ||
			   (cur->skb == skb && atomic_dec_and_test(&cur->extralife))) {
				hash_del(&cur->hlist);
				hlist_add_head(&cur->hlist, &disposal);
			} else {
				cap--;
			}
		}
	}
	spin_unlock_irqrestore(&skb_log_lock[bkt], flags);

	hlist_for_each_entry_safe(cur, tmp, &disposal, hlist) { kfree(cur); }
}
EXPORT_SYMBOL(skb_log_add);

static int _skb_log_show(struct sk_buff *skb)
{
	bool found = 0;
	struct skb_log_record *cur;
	unsigned long flags;
	unsigned long bkt;

	printk("Searching the SKB free log for *%p\n", skb);

	bkt = hash_min((u32)skb, HASH_BITS(skb_log));
	spin_lock_irqsave(&skb_log_lock[bkt], flags);
	{
		hash_for_each_possible(skb_log, cur, hlist, (unsigned long)skb)
		{
			if(skb == cur->skb) {
				printk(
				 "skb(%p) #%u: cpu=%u jiffies=%llu skb->next=%p skb->prev=%p\n",
				 skb,
				 3 - atomic_read(&cur->extralife),
				 cur->cpu,
				 cur->jiffies,
				 skb->next,
				 skb->prev);
				print_stack_trace(&cur->stack.trace, 1);
				found = true;
			}
		}
	}
	spin_unlock_irqrestore(&skb_log_lock[bkt], flags);

	if(!found) printk("Nothing found.\n");

	return found;
}

void skb_log_show(struct sk_buff *skb)
{
	bool found = 0;

	found = _skb_log_show(skb);
	if(skb->next && skb->next != skb && skb->next->prev != skb) {
		printk(
		 "skb->next (*%p) inconsistency: skb->next->prev (*%p) != skb (%p)\n",
		 skb->next,
		 skb->next->prev,
		 skb);
		_skb_log_show(skb->next);
	}
	if(skb->prev && skb->prev != skb && skb->prev->next != skb) {
		printk(
		 "skb->prev (*%p) inconsistency: skb->prev->next (*%p) != skb (%p)\n",
		 skb->prev,
		 skb->prev->next,
		 skb);
		_skb_log_show(skb->prev);
	}
	printk("%s stack trace:\n", __func__);
	dump_stack();
	avm_DebugSignal(1); /* crashreport */
}
EXPORT_SYMBOL(skb_log_show);

void save_skb_free_stack(struct skb_free_stack *fstack)
{
	fstack->trace.nr_entries = 0;
	fstack->trace.entries = fstack->trace_entries;
	fstack->trace.max_entries = ARRAY_SIZE(fstack->trace_entries);
	fstack->trace.skip = 0;
	save_stack_trace(&fstack->trace);
}

static int proc_write_stack(char *buffer, void *priv __attribute__((unused)))
{
	if(strstr(buffer, "disable")) {
		skb_stack_disabled = true;
	}
	if(strstr(buffer, "enable")) {
		skb_stack_disabled = false;
	}
	return 0;
}

static void proc_read_stack(struct seq_file *seq,
                            void *priv __attribute__((unused)))
{
	unsigned long bkt, acc_bkt;
	unsigned int acc;
	struct skb_log_record *cur;

	acc_bkt = acc = 0;
	hash_for_each(skb_log, bkt, cur, hlist)
	{
		if(bkt != acc_bkt) {
			if(acc > 0) seq_printf(seq, "Bucket %lu: %u\n", bkt, acc);
			acc = 0;
			acc_bkt = bkt;
		}
		acc++;
	}
	if(acc > 0) seq_printf(seq, "Bucket %lu: %u\n", bkt, acc);
}


static __init int init_free_stack(void)
{
	add_simple_proc_file("skb_free_stack",
	                     proc_write_stack,
	                     proc_read_stack,
	                     NULL);
	return 0;
}
module_init(init_free_stack)

