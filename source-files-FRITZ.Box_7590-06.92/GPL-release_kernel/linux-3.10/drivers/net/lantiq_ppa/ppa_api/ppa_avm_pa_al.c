#include <linux/ip.h>
#include <net/ip.h>
#include <net/ipv6.h>
#include <net/ip6_tunnel.h>
#include <linux/netdevice.h>
#include <net/ppa_stack_al.h>
#include <net/ppa_api.h>
#include <net/ppa_hook.h>
#include <linux/types.h>
#include <linux/avm_pa.h>
#include <linux/avm_pa_hw.h>
#include <linux/avm_pa_intern.h>
#include <net/ppa_avm_pa_al.h>
#include <linux/workqueue.h>
#include <linux/pkt_sched.h>
#include <net/ppa_avm_pa_al.h>
#include <lantiq.h>
#include <lantiq_soc.h>
#include <net/ltq_mpe_api.h>
#include <linux/hashtable.h>
#include <linux/avm_profile.h>
#include <net/datapath_api.h>
#include <asm/mach-lantiq/cpufreq/ltq_cpufreq.h>

#include "ppa_api_misc.h"
#include "ppa_api_netif.h"
#include "ppa_stack_tnl_al.h"
#define FENTRY_BUF(ppa_buf)                                               \
	pr_debug("called sh=%d, by %pF\n", ((ppa_buf) && (ppa_buf)->pa_session) ?     \
	                            ((ppa_buf)->pa_session->session_handle) : \
	                            0, __builtin_return_address(0))
#define FENTRY_NO_BUF pr_debug("called\n");

#define RT_EXTID_TCP	    0
#define RT_EXTID_UDP	    100

#define TX_CHAN_GRACE_PERIOD 100

atomic_t active_sessions = ATOMIC_INIT(0);
static const uint8_t pae_hash_width = 12;
static DEFINE_HASHTABLE(pae_hash_table, 12);
struct net_device *avm_ppa_localif;
EXPORT_SYMBOL(avm_ppa_localif);

/* used to inhibit CPU downscaling e.g. for CPU-bound real-time sessions */
static bool low_cpu_latency_required;

struct session_work_item {
	struct avm_pa_session *session;
	struct work_struct work;
};

struct pae_hash_item {
	struct hlist_node hlist;
	struct avm_pa_session *session;
	struct kref kref;
	unsigned long last_hit;
	bool alive;
};

PPA_FILE_OPERATIONS *ppadev_fops;
EXPORT_SYMBOL(ppadev_fops);
static struct workqueue_struct *sessions_workqueue;
static int get_brif(PPA_NETIF *netif, PPA_NETIF **brif);

static uint64_t hw_bytes_last[CONFIG_AVM_PA_MAX_SESSION];
static struct avm_pa_session *active_pa_sessions[CONFIG_AVM_PA_MAX_SESSION];
static struct list_head virt_avm_dpipe_netdevs;
static bool pid_is_local(uint32_t pid_handle);
static bool pid_is_valid(uint32_t pid_handle);
static void *get_hdr_from_pa_match(const struct avm_pa_pkt_match *match,
                                   unsigned char type);
static void release_hash_item(struct kref *r);

int32_t (*build_netif_tree_hook)(PPA_BUF *) = NULL;
EXPORT_SYMBOL(build_netif_tree_hook);

void (*reset_netif_tree_hook)(void) = NULL;
EXPORT_SYMBOL(reset_netif_tree_hook);

/*
 * AVM PA hardware session handler
 */

static uint32_t ppacmd(uint32_t cmd, void *arg)
{
	uint32_t ppa_rv;
	mm_segment_t oldseg;

	/* adjust segment boundaries in order to pass access_ok() checks
	 * during ioctl call */
	oldseg = get_fs();
	set_fs(get_ds());

	ppa_rv = ppadev_fops->unlocked_ioctl(NULL, cmd, (unsigned long)arg);

	set_fs(oldseg);

	return ppa_rv;
}

static int32_t
get_routing_session_by_handle(PPA_CMD_SESSIONS_INFO *sessions_info,
                              uint32_t max_count,
                              uint32_t session_handle)
{
	uint32_t ppa_rv;

	memset(&sessions_info->count_info, 0, sizeof(sessions_info->count_info));

	sessions_info->count_info.count = max_count;
	sessions_info->count_info.stamp_flag = 0;
	sessions_info->count_info.hash_index = session_handle + 1;
	sessions_info->count_info.flag = SESSION_ADDED_IN_HW;

	ppa_rv = ppacmd(PPA_CMD_GET_LAN_WAN_SESSIONS, sessions_info);

	return ppa_rv;
}

static int collect_bridging_count(struct avm_pa_session *session,
                                  struct avm_pa_session_stats *ingress)
{
	struct avm_pa_pid_hwinfo *hwinfo;
	uint32_t last_hit_time;
	PPA_NETIF *brif;

	ingress->validflags = 0;
	if(active_pa_sessions[session->session_handle] && session->bsession) {

		hwinfo = avm_pa_pid_get_hwinfo(session->egress[0].pid_handle);
		/* Race with avm_pa_dev_unregister. hwinfo still might be
		 * invalid, see JZ-27130. */
		if(!hwinfo) return -ENOENT; 

		brif = get_brif(hwinfo->ppa.netdev, &brif) ? NULL : brif;
		if(ppa_hook_bridge_entry_hit_time_fn(session->bsession->ethh.h_dest,
		                                     brif,
		                                     &last_hit_time) == PPA_HIT) {
			uint8_t is_static;
			uint32_t now, delta;

			/* PPA_HIT just means that the session isn't
			 * timed out yet, so we have to consider the
			 * actual hit time */
			now = ppa_get_time_in_sec();
			is_static = now < last_hit_time; /* false -> not sure */
			/* DEFAULT_BRIDGING_TIMEOUT_IN_SEC */
			if(is_static) last_hit_time += UINT_MAX - 300;
			delta = now - last_hit_time;

			pr_debug("session %d hit last at %u (delta=%u).\n",
				 session->session_handle, last_hit_time, delta);
			if(delta <= CONFIG_LTQ_PPA_AVM_PA_COUNTER_INTERVAL/1000) {
				pr_debug("report.\n");
				ingress->validflags = AVM_PA_SESSION_STATS_VALID_HIT;
			} else {
				ingress->validflags = 0;
			}
		} else {
			pr_debug("bridging session %d not (yet) registered with PPA.\n",
				 session->session_handle);
		}
	}

	return 0;
}

static int collect_routing_count(struct avm_pa_session *pa_session,
                                 struct avm_pa_session_stats *ingress)
{
	PPA_CMD_SESSION_ENTRY *session_entry;
	uint64_t delta;
	uint64_t hw_bytes;
	PPA_CMD_SESSIONS_INFO sessions_info;
	uint32_t ppa_rv;
	const uint32_t max_count = 1;
	void *hw_session;

	FENTRY_NO_BUF;

	/* only fetch session count if session was added successfully */
	hw_session = avm_pa_get_hw_session(pa_session);
	if(hw_session == NULL || ((char *)hw_session)[0] != 'y') return -ENOENT;

	ppa_rv = get_routing_session_by_handle(&sessions_info,
	                                       max_count,
	                                       pa_session->session_handle);

	if(ppa_rv != PPA_SUCCESS || sessions_info.count_info.count != 1) {
		pr_debug(
		 "could not get ppa session info for avm_pa session %d (results: %d)\n",
		 pa_session->session_handle,
		 sessions_info.count_info.count);
		return -ENOENT;
	}

	/* aliases */
	session_entry = &sessions_info.session_list[0];
	hw_bytes = session_entry->hw_bytes;

	if(unlikely(hw_bytes < hw_bytes_last[pa_session->session_handle])) {
		/* counter wraparound behandeln, falls eine session
		 * mal 4679 Jahre mit 1Gbps läuft. ;) */
		delta =
		 hw_bytes + ULLONG_MAX - hw_bytes_last[pa_session->session_handle];
	} else {
		delta = hw_bytes - hw_bytes_last[pa_session->session_handle];
	}

	pr_debug(
	 "pa_session %d (hash: %u) delta=%llu hw_bytes=%llu hw_bytes_last=%llu\n",
	 pa_session->session_handle,
	 session_entry->hash,
	 delta,
	 hw_bytes,
	 hw_bytes_last[pa_session->session_handle]);

	if (delta){
		ingress->validflags = AVM_PA_SESSION_STATS_VALID_HIT;
		ingress->validflags |= AVM_PA_SESSION_STATS_VALID_BYTES;
		ingress->tx_bytes = delta;
	} else  {
		ingress->validflags = 0;
	}

	hw_bytes_last[pa_session->session_handle] = hw_bytes;

	return 0;
}

/* get bridge from a bridge port via linux stack */
static int _get_brif(PPA_NETIF *netif, PPA_NETIF **brif, bool wait)
{
	if(!netif || !brif) return -EINVAL;
	if((netif->priv_flags & IFF_BRIDGE_PORT) == 0) {
		return -ENOENT;
	}

	if(wait) {
		rtnl_lock();
	} else if(rtnl_trylock() != 1) {
		return -EAGAIN;
	}

	*brif = ppa_netdev_master_upper_dev_get(netif);
	if(!*brif || !ppa_if_is_br_if(*brif, (*brif)->name)) {
		rtnl_unlock();
		return -ENOENT;
	}

	rtnl_unlock();
	return 0;
}

static int get_brif(PPA_NETIF *netif, PPA_NETIF **brif)
{
	return _get_brif(netif, brif, 0);
}

static int get_brif_bh(PPA_NETIF *netif, PPA_NETIF **brif)
{
	BUG_ON(in_atomic());
	return _get_brif(netif, brif, 1);
}

static int add_br_session(PPA_BUF *ppa_buf)
{
	struct avm_pa_pid_hwinfo *avm_hwinfo;
	PPA_NETIF *brif, *portif;
	int ppa_rv = PPA_FAILURE;
	struct avm_pa_session *session;

	BUG_ON(!ppa_buf);
	session = ppa_buf->pa_session;

	avm_hwinfo = avm_pa_pid_get_hwinfo(session->egress[0].pid_handle);
	if(!avm_hwinfo) {
		avm_pa_set_hw_session( session, "no hwinfo" );
		return AVM_PA_TX_ERROR_SESSION;
	}
	portif = avm_hwinfo->ppa.netdev;

	if(get_brif_bh(portif, &brif)){
		avm_pa_set_hw_session( session, "no bridging if" );
	       	return AVM_PA_TX_ERROR_SESSION;
	}

	if(ppa_hook_bridge_entry_add_fn != NULL) {
		ppa_rv =
		 ppa_hook_bridge_entry_add_fn(session->bsession->ethh.h_dest,
		                              // brif, portif, PPA_F_STATIC_ENTRY);
		                              brif, portif, 0);
	}

	if(ppa_rv == PPA_SESSION_ADDED) {
		return AVM_PA_TX_SESSION_ADDED;
	} else {
		/* this happens when the GSWIP-R MAC table lock couldn't be
		 * claimed for too long */
		pr_warn("[%s] failed\n", __func__);
		avm_pa_set_hw_session( session, "failed" );
		return AVM_PA_TX_ERROR_SESSION;
	}
}

static int remove_br_session(PPA_BUF *ppa_buf)
{
	struct avm_pa_pid_hwinfo *avm_hwinfo;
	PPA_NETIF *brif, *portif;
	struct avm_pa_session *session;

	BUG_ON(!ppa_buf);
	session = ppa_buf->pa_session;

	avm_hwinfo = avm_pa_pid_get_hwinfo(session->egress[0].pid_handle);
	if(!avm_hwinfo) return AVM_PA_TX_ERROR_SESSION;
	portif = avm_hwinfo->ppa.netdev;

	if(get_brif_bh(portif, &brif)) return AVM_PA_TX_ERROR_SESSION;

	if(ppa_hook_bridge_entry_delete_fn != NULL) {
		ppa_hook_bridge_entry_delete_fn(session->bsession->ethh.h_dest, brif,
		                                0);
		return AVM_PA_TX_OK;
	} else {
		return AVM_PA_TX_ERROR_SESSION;
	}
}

int tx_channel_session_assoc(struct avm_pa_session *session, uint32_t pae_entry)
{
	static bool first_run = 1;
	struct pae_hash_item *hashed_item, *entry;
	struct hlist_node *next;

	if(first_run) {
		hash_init(pae_hash_table);
		first_run = 0;
	}

	if(pae_entry >= (1 << pae_hash_width)) return -EINVAL;

	pr_debug("GSWIP hash for avm_pa session %u: %hx\n",
	         session->session_handle,
	         pae_entry);

	hash_for_each_possible_safe(pae_hash_table, entry, next, hlist, pae_entry) {
		if(entry->alive == false &&
		   jiffies_to_msecs(jiffies - entry->last_hit) > TX_CHAN_GRACE_PERIOD) {
			/* The session belonging to this entry has
			 * been removed more than a second ago. All
			 * matching packets should have left the TX
			 * channel by now. This delay is necessary to
			 * avoid mismatches among successive
			 * connections with identical tuple hashes.*/
			pr_debug("remove dead hash_item\n");
			hash_del(&entry->hlist);
			kfree(entry);
		}
	}

	hashed_item = kmalloc(sizeof(*hashed_item), GFP_ATOMIC);
	if(hashed_item) {
		hashed_item->session = session;
		kref_init(&hashed_item->kref);
		hashed_item->last_hit = jiffies;
		hashed_item->alive = true;
		hash_add(pae_hash_table, &hashed_item->hlist, pae_entry);
	} else {
		return -EAGAIN;
	}

	return 0;
}
EXPORT_SYMBOL(tx_channel_session_assoc);

int add_rt_session(PPA_BUF *ppa_buf)
{
	int32_t ppa_rv;
	struct avm_pa_session *session;

	BUG_ON(!ppa_buf);
	session = ppa_buf->pa_session;
	FENTRY_BUF(ppa_buf);

	pr_debug("add from PREROUTING (%d)\n", ppa_buf->pa_session->session_handle);
	ppa_buf->state = PPA_BUF_PREROUTING;
	ppa_rv =
	 ppa_hook_session_add_fn(ppa_buf, session, PPA_F_BEFORE_NAT_TRANSFORM);
	if(ppa_rv != PPA_SESSION_FILTED) {
		pr_err("[%s] session not filtered as expected (rv=%d)\n", __func__,
		       ppa_rv);
		avm_pa_set_hw_session( session, "session not filtered as expected" );
		return AVM_PA_TX_ERROR_SESSION;
	}

	pr_debug("add from POSTROUTING (%d)\n",
	         ppa_buf->pa_session->session_handle);
	ppa_buf->state = PPA_BUF_POSTROUTING;

	if (possible_lro_session(session))
		ppa_buf->post_routing_flags |= PPA_F_SESSION_LOCAL_IN;

	ppa_rv = ppa_hook_session_add_fn(ppa_buf, session, ppa_buf->post_routing_flags);
	if(ppa_rv != PPA_SESSION_ADDED) {
		pr_debug("[%s] session not added (rv=%d)\n", __func__, ppa_rv);
		avm_pa_set_hw_session( session, "failed" );
		return AVM_PA_TX_ERROR_SESSION;
	}

	hw_bytes_last[session->session_handle] = 0;

	pr_debug("success\n");
	return AVM_PA_TX_SESSION_ADDED;
}

struct pae_hash_item *avm_ppa_get_distinct_hash_item(uint16_t pae_hash)
{
	struct pae_hash_item *entry;
	
	hash_for_each_possible(pae_hash_table, entry, hlist, pae_hash)
	{
		/* check for other candidates */
		if(!entry->hlist.next) {
			pr_debug("found session %u\n", entry->session->session_handle);
			return entry->alive ? entry : NULL;
		} else {
			pr_warn_ratelimited("[%s] hash is not unique\n", __func__);
			break;
		}
	}
	return NULL;
}
EXPORT_SYMBOL(avm_ppa_get_distinct_hash_item);

void add_session_worker(struct work_struct *work)
{
	struct session_work_item *session_work;
	struct avm_pa_session *session;
	PPA_BUF ppa_buf;
	int rv;

	BUG_ON(!work);

	session_work = container_of(work, typeof(*session_work), work);
	session = session_work->session;
	memset(&ppa_buf, 0, sizeof(ppa_buf));
	ppa_buf.pa_session = session;


	if( build_netif_tree_hook &&
	    build_netif_tree_hook(&ppa_buf) != PPA_SUCCESS){
		avm_pa_set_hw_session( session, "vdevice not supported" );
		kfree(session_work);
		return;
	}

	if(session->bsession) {
		rv = add_br_session(&ppa_buf);
	} else {
		rv = add_rt_session(&ppa_buf);
	}

	if(rv == AVM_PA_TX_SESSION_ADDED) {
		avm_pa_set_hw_session( session, "yes" );
	}

	atomic_inc(&active_sessions);

	kfree(session_work);
}

static bool pid_is_local(uint32_t pid_handle)
{
	struct avm_pa_pid_hwinfo *hwinfo;

	hwinfo = avm_pa_pid_get_hwinfo(pid_handle);

	return (hwinfo && (hwinfo->ppa.local_stack));
}

static int session_proto_udp(const struct avm_pa_session *sess)
{
	const struct avm_pa_pkt_match *info = &sess->ingress;

	if(info->pkttype & AVM_PA_PKTTYPE_IP_MASK)
		return (info->pkttype & AVM_PA_PKTTYPE_PROTO_MASK) == IPPROTO_UDP;

	return false;
}

static int session_proto_tcp(const struct avm_pa_session *sess)
{
	const struct avm_pa_pkt_match *info = &sess->ingress;

	if(info->pkttype & AVM_PA_PKTTYPE_IP_MASK)
		return (info->pkttype & AVM_PA_PKTTYPE_PROTO_MASK) == IPPROTO_TCP;

	return false;
}


bool possible_lro_session(struct avm_pa_session *session){
	bool res = false;

	if(!avm_toe_lro_enabled()) {
		pr_debug("No TOE support for GRX350 600MHz\n");
		return false;
	}

	if (pid_is_local(session->egress[0].pid_handle)) {
		pr_debug("pid is local\n");
		if (session_proto_tcp(session)){
			pr_debug("session is tcp\n");
			res = true;
		}
	}
	pr_debug("do lro check: %s\n", res?"is_lro":"no_lro");
	return res;
}
EXPORT_SYMBOL(possible_lro_session);

static bool pid_is_valid(uint32_t pid_handle)
{
	struct avm_pa_pid_hwinfo *hwinfo;

	hwinfo = avm_pa_pid_get_hwinfo(pid_handle);

	if(hwinfo) {
		pr_debug("atmvcc=%p netdev=%p\n", hwinfo->atmvcc, hwinfo->ppa.netdev);
	}

	return (hwinfo && (!hwinfo->atmvcc != !hwinfo->ppa.netdev));
}

static void release_hash_item(struct kref *r)
{
	struct pae_hash_item *hash_item;
	hash_item = container_of(r, struct pae_hash_item, kref);
	hash_item->alive = false;
	smp_wmb();
	avm_pa_set_hw_session(hash_item->session, NULL);
}

int add_session(struct avm_pa_session *session)
{
	struct session_work_item *session_work;

	pr_debug("avm_pa sh=%d\n", session->session_handle);

	/* constraint checking */
	if(session->negress < 1) {
		pr_debug("constraint failure: no support for local sessions\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if(session->negress > 1) {
		pr_debug("constraint failure: multiple egress not supported\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if(!session->bsession && !session_proto_tcp(session) &&
	   !session_proto_udp(session)) {
		pr_debug("routing session is neither UDP nor TCP.\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if(!session->bsession &&
	   (((session->ingress.pkttype & AVM_PA_PKTTYPE_IPENCAP_MASK) ==
	     AVM_PA_PKTTYPE_IPV4ENCAP) ||
	    ((session->egress[0].match.pkttype & AVM_PA_PKTTYPE_IPENCAP_MASK) ==
	     AVM_PA_PKTTYPE_IPV4ENCAP))) {
		pr_debug("routing session uses IPv4 encapsulation.\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if(session->ingress.casttype == AVM_PA_IS_MULTICAST ||
	   session->egress[0].match.casttype == AVM_PA_IS_MULTICAST) {
		pr_debug("constraint failure: multicast not yet supported\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if(!pid_is_valid(session->ingress_pid_handle)) {
		pr_debug("constraint failure: no ingress device\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if (pid_is_local(session->egress[0].pid_handle)) {
		pr_debug("add_session: local session(maybe lro)\n");
	} else if(!pid_is_valid(session->egress[0].pid_handle)) {
		pr_debug("constraint failure: no egress device\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	session_work = kmalloc(sizeof(*session_work), GFP_ATOMIC);
	if(!session_work) {
		pr_err("[%s] oom\n", __func__);
		return AVM_PA_TX_ERROR_SESSION;
	}
	INIT_WORK(&session_work->work, add_session_worker);
	session_work->session = session;

	avm_pa_set_hw_session( session, "adding" );
	active_pa_sessions[session->session_handle] = session;
	queue_work(sessions_workqueue, &session_work->work);

	return AVM_PA_TX_SESSION_ADDED;
}

int tx_channel_session_disassoc(struct avm_pa_session *session, uint32_t pae_entry)
{
	struct pae_hash_item *hashed_item;

	if(pae_entry >= (1 << pae_hash_width)) return -EINVAL;

	hash_for_each_possible(pae_hash_table, hashed_item, hlist, pae_entry)
	{
		if(hashed_item->alive && hashed_item->session == session) {
			pr_debug("found and invalidate hashed session %u\n",
				 hashed_item->session->session_handle);
			/* Don't remove hashed_item just yet.
			 * Matching packets might still be pending. */
			kref_put(&hashed_item->kref, release_hash_item);
			return 0;
		}
	}

	return -ENOENT;
}
EXPORT_SYMBOL(tx_channel_session_disassoc);

int remove_rt_session(PPA_BUF *ppa_buf)
{
	struct avm_pa_session *session;

	BUG_ON(!ppa_buf);
	session = ppa_buf->pa_session;

	ppa_hook_session_del_fn(session, 0);

	/* TODO report remaining byte count */
	return AVM_PA_TX_OK;
}

void remove_session_worker(struct work_struct *work)
{
	struct session_work_item *session_work;
	struct avm_pa_session *session;
	PPA_BUF ppa_buf;
	int rv;

	BUG_ON(!work);

	session_work = container_of(work, typeof(*session_work), work);
	session = session_work->session;
	memset(&ppa_buf, 0, sizeof(ppa_buf));
	ppa_buf.pa_session = session;

	if(session->bsession) {
		rv = remove_br_session(&ppa_buf);
	} else {
		rv = remove_rt_session(&ppa_buf);
	}

	if(rv == AVM_PA_TX_OK) {
		avm_pa_set_hw_session( session, NULL );
	}

	if(reset_netif_tree_hook &&
	   atomic_dec_and_test(&active_sessions))
	   reset_netif_tree_hook();

	kfree(session_work);
}

int remove_session(struct avm_pa_session *session)
{
	struct session_work_item *session_work;

	pr_debug("avm_pa sh=%d\n", session->session_handle);
	avm_pa_set_hw_session( session, "removing" );
	active_pa_sessions[session->session_handle] = NULL;

	session_work = kmalloc(sizeof(*session_work), GFP_ATOMIC);
	if(!session_work) {
		pr_err("[%s] oom\n", __func__);
		return AVM_PA_TX_ERROR_SESSION;
	}
	INIT_WORK(&session_work->work, remove_session_worker);
	session_work->session = session;

	queue_work(sessions_workqueue, &session_work->work);

	return 0;
}

const char *session_state(struct avm_pa_session *session)
{
	BUG_ON(!session);

	return avm_pa_get_hw_session(session);
}

static int32_t tx_chan_receive_slow(struct sk_buff *skb)
{
	int parser_size_via_index(u8 index); /* TODO export from datapath_api.c */
	struct dma_rx_desc_1 *desc_1 = (struct dma_rx_desc_1 *)&skb->DW1;
	struct dma_rx_desc_0 *desc_0 = (struct dma_rx_desc_0 *)&skb->DW0;
	int parser_size;

	/* set skb->data to pmac or parser info. Note that the actual parser
	 * info might be garbage */
	u8 index = (desc_1->field.mpe2 << 1) + desc_1->field.mpe1;
	parser_size = parser_size_via_index(index);
	skb_push(skb, sizeof(struct pmac_rx_hdr) + parser_size);
	desc_1->field.ep = PMAC_CPU_ID;
	desc_0->field.dest_sub_if_id = 0;

	return dp_rx(skb, 0);
}

/* Called by the directpath driver, this will return the skb to AVM_PA after
 * looping it through PAE/MPE for manipulation (rxif set) OR forward packets
 * belonging to the opposite direction of an accelerated connection (txif
 * set). */
int32_t directpath_rx_fn(PPA_NETIF *rxif,
                         PPA_NETIF *txif,
                         PPA_BUF *ppabuf,
                         int32_t len __attribute__((unused)))
{
	struct sk_buff *skb = (struct sk_buff *)ppabuf;
	avm_pid_handle pid_handle;
	// pr_debug("called\n");
	if(unlikely(!rxif && !txif)) return PPA_FAILURE;
	if(unlikely(rxif)) {
		// pr_debug("rxif set\n");
		// print_hex_dump_bytes("", DUMP_PREFIX_OFFSET, skb->data, 60);
		pid_handle = rxif->avm_pa.devinfo.pid_handle;
		skb->dev = rxif;
		avm_pa_rx_channel_packet_not_accelerated(pid_handle, skb);
	} else {
		// pr_debug("txif set\n");
		// print_hex_dump_bytes("", DUMP_PREFIX_OFFSET, skb->data, 60);
		skb->dev = txif;
		pid_handle = txif->avm_pa.devinfo.pid_handle;

		/* Some PPA session addressed this port, so further
		 * modifications shouldn't be required */
		avm_pa_mark_routed(skb);
		avm_pa_do_not_accelerate(skb);

		/* TODO find a better way to identify TX channel netdevs,
		 * possibly by registering a dedicated function */
		if(unlikely(!strncmp(txif->name, "pid", 3))) {
			/* AVM|PHU:
			 * - Packets only end up here because of a
			 *   corresponding AVM PA HW session.
			 *
			 * - session_id is either the 5-tuple hash or some
			 *   other item out of the hash bucket. (collision)
			 *
			 * - XXX we can't reliably detect collisions, so at
			 *   this point we need to trust that such packets
			 *   never arrive here.
			 *
			 * - only session hits should arrive here
			 *
			 * - out of curiosity: how does "no hit after
			 *   collision" look like in terms of the session_id
			 *   field
			 */
			struct avm_pa_session *avm_ppa_get_session(uint16_t pae_hash);
			struct avm_pa_session *session;
			struct dma_rx_desc_1 *desc_1 = (struct dma_rx_desc_1 *)&skb->DW1;
			struct pae_hash_item *hash_item;

			hash_item =
			 avm_ppa_get_distinct_hash_item(desc_1->field.session_id);

			if(hash_item && kref_get_unless_zero(&hash_item->kref)) {
				session = hash_item->session;
			} else {
				session = NULL;
			}

			if(session) {
				uint32_t pid_handle = session->egress[0].pid_handle;
				BUG_ON(!pid_is_local(pid_handle));
				avm_pa_tx_channel_accelerated_packet(pid_handle,
					                             session
					                              ->session_handle,
					                             skb);
				hash_item->last_hit = jiffies;
				kref_put(&hash_item->kref, release_hash_item);
				return DP_SUCCESS;
			}

			/* TCP FIN arrives a little early if the
			 * congestion on the default ingress queue is
			 * low. This leads to situations where the AVM
			 * PA session is gone but we still need to
			 * process packets belonging to this
			 * connection here.
			 * We solve this by modifying the descriptors
			 * to look like a packet that belongs to the
			 * slow path. This way the datapath can
			 * process it as usual. Information on the
			 * original ingress device are retained in the
			 * PMAC header. */
			return tx_chan_receive_slow(skb);
		} else {
			struct pmac_rx_hdr *pmac =
			 (void *)skb->data - sizeof(struct pmac_rx_hdr);

			if(pmac->ip_offset) {
				skb_set_network_header(skb, pmac->ip_offset);
			} else {
				pr_debug_ratelimited("no pmac->ip_offset set\n");
				skb_reset_network_header(skb);
			}
			skb_reset_mac_header(skb);
			skb->protocol = eth_hdr(skb)->h_proto;
			//dev_queue_xmit(skb);
			txif->netdev_ops->ndo_start_xmit(skb, txif);
		}
	}
	return PPA_SUCCESS; /* TODO validate rv */
}
static int32_t directpath_stop_tx_fn(PPA_NETIF *dev)
{
	/* TODO this rather seems to discard the try_to_accelerate route
	 * instead of queuing the packets for deferred transmission. Not sure
	 * if that's the expected thing to do. -AVM:PHU */
	if(dev) avm_pa_rx_channel_suspend(dev->avm_pa.devinfo.pid_handle);
	return 0;
}

static int32_t directpath_start_tx_fn(PPA_NETIF *dev)
{
	if(dev) avm_pa_rx_channel_resume(dev->avm_pa.devinfo.pid_handle);
	return 0;
}

PPA_DIRECTPATH_CB dp_callbacks = {
	.stop_tx_fn = directpath_stop_tx_fn,
	.start_tx_fn = directpath_start_tx_fn,
	.rx_fn = directpath_rx_fn,
};
EXPORT_SYMBOL(dp_callbacks);

static int alloc_rx_channel(avm_pid_handle pid_handle)
{
	uint32_t subif_id;
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	PPA_NETIF *netif;

	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);
	pr_debug("[%s] setup PID %d \n", __func__, pid_handle);

	if( !pa_hwinfo ) {
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__, pid_handle);
		return -1; /* failure */
	}

	if( pa_hwinfo->ppa.local_stack ) {
		pr_debug("[%s] setup PID %d for local_stack\n", __func__, pid_handle);
		return 0;
	}

	if( !pa_hwinfo->ppa.netdev ) {
		/* hwinfo has been specifically set before, so full
		 * acceleration via PAE is expected */
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__, pid_handle);
		return -2; /* failure */
	}

	if(!pa_hwinfo->ppa.dp_enabled) {
		pr_debug("directpath not enabled for PID %d\n", pid_handle);
		return -2;
	}

	netif = pa_hwinfo->ppa.netdev;

	/* TODO wann wird PPA_F_DIRECTPATH_WAN gebraucht? unnötig?! */
	if(ppa_hook_directpath_register_dev_fn(
	    &subif_id, netif, &dp_callbacks,
	    PPA_F_DIRECTPATH_REGISTER | PPA_F_DIRECTPATH_ETH_IF) == PPA_SUCCESS) {
		/* TODO */
		pa_hwinfo->ppa.dp_subif_id = subif_id;
	}

	return 0; /* success */
}

static int free_rx_channel(avm_pid_handle pid_handle)
{
	uint32_t subif_id;
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	PPA_NETIF *netif;

	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);
	if(!pa_hwinfo || !pa_hwinfo->ppa.netdev) {
		/* hwinfo has been specifically set before, so full
		 * acceleration via PAE is expected */
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__,
		        pid_handle);
		return -1; /* failure */
	}

	if(!pa_hwinfo->ppa.dp_enabled) {
		pr_debug("directpath not enabled for PID %d\n", pid_handle);
		return -2;
	}

	netif = pa_hwinfo->ppa.netdev;
	subif_id = pa_hwinfo->ppa.dp_subif_id;

	/* unregister */
	if(ppa_hook_directpath_register_dev_fn(
	    &subif_id,
	    netif,
	    &dp_callbacks,
	    /* PPA_F_DIRECTPATH_REGISTER |*/ PPA_F_DIRECTPATH_ETH_IF) ==
	   PPA_SUCCESS) {
		pa_hwinfo->ppa.dp_subif_id = subif_id;
		pr_debug("success\n");
	}

	return 0; /* success */
}

static int try_to_accelerate(avm_pid_handle pid_handle, struct sk_buff *skb)
{
	uint32_t subif_id;
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	uint32_t rv;

	pr_debug("called\n");
	avm_simple_profiling_skb(0, skb);
	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);
	if(!pa_hwinfo || !pa_hwinfo->ppa.netdev) {
		/* hwinfo has been specifically set before, so full
		 * acceleration via PAE is expected */
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__, pid_handle);
		return AVM_PA_RX_BYPASS; /* failure */
	}

	if(!pa_hwinfo->ppa.dp_enabled) {
		pr_debug("directpath not enabled for PID %d\n", pid_handle);
		return AVM_PA_RX_BYPASS;
	}

	subif_id = pa_hwinfo->ppa.dp_subif_id;

	pr_debug("calling ppa_hook_directpath_send_fn with ifid=%d.\n", subif_id);
	rv = ppa_hook_directpath_send_fn(subif_id, (PPA_BUF *)skb, skb->len, 0);

	pr_debug("-> %d\n", rv);

	/* be clear about consumption */
	return AVM_PA_RX_STOLEN;
}

int  alloc_tx_channel(avm_pid_handle pid_handle){
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);
	pr_debug("[%s] setup PID %d \n", __func__, pid_handle);

	if( !pa_hwinfo ) {
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__, pid_handle);
		return -1; /* failure */
	}

	if( pa_hwinfo->ppa.local_stack ) {
		PPA_SUBIF *new_subif;
		const PPA_SUBIF  *subif;
		struct net_device *localif;
		int32_t rv;

		localif = pa_hwinfo->ppa.netdev = alloc_etherdev(sizeof(PPA_SUBIF));
		sprintf(localif->name, "pid%d", pid_handle);
		localif->netdev_ops = avm_ppa_localif->netdev_ops;
		new_subif = (PPA_SUBIF *)netdev_priv(localif);

		memset(new_subif, 0, sizeof(*new_subif));
		subif = (PPA_SUBIF *)netdev_priv(avm_ppa_localif);
		new_subif->port_id = subif->port_id;
		new_subif->subif = -1;
		ppa_add_avm_dpipe_vitf(localif);
		rv = ppa_hook_directpath_ex_register_dev_fn(new_subif,
		                                            localif,
		                                            &dp_callbacks,
		                                            PPA_F_DIRECTPATH_REGISTER |
		                                             PPA_F_DIRECTPATH_ETH_IF);
		pr_debug("%s: rv=%d subif=%d:%d\n", localif->name, rv, new_subif->port_id, new_subif->subif);
		pr_debug("[%s] setup PID %d for local_stack\n", __func__, pid_handle);
		return 0;
	}
	return -2; /* failure */
}

int free_tx_channel(avm_pid_handle pid_handle)
{
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	struct net_device *netif;
	PPA_SUBIF *subif;

	pr_debug("[%s] free PID %d \n", __func__, pid_handle);

	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);

	if(!pa_hwinfo) {
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__,
		        pid_handle);
		return -1; /* failure */
	}

	if(pa_hwinfo->ppa.local_stack && pa_hwinfo->ppa.netdev) {
		netif = pa_hwinfo->ppa.netdev;
		subif = (PPA_SUBIF *)netdev_priv(netif);
		if(ppa_hook_directpath_ex_register_dev_fn(subif,
		                                          netif,
		                                          NULL,
		                                          PPA_F_DIRECTPATH_ETH_IF) !=
		   PPA_SUCCESS) {
			pr_err("Could not remove DirectPath interface for %s\n",
			       netif->name);
		}
		ppa_del_avm_dpipe_vitf(pa_hwinfo->ppa.netdev);
	}
	return 0;
}

void telephony_state(int active)
{
	pr_debug("telephony is %sactive\n", &(active<<1)["in"]);
	low_cpu_latency_required = !!active;
}

static int hw_pa_cpufreq_notifier(struct notifier_block *nb,
				   unsigned long val,  void *data)
{
	struct cpufreq_freqs *freq = data;
	enum ltq_cpufreq_state new_state, old_state;

	new_state = ltq_cpufreq_get_ps_from_khz(freq->new);
	old_state = ltq_cpufreq_get_ps_from_khz(freq->old);

	if(val == CPUFREQ_PRECHANGE) {
		pr_debug("prechange from %d to %d: ", old_state, new_state);

		if(low_cpu_latency_required && new_state > old_state) {
			pr_debug("rejected\n");
			return NOTIFY_STOP_MASK | (LTQ_CPUFREQ_MODULE_DP << 4);
		}
	}
	pr_debug("accepted\n");
	return NOTIFY_OK | (LTQ_CPUFREQ_MODULE_DP << 4);
}

static struct notifier_block hw_pa_cpufreq_notifier_block = {
	.notifier_call = hw_pa_cpufreq_notifier
};

int session_stats(struct avm_pa_session *avm_session,
                  struct avm_pa_session_stats *ingress)
{
	if(avm_session->bsession) {
		return collect_bridging_count(avm_session, ingress);
	} else {
		return collect_routing_count(avm_session, ingress);
	}
}

/*
    int  (*alloc_rx_channel)(avm_pid_handle pid_handle);
    int  (*alloc_tx_channel)(avm_pid_handle pid_handle);
    int  (*free_rx_channel)(avm_pid_handle pid_handle);
    int  (*free_tx_channel)(avm_pid_handle pid_handle);
    int  (*try_to_accelerate)(avm_pid_handle pid_handle, struct sk_buff *skb);
*/

static struct avm_hardware_pa hw_pa = {
	.add_session = add_session,
	.remove_session = remove_session,
	.session_state = session_state,
	.alloc_rx_channel = alloc_rx_channel,
	.alloc_tx_channel = alloc_tx_channel,
	.free_rx_channel = free_rx_channel,
	.free_tx_channel = free_tx_channel,
	.try_to_accelerate = try_to_accelerate,
	.telephony_state = telephony_state,
	.session_stats = session_stats,
};

void avm_ppa_register_hardware_pa(void)
{
	FENTRY_NO_BUF
	/* new ordered workqueue */
	sessions_workqueue = alloc_ordered_workqueue("hwpa_sessions", 0);
	avm_pa_register_hardware_pa(&hw_pa);

	if (cpufreq_register_notifier(&hw_pa_cpufreq_notifier_block,
				      CPUFREQ_TRANSITION_NOTIFIER)) {
		pr_err("hw_pa_cpufreq_init failed:cpufreq_register_notifier failed ?\n");
	}
	
}
EXPORT_SYMBOL(avm_ppa_register_hardware_pa);

static DEFINE_SPINLOCK(avm_dpipe_lock);
void ppa_init_avm_dpipe_vitf_list(void)
{
	pr_debug("[%s] \n", __func__);
	INIT_LIST_HEAD(&virt_avm_dpipe_netdevs);
}

void ppa_add_avm_dpipe_vitf(PPA_NETIF *dev)
{
	pr_debug("[%s] dev=%p/%s\n", __func__, dev, dev->name);

	spin_lock_bh(&avm_dpipe_lock);
	dev_hold(dev);
	list_add_tail(&dev->dev_list, &virt_avm_dpipe_netdevs);
	spin_unlock_bh(&avm_dpipe_lock);
}

void ppa_del_avm_dpipe_vitf(PPA_NETIF *dev)
{
	spin_lock_bh(&avm_dpipe_lock);
	list_del_init(&dev->dev_list);
	dev_put(dev);
	spin_unlock_bh(&avm_dpipe_lock);
}

void ppa_free_avm_dpipe_vitf(PPA_NETIF *dev)
{
	int refcnt;
	spin_lock_bh(&avm_dpipe_lock);
	dev_put(dev);
	refcnt = netdev_refcnt_read(dev);
	if(refcnt == 0) {
		free_netdev(dev);
	} else {
		BUG_ON(refcnt < 0);
	}

	spin_unlock_bh(&avm_dpipe_lock);
}

PPA_NETIF *ppa_get_avm_dpipe_vitf(PPA_IFNAME *ifname)
{
	struct net_device *dev;

	if(!ifname) return 0;

	spin_lock_bh(&avm_dpipe_lock);
	list_for_each_entry(dev, &virt_avm_dpipe_netdevs, dev_list)
	{
		if(strncmp(dev->name, ifname, PPA_IF_NAME_SIZE) == 0) {
			dev_hold(dev);
			spin_unlock_bh(&avm_dpipe_lock);
			return dev;
		}
	}
	pr_debug("[%s] no vdev found: %s\n", __func__, ifname);
	spin_unlock_bh(&avm_dpipe_lock);
	return 0;
}

EXPORT_SYMBOL(ppa_get_avm_dpipe_vitf);
EXPORT_SYMBOL(ppa_init_avm_dpipe_vitf_list);
EXPORT_SYMBOL(ppa_add_avm_dpipe_vitf);
EXPORT_SYMBOL(ppa_del_avm_dpipe_vitf);
EXPORT_SYMBOL(ppa_free_avm_dpipe_vitf);

/*
 * PPA <> PA Adaption Layer
 *
 *
 * [XXX] für ipv6 wird nicht zwischen PRE- und POSTROUTING unterschieden
 */

#define HDRCOPY(info) ((info)->hdrcopy + (info)->hdroff)
static void *get_hdr_from_pa_match(const struct avm_pa_pkt_match *match,
                                   unsigned char type)
{
	unsigned int i;
	FENTRY_NO_BUF
	if(!match) return NULL;

	for(i = 0; i < match->nmatch; i++) {
		const struct avm_pa_match_info *info = &match->match[i];
		if(info && info->type == type) {
			return (void *)(HDRCOPY(match) + info->offset);
		}
	}

	return NULL;
}

static void *get_hdr_from_ppa_buf(const PPA_BUF *ppa_buf,
                                  unsigned char type,
                                  HDR_DIRECTION hdr_dir)
{
	unsigned int i;
	void *hdr = NULL;
	const struct avm_pa_pkt_match *match;
	FENTRY_BUF(ppa_buf);

	if(!ppa_buf) return NULL;

	if(hdr_dir == HDR_EGRESS) {
		for(i = 0; i < ppa_buf->pa_session->negress; i++) {
			match = &ppa_buf->pa_session->egress[i].match;
			hdr = get_hdr_from_pa_match(match, type);
			pr_debug("found header in egress match #%d\n", i);
			if(hdr) return hdr;
		}
	}

	if(hdr_dir == HDR_INGRESS) {
		hdr = get_hdr_from_pa_match(&ppa_buf->pa_session->ingress, type);
		if(hdr) pr_debug("found header in ingress\n");
	}

	return hdr;
}

static struct ipv6hdr *ipv6_hdr_from_ppa_buf(const PPA_BUF *ppa_buf)
{
	HDR_DIRECTION dir;
	FENTRY_BUF(ppa_buf);

	dir = (ppa_buf->state == PPA_BUF_PREROUTING) ? HDR_INGRESS : HDR_EGRESS;
	return get_hdr_from_ppa_buf(ppa_buf, AVM_PA_IPV6, dir);
}

static struct iphdr *ip_hdr_from_ppa_buf(const PPA_BUF *ppa_buf)
{
	HDR_DIRECTION dir;
	FENTRY_BUF(ppa_buf);

	dir = (ppa_buf->state == PPA_BUF_PREROUTING) ? HDR_INGRESS : HDR_EGRESS;
	return get_hdr_from_ppa_buf(ppa_buf, AVM_PA_IPV4, dir);
}

PPA_SESSION *ppa_get_session(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	return ppa_buf->pa_session;
}

uint8_t ppa_is_pkt_ipv6(const PPA_BUF *ppa_buf)
{
	uint16_t pkttype = ppa_buf->pa_session->ingress.pkttype;
	FENTRY_BUF(ppa_buf);
	return ((pkttype & AVM_PA_PKTTYPE_IP_MASK) == AVM_PA_PKTTYPE_IPV6);
}

uint8_t ppa_get_ip_tos(PPA_BUF *ppa_buf)
{
	struct iphdr *hdr;
	uint8_t tos;
	struct avm_pa_v4_mod_rec *mod;

	FENTRY_BUF(ppa_buf);
	mod = &ppa_buf->pa_session->mod.v4_mod;
	if(ppa_buf->state == PPA_BUF_POSTROUTING &&
	   mod->flags & AVM_PA_V4_MOD_TOS) {
		tos = mod->tos;
	} else {
		hdr = ip_hdr_from_ppa_buf(ppa_buf);
		tos = hdr ? hdr->tos : 0;
	}

	return tos;
}

uint8_t ppa_get_ipv6_tos(PPA_BUF *ppa_buf)
{
	struct ipv6hdr *hdr;
	uint8_t tc;
	FENTRY_BUF(ppa_buf);

	hdr = ipv6_hdr_from_ppa_buf(ppa_buf);
	if(!hdr) return 0;
	tc = hdr->priority << 4;
	tc |= hdr->flow_lbl[0] >> 4;

	return tc;
}

uint8_t ppa_get_pkt_ip_tos(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	if(ppa_is_pkt_ipv6(ppa_buf)) {
		return ppa_get_ipv6_tos(ppa_buf);
	}

	return ppa_get_ip_tos(ppa_buf);
}

uint32_t ppa_get_pkt_ip_len(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	if(ppa_is_pkt_ipv6(ppa_buf)) {
		return sizeof(struct in6_addr);
	}
	return sizeof(uint32_t);
}

/* [TODO] get_ip_{s,d}addr zusammenlegen */
PPA_IPADDR ppa_get_ip_saddr(PPA_BUF *ppa_buf)
{
	struct iphdr *hdr;
	PPA_IPADDR saddr;
	struct avm_pa_v4_mod_rec *mod;

	FENTRY_BUF(ppa_buf);
	pr_debug("(%s: %d)\n",
	         (ppa_buf->state == PPA_BUF_POSTROUTING) ? "post" : "pre",
	         ppa_buf->pa_session->session_handle);

	mod = &ppa_buf->pa_session->mod.v4_mod;
	if((ppa_buf->state == PPA_BUF_POSTROUTING) &&
	   (mod->flags & AVM_PA_V4_MOD_SADDR)) {
		pr_debug("return snat ip addr\n");
		saddr.ip = mod->saddr;
	} else {
		pr_debug("return ip-header saddr\n");
		hdr = ip_hdr_from_ppa_buf(ppa_buf);
		saddr.ip = hdr ? hdr->saddr : 0;
	}

	return saddr;
}

PPA_IPADDR ppa_get_ipv6_saddr(PPA_BUF *ppa_buf)
{
	struct ipv6hdr *hdr;
	PPA_IPADDR addr;

	FENTRY_BUF(ppa_buf);
	hdr = ipv6_hdr_from_ppa_buf(ppa_buf);
	memcpy(&addr.ip6, &hdr->saddr, sizeof(addr.ip6));

	return addr;
}

PPA_IPADDR ppa_get_pkt_src_ip(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	if(ppa_is_pkt_ipv6(ppa_buf)) {
		return ppa_get_ipv6_saddr(ppa_buf);
	}

	return ppa_get_ip_saddr(ppa_buf);
}


PPA_IPADDR ppa_get_ip_daddr(PPA_BUF *ppa_buf)
{
	struct iphdr *hdr;
	PPA_IPADDR daddr;
	struct avm_pa_v4_mod_rec *mod;
	FENTRY_BUF(ppa_buf);
	pr_debug("(%s: %d)\n",
	         (ppa_buf->state == PPA_BUF_POSTROUTING) ? "post" : "pre",
	         ppa_buf->pa_session->session_handle);

	mod = &ppa_buf->pa_session->mod.v4_mod;
	if((ppa_buf->state == PPA_BUF_POSTROUTING) &&
	   (mod->flags & AVM_PA_V4_MOD_DADDR)) {
		daddr.ip = mod->daddr;
		pr_debug("return dnat ip addr\n");
	} else {
		hdr = ip_hdr_from_ppa_buf(ppa_buf);
		daddr.ip = hdr ? hdr->daddr : 0;
		pr_debug("return ip-header\n");
	}

	return daddr;
}

PPA_IPADDR ppa_get_ipv6_daddr(PPA_BUF *ppa_buf)
{
	struct ipv6hdr *hdr;
	PPA_IPADDR addr;
	FENTRY_BUF(ppa_buf);

	hdr = ipv6_hdr_from_ppa_buf(ppa_buf);
	memcpy(&addr.ip6, &hdr->daddr, sizeof(addr.ip6));

	return addr;
}

PPA_IPADDR ppa_get_pkt_dst_ip(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	if(ppa_is_pkt_ipv6(ppa_buf)) {
		return ppa_get_ipv6_daddr(ppa_buf);
	}

	return ppa_get_ip_daddr(ppa_buf);
}


static uint16_t get_port(PPA_BUF *ppa_buf, bool is_src)
{
	uint16_t *ports, modflag;
	struct avm_pa_v4_mod_rec *mod;
	FENTRY_BUF(ppa_buf);
	mod = &ppa_buf->pa_session->mod.v4_mod;

	modflag = is_src ? AVM_PA_V4_MOD_SPORT : AVM_PA_V4_MOD_DPORT;
	if(ppa_buf->state == PPA_BUF_POSTROUTING && mod->flags & modflag) {
		return is_src ? mod->sport : mod->dport;
	} else {
		ports =
		 get_hdr_from_pa_match(&ppa_buf->pa_session->ingress, AVM_PA_PORTS);
		return ports ? ports[is_src ? 0 : 1] : 0;
	}
}

uint16_t ppa_get_pkt_src_port(PPA_BUF *ppa_buf)
{
	const bool is_src = 1;
	FENTRY_BUF(ppa_buf);
	return get_port(ppa_buf, is_src);
}

uint16_t ppa_get_pkt_dst_port(PPA_BUF *ppa_buf)
{
	const bool is_src = 0;
	FENTRY_BUF(ppa_buf);
	return get_port(ppa_buf, is_src);
}

uint32_t ppa_is_ip_fragment(PPA_BUF *ppa_buf)
{
	struct iphdr *hdr = ip_hdr_from_ppa_buf(ppa_buf);
	FENTRY_BUF(ppa_buf);
	return (hdr && hdr->frag_off & htons(IP_MF | IP_OFFSET)) == 0 ? 0 : 1;
}

uint8_t ppa_get_pkt_ip_proto(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	return (uint8_t)(ppa_buf->pa_session->ingress.pkttype &
	                 AVM_PA_PKTTYPE_PROTO_MASK);
}

static void get_tuple_from_packet(PPA_BUF *ppa_buf, PPA_TUPLE *tuple)
{
	PPA_IPADDR src_ip, dst_ip;
	FENTRY_BUF(ppa_buf);
	src_ip = ppa_get_pkt_src_ip(ppa_buf);
	dst_ip = ppa_get_pkt_dst_ip(ppa_buf);

	/* have to keep this in sync with ppa_compare_with_tuple */
	tuple->src.l3num = ppa_is_pkt_ipv6(ppa_buf) ? AF_INET6 : AF_INET;
	tuple->dst.protonum = ppa_get_pkt_ip_proto(ppa_buf);
	tuple->src.u.all = ppa_get_pkt_src_port(ppa_buf);
	tuple->src.u3.all[0] = src_ip.ip6[0];
	tuple->src.u3.all[1] = src_ip.ip6[1];
	tuple->src.u3.all[2] = src_ip.ip6[2];
	tuple->src.u3.all[3] = src_ip.ip6[3];
	tuple->dst.u.all = ppa_get_pkt_dst_port(ppa_buf);
	tuple->dst.u3.all[0] = dst_ip.ip6[0];
	tuple->dst.u3.all[1] = dst_ip.ip6[1];
	tuple->dst.u3.all[2] = dst_ip.ip6[2];
	tuple->dst.u3.all[3] = dst_ip.ip6[3];
}

int ppa_get_hash_from_packet(PPA_BUF *ppa_buf,
                             unsigned char pf __attribute__((unused)),
                             uint32_t *u32_hash,
                             PPA_TUPLE *tuple)
{
	FENTRY_BUF(ppa_buf);
	*u32_hash = (uint32_t)ppa_buf->pa_session->session_handle;
	get_tuple_from_packet(ppa_buf, tuple);

	return 0;
}
EXPORT_SYMBOL(ppa_get_hash_from_packet);

uint32_t ppa_get_hash_from_ct(const PPA_SESSION *pa_session,
                              uint8_t dir __attribute__((unused)),
                              PPA_TUPLE *tuple)
{
	PPA_BUF ppa_buf;
	FENTRY_NO_BUF

	ppa_buf.pa_session = (PPA_SESSION *)pa_session;
	ppa_buf.state = PPA_BUF_PREROUTING; /* [TODO] richtig? */
	get_tuple_from_packet(&ppa_buf, tuple);

	return pa_session->session_handle;
}
EXPORT_SYMBOL(ppa_get_hash_from_ct);

uint32_t ppa_is_pkt_fragment(PPA_BUF *ppa_buf __attribute__((unused)))
{
	/* AVM PA would only request a session if it were accelerable */
	return false;
}
int32_t ppa_is_pkt_host_output(PPA_BUF *ppa_buf __attribute__((unused)))
{
	/* AVM PA would only request a session if it were accelerable */
	return false;
}
int32_t ppa_is_pkt_broadcast(PPA_BUF *ppa_buf __attribute__((unused)))
{
	/* AVM PA would only request a session if it were accelerable */
	return false;
}
int32_t ppa_is_pkt_loopback(PPA_BUF *ppa_buf __attribute__((unused)))
{
	/* AVM PA would only request a session if it were accelerable */
	return false;
}

PPA_NETIF *ppa_get_pkt_src_if(PPA_BUF *ppa_buf)
{
	struct avm_pa_pid_hwinfo *hwinfo;
	FENTRY_BUF(ppa_buf);

	if(ppa_buf->ingress_vitf) {
		return ppa_buf->ingress_vitf->netif;
	}

	hwinfo = avm_pa_pid_get_hwinfo(ppa_buf->pa_session->ingress_pid_handle);
	BUG_ON(!hwinfo);
	return hwinfo->ppa.netdev;
}

PPA_NETIF *ppa_get_pkt_dst_if(PPA_BUF *ppa_buf)
{
	struct avm_pa_pid_hwinfo *hwinfo;
	FENTRY_BUF(ppa_buf);
	/* [TODO] consider multiple egress pids */

	if(ppa_buf->egress_vitf) {
		return ppa_buf->egress_vitf->netif;
	}

	hwinfo = avm_pa_pid_get_hwinfo(ppa_buf->pa_session->egress[0].pid_handle);
	BUG_ON(!hwinfo);
	return hwinfo->ppa.netdev;
}

uint32_t ppa_check_is_special_session(PPA_BUF *ppa_buf, PPA_SESSION *p_session)
{
	FENTRY_BUF(ppa_buf);
	/* AVM PA would only request a session if it were accelerable */
	return false;
}

int32_t ppa_is_pkt_local(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	BUG(); /* not implemented */
	return 0;
}

int32_t ppa_is_tcp_established(PPA_SESSION *ppa_session __attribute__((unused)))
{
	FENTRY_NO_BUF
	/* AVM PA handles this */
	return true;
}

int32_t ppa_is_tcp_open(PPA_SESSION *ppa_session)
{
	FENTRY_NO_BUF
	BUG(); /* unused */
}

int32_t ppa_is_pkt_routing(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	return ppa_buf->pa_session->routed;
}

int32_t ppa_is_pkt_mc_routing(PPA_BUF *ppa_buf __attribute__((unused)))
{
	FENTRY_BUF(ppa_buf);
	BUG(); /* unused */
}

void ppa_get_pkt_rx_src_mac_addr(PPA_BUF *ppa_buf, uint8_t mac[PPA_ETH_ALEN])
{
	int8_t strbuf[64];
	struct ethhdr *ethh;
	HDR_DIRECTION dir;

	FENTRY_BUF(ppa_buf);

	dir = (ppa_buf->state == PPA_BUF_PREROUTING) ? HDR_INGRESS : HDR_EGRESS;
	ethh = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_ETH, dir);

	memcpy(mac, ethh->h_source, PPA_ETH_ALEN);
	pr_debug("src_mac: %s\n", ppa_get_pkt_mac_string(mac, strbuf));
}

void ppa_get_pkt_rx_dst_mac_addr(PPA_BUF *ppa_buf, uint8_t mac[PPA_ETH_ALEN])
{
	struct ethhdr *ethh;
	HDR_DIRECTION dir;
	FENTRY_BUF(ppa_buf);

	dir = (ppa_buf->state == PPA_BUF_PREROUTING) ? HDR_INGRESS : HDR_EGRESS;
	ethh = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_ETH, dir);
	memcpy(mac, ethh->h_dest, PPA_ETH_ALEN);
}

int ppa_get_multicast_pkt_ip(PPA_BUF *ppa_buf __attribute__((unused)),
                             void *dst_ip __attribute__((unused)),
                             void *src_ip __attribute__((unused)))
{
	FENTRY_BUF(ppa_buf);
	/* [TODO] we don't get those anyway. or do we?! */
	return PPA_FAILURE;
}

uint32_t ppa_get_pkt_priority(PPA_BUF *ppa_buf)
{
	uint32_t prio;
	struct avm_pa_session *session;
	FENTRY_BUF(ppa_buf);

	session = ppa_buf->pa_session;
	BUG_ON(!session);

	switch(session->egress[0].type) {
	case avm_pa_egresstype_local:
	case avm_pa_egresstype_rtp:
		/* map rt ingress to a dedicated high prio queue */
		prio = session->realtime ? PPA_REALTIME_CLASSID : 0;
		break;
	case avm_pa_egresstype_output:
	default:
		prio = session->egress[0].output.priority;
		break;
	}

	if(prio) {
		prio = TC_H_MIN(prio) - 1; /* TC starts at 1, skb->priority at 0 */
	}

	pr_debug("TC: %u -> %u (%u)\n",
	         TC_H_MIN(session->egress[0].output.priority),
	         prio,
	         session->egress[0].output.priority);
	return prio;
}

uint32_t ppa_get_low_prio_thresh(uint32_t flags __attribute__((unused)))
{
	FENTRY_NO_BUF
	return 0; /* [TODO] lowest? */
}

uint32_t ppa_get_skb_mark(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	/* haben wir nicht */
	return 0;
}

uint32_t ppa_get_skb_extmark(PPA_BUF *ppa_buf)
{
	uint32_t prio = ppa_buf->pa_session->egress[0].output.priority;
	FENTRY_BUF(ppa_buf);

	/* 
	 * only apply classes derived from TC priorities or real-time sessions
	 */
	if(TC_H_MAJ(prio) || ppa_buf->pa_session->realtime) {
		return SESSION_FLAG_TC_REMARK;
	} else {
		return 0;
	}
}

PPA_BUF *ppa_buf_clone(PPA_BUF *ppa_buf, uint32_t flags)
{
	FENTRY_BUF(ppa_buf);
	BUG();
	return NULL;
}

int32_t ppa_buf_cloned(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	BUG();
	return false;
}

PPA_BUF *ppa_buf_get_prev(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	pr_warn("%s not implemented\n", __func__);
	return NULL;
}

PPA_BUF *ppa_buf_get_next(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	pr_warn("%s not implemented\n", __func__);
	return NULL;
}

void ppa_buf_free(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	pr_warn("%s not implemented\n", __func__);
}

uint16_t ppa_vlan_dev_get_egress_qos_mask(PPA_NETIF *dev, PPA_BUF *ppa_buf)
{
	struct vlanhdr *vlanhdr;
	uint16_t vlan_qos = 0;
	FENTRY_BUF(ppa_buf);

	vlanhdr = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_VLAN, HDR_EGRESS);
	if(vlanhdr) vlan_qos = vlanhdr->vlan_tci & ((8 - 1) << 13);

	pr_debug("vlan_qos: 0x%04hx (hdr: %p)\n", vlan_qos, vlanhdr);
	return vlan_qos;
}

int32_t ppa_is_pkt_multicast(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	return ppa_buf->pa_session->ingress.casttype == AVM_PA_IS_MULTICAST;
}

int32_t ppa_get_dst_mac(PPA_BUF *ppa_buf,
                        PPA_SESSION *p_session,
                        uint8_t mac[PPA_ETH_ALEN],
                        uint32_t daddr)
{
	struct ethhdr *ethh = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_ETH, HDR_EGRESS);
	int8_t macstr[18];
	FENTRY_BUF(ppa_buf);
	if(!ethh) {
		if(pid_is_local(ppa_buf->pa_session->egress[0].pid_handle)) {
			ethh = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_ETH, HDR_INGRESS);
		} else {
			return PPA_FAILURE;
		}
	}
	memcpy(mac, ethh->h_dest, PPA_ETH_ALEN);
	pr_debug("found mac %s\n", ppa_get_pkt_mac_string(ethh->h_dest, macstr));
	pr_debug("found mac %s\n", ppa_get_pkt_mac_string(mac, macstr));
	if(mac[0] | mac[1] | mac[2] | mac[3] | mac[4] | mac[5]) {
		return PPA_SUCCESS;
	} else {
		return PPA_FAILURE;
	}
}

uint32_t ppa_set_pkt_priority(PPA_BUF *ppa_buf, uint32_t new_pri)
{
	FENTRY_BUF(ppa_buf);
	ppa_buf->pa_session->egress[0].output.priority = new_pri;
	return new_pri;
}

uint32_t ppa_get_session_helper(PPA_SESSION *p_session)
{
	FENTRY_NO_BUF
	pr_warn("%s not implemented\n", __func__);
	return 0;
}

int32_t ppa_register_chrdev(int32_t major,
                            const uint8_t *name,
                            PPA_FILE_OPERATIONS *fops)
{
	ppadev_fops = fops;
	return register_chrdev(major, (char *)name, fops);
}

void ppa_unregister_chrdev(int32_t major, const uint8_t *name)
{
	ppadev_fops = NULL;
	unregister_chrdev(major, (char *)name);
	return;
}

int32_t ppa_pppoe_get_pppoe_addr(PPA_NETIF *netif, struct pppoe_addr *pa)
{
	BUG();
	return PPA_EPERM;
}

__u16 ppa_pppoe_get_pppoe_session_id(PPA_NETIF *netif)
{
	struct avm_dpipe_vitf_pppoe *priv;

	if(!(netif->priv_flags & IFF_PPA_DUMMY) ||
	   !(netif->flags & IFF_POINTOPOINT)) {
		pr_err("[%s] no ppoedev\n", __func__);
		return 0;
	}

	priv = netdev_priv(netif);
	return priv->pppoe_session_id;
}

__u16 ppa_get_pkt_pppoe_session_id(PPA_BUF *ppa_buf)
{
	BUG();
	return 0;
}

int32_t ppa_pppoe_get_eth_netif(PPA_NETIF *netif,
                                PPA_IFNAME pppoe_eth_ifname[PPA_IF_NAME_SIZE])
{
	struct avm_dpipe_vitf_pppoe *priv;

	if(!(netif->priv_flags & IFF_PPA_DUMMY) ||
	   !(netif->flags & IFF_POINTOPOINT)) {
		pr_err("[%s] no ppoedev\n", __func__);
		return PPA_FAILURE;
	}

	priv = netdev_priv(netif);
	strlcpy(pppoe_eth_ifname, priv->lower_dev->name, PPA_IF_NAME_SIZE);
	return PPA_SUCCESS;
}


uint32_t ppa_check_is_pppoe_netif(PPA_NETIF *netif)
{
	return (netif->type == ARPHRD_PPP && (netif->flags & IFF_POINTOPOINT));
}

int32_t ppa_pppoe_get_dst_mac(PPA_NETIF *netif, uint8_t mac[PPA_ETH_ALEN])
{
	struct avm_dpipe_vitf_pppoe *priv;

	if(!(netif->priv_flags & IFF_PPA_DUMMY) ||
	   !(netif->flags & IFF_POINTOPOINT)) {
		pr_err("[%s] no ppoedev\n", __func__);
		return PPA_FAILURE;
	}

	priv = netdev_priv(netif);
	memcpy(mac, priv->remote_mac, PPA_ETH_ALEN);
	return PPA_SUCCESS;
}

int32_t ppa_pppoe_get_physical_if(PPA_NETIF *netif,
                                  PPA_IFNAME *ifname,
                                  PPA_IFNAME phy_ifname[PPA_IF_NAME_SIZE])
{
	if(!netif) netif = ppa_get_netif(ifname);

	if(!netif) return PPA_EINVAL;

	if(!ppa_check_is_pppoe_netif(netif)) return PPA_EINVAL;

	return ppa_pppoe_get_eth_netif(netif, phy_ifname);
}

uint32_t ppa_check_is_ppp_netif(PPA_NETIF *netif)
{
	return (netif->type == ARPHRD_PPP && (netif->flags & IFF_POINTOPOINT));
}

extern int vlan_dev_get_vid(const char *dev_name, unsigned short *result);
uint32_t ppa_get_vlan_id(PPA_NETIF *netif)
{
	unsigned short vid = ~0;

	if(!netif) return ~0;
	if((netif->priv_flags & IFF_802_1Q_VLAN) &&
	   ((netif->priv_flags & IFF_PPA_DUMMY))) {
		struct avm_dpipe_vitf_vlan *vlan_priv = netdev_priv(netif);
		return vlan_priv->vid;
	}
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	if(vlan_dev_get_vid(netif->name, &vid) == 0) return (uint32_t)vid;
#endif
#if defined(CONFIG_WAN_VLAN_SUPPORT)
	if(br2684_vlan_dev_get_vid(netif, &vid) == 0) return (uint32_t)vid;
#endif

	return ~0;
}

int32_t ppa_br2684_get_vcc(PPA_NETIF *netif, PPA_VCC **pvcc)
{
	pr_debug("name=%s priv_flags=0x%x flags=0x%x\n",
	         netif->name,
	         netif->priv_flags,
	         netif->flags);
	if(netif->priv_flags & IFF_PPA_DUMMY && netif->flags & IFF_BR2684) {
		struct avm_dpipe_vitf_vcc *vcc_priv = netdev_priv(netif);
		*pvcc = vcc_priv->vcc;

		return PPA_SUCCESS;
	}
	pr_debug("nope\n");
	return PPA_EINVAL;
}

int32_t ppa_if_is_br2684(PPA_NETIF *netif, PPA_IFNAME *ifname)
{
	pr_debug("name=%s priv_flags=0x%x flags=0x%x\n",
	         ifname,
	         netif->priv_flags,
	         netif->flags);
	return (netif->priv_flags & IFF_PPA_DUMMY && netif->flags & IFF_BR2684);
}

int32_t ppa_if_is_ipoa(PPA_NETIF * netif, PPA_IFNAME * ifname) { return 0; }

void ppa_if_force_remove(char *ifname)
{
	PPA_IFINFO lan_ifinfo, wan_ifinfo;

	pr_debug("called\n");

	lan_ifinfo.ifname = ifname;
	lan_ifinfo.if_flags = PPA_F_LAN_IF;

	wan_ifinfo.ifname = ifname;
	wan_ifinfo.if_flags = 0;

	/* attempt to remove the device from both, LAN and WAN */
	ppa_hook_del_if_fn(&lan_ifinfo, 0);
	ppa_hook_del_if_fn(&wan_ifinfo, 0);
}

 EXPORT_SYMBOL(ppa_br2684_get_vcc);
 EXPORT_SYMBOL(ppa_if_is_br2684);
 EXPORT_SYMBOL(ppa_if_is_ipoa);
 EXPORT_SYMBOL(ppa_if_force_remove);

struct net_device* ppa_get_dslite_phyif(struct net_device *netif)
{
	struct ip6_tnl *tip6 = netdev_priv(netif);
	return tip6->dev;
}
EXPORT_SYMBOL(ppa_get_dslite_phyif);

int32_t ppa_msec_timer_add(PPA_TIMER *p_timer, uint32_t timeout_in_msec)
{
    p_timer->expires = jiffies + msecs_to_jiffies(timeout_in_msec);
    add_timer(p_timer);

    return PPA_SUCCESS;
}
EXPORT_SYMBOL(ppa_msec_timer_add);
