#ifndef __DLTX_FW_DATA_STRUCTURE_BE_H_
#define __DLTX_FW_DATA_STRUCTURE_BE_H_

typedef struct {

	unsigned int packet_pointer;

	unsigned int rsvd0:16;
	unsigned int fragment_length:16;

	unsigned int rsvd1;

	unsigned int rsvd2;

	unsigned int rsvd3;

	unsigned int rsvd4;

	unsigned int payload_len:16;
	unsigned int flags:8;
	unsigned int endpoint_id:8;

	unsigned int rsvd5:16;
	unsigned int control_bytes_1:8;
	unsigned int control_bytes_2:8;

	unsigned int rsvd6:1;
	unsigned int chk_sum:2;
	unsigned int mib:1;
	unsigned int postponed:1;
	unsigned int ext_tid:5;
	unsigned int vdec_id:6;
	unsigned int pkt_type:3;
	unsigned int pkt_subtype:5;
	unsigned int msg_type:8;

	unsigned int pb_pointer:16;
	unsigned int length:16;

	unsigned int frag_desc_pointer;

	unsigned int peer_id;

} dltx_msg_header_t;

typedef struct {

	unsigned int rsvd9:3;
	unsigned int tunnel_id:4;
	unsigned int flow_id:8;
	unsigned int eth_type:2;
	unsigned int dest_sub_if_id:15;

	unsigned int session_id:12;
	unsigned int tcp_err:1;
	unsigned int nat:1;
	unsigned int dec:1;
	unsigned int enc:1;
	unsigned int mpe2:1;
	unsigned int mpe1:1;
	unsigned int color:2;
	unsigned int ep:4;
	unsigned int res:4;
	unsigned int class:4;

	unsigned int dram_address;

	unsigned int own:1;
	unsigned int c_bit:1;
	unsigned int sop:1;
	unsigned int eop:1;
	unsigned int dic:1;
	unsigned int pdu_type:1;
	unsigned int byte_offset:3;
	unsigned int rsvd10:7;
	unsigned int data_length:16;

} dltx_dma_rx_desc_t;

typedef struct {

	unsigned int source_buffer_pointer;

	unsigned int meta_data:14;
	unsigned int byte_swap:1;
	unsigned int gather:1;
	unsigned int source_buffer_length:16;

} dltx_ce4des_format_t;

typedef struct {

	unsigned int ce4_write_index;

} dltx_ce4_write_index_track_t;

typedef struct {

	unsigned int packet_id;

} dltx_circular_list_t;

typedef struct {

	unsigned int pointer_address;

} dltx_buffer_pointer_table_t;

typedef struct {

	unsigned int rsvd11:15;
	unsigned int frag_len:17;

	unsigned int payloadlen:16;
	unsigned int flags:8;
	unsigned int endpointid:8;

	unsigned int rsvd12:16;
	unsigned int controlbyte1:8;
	unsigned int controlbyte2:8;

	unsigned int rsvd13:1;
	unsigned int chksum:2;
	unsigned int mib:1;
	unsigned int postponed:1;
	unsigned int exttid:5;
	unsigned int vdevid:6;
	unsigned int pkttype:3;
	unsigned int pktsubtype:5;
	unsigned int msgtype:8;

	unsigned int packet_id:16;
	unsigned int length:16;

	unsigned int _dw_res0[3];

} htt_tx_des_t;

typedef struct {

	unsigned int qca_vap_id;

} dltx_qca_vap_id_map_t;

typedef struct {

	unsigned int txpdu_low;

	unsigned int txpdu_high;

	unsigned int txbytes_low;

	unsigned int txbytes_high;

	unsigned int txdrop_low;

	unsigned int txdrop_high;

	unsigned int _dw_res0[10];

} dltx_vap_data_mib_t;

typedef struct {

	unsigned int cpu_ce4_msg_low;

	unsigned int cpu_ce4_msg_high;

	unsigned int cpu_ce4_bytes_low;

	unsigned int cpu_ce4_bytes_high;

	unsigned int _dw_res0[4];

} dltx_data_mib_t;

typedef struct {

	unsigned int cfg_badr_ce4buf;

	unsigned int cfg_num_ce4buf;

	unsigned int cfg_ce4_read_index_addr;

	unsigned int cfg_ce4_write_index_addr;

	unsigned int local_ce4_read_index;

	unsigned int local_ce4_write_index;

	unsigned int load_ce4_read_index_req;

	unsigned int cfg_ce4des_low;

	unsigned int cfg_ce4_des_full;

	unsigned int _dw_res0[7];

} dltx_cfg_ctxt_ce4buf_t;

typedef struct {

	unsigned int circular_list_badr;

	unsigned int circular_list_num;

	unsigned int circular_list_read_index;

	unsigned int circular_list_write_index;

	unsigned int consumed_pkt_ids;

	unsigned int _dw_res0[3];

} dltx_cfg_ctxt_circular_list_t;

typedef struct {

	unsigned int buffer_pointer_badr;

	unsigned int buffer_pointer_num;

	unsigned int _dw_res0[2];

} dltx_cfg_ctxt_buffer_pointer_table_t;

typedef struct {

	unsigned int write_index_track_badr;

	unsigned int write_index_track_num;

	unsigned int _dw_res0[2];

} dltx_cfg_ctxt_ce4_write_index_track_t;

typedef struct {

	unsigned int cfg_size_tx_header;

	unsigned int _dw_res0[3];

} dltx_cfg_ctxt_tx_msg_t;

typedef struct {

	unsigned int qca_vap_id_map_badr;

	unsigned int qca_vap_id_map_num;

	unsigned int _dw_res0[2];

} dltx_cfg_ctxt_qca_vap_id_map_t;

typedef struct {

	unsigned int ext_tid;

	unsigned int pkt_type;

	unsigned int pkt_subtype;

	unsigned int msg_type;

	unsigned int pb_pointer;

	unsigned int pb_length;

	unsigned int peer_id;

	unsigned int vap;

	unsigned int radio_id;

	unsigned int _dw_res0[15];

} dltx_ctxt_msg_t;

typedef struct {

	unsigned int dltx_enable;

	unsigned int dltx_base_address;

	unsigned int fw_ver_id;

	unsigned int fw_feature;

	unsigned int debug_print_enable;

	unsigned int switch_parser_flag;

	unsigned int qca_addr_dbg;

	unsigned int _dw_res0[9];

} dltx_cfg_global_t;

typedef struct {

	unsigned int fragment_ptr[16];

} dltx_frag_data_t;

typedef struct {

	unsigned int frag_data_badr;

	unsigned int frag_data_num;

	unsigned int _dw_res0[2];

} dltx_cfg_ctxt_frag_data_t;

#ifdef SUPPORT_MULTICAST_TO_UNICAST
struct _dl_mcast_group_table {
	/* first dw */
	unsigned int valid:1;
	unsigned int res:23;
	unsigned int grpIdx:8;
	/* Bit map */
	unsigned int bitmap[4];
};

struct _dl_peer_mac_mapping_table {
	/* first dw */
	unsigned int valid:1;
	unsigned int vap_id:4;
	unsigned int res:11;
	unsigned int mac5:8;
	unsigned int mac4:8;
	/* second dw */
	unsigned int mac3:8;
	unsigned int mac2:8;
	unsigned int mac1:8;
	unsigned int mac0:8;
};
#endif /* SUPPORT_MULTICAST_TO_UNICAST */

#define DTLK_DEBUG_TX_COMPLETION_NUM 16
typedef struct {

	unsigned int tx_count;

	unsigned int tx_ptr;

	unsigned int tx_completion_count;

	unsigned int tx_completion_count_idx;

	unsigned int tx_completion_ptr[DTLK_DEBUG_TX_COMPLETION_NUM - 4];

} dltx_packet_id_trace_circular_list_t;

#endif

