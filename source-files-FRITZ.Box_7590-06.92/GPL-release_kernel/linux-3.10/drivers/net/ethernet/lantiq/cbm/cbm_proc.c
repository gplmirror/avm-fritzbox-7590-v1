#include <linux/klogging.h>
#include "cbm_proc.h"
#include "cbm.h"
#include <net/datapath_proc_api.h>
static struct dp_proc_entry cbm_proc_entries[] = {
/*name single_callback_t multi_callback_t/_start write_callback_t */
	{PROC_FILE_CBM_ENQ, cbm_enqueue_dump, NULL, NULL,
	 NULL},
	{PROC_FILE_CBM_DEQ, cbm_dequeue_dump, NULL, NULL,
	 NULL},
	{PROC_FILE_CBM_LS, cbm_ls_dump, NULL, NULL,
	 NULL},
	{PROC_FILE_CBM_SBA_JBA, cbm_sba_jba_dump, NULL, NULL,
	 NULL},
	{PROC_FILE_CBM_STAT_REG, cbm_status_reg_dump, NULL, NULL,
	 NULL},
	{PROC_FILE_CBM_DES64_INGRESS_REG, cbm_des64_ingress_dump, NULL,
	 NULL, NULL},
	{PROC_FILE_CBM_DES64_EGRESS_REG, cbm_des64_egress_dump, NULL,
	 NULL, NULL},
	/*{PROC_FILE_RCNT_REG, NULL, NULL, NULL,
	 fsqm_rnt_read},*/
	{PROC_FILE_FSC_FSQT_REG, NULL, NULL, NULL,
	 fsqm_freesegment_read},
	{PROC_FILE_CBM_ENQ_OVH_BYTES, cbm_enq_ovh_bytes_read, NULL, NULL,
	 cbm_enq_ovh_bytes_write},
	{NULL, NULL, NULL, NULL, NULL}
};

/**
   Initialize and install the proc entry

   \return
   -1 or 0 on success

   \remarks
   Called by the kernel.
*/
int cbm_proc_install(struct proc_dir_entry *parent)
{
	struct proc_dir_entry *driver_proc_node;
	driver_proc_node = proc_mkdir(CBM_PROC_PARENT CBM_PROC_NAME, parent);
	if (driver_proc_node != NULL) {
		int i;
		for (i = 0; i < ARRAY_SIZE(cbm_proc_entries); i++)
			dp_proc_entry_create(driver_proc_node,
					     &cbm_proc_entries[i]);
	} else {
		LOGF_KLOG_ERROR("cannot create proc entry");
		return -1;
	}
	return 0;
}
EXPORT_SYMBOL(cbm_proc_install);

/*! Register Offset (relative)*/
#define CBM_STAT 0x210
/*! Register Offset (absolute) for 1st Instance CBM*/
#define CBM_CBM_STAT 0x1E710210u

/*! Register Reset Value*/
#define CBM_STAT_RST 0x00000000u

/*! Field ACT - CBM Activity Status*/
#define CBM_STAT_ACT_POS 0
/*! Field ACT - CBM Activity Status*/
#define CBM_STAT_ACT_MASK 0x1u
/*! Constant INACTIVE - Inactive*/
#define CONST_CBM_STAT_ACT_INACTIVE 0x0
/*! Constant ACTIVE - Active*/
#define CONST_CBM_STAT_ACT_ACTIVE 0x1

/*! Field LSBYP - Hardware SMS lightspeed mode*/
#define CBM_STAT_LSBYP_POS 16
/*! Field LSBYP - Hardware SMS lightspeed mode*/
#define CBM_STAT_LSBYP_MASK 0x10000u
/*! Constant LS - Lightspeed mode enable*/
#define CONST_CBM_STAT_LSBYP_LS 0x0
/*! Constant BYP - Lightspeed mode bypass*/
#define CONST_CBM_STAT_LSBYP_BYP 0x1

/*! Field JSEL - Jumbo Select*/
#define CBM_STAT_JSEL_POS 17
/*! Field JSEL - Jumbo Select*/
#define CBM_STAT_JSEL_MASK 0x20000u
/*! Constant JSEL0 - Jumbo frame size 8 KBytes*/
#define CONST_CBM_STAT_JSEL_JSEL0 0x0
/*! Constant JSEL1 - Jumbo frame size 16 KBytes*/
#define CONST_CBM_STAT_JSEL_JSEL1 0x1

#define REG64(addr)      (*((volatile uint64_t *)(addr)))

void print_reg64(char *name, uint32_t addr)
{
	uint32_t reg1, reg2;
	uint64_t reg;

	reg = REG64(addr);
	reg1 = (uint32_t) (reg >> 32);
	reg2 = (reg & 0xFFFFFFFF);

	LOGF_KLOG_CONT("%s: addr:0x%8x, val[32-63]: 0x%8x, val[0-31]: 0x%8x\n",
		  name, addr, reg1, reg2);
}

static uint32_t cbm_eqm_addr_base_d = KSEG1ADDR(CBM_EQM_MODULE_BASE);
#define CBM_ENQ_BASE_D				cbm_eqm_addr_base_d

void cbm_enqueue_dump(struct seq_file *s)
{
	seq_printf(s, "CBM_ENQ_BASEe=0x%x\n", CBM_ENQ_BASE_D);

	seq_printf(s, "%8s,%8s,%8s,%8s,%8s,%8s\n", "portno :", "pocc", "eqpc",
	"disc_desc1", "disc_desc2", "disc_desc3");
	seq_printf(s, "cpu_p0: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_CPU_IGP_0),
	REG32(CBM_ENQ_BASE_D + EQPC_CPU_IGP_0),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_CPU_IGP_0),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_CPU_IGP_0),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_CPU_IGP_0));

	seq_printf(s, "cpu_p1: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_CPU_IGP_1),
	REG32(CBM_ENQ_BASE_D + EQPC_CPU_IGP_1),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_CPU_IGP_1),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_CPU_IGP_1),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_CPU_IGP_1));

	seq_printf(s, "cpu_p2: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_CPU_IGP_2),
	REG32(CBM_ENQ_BASE_D + EQPC_CPU_IGP_2),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_CPU_IGP_2),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_CPU_IGP_2),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_CPU_IGP_2));

	seq_printf(s, "cpu_p3: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_CPU_IGP_3),
	REG32(CBM_ENQ_BASE_D + EQPC_CPU_IGP_3),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_CPU_IGP_3),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_CPU_IGP_3),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_CPU_IGP_3));

	seq_printf(s, "cpu_p4: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_CPU_IGP_4),
	REG32(CBM_ENQ_BASE_D + EQPC_CPU_IGP_4),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_CPU_IGP_4),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_CPU_IGP_4),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_CPU_IGP_4));

	seq_printf(s, "dma_p5: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_5),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_5),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_5),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_5),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_5));

	seq_printf(s, "dma_p6: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_6),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_6),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_6),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_6),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_6));

	seq_printf(s, "dma_p7: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_7),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_7),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_7),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_7),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_7));

	seq_printf(s, "dma_p8: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_8),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_8),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_8),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_8),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_8));

	seq_printf(s, "dma_p9: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_9),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_9),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_9),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_9),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_9));

	seq_printf(s, "dma_p10: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_10),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_10),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_10),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_10),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_10));

	seq_printf(s, "dma_p11: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_11),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_11),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_11),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_11),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_11));

	seq_printf(s, "dma_p12: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_12),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_12),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_12),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_12),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_12));

	seq_printf(s, "dma_p13: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_13),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_13),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_13),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_13),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_13));

	seq_printf(s, "dma_p14: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_14),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_14),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_14),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_14),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_14));

	seq_printf(s, "dma_p15: 0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
	REG32(CBM_ENQ_BASE_D + POCC_DMA_IGP_15),
	REG32(CBM_ENQ_BASE_D + EQPC_DMA_IGP_15),
	REG32(CBM_ENQ_BASE_D + DISC_DESC1_DMA_IGP_15),
	REG32(CBM_ENQ_BASE_D + DISC_DESC2_DMA_IGP_15),
	REG32(CBM_ENQ_BASE_D + DISC_DESC3_DMA_IGP_15));
}

static uint32_t cbm_deqm_addr_base_d = KSEG1ADDR(CBM_DQM_MODULE_BASE);
#define CBM_DEQ_BASE_D				cbm_deqm_addr_base_d

void cbm_dequeue_dump(struct seq_file *s)
{

	seq_printf(s, "CBM_DEQ_BASEe=0x%x\n", CBM_DEQ_BASE_D);

	seq_printf(s, "%8s\n", "cpu_p0_dqpc");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_CPU_EGP_0));

	seq_printf(s, "%8s\n", "cpu_p1_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_CPU_EGP_1));

	seq_printf(s, "%8s\n", "cpu_p2_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_CPU_EGP_2));

	seq_printf(s, "%8s\n", "cpu_p3_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_CPU_EGP_3));

	seq_printf(s, "%8s\n", "cpu_p4_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_CPU_EGP_4));

	seq_printf(s, "%8s\n", "cpu_p5_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_CPU_EGP_5));

	seq_printf(s, "%8s\n", "dma_p6_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_6));

	seq_printf(s, "%8s\n", "dma_p7_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_7));

	seq_printf(s, "%8s\n", "dma_p8_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_8));

	seq_printf(s, "%8s\n", "dma_p9_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_9));

	seq_printf(s, "%8s\n", "dma_p10_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_10));

	seq_printf(s, "%8s\n", "dma_p11_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_11));

	seq_printf(s, "%8s\n", "dma_p12_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_12));

	seq_printf(s, "%8s\n", "dma_p13_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_13));

	seq_printf(s, "%8s\n", "dma_p14_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_14));

	seq_printf(s, "%8s\n", "dma_p15_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_15));

	seq_printf(s, "%8s\n", "dma_p16_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_16));

	seq_printf(s, "%8s\n", "dma_p17_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_17));

	seq_printf(s, "%8s\n", "dma_p18_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_18));

	seq_printf(s, "%8s\n", "dma_p19_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_19));

	seq_printf(s, "%8s\n", "dma_p20_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_20));

	seq_printf(s, "%8s\n", "dma_p21_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_21));

	seq_printf(s, "%8s\n", "dma_p22_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_22));

	seq_printf(s, "%8s\n", "dma_p23_DQPC");
	seq_printf(s, "0x%08x\n", REG32(CBM_DEQ_BASE_D + DQPC_DMA_EGP_23));

}

static uint32_t cbm_ls_status_port0_base_d =
KSEG1ADDR(CBM_LS_LS_STATUS_PORT0);
static uint32_t cbm_ls_status_port1_base_d =
KSEG1ADDR(CBM_LS_LS_STATUS_PORT1);
static uint32_t cbm_ls_status_port2_base_d =
KSEG1ADDR(CBM_LS_LS_STATUS_PORT2);
static uint32_t cbm_ls_status_port3_base_d =
KSEG1ADDR(CBM_LS_LS_STATUS_PORT3);

#define CBM_LS_P0_D					cbm_ls_status_port0_base_d
#define CBM_LS_P1_D					cbm_ls_status_port1_base_d
#define CBM_LS_P2_D					cbm_ls_status_port2_base_d
#define CBM_LS_P3_D					cbm_ls_status_port3_base_d

void cbm_ls_dump(struct seq_file *s)
{
	unsigned int reg_r_data, q_len0 = 0, q_full0 = 0, q_empty0 =
	0, q_cnt0 = 0, q_len1 = 0, q_full1 = 0, q_empty1 = 0, q_cnt1 = 0;
	unsigned int q_len2 = 0, q_full2 = 0, q_empty2 = 0, q_cnt2 =
	0, q_len3 = 0, q_full3 = 0, q_empty3 = 0, q_cnt3 = 0;

	seq_printf(s, "%8s,%8s,%8s,%8s,%8s\n", "portno:", "qlen", "qfull",
	"qempty", "cntval");
	reg_r_data = 0;
	reg_r_data = REG32(CBM_LS_P0_D);
	q_len0 =
	get_val(reg_r_data, LS_STATUS_PORT0_QUEUE_LEN_MASK,
	LS_STATUS_PORT0_QUEUE_LEN_POS);
	q_full0 =
	get_val(reg_r_data, LS_STATUS_PORT0_QUEUE_FULL_MASK,
	LS_STATUS_PORT0_QUEUE_FULL_POS);
	q_empty0 =
	get_val(reg_r_data, LS_STATUS_PORT0_QUEUE_EMPTY_MASK,
	LS_STATUS_PORT0_QUEUE_EMPTY_POS);
	q_cnt0 =
	get_val(reg_r_data, LS_STATUS_PORT0_CNT_VAL_MASK,
	LS_STATUS_PORT0_CNT_VAL_POS);
	seq_printf(s, "port0:%08d,%08d,%08d,0x%08x\n", q_len0, q_full0,
	q_empty0, q_cnt0);

	reg_r_data = 0;
	reg_r_data = REG32(CBM_LS_P1_D);
	q_len1 =
	get_val(reg_r_data, LS_STATUS_PORT1_QUEUE_LEN_MASK,
	LS_STATUS_PORT1_QUEUE_LEN_POS);
	q_full1 =
	get_val(reg_r_data, LS_STATUS_PORT1_QUEUE_FULL_MASK,
	LS_STATUS_PORT1_QUEUE_FULL_POS);
	q_empty1 =
	get_val(reg_r_data, LS_STATUS_PORT1_QUEUE_EMPTY_MASK,
	LS_STATUS_PORT1_QUEUE_EMPTY_POS);
	q_cnt1 =
	get_val(reg_r_data, LS_STATUS_PORT1_CNT_VAL_MASK,
	LS_STATUS_PORT1_CNT_VAL_POS);
	seq_printf(s, "port1:%08d,%08d,%08d,0x%08x\n", q_len1, q_full1,
	q_empty1, q_cnt1);

	reg_r_data = 0;
	reg_r_data = REG32(CBM_LS_P2_D);
	q_len2 =
	get_val(reg_r_data, LS_STATUS_PORT2_QUEUE_LEN_MASK,
	LS_STATUS_PORT2_QUEUE_LEN_POS);
	q_full2 =
	get_val(reg_r_data, LS_STATUS_PORT2_QUEUE_FULL_MASK,
	LS_STATUS_PORT2_QUEUE_FULL_POS);
	q_empty2 =
	get_val(reg_r_data, LS_STATUS_PORT2_QUEUE_EMPTY_MASK,
	LS_STATUS_PORT2_QUEUE_EMPTY_POS);
	q_cnt2 =
	get_val(reg_r_data, LS_STATUS_PORT2_CNT_VAL_MASK,
	LS_STATUS_PORT2_CNT_VAL_POS);
	seq_printf(s, "port2:%08d,%08d,%08d,0x%08x\n", q_len2, q_full2,
	q_empty2, q_cnt2);

	reg_r_data = 0;
	reg_r_data = REG32(CBM_LS_P3_D);
	q_len3 =
	get_val(reg_r_data, LS_STATUS_PORT3_QUEUE_LEN_MASK,
	LS_STATUS_PORT3_QUEUE_LEN_POS);
	q_full3 =
	get_val(reg_r_data, LS_STATUS_PORT3_QUEUE_FULL_MASK,
	LS_STATUS_PORT3_QUEUE_FULL_POS);
	q_empty3 =
	get_val(reg_r_data, LS_STATUS_PORT3_QUEUE_EMPTY_MASK,
	LS_STATUS_PORT3_QUEUE_EMPTY_POS);
	q_cnt3 =
	get_val(reg_r_data, LS_STATUS_PORT3_CNT_VAL_MASK,
	LS_STATUS_PORT3_CNT_VAL_POS);
	seq_printf(s, "port3:%08d,%08d,%08d,0x%08x\n", q_len3, q_full3,
	q_empty3, q_cnt3);
}

static uint32_t cbm_sba0_base_d = KSEG1ADDR(CBM_CBM_SBA_0);
static uint32_t cbm_sba1_base_d = KSEG1ADDR(CBM_CBM_SBA_1);
static uint32_t cbm_jba0_base_d = KSEG1ADDR(CBM_CBM_JBA_0);
static uint32_t cbm_jba1_base_d = KSEG1ADDR(CBM_CBM_JBA_1);

#define CBM_SBA0_D			cbm_sba0_base_d
#define CBM_SBA1_D			cbm_sba1_base_d
#define CBM_JBA0_D			cbm_jba0_base_d
#define CBM_JBA1_D			cbm_jba1_base_d

void cbm_sba_jba_dump(struct seq_file *s)
{
	unsigned int sba0 = 0, sba1 = 0, jba0 = 0, jba1 = 0;

	sba0 = REG32(CBM_SBA0_D);
	sba1 = REG32(CBM_SBA1_D);
	jba0 = REG32(CBM_JBA0_D);
	jba1 = REG32(CBM_JBA1_D);

	seq_printf(s, "%8s,%8s,%8s,%8s\n", "sba0", "sba1", "jba0", "jba1");
	seq_printf(s, "0x%08x,0x%08x,0x%08x,0x%08x\n", sba0, sba1, jba0,
		jba1);
}

static uint32_t cbm_status_base_d = KSEG1ADDR(CBM_CBM_STAT);
#define CBM_STAT_D				cbm_status_base_d

void cbm_status_reg_dump(struct seq_file *s)
{
	unsigned int reg_r_data, act_stat = 0, lsbyp = 0, jsel = 0;
	reg_r_data = 0;
	reg_r_data = REG32(CBM_STAT_D);
	act_stat = get_val(reg_r_data, CBM_STAT_ACT_MASK, CBM_STAT_ACT_POS);
	lsbyp = get_val(reg_r_data, CBM_STAT_LSBYP_MASK, CBM_STAT_LSBYP_POS);
	jsel = get_val(reg_r_data, CBM_STAT_JSEL_MASK, CBM_STAT_JSEL_POS);
	seq_printf(s, "%8s,%8s,%8s\n", "activity_status", "LSBYP", "JSEL");
	seq_printf(s, "0x%08x,0x%08x,0x%08x\n", act_stat, lsbyp, jsel);
}
#if 0
static unsigned int cbm_std_frame_size = 0x800;	/*  2k    */
static unsigned int cbm_jbo_frame_size_8k = 0x2000;	/*  16k  */
static unsigned int cbm_jbo_frame_size_16k = 0x4000;	/*  16k  */
#endif
static uint32_t fsqm0_base_d = KSEG1ADDR(FSQM0_MODULE_BASE);
static uint32_t fsqm1_base_d = KSEG1ADDR(FSQM1_MODULE_BASE);
#define FSQM0_BASE_D			fsqm0_base_d
#define FSQM1_BASE_D			fsqm1_base_d
#define FSQM_BASE_D(idx)		(idx == 0 ? fsqm0_base_d : fsqm1_base_d)

ssize_t cbm_enq_ovh_bytes_write(struct file *file, const char *buf,
			size_t count, loff_t *ppos)
{
	uint32_t port;
	int8_t ovh_bytes;
	int len;
	char str[64];
	char *p = (char *)str;
	char *param_list[5] = { 0 };
	int num = 0;

	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;
	if (!len)
		return count;

	num = dp_split_buffer(p, param_list, ARRAY_SIZE(param_list));

	if (num == 2) {
		port = dp_atoi(param_list[0]);
		ovh_bytes = dp_atoi(param_list[1]);

		if(((port >= 0) && (port <= 15))
		&&((ovh_bytes <= 127) && (ovh_bytes >= -127)))
			cbm_enqueue_port_overhead_set(port, ovh_bytes);
	} else if (num == 1) {
		port = dp_atoi(param_list[0]);
		if((port >= 0) && (port <= 15)) {
			cbm_enqueue_port_overhead_get(port, &ovh_bytes);
			LOGF_KLOG_CONT("%d\n", ovh_bytes);
		}
	} else {
		LOGF_KLOG_CONT("Wrong Parameter(try): echo fsqmbase_index >  fsqmfsc\n");
		return count;
	}
	return count;
}

void  cbm_enq_ovh_bytes_read(struct seq_file *s)
{
	int i;
	int8_t ovh_bytes;

	for(i = 0; i <= 15; i++) {
		cbm_enqueue_port_overhead_get(i, &ovh_bytes);
		LOGF_KLOG_CONT("%d \t", ovh_bytes);
	}
	LOGF_KLOG_CONT("\n");
}
ssize_t fsqm_freesegment_read(struct file *file, const char *buf,
			size_t count, loff_t *ppos)
{
	uint32_t index;
	int len;
	char str[64];
	char *p = (char *)str;
	char *param_list[5] = { 0 };
	int num = 0;

	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;
	if (!len)
		return count;

	num = dp_split_buffer(p, param_list, ARRAY_SIZE(param_list));

	if (num == 1) {
		index = dp_atoi(param_list[0]);
		LOGF_KLOG_CONT("%8s,%8s,%8s,%8s,%8s,%8s\n", "FSC", "FSQT0",
			  "FSQT1", "FSQT2", "FSQT3", "FSQT4");
		LOGF_KLOG_CONT("0x%08x,0x%08x,0x%08x,0x%08x,0x%08x,0x%08x\n",
			  REG32(FSQM_BASE_D(index)
				+ OFSC), REG32(FSQM_BASE_D(index) + FSQT0),
			  REG32(FSQM_BASE_D(index) + FSQT1),
			  REG32(FSQM_BASE_D(index) + FSQT2),
			  REG32(FSQM_BASE_D(index) + FSQT3),
			  REG32(FSQM_BASE_D(index) + FSQT4));
	} else {
		LOGF_KLOG_CONT("Wrong Parameter(try): echo fsqmbase_index >  fsqmfsc\n");
		return count;
	}
		return count;
}
#if 0
ssize_t fsqm_rnt_read(struct file *file, const char *buf, size_t count,
		loff_t *ppos)
{
	uint32_t buf_type, idx, reg_r_data, jsel = 0;
	static unsigned int std_buffer_addr_base;
	static unsigned int jbo_buffer_addr_base;
	uint32_t buf_addr;
	int len;
	char str[64];
	char *p = (char *)str;
	char *param_list[20] = { 0 };
	int num = 0;

	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;
	if (!len)
		return count;

	num = dp_split_buffer(p, param_list, ARRAY_SIZE(param_list));

	if (num == 1) {
		buf_addr = dp_atoi(param_list[0]);
		LOGF_KLOG_CONT("buf_addr: 0x%08x\n", buf_addr);
		std_buffer_addr_base = REG32(CBM_SBA0_D);
		LOGF_KLOG_CONT("std_buffer_addr_base: 0x%08x\n",
			  std_buffer_addr_base);
		jbo_buffer_addr_base = REG32(CBM_JBA0_D);
		LOGF_KLOG_CONT("jbo_buffer_addr_base: 0x%08x\n",
			  jbo_buffer_addr_base);
		buf_type = check_ptr_validation(buf_addr);
		LOGF_KLOG_CONT("buf_type: 0x%08x\n", buf_type);

		if (buf_type == SBA0_STD || buf_type == SBA1_STD) {
			idx =
			    (buf_addr -
			     std_buffer_addr_base) / cbm_std_frame_size;
		} else if (buf_type == SBA0_JBO || buf_type == SBA1_JBO) {
			reg_r_data = 0;
			reg_r_data = REG32(CBM_STAT_D);
			jsel =
			    get_val(reg_r_data, CBM_STAT_JSEL_MASK,
				    CBM_STAT_JSEL_POS);
			LOGF_KLOG_CONT("jsel: %d\n", jsel);

			if (jsel)
				idx =
				    (buf_addr -
				     jbo_buffer_addr_base) /
				    cbm_jbo_frame_size_16k;
			else
				idx =
				    (buf_addr -
				     jbo_buffer_addr_base) /
				    cbm_jbo_frame_size_8k;

		} else {
			LOGF_KLOG_CONT("ERROR: buffer type not found\n");
			return count;

		}
		LOGF_KLOG_CONT("idx: %d\n", idx);
		LOGF_KLOG_CONT("RCNT: 0x%08x\n",
			  REG32(FSQM_BASE_D(idx) + RCNT + (idx * 4)));
	} else {
		LOGF_KLOG_CONT("Wrong Parameter(try): echo buffer_address > fsqmrnt\n");
		return count;
	}
		return count;
}
#endif
static uint32_t cbm_desc64_base_d = KSEG1ADDR(CBM_DESC64B_MODULE_BASE);
#define CBM_DES64_D				cbm_desc64_base_d

void cbm_des64_ingress_dump(struct seq_file *s)
{
	print_reg64("SDESC0_0_IGP_5:	", CBM_DES64_D + SDESC0_0_IGP_5);
	print_reg64("SDESC1_0_IGP_5:	", CBM_DES64_D + SDESC1_0_IGP_5);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_5:	", CBM_DES64_D + SDESC0_1_IGP_5);
	print_reg64("SDESC0_1_IGP_5:	", CBM_DES64_D + SDESC1_1_IGP_5);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_5:	", CBM_DES64_D + JDESC0_0_IGP_5);
	print_reg64("JDESC1_0_IGP_5:	", CBM_DES64_D + JDESC1_0_IGP_5);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_5:	", CBM_DES64_D + JDESC0_1_IGP_5);
	print_reg64("JDESC1_1_IGP_5:	", CBM_DES64_D + JDESC1_1_IGP_5);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_0_IGP_6:	", CBM_DES64_D + SDESC0_0_IGP_6);
	print_reg64("SDESC1_0_IGP_6:	", CBM_DES64_D + SDESC1_0_IGP_6);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_6:	", CBM_DES64_D + SDESC0_1_IGP_6);
	print_reg64("SDESC1_1_IGP_6:	", CBM_DES64_D + SDESC1_1_IGP_6);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_6:	", CBM_DES64_D + JDESC0_0_IGP_6);
	print_reg64("JDESC1_0_IGP_6:	", CBM_DES64_D + JDESC1_0_IGP_6);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_6:	", CBM_DES64_D + JDESC0_1_IGP_6);
	print_reg64("JDESC1_1_IGP_6:	", CBM_DES64_D + JDESC1_1_IGP_6);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_0_IGP_7:	", CBM_DES64_D + SDESC0_0_IGP_7);
	print_reg64("SDESC1_0_IGP_7:	", CBM_DES64_D + SDESC1_0_IGP_7);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_7:	", CBM_DES64_D + SDESC0_1_IGP_7);
	print_reg64("SDESC1_1_IGP_7:	", CBM_DES64_D + SDESC1_1_IGP_7);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_7:	", CBM_DES64_D + JDESC0_0_IGP_7);
	print_reg64("JDESC1_0_IGP_7:	", CBM_DES64_D + JDESC1_0_IGP_7);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_7:	", CBM_DES64_D + JDESC0_1_IGP_7);
	print_reg64("JDESC1_1_IGP_7:	", CBM_DES64_D + JDESC1_1_IGP_7);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_0_IGP_8:	", CBM_DES64_D + SDESC0_0_IGP_8);
	print_reg64("SDESC1_0_IGP_8:	", CBM_DES64_D + SDESC1_0_IGP_8);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_8:	", CBM_DES64_D + SDESC0_1_IGP_8);
	print_reg64("SDESC1_1_IGP_8:	", CBM_DES64_D + SDESC1_1_IGP_8);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_8:	", CBM_DES64_D + JDESC0_0_IGP_8);
	print_reg64("JDESC1_0_IGP_8:	", CBM_DES64_D + JDESC1_0_IGP_8);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_8:	", CBM_DES64_D + JDESC0_1_IGP_8);
	print_reg64("JDESC1_1_IGP_8:	", CBM_DES64_D + JDESC1_1_IGP_8);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_0_IGP_9:	", CBM_DES64_D + SDESC0_0_IGP_9);
	print_reg64("SDESC1_0_IGP_9:	", CBM_DES64_D + SDESC1_0_IGP_9);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_9:	", CBM_DES64_D + SDESC0_1_IGP_9);
	print_reg64("SDESC1_1_IGP_9:	", CBM_DES64_D + SDESC1_1_IGP_9);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_9:	", CBM_DES64_D + JDESC0_0_IGP_9);
	print_reg64("JDESC1_0_IGP_9:	", CBM_DES64_D + JDESC1_0_IGP_9);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_9:	", CBM_DES64_D + JDESC0_1_IGP_9);
	print_reg64("JDESC1_1_IGP_9:	", CBM_DES64_D + JDESC1_1_IGP_9);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_0_IGP_10:	", CBM_DES64_D + SDESC0_0_IGP_10);
	print_reg64("SDESC1_0_IGP_10:	", CBM_DES64_D + SDESC1_0_IGP_10);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_10:	", CBM_DES64_D + SDESC0_1_IGP_10);
	print_reg64("SDESC1_1_IGP_10:	", CBM_DES64_D + SDESC1_1_IGP_10);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_10:	", CBM_DES64_D + JDESC0_0_IGP_10);
	print_reg64("JDESC1_0_IGP_10:	", CBM_DES64_D + JDESC1_0_IGP_10);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_10:	", CBM_DES64_D + JDESC0_1_IGP_10);
	print_reg64("JDESC1_1_IGP_10:	", CBM_DES64_D + JDESC1_1_IGP_10);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_0_IGP_11:	", CBM_DES64_D + SDESC0_0_IGP_11);
	print_reg64("SDESC1_0_IGP_11:	", CBM_DES64_D + SDESC1_0_IGP_11);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_11:	", CBM_DES64_D + SDESC0_1_IGP_11);
	print_reg64("SDESC1_1_IGP_11:	", CBM_DES64_D + SDESC1_1_IGP_11);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_11:	", CBM_DES64_D + JDESC0_0_IGP_11);
	print_reg64("JDESC1_0_IGP_11:	", CBM_DES64_D + JDESC1_0_IGP_11);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_11:	", CBM_DES64_D + JDESC0_1_IGP_11);
	print_reg64("JDESC1_1_IGP_11:	", CBM_DES64_D + JDESC1_1_IGP_11);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_0_IGP_12:	", CBM_DES64_D + SDESC0_0_IGP_12);
	print_reg64("SDESC1_0_IGP_12:	", CBM_DES64_D + SDESC1_0_IGP_12);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_12:	", CBM_DES64_D + SDESC0_1_IGP_12);
	print_reg64("SDESC1_1_IGP_12:	", CBM_DES64_D + SDESC1_1_IGP_12);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_12:	", CBM_DES64_D + JDESC0_0_IGP_12);
	print_reg64("JDESC1_0_IGP_12:	", CBM_DES64_D + JDESC1_0_IGP_12);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_12:	", CBM_DES64_D + JDESC0_1_IGP_12);
	print_reg64("JDESC1_1_IGP_12:	", CBM_DES64_D + JDESC1_1_IGP_12);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_0_IGP_13:	", CBM_DES64_D + SDESC0_0_IGP_13);
	print_reg64("SDESC1_0_IGP_13:	", CBM_DES64_D + SDESC1_0_IGP_13);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_13:	", CBM_DES64_D + SDESC0_1_IGP_13);
	print_reg64("SDESC1_1_IGP_13:	", CBM_DES64_D + SDESC1_1_IGP_13);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_13:	", CBM_DES64_D + JDESC0_0_IGP_13);
	print_reg64("JDESC1_0_IGP_13:	", CBM_DES64_D + JDESC1_0_IGP_13);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_13:	", CBM_DES64_D + JDESC0_1_IGP_13);
	print_reg64("JDESC1_1_IGP_13:	", CBM_DES64_D + JDESC1_1_IGP_13);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_0_IGP_14:	", CBM_DES64_D + SDESC0_0_IGP_14);
	print_reg64("SDESC1_0_IGP_14:	", CBM_DES64_D + SDESC1_0_IGP_14);
	LOGF_KLOG_CONT("\n");
	print_reg64("SDESC0_1_IGP_14:	", CBM_DES64_D + SDESC0_1_IGP_14);
	print_reg64("SDESC1_1_IGP_14:	", CBM_DES64_D + SDESC1_1_IGP_14);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_0_IGP_14:	", CBM_DES64_D + JDESC0_0_IGP_14);
	print_reg64("JDESC1_0_IGP_14:	", CBM_DES64_D + JDESC1_0_IGP_14);
	LOGF_KLOG_CONT("\n");
	print_reg64("JDESC0_1_IGP_14:	", CBM_DES64_D + JDESC0_1_IGP_14);
	print_reg64("JDESC1_1_IGP_14:	", CBM_DES64_D + JDESC1_1_IGP_14);
	LOGF_KLOG_CONT("\n");
}

void cbm_des64_egress_dump(struct seq_file *s)
{
	print_reg64("DESC0_0_EGP_5:	", CBM_DES64_D + DESC0_0_EGP_5);
	print_reg64("DESC1_0_EGP_5:	", CBM_DES64_D + DESC1_0_EGP_5);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_5:	", CBM_DES64_D + DESC0_1_EGP_5);
	print_reg64("DESC1_1_EGP_5:	", CBM_DES64_D + DESC1_1_EGP_5);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_6:	", CBM_DES64_D + DESC0_0_EGP_6);
	print_reg64("DESC1_0_EGP_6:	", CBM_DES64_D + DESC1_0_EGP_6);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_6:	", CBM_DES64_D + DESC0_1_EGP_6);
	print_reg64("DESC1_1_EGP_6:	", CBM_DES64_D + DESC1_1_EGP_6);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_7:	", CBM_DES64_D + DESC0_0_EGP_7);
	print_reg64("DESC1_0_EGP_7:	", CBM_DES64_D + DESC1_0_EGP_7);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_7:	", CBM_DES64_D + DESC0_1_EGP_7);
	print_reg64("DESC1_1_EGP_7:	", CBM_DES64_D + DESC1_1_EGP_7);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_8:	", CBM_DES64_D + DESC0_0_EGP_8);
	print_reg64("DESC1_0_EGP_8:	", CBM_DES64_D + DESC1_0_EGP_8);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_8:	", CBM_DES64_D + DESC0_1_EGP_8);
	print_reg64("DESC1_1_EGP_8:	", CBM_DES64_D + DESC1_1_EGP_8);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_9:	", CBM_DES64_D + DESC0_0_EGP_9);
	print_reg64("DESC1_0_EGP_9:	", CBM_DES64_D + DESC1_0_EGP_9);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_9:	", CBM_DES64_D + DESC0_1_EGP_9);
	print_reg64("DESC1_1_EGP_9:	", CBM_DES64_D + DESC1_1_EGP_9);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_10:	", CBM_DES64_D + DESC0_0_EGP_10);
	print_reg64("DESC1_0_EGP_10:	", CBM_DES64_D + DESC1_0_EGP_10);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_10:	", CBM_DES64_D + DESC0_1_EGP_10);
	print_reg64("DESC1_1_EGP_10:	", CBM_DES64_D + DESC1_1_EGP_10);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_11:	", CBM_DES64_D + DESC0_0_EGP_11);
	print_reg64("DESC1_0_EGP_11:	", CBM_DES64_D + DESC1_0_EGP_11);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_11:	", CBM_DES64_D + DESC0_1_EGP_11);
	print_reg64("DESC1_1_EGP_11:	", CBM_DES64_D + DESC1_1_EGP_11);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_12:	", CBM_DES64_D + DESC0_0_EGP_12);
	print_reg64("DESC1_0_EGP_12:	", CBM_DES64_D + DESC1_0_EGP_12);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_12:	", CBM_DES64_D + DESC0_1_EGP_12);
	print_reg64("DESC1_1_EGP_12:	", CBM_DES64_D + DESC1_1_EGP_12);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_13:	", CBM_DES64_D + DESC0_0_EGP_13);
	print_reg64("DESC1_0_EGP_13:	", CBM_DES64_D + DESC1_0_EGP_13);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_13:	", CBM_DES64_D + DESC0_1_EGP_13);
	print_reg64("DESC1_1_EGP_13:	", CBM_DES64_D + DESC1_1_EGP_13);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_14:	", CBM_DES64_D + DESC0_0_EGP_14);
	print_reg64("DESC1_0_EGP_14:	", CBM_DES64_D + DESC1_0_EGP_14);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_14:	", CBM_DES64_D + DESC0_1_EGP_14);
	print_reg64("DESC1_1_EGP_14:	", CBM_DES64_D + DESC1_1_EGP_14);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_15:	", CBM_DES64_D + DESC0_0_EGP_15);
	print_reg64("DESC1_0_EGP_15:	", CBM_DES64_D + DESC1_0_EGP_15);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_15:	", CBM_DES64_D + DESC0_1_EGP_15);
	print_reg64("DESC1_1_EGP_15:	", CBM_DES64_D + DESC1_1_EGP_15);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_16:	", CBM_DES64_D + DESC0_0_EGP_16);
	print_reg64("DESC1_0_EGP_16:	", CBM_DES64_D + DESC1_0_EGP_16);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_16:	", CBM_DES64_D + DESC0_1_EGP_16);
	print_reg64("DESC1_1_EGP_16:	", CBM_DES64_D + DESC1_1_EGP_16);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_17:	", CBM_DES64_D + DESC0_0_EGP_17);
	print_reg64("DESC1_0_EGP_17:	", CBM_DES64_D + DESC1_0_EGP_17);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_17:	", CBM_DES64_D + DESC0_1_EGP_17);
	print_reg64("DESC1_1_EGP_17:	", CBM_DES64_D + DESC1_1_EGP_17);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_18:	", CBM_DES64_D + DESC0_0_EGP_18);
	print_reg64("DESC1_0_EGP_18:	", CBM_DES64_D + DESC1_0_EGP_18);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_18:	", CBM_DES64_D + DESC0_1_EGP_18);
	print_reg64("DESC1_1_EGP_18:	", CBM_DES64_D + DESC1_1_EGP_18);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_19:	", CBM_DES64_D + DESC0_0_EGP_19);
	print_reg64("DESC1_0_EGP_19:	", CBM_DES64_D + DESC1_0_EGP_19);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_19:	", CBM_DES64_D + DESC0_1_EGP_19);
	print_reg64("DESC1_1_EGP_19:	", CBM_DES64_D + DESC1_1_EGP_19);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_20:	", CBM_DES64_D + DESC0_0_EGP_20);
	print_reg64("DESC1_0_EGP_20:	", CBM_DES64_D + DESC1_0_EGP_20);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_20:	", CBM_DES64_D + DESC0_1_EGP_20);
	print_reg64("DESC1_1_EGP_20:	", CBM_DES64_D + DESC1_1_EGP_20);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_21:	", CBM_DES64_D + DESC0_0_EGP_21);
	print_reg64("DESC1_0_EGP_21:	", CBM_DES64_D + DESC1_0_EGP_21);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_21:	", CBM_DES64_D + DESC0_1_EGP_21);
	print_reg64("DESC1_1_EGP_21:	", CBM_DES64_D + DESC1_1_EGP_21);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_0_EGP_22:	", CBM_DES64_D + DESC0_0_EGP_22);
	print_reg64("DESC1_0_EGP_22:	", CBM_DES64_D + DESC1_0_EGP_22);
	LOGF_KLOG_CONT("\n");
	print_reg64("DESC0_1_EGP_22:	", CBM_DES64_D + DESC0_1_EGP_22);
	print_reg64("DESC1_1_EGP_22:	", CBM_DES64_D + DESC1_1_EGP_22);
	LOGF_KLOG_CONT("\n");
}
