#include<linux/init.h>
#include<linux/module.h>
#include <linux/kernel.h>	/* printk */
#include <linux/types.h>	/* size_t */
#include <linux/version.h>
#include <linux/timer.h>
#include <linux/skbuff.h>
#include <linux/if_ether.h>
#include <linux/ethtool.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/if_ether.h>
#include <linux/clk.h>

#include <lantiq.h>
#include <lantiq_soc.h>
#include <net/lantiq_cbm.h>
#include <net/datapath_api.h>
#include <net/datapath_proc_api.h>
#include "datapath.h"
#include "datapath_pmac.h"
#include <net/lantiq_cbm_api.h>
#include <xway/switch-api/lantiq_gsw_api.h>
#include <xway/switch-api/lantiq_gsw_flow.h>
#ifdef CONFIG_LTQ_TMU
#include <net/drv_tmu_ll.h>
#endif
#include <linux/ltq_hwmcpy.h>
#if defined(CONFIG_LTQ_PPA_API) || defined(CONFIG_LTQ_PPA_API_MODULE)
#include <net/ppa_api.h>
#endif

int32_t (*tmu_hal_get_qos_mib)(struct net_device *, dp_subif_t *, int32_t , struct rtnl_link_stats64 *, uint32_t *, uint32_t) = NULL;
int32_t (*tmu_hal_clear_qos_mib)(struct net_device *, dp_subif_t *, int32_t *, uint32_t) = NULL;
int32_t (*tmu_hal_get_csum_ol_mib)(struct rtnl_link_stats64 *, uint32_t) = NULL;
int32_t (*tmu_hal_clear_csum_ol_mib)(uint32_t) = NULL;

#define WRAPAROUND32   0xFFFFFFFF
/*timer interval for mib wraparound handling:
Most mib counter is 32 bits, ie, maximu ix 0xFFFFFFFF
one pmac port maximum (cpu port) can support less than 3G, ie,
1488096 * 3 packets for 64 bytes case. so the time to have 1 wrapround is:
0xFFFFFFFF / (1488096 * 3) = 962 seconds
*/
#if 0
#define POLL_INTERVAL (2 * HZ)
#else
#define POLL_INTERVAL (962 * HZ)
#endif
#define LAN_EP_MAX      6    /*LAN interface's EP range: 1 ~ 6 */
#define WAN_EP          15   /*WAN Interface's EP value */
#define MAX_RMON_ITF    256  /*maximum 256 GSW RMON interface supported */

struct mibs_port {
	u64 rx_good_bytes;
	u64 rx_bad_bytes;
	u64 rx_good_pkts;
	u64 rx_drop_pkts;
	u64 rx_drop_pkts_pae; /*For eth0_x only, for all others, must keep it
	                        to zero in order to share same algo*/
	u64 rx_disc_pkts_redir ; /*for eth1 only*/
	u64 rx_fcs_err_pkts;
	u64 rx_undersize_good_pkts;
	u64 rx_oversize_good_pkts;
	u64 rx_undersize_err_pkts;
	u64 rx_oversize_err_pkts;
	u64 rx_align_err_pkts;
	u64 rx_filter_pkts;

	u64 tx_good_pkts;
	u64 tx_drop_pkts;
	u64 tx_drop_pkts_pae; /*For eth0_x only, for all others, must keep it
	                        to zero in order to share same algo*/
	u64 tx_acm_drop_pkts;
	u64 tx_acm_drop_pkts_pae; /*for eth0_x only */
	u64 tx_disc_pkts_redir; /*for eth1 only*/
	u64 tx_coll_pkts;
	u64 tx_coll_pkts_pae;  /*for eth0_x only */
	u64 tx_pkts_redir; /*for eth1 only*/

	/*tmu related*/
	u64 rx_tmu_drop_pkts;
	u64 tx_tmu_drop_pkts;
	u64 rx_tmu_csum_offload_pkts;
	u64 rx_tmu_csum_offload_bytes;

	/*driver related */
	u64 rx_drv_drop_pkts;
	u64 rx_drv_error_pkts;
	u64 tx_drv_drop_pkts;
	u64 tx_drv_error_pkts;
};

struct mib_vap {
	u64 rx_pkts;
	u64 rx_disc_pkts;
	u64 tx_pkts;
	u64 tx_disc_pkts;
};

typedef struct dp_tmu_mib {
	u32 rx_drop_pkts;
	u32 tx_drop_pkts;
	u32 csum_offload_pkts;
	u32 csum_offload_bytes;
} dp_tmu_mib_t;

struct port_mib {
	struct mibs_port curr;  /*tmp variable used for mib counter calculation*/
	struct mib_vap curr_vap[MAX_SUBIF_PER_PORT]; /*for future*/
};

struct mibs_low_lvl_port {
	GSW_RMON_Port_cnt_t l;
	GSW_RMON_Port_cnt_t r;
	GSW_RMON_Redirect_cnt_t redir;
	dp_drv_mib_t drv;
	dp_tmu_mib_t tmu;
};

struct mibs_low_lvl_vap {
	GSW_RMON_If_cnt_t m;
	/*dp_drv_mib_t drv;*/
	/*dp_tmu_mib_t tmu;*/
};

static unsigned int proc_mib_vap_start_id = 1;
static unsigned int proc_mib_vap_end_id = PMAC_MAX_NUM - 1;
static spinlock_t dp_mib_lock;
static unsigned long poll_interval = POLL_INTERVAL;

struct mibs_low_lvl_port last[PMAC_MAX_NUM]; /*save port based lower level last
                                        mib counter for wraparound checking */
struct mibs_low_lvl_vap last_vap[MAX_RMON_ITF]; /*save vap based lower level
                                    last mib counter for wraparound checking */
static struct port_mib aggregate_mib[PMAC_MAX_NUM]; /*Save all necessary aggregated basic MIB */

static struct timer_list  exp_timer; /*timer setting */
int dp_get_itf_mib(dp_subif_t *subif, void *priv, struct rtnl_link_stats64 *net_mib, u32 flag);

/*internal API: update local net mib counters periodically */
static int update_port_mib_lower_lvl(dp_subif_t *subif, u32 flag);
static int update_vap_mib_lower_lvl(dp_subif_t *subif, u32 flag);

/* ----- API implementation ------- */
static u64 wraparound(u64 curr, u64 last, u32 size)
{
#define WRAPAROUND_MAX_32 0xFFFFFFFF

	if ((size > 4) || /* for 8 bytes(64bit mib),no need to do wraparound*/
	    (curr >= last))
		return curr - last;

	return ((u64)WRAPAROUND_MAX_32) + (u64)curr - last;
}

static int port_mib_wraparound(u32 ep, struct mibs_low_lvl_port *curr,
			       struct mibs_low_lvl_port *last)
{
#define PORT_RMON_WRAP(c, l, x) wraparound(c->x, l->x, sizeof(l->x))
	GSW_RMON_Port_cnt_t *curr_tmp;
	GSW_RMON_Port_cnt_t *last_tmp;

	if (ep <= LAN_EP_MAX) {
		curr_tmp = &curr->l;
		last_tmp = &last->l;
	} else {
		curr_tmp = &curr->r;
		last_tmp = &last->r;
	}

	spin_lock(&dp_mib_lock);
	aggregate_mib[ep].curr.rx_good_bytes +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxGoodBytes);
	aggregate_mib[ep].curr.rx_bad_bytes +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxBadBytes);
	aggregate_mib[ep].curr.rx_good_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxGoodPkts);
	aggregate_mib[ep].curr.rx_drop_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxDroppedPkts);
	aggregate_mib[ep].curr.rx_fcs_err_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxFCSErrorPkts);
	aggregate_mib[ep].curr.rx_undersize_good_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxUnderSizeGoodPkts);
	aggregate_mib[ep].curr.rx_oversize_good_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxOversizeGoodPkts);
	aggregate_mib[ep].curr.rx_undersize_err_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxUnderSizeErrorPkts);
	aggregate_mib[ep].curr.rx_oversize_err_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxOversizeErrorPkts);
	aggregate_mib[ep].curr.rx_align_err_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxAlignErrorPkts);
	aggregate_mib[ep].curr.rx_filter_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nRxFilteredPkts);

	aggregate_mib[ep].curr.tx_good_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nTxGoodPkts);
	aggregate_mib[ep].curr.tx_drop_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nTxDroppedPkts);
	aggregate_mib[ep].curr.tx_acm_drop_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nTxAcmDroppedPkts);
	aggregate_mib[ep].curr.tx_coll_pkts +=
		PORT_RMON_WRAP(curr_tmp, last_tmp, nTxCollCount);

	/*TMu drop counters*/
	aggregate_mib[ep].curr.rx_tmu_drop_pkts += wraparound(curr->tmu.rx_drop_pkts,
			last->tmu.rx_drop_pkts,
			sizeof(last->tmu.rx_drop_pkts));
	aggregate_mib[ep].curr.tx_tmu_drop_pkts += wraparound(curr->tmu.tx_drop_pkts,
			last->tmu.tx_drop_pkts,
			sizeof(last->tmu.tx_drop_pkts));
	aggregate_mib[ep].curr.rx_tmu_csum_offload_pkts += wraparound(curr->tmu.csum_offload_pkts,
			last->tmu.csum_offload_pkts,
			sizeof(last->tmu.csum_offload_pkts));
	aggregate_mib[ep].curr.rx_tmu_csum_offload_bytes += wraparound(curr->tmu.csum_offload_bytes,
			last->tmu.csum_offload_bytes,
			sizeof(last->tmu.csum_offload_bytes));

	/*workaround for GSWIP-L: get drop cnt from its pae's mapped port */
	if (ep <= LAN_EP_MAX) {
		/*special handling for eth0_x.
				Don't save whole last as *last = *curr */
		curr_tmp = &curr->r;
		last_tmp = &last->r;
		aggregate_mib[ep].curr.rx_drop_pkts_pae +=
			PORT_RMON_WRAP(curr_tmp, last_tmp, nRxDroppedPkts);
		aggregate_mib[ep].curr.tx_drop_pkts_pae +=
			PORT_RMON_WRAP(curr_tmp, last_tmp, nTxDroppedPkts);
		aggregate_mib[ep].curr.tx_coll_pkts_pae +=
			PORT_RMON_WRAP(curr_tmp, last_tmp, nTxCollCount);
	} else if (ep == WAN_EP) { /*redirect mib*/
		aggregate_mib[ep].curr.rx_disc_pkts_redir += wraparound(
					curr->redir.nRxDiscPktsCount,
					last->redir.nRxDiscPktsCount,
					sizeof(last->redir.nRxDiscPktsCount)) ;
		aggregate_mib[ep].curr.tx_disc_pkts_redir += wraparound(
					curr->redir.nTxDiscPktsCount,
					last->redir.nTxDiscPktsCount,
					sizeof(last->redir.nTxDiscPktsCount)) ;
		aggregate_mib[ep].curr.tx_pkts_redir += wraparound(
				curr->redir.nTxPktsCount,
				last->redir.nTxPktsCount,
				sizeof(last->redir.nTxPktsCount));
	}

	/*save*/
	*last = *curr;
	spin_unlock(&dp_mib_lock);
	return 0;
}

static int itf_mib_wraparound(dp_subif_t *subif, struct mibs_low_lvl_vap *curr,
			      struct mibs_low_lvl_vap *last)
{
#define VAP_RMON_WRAP(c, l, x) wraparound(c->m.x, l->m.x, sizeof(l->m.x))
	int ep = subif->port_id;
	int vap = get_vap(subif->subif, 0);

	spin_lock(&dp_mib_lock);
	aggregate_mib[ep].curr_vap[vap].rx_pkts =
		VAP_RMON_WRAP(curr, last, nRxPktsCount);
	aggregate_mib[ep].curr_vap[vap].rx_disc_pkts =
		VAP_RMON_WRAP(curr, last, nRxDiscPktsCount);

	aggregate_mib[ep].curr_vap[vap].tx_pkts =
		VAP_RMON_WRAP(curr, last, nTxPktsCount);
	aggregate_mib[ep].curr_vap[vap].tx_disc_pkts =
		VAP_RMON_WRAP(curr, last, nTxDiscPktsCount);

	/*save*/
	*last = *curr;
	spin_unlock(&dp_mib_lock);
	return 0;
}

int get_gsw_port_rmon(u32 ep, char *gsw_drv_name,
		      GSW_RMON_Port_cnt_t *mib)
{
	GSW_API_HANDLE gsw_handle = 0;
	int ret;

	if (!mib) {
		PR_ERR("why mib pointer is %p\n", mib);
		return -1;
	}

	memset(mib, 0, sizeof(*mib));

	gsw_handle = gsw_api_kopen(gsw_drv_name);

	if (gsw_handle == 0) {
		PR_ERR("gsw_api_kopen %s FAILED !\n", gsw_drv_name);
		return -1;
	}

	mib->nPortId = ep;
	ret = gsw_api_kioctl(gsw_handle, GSW_RMON_PORT_GET, (u32)mib);

	if (ret) {
		PR_ERR("GSW_RMON_PORT_GET failed from %s for port %d\n",
		       gsw_drv_name, ep);
		return -1;
	}

	return 0;
}

static int get_gsw_redirect_rmon(u32 ep, char *gsw_drv_name,
				 GSW_RMON_Redirect_cnt_t *mib)
{
	GSW_API_HANDLE gsw_handle = 0;
	int ret;

	if (!mib) {
		PR_ERR("why mib pointer is %p\n", mib);
		return -1;
	}

	memset(mib, 0, sizeof(*mib));

	gsw_handle = gsw_api_kopen(gsw_drv_name);

	if (gsw_handle == 0) {
		PR_ERR("Open %s FAILED !\n", gsw_drv_name);
		return -1;
	}

	ret = gsw_api_kioctl(gsw_handle, GSW_RMON_REDIRECT_GET,
			     (u32)mib);

	if (ret) {
		PR_ERR("GSW_RMON_REDIRECT_GET failed from %s\n", gsw_drv_name);
		return -1;
	}

	return 0;
}

static int get_gsw_itf_rmon(u32 index, char *gsw_drv_name,
			    GSW_RMON_If_cnt_t *mib)
{
	GSW_API_HANDLE gsw_handle = 0;
	int ret;

	if (!mib) {
		PR_ERR("why mib pointer is %p\n", mib);
		return -1;
	}

	memset(mib, 0, sizeof(*mib));
	//return 0; //for testing only without calling gsw API

	gsw_handle = gsw_api_kopen(gsw_drv_name);

	if (gsw_handle == 0) {
		PR_ERR("Open %s FAILED !\n", gsw_drv_name);
		return -1;
	}

	mib->nIfId = index;
	ret = gsw_api_kioctl(gsw_handle, GSW_RMON_IF_GET, (u32)mib);

	if (ret) {
		PR_ERR("GSW_RMON_PORT_GET GSW_RMON_IF_GET from %s: index %d\n",
		       gsw_drv_name, index);
		return -1;
	}

	return 0;
}

int get_gsw_interface_base(int port_id)
{
	union gsw_var tmp;

	if (port_id <= 0 ||
	    port_id >= PMAC_MAX_NUM) {
		PR_ERR("Wrong subif\n");
		return -1;
	}

	/*get this ports itf base */
	tmp.port_cfg.nPortId = port_id;

	if (dp_gsw_kioctl(GSWIP_R_DEV_NAME, GSW_PORT_CFG_GET, (u32)&tmp)) {
		DP_DEBUG(DP_DBG_FLAG_MIB, "Why dp_gsw_kioctl return failjure\n");
		return -1;
	}

	if (!tmp.port_cfg.bIfCounters) {/*Interface not enabled yet */
		DP_DEBUG(DP_DBG_FLAG_MIB, "port[%d]'s interface is not enabled\n",
			 port_id);
		return -1;
	}

	return tmp.port_cfg.nIfCountStartIdx;
}

#if 0
int get_tmu_drop_cnt(dp_subif_t *subif, u32 *rx_drop_pkts, u32 *tx_drop_pkts)
{
	uint32_t num_tmu_port = 0, i, j;
	int q_num = 0;
	uint32_t  q_buff[32];
	uint32_t qdc[4];
	cbm_tmu_res_t *res = NULL;
	uint32_t flag = 0;
	struct pmac_port_info *port;

	*rx_drop_pkts = 0; /*always zero since TMU cannot support it */
	*tx_drop_pkts = 0;
	port = get_port_info(subif->port_id);

	if (cbm_dp_port_resources_get(&subif->port_id, &num_tmu_port, &res,
				      port ? port->alloc_flags : flag))
		return -1;

	for (i = 0; i < num_tmu_port; i++) { /*loop multiple tmu egress port*/
		DP_DEBUG(DP_DBG_FLAG_MIB,
			 "ep=%d tmu_port=%d queue=%d sid=%d num_tmu_port=%d\n",
			 subif->port_id,
			 res[i].tmu_port,
			 res[i].tmu_q,
			 res[i].tmu_sched,
			 num_tmu_port);
		/*get its queue list per tmu egress port */
#if 1
		tmu_queue_list(res[i].tmu_port, q_buff, ARRAY_SIZE(q_buff),
			       &q_num, NULL);
		DP_DEBUG(DP_DBG_FLAG_MIB, "tmu_queue_list done\n");
#endif

		if (!q_num)
			continue;

		tmu_qdct_read(q_buff[i], qdc);

		for (j = 0; j < q_num; j++) /*loop multiple queues */
			tx_drop_pkts += qdc[0] + qdc[1] + qdc[2] + qdc[3];
	}

	if (res)
		kfree(res);

	return 0;
}
#endif

static int update_port_mib_lower_lvl(dp_subif_t *subif, u32 flag)
{
	int ret;
	struct mibs_low_lvl_port curr;
	struct rtnl_link_stats64 tmp_stat;

	/*update struct pmac_port_info[subif->ep].net_mib */
	if (!subif || subif->port_id <= 0 || subif->port_id >= PMAC_MAX_NUM) {
		DP_DEBUG(DP_DBG_FLAG_MIB, "update_port_mib_lower_lvl wrong subif\n");
		return -1;
	}

	memset(&curr, 0, sizeof(curr));

	if (subif->port_id <= LAN_EP_MAX) {
		ret = get_gsw_port_rmon(subif->port_id, GSWIP_L_DEV_NAME, &curr.l);

		if (ret) /*workaroud otherwise wrongly wrapround */
			curr.l = last[subif->port_id].l;
	}

	/*still needs to get pae mib counter for eth0_x for drop counters*/
	ret = get_gsw_port_rmon(subif->port_id, GSWIP_R_DEV_NAME, &curr.r);

	if (ret) /*workaroud */
		curr.r = last[subif->port_id].r;

	if (!tmu_hal_get_qos_mib)
		ret = -1; /*For workaround later*/
	else {
		dp_subif_t tmp = *subif;
		tmp.subif = -1;
		ret = tmu_hal_get_qos_mib(NULL, &tmp, -1, &tmp_stat, NULL, 0);
	}

	if (ret) { /*workaournd */
		curr.tmu.rx_drop_pkts = last[subif->port_id].tmu.rx_drop_pkts;
		curr.tmu.tx_drop_pkts = last[subif->port_id].tmu.tx_drop_pkts;
	} else {
		curr.tmu.rx_drop_pkts = tmp_stat.rx_dropped;
		curr.tmu.tx_drop_pkts = tmp_stat.tx_dropped;
	}

	//get redirect mib for eth1 only
	if (subif->port_id == WAN_EP) {
		ret = get_gsw_redirect_rmon(subif->port_id,
					    GSWIP_R_DEV_NAME, &curr.redir);

		if (ret) /*workaroud */
			curr.redir = last[subif->port_id].redir;

		if (!tmu_hal_get_csum_ol_mib)
			ret = -1;
		else
			ret = tmu_hal_get_csum_ol_mib(&tmp_stat, 0);

		if (ret) {
			curr.tmu.csum_offload_bytes = last[subif->port_id].tmu.csum_offload_bytes;
			curr.tmu.csum_offload_pkts = last[subif->port_id].tmu.csum_offload_pkts;
		} else {
			curr.tmu.csum_offload_bytes = tmp_stat.rx_bytes;
			curr.tmu.csum_offload_pkts = tmp_stat.rx_packets;
		}
	}

	/*get drv mib*/
	ret = dp_get_drv_mib(subif, &curr.drv, flag);

	if (ret) /*workaroud */
		curr.drv = last[subif->port_id].drv;

	ret = port_mib_wraparound(subif->port_id, &curr, &last[subif->port_id]);
	return ret;
}

static void mib_wraparound_timer_poll(unsigned long data)
{
	int i, j;
	dp_subif_t subif;

	for (i = 1; i < PMAC_MAX_NUM; i++) { /*start from port 1, not 0 since 0 is no device registered*/
		subif.port_id = i;
		subif.subif = 0;
		update_port_mib_lower_lvl(&subif, 0);

		for (j = 0; j < MAX_SUBIF_PER_PORT; j++) {
			subif.subif = set_vap(j, 0);
			update_vap_mib_lower_lvl(&subif, DP_STAT_MIB_SUBITF_ONLY);
		}
	}

	exp_timer.expires = jiffies + poll_interval;
	add_timer(&exp_timer);
}

static int update_vap_mib_lower_lvl(dp_subif_t *subif, u32 flag)
{
	int ret;
	u8 vap;
	int itf_index;
	int itf_base;
	struct mibs_low_lvl_vap curr;

	/*update struct pmac_port_info[subif->ep].net_mib */
	if (!subif || subif->port_id <= 0 || subif->port_id >= PMAC_MAX_NUM)
		return -1;

	vap = get_vap(subif->subif, 0);
	itf_base = get_gsw_interface_base(subif->port_id);

	if (itf_base < 0)
		return -1;

	itf_index = itf_base + vap;

	if (itf_index >= MAX_RMON_ITF)
		return -1;

	memset(&curr, 0, sizeof(curr));
	ret = get_gsw_itf_rmon(itf_index, GSWIP_R_DEV_NAME, &curr.m);

	if (ret)
		return -1;

	/*get TMU mib*/

	/*get drv mib*/

	/*dp_get_drv_mib(subif, &curr.drv, flag);*/

	ret = itf_mib_wraparound(subif, &curr, &last_vap[itf_index]);

	return ret;
}

int dp_reset_mib(u32 flag)
{
	dp_clear_netif_stats(NULL, NULL, 0);
	/*reset TMU mib counters mainly for workaround only*/
	tmu_reset_mib_all(flag);
	return 0;
}

void proc_mib_timer_read(struct seq_file *s)
{
	seq_printf(s, "\nMib timer interval is %u sec\n",
		   (unsigned int)poll_interval / HZ);
}

ssize_t proc_mib_timer_write(struct file *file, const char *buf, size_t count,
			     loff_t *ppos)
{
	int len, num;
	char str[64];
	char *param_list[2];
#define MIN_POLL_TIME 2
	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;
	num = dp_split_buffer(str, param_list, ARRAY_SIZE(param_list));
	poll_interval = dp_atoi(param_list[0]);

	if (poll_interval < MIN_POLL_TIME)
		poll_interval = MIN_POLL_TIME;

	poll_interval *= HZ;
	mod_timer(&exp_timer, jiffies + poll_interval);
	PRINTK("new poll_interval=%u sec\n", (unsigned int)poll_interval / HZ);
	return count;
}

static unsigned int proc_mib_port_start_id = 1;
static unsigned int proc_mib_port_end_id = PMAC_MAX_NUM - 1;
int proc_mib_inside_dump(struct seq_file *s, int pos)
{
	int ret;
	dp_subif_t subif;
	struct rtnl_link_stats64 net_mib;

	seq_printf(s, "EP=%d\n", pos);
	/*l*/
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxGoodPkts", last[pos].l.nRxGoodPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxFCSErrorPkts", last[pos].l.nRxFCSErrorPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxUnderSizeGoodPkts", last[pos].l.nRxUnderSizeGoodPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxOversizeGoodPkts", last[pos].l.nRxOversizeGoodPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxUnderSizeErrorPkts", last[pos].l.nRxUnderSizeErrorPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxOversizeErrorPkts", last[pos].l.nRxOversizeErrorPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxAlignErrorPkts", last[pos].l.nRxAlignErrorPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxDroppedPkts", last[pos].l.nRxDroppedPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxFilteredPkts", last[pos].l.nRxFilteredPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nRxGoodPkts", last[pos].l.nTxGoodPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nTxDroppedPkts", last[pos].l.nTxDroppedPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.l.nTxCollCount", last[pos].l.nTxCollCount);
	/*r*/
	seq_printf(s, "  %-35s=%30u\n", "last.r.nRxGoodPkts", last[pos].r.nRxGoodPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.r.nRxFCSErrorPkts", last[pos].r.nRxFCSErrorPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.r.nRxUnderSizeGoodPkts", last[pos].r.nRxUnderSizeGoodPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.r.nRxOversizeGoodPkts", last[pos].r.nRxOversizeGoodPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.r.nRxUnderSizeErrorPkts", last[pos].r.nRxUnderSizeErrorPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.r.nRxOversizeErrorPkts", last[pos].r.nRxOversizeErrorPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.r.nRxAlignErrorPkts", last[pos].r.nRxAlignErrorPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.r.nRxDroppedPkts", last[pos].r.nRxDroppedPkts);
	seq_printf(s, "  %-35s=%30u\n", "last.r.nRxFilteredPkts", last[pos].r.nRxFilteredPkts);
	/*redirct*/
	seq_printf(s, "  %-35s=%30u\n", "last.redir.nRxPktsCount", last[pos].redir.nRxPktsCount);
	seq_printf(s, "  %-35s=%30u\n", "last.redir.nRxDiscPktsCount", last[pos].redir.nRxDiscPktsCount);
	seq_printf(s, "  %-35s=%30u\n", "last.redir.nTxPktsCount", last[pos].redir.nTxPktsCount);
	seq_printf(s, "  %-35s=%30u\n", "last.redir.nTxDiscPktsCount", last[pos].redir.nTxDiscPktsCount);

	/*redirct*/
	seq_printf(s, "  %-35s=%30u\n", "last.tmu.rx_drop_pkts", last[pos].tmu.rx_drop_pkts);
	seq_printf(s, "  %-35s=%30u\n", "last.tmu.tx_drop_pkts", last[pos].tmu.tx_drop_pkts);
	seq_printf(s, "  %-35s=%30u\n", "last.tmu.csum_offload_bytes", last[pos].tmu.csum_offload_bytes);
	seq_printf(s, "  %-35s=%30u\n", "last.tmu.csum_offload_pkts", last[pos].tmu.csum_offload_pkts);

	/*mib array*/
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_good_pkts", aggregate_mib[pos].curr.rx_good_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_drop_pkts_pae", aggregate_mib[pos].curr.rx_drop_pkts_pae);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_disc_pkts_redir", aggregate_mib[pos].curr.rx_disc_pkts_redir);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_drop_pkts", aggregate_mib[pos].curr.rx_drop_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_drop_pkts_pae", aggregate_mib[pos].curr.rx_drop_pkts_pae);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_disc_pkts_redir", aggregate_mib[pos].curr.rx_disc_pkts_redir);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_fcs_err_pkts", aggregate_mib[pos].curr.rx_fcs_err_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_undersize_good_pkts", aggregate_mib[pos].curr.rx_undersize_good_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_oversize_good_pkts", aggregate_mib[pos].curr.rx_oversize_good_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_undersize_err_pkts", aggregate_mib[pos].curr.rx_undersize_err_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_oversize_err_pkts", aggregate_mib[pos].curr.rx_oversize_err_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_align_err_pkts", aggregate_mib[pos].curr.rx_align_err_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_filter_pkts", aggregate_mib[pos].curr.rx_filter_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_good_pkts", aggregate_mib[pos].curr.tx_good_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_drop_pkts", aggregate_mib[pos].curr.tx_drop_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_drop_pkts_pae", aggregate_mib[pos].curr.tx_drop_pkts_pae);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_acm_drop_pkts", aggregate_mib[pos].curr.tx_acm_drop_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_acm_drop_pkts_pae", aggregate_mib[pos].curr.tx_acm_drop_pkts_pae);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_disc_pkts_redir", aggregate_mib[pos].curr.tx_disc_pkts_redir);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_coll_pkts", aggregate_mib[pos].curr.tx_coll_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_coll_pkts_pae", aggregate_mib[pos].curr.tx_coll_pkts_pae);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_tmu_drop_pkts", aggregate_mib[pos].curr.rx_tmu_drop_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_tmu_drop_pkts", aggregate_mib[pos].curr.tx_tmu_drop_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_tmu_csum_offload_bytes", aggregate_mib[pos].curr.rx_tmu_csum_offload_bytes);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.rx_tmu_csum_offload_pkts", aggregate_mib[pos].curr.rx_tmu_csum_offload_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_drv_drop_pkts", aggregate_mib[pos].curr.tx_drv_drop_pkts);
	seq_printf(s, "  %-35s=%30llu\n", "aggregate_mib.curr.tx_drv_error_pkts", aggregate_mib[pos].curr.tx_drv_error_pkts);
	subif.port_id = pos;
	subif.subif = 0;

	if (dp_get_itf_mib(&subif, NULL, &net_mib, 0)) {
		seq_printf(s, "dp_get_itf_mib failed\n");
		goto EXIT;
	}

	seq_printf(s, "  %-35s=%30llu\n", "aggregated rx_packets", net_mib.rx_packets);
	seq_printf(s, "  %-35s=%30llu\n", "aggregated rx_dropped", net_mib.rx_dropped);
	seq_printf(s, "  %-35s=%30llu\n", "aggregated tx_packets", net_mib.tx_packets);
	seq_printf(s, "  %-35s=%30llu\n", "aggregated tx_dropped", net_mib.tx_dropped);
	ret = seq_printf(s, "\n");

	if (ret) /*buffer over flow and don't increase pos */
		return pos;

EXIT:

	if ((pos >= PMAC_MAX_NUM - 1) || (pos >= proc_mib_port_end_id))
		return -1; /*loop finished*/

	pos++;
	return pos;
}

int proc_mib_inside_start(void)
{
	return proc_mib_port_start_id;
}

ssize_t proc_mib_inside_write(struct file *file, const char *buf, size_t count,
			      loff_t *ppos)
{
	int len;
	char str[64];
	char *s[2] = { 0 };
	unsigned int num[2] = { -1 };
	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;

	if (dp_split_buffer(str, s, 2) < 2)
		goto help;

	if (s[0] && strlen(s[0]))
		num[0] = dp_atoi(s[0]);

	if (s[1] && strlen(s[1]))
		num[1] = dp_atoi(s[1]);

	set_start_end_id(num[0], num[1],
			 PMAC_MAX_NUM - 1, PMAC_MAX_NUM - 1,
			 0, PMAC_MAX_NUM - 1, &proc_mib_port_start_id, &proc_mib_port_end_id);
	PRINTK("proc_mib_port_start_id=%u, proc_mib_port_end_id=%u\n",
	       proc_mib_port_start_id, proc_mib_port_end_id);
	return count;

help:
	PRINTK("ussage echo start_id end_id > /proc/dp/mib_inside\n");
	PRINTK("       then display the selected port info via cat /proc/dp/mib_inside\n");
	return count;
}

/*Note:
  if (flag & DP_STAT_MIB_SUBITF_ONLY), get sub-interface mib only
  otherwise, get port mib
*/
int dp_get_itf_mib(dp_subif_t *subif, void *priv, struct rtnl_link_stats64 *net_mib,
		   u32 flag)
{
	dp_subif_t tmp_subif;
	unsigned int port_id, vap;
	struct pmac_port_info *port;

	if (!subif ||
	    subif->port_id <= 0 ||
	    subif->port_id >= PMAC_MAX_NUM)
		return -1;

	port_id = subif->port_id;
	vap = get_vap(subif->subif, 0);
	port = get_port_info(port_id);
	

	if (!(flag & DP_STAT_MIB_SUBITF_ONLY)) { /*only port based mib */
		DP_DEBUG(DP_DBG_FLAG_MIB, "dp_get_itf_mib port %u mib flag=0x%x\n",
			 port_id, flag);

		tmp_subif.port_id = port_id;
		tmp_subif.subif = 0; /*for dp_get_drv_mib to get all mib of all VAPs*/

		if (update_port_mib_lower_lvl(&tmp_subif, 0)) {
			PR_ERR("update_port_mib_lower_lvl failed for port %d VAP=%d\n",
			       tmp_subif.port_id, tmp_subif.subif);
			return -1;
		}

		if (!net_mib) return 0;

		memset(net_mib, 0, sizeof(*net_mib));

		if (port_id == WAN_EP) {
			/*tx mib*/
			net_mib->tx_errors = aggregate_mib[port_id].curr.tx_drv_error_pkts +
					     aggregate_mib[port_id].curr.tx_coll_pkts;
			net_mib->tx_dropped = aggregate_mib[port_id].curr.tx_drop_pkts +
					      aggregate_mib[port_id].curr.tx_acm_drop_pkts +
					      aggregate_mib[port_id].curr.tx_disc_pkts_redir +
					      aggregate_mib[port_id].curr.tx_tmu_drop_pkts +
					      aggregate_mib[port_id].curr.tx_drv_drop_pkts;
			net_mib->tx_packets = aggregate_mib[port_id].curr.tx_good_pkts +
					      net_mib->tx_dropped + net_mib->tx_errors +
					      aggregate_mib[port_id].curr.tx_pkts_redir; /*sgh add it */
			net_mib->tx_bytes = aggregate_mib[port_id].curr.tx_good_pkts;

			/*rx mib*/
			net_mib->rx_dropped = aggregate_mib[port_id].curr.rx_drop_pkts +
					      aggregate_mib[port_id].curr.rx_filter_pkts +
					      aggregate_mib[port_id].curr.rx_tmu_drop_pkts +
					      aggregate_mib[port_id].curr.rx_drv_drop_pkts;
			net_mib->rx_errors = aggregate_mib[port_id].curr.rx_fcs_err_pkts +
					     aggregate_mib[port_id].curr.rx_undersize_good_pkts +
					     aggregate_mib[port_id].curr.rx_undersize_err_pkts +
					     aggregate_mib[port_id].curr.rx_oversize_good_pkts +
					     aggregate_mib[port_id].curr.rx_oversize_err_pkts +
					     /*aggregate_mib[port_id].curr.rx_align_err_pkts +
					     aggregate_mib[port_id].curr.rx_filter_pkts +*/ /*Not seen in V2.0 */
					     aggregate_mib[port_id].curr.rx_drv_error_pkts;
			net_mib->rx_packets = aggregate_mib[port_id].curr.rx_good_pkts +
					      net_mib->rx_dropped +
					      net_mib->rx_errors +
					      aggregate_mib[port_id].curr.rx_drv_drop_pkts +
					      aggregate_mib[port_id].curr.rx_tmu_drop_pkts;
			net_mib->rx_bytes = aggregate_mib[port_id].curr.rx_good_bytes +
					    aggregate_mib[port_id].curr.rx_bad_bytes;
		} else if ((port_id <= LAN_EP_MAX) && (port_id > 0)) {
			/*tx mib*/
			net_mib->tx_errors = aggregate_mib[port_id].curr.tx_drv_error_pkts +
					     aggregate_mib[port_id].curr.tx_coll_pkts +
					     aggregate_mib[port_id].curr.tx_coll_pkts_pae;
			net_mib->tx_dropped = aggregate_mib[port_id].curr.tx_drop_pkts +
					      aggregate_mib[port_id].curr.tx_acm_drop_pkts +
					      aggregate_mib[port_id].curr.tx_drop_pkts_pae +
					      aggregate_mib[port_id].curr.tx_acm_drop_pkts_pae +
					      aggregate_mib[port_id].curr.tx_tmu_drop_pkts +
					      aggregate_mib[port_id].curr.tx_drv_drop_pkts;
			net_mib->tx_packets = aggregate_mib[port_id].curr.tx_good_pkts +
					      net_mib->tx_dropped + net_mib->tx_errors;
			net_mib->tx_bytes = aggregate_mib[port_id].curr.tx_good_pkts;

			/*rx mib*/
			net_mib->rx_dropped = aggregate_mib[port_id].curr.rx_drop_pkts +
					      aggregate_mib[port_id].curr.rx_filter_pkts +
					      aggregate_mib[port_id].curr.rx_drop_pkts_pae +
					      aggregate_mib[port_id].curr.tx_tmu_drop_pkts +
					      aggregate_mib[port_id].curr.rx_drv_drop_pkts;
			net_mib->rx_errors = aggregate_mib[port_id].curr.rx_fcs_err_pkts +
					     aggregate_mib[port_id].curr.rx_undersize_good_pkts +
					     aggregate_mib[port_id].curr.rx_undersize_err_pkts +
					     aggregate_mib[port_id].curr.rx_oversize_good_pkts +
					     aggregate_mib[port_id].curr.rx_oversize_err_pkts +
					     /*aggregate_mib[port_id].curr.rx_align_err_pkts +
					     aggregate_mib[port_id].curr.rx_filter_pkts + */ /*not seen in V2.0 */
					     aggregate_mib[port_id].curr.rx_drv_error_pkts;
			net_mib->rx_packets = aggregate_mib[port_id].curr.rx_good_pkts +
					      net_mib->rx_dropped +
					      net_mib->rx_errors;
					      /*aggregate_mib[port_id].curr.rx_drv_drop_pkts +
					      aggregate_mib[port_id].curr.rx_drv_drop_pae  + 
					      aggregate_mib[port_id].curr.tx_tmu_drop_pkts  */ /*not seen in V2.0*/
			net_mib->rx_bytes = aggregate_mib[port_id].curr.rx_good_bytes +
					    aggregate_mib[port_id].curr.rx_bad_bytes;
		} else {
			if (port && !(port->alloc_flags & DP_F_FAST_DSL)) { /*WIFI/LTE Directpath */
				/*tx mib*/
				net_mib->tx_errors = aggregate_mib[port_id].curr.tx_drv_error_pkts +
						     aggregate_mib[port_id].curr.tx_coll_pkts;
				net_mib->tx_dropped = aggregate_mib[port_id].curr.tx_drop_pkts +
						      aggregate_mib[port_id].curr.tx_acm_drop_pkts +
						      aggregate_mib[port_id].curr.tx_tmu_drop_pkts +
						      aggregate_mib[port_id].curr.tx_drv_drop_pkts;
				net_mib->tx_packets = aggregate_mib[port_id].curr.tx_good_pkts +
						      net_mib->tx_dropped + net_mib->tx_errors;
				net_mib->tx_bytes = aggregate_mib[port_id].curr.tx_good_pkts;

				/*rx mib*/
				net_mib->rx_dropped = aggregate_mib[port_id].curr.rx_drop_pkts +
						      aggregate_mib[port_id].curr.rx_filter_pkts +
						      aggregate_mib[port_id].curr.rx_tmu_drop_pkts +
						      aggregate_mib[port_id].curr.rx_drv_drop_pkts;
				net_mib->rx_errors = aggregate_mib[port_id].curr.rx_fcs_err_pkts +
						     aggregate_mib[port_id].curr.rx_undersize_good_pkts +
						     aggregate_mib[port_id].curr.rx_undersize_err_pkts +
						     aggregate_mib[port_id].curr.rx_oversize_good_pkts +
						     aggregate_mib[port_id].curr.rx_oversize_err_pkts;
				net_mib->rx_packets = aggregate_mib[port_id].curr.rx_good_pkts +
						      net_mib->rx_dropped +
						      net_mib->rx_errors;
				net_mib->rx_bytes = aggregate_mib[port_id].curr.rx_good_bytes +
						    aggregate_mib[port_id].curr.rx_bad_bytes;
			}
			else { /* VRX318 ??? */
			        if(1) /*PTM */ {
					net_mib->tx_errors = aggregate_mib[port_id].curr.tx_drv_error_pkts +
							     aggregate_mib[port_id].curr.tx_coll_pkts;
					net_mib->tx_dropped = aggregate_mib[port_id].curr.tx_drop_pkts +
							      aggregate_mib[port_id].curr.tx_acm_drop_pkts +
							      aggregate_mib[port_id].curr.tx_tmu_drop_pkts +
							      aggregate_mib[port_id].curr.tx_drv_drop_pkts;
					net_mib->tx_packets = aggregate_mib[port_id].curr.tx_good_pkts +
							      net_mib->tx_dropped + net_mib->tx_errors;
					net_mib->tx_bytes = aggregate_mib[port_id].curr.tx_good_pkts;

					/*rx mib*/
					net_mib->rx_dropped = aggregate_mib[port_id].curr.rx_drop_pkts +
							      aggregate_mib[port_id].curr.rx_filter_pkts +
							      aggregate_mib[port_id].curr.rx_tmu_drop_pkts +
							      aggregate_mib[port_id].curr.rx_drv_drop_pkts;
					net_mib->rx_errors = aggregate_mib[port_id].curr.rx_fcs_err_pkts +
							     aggregate_mib[port_id].curr.rx_undersize_good_pkts +
							     aggregate_mib[port_id].curr.rx_undersize_err_pkts +
							     aggregate_mib[port_id].curr.rx_oversize_good_pkts +
							     aggregate_mib[port_id].curr.rx_oversize_err_pkts +
							     aggregate_mib[port_id].curr.rx_drv_error_pkts;
					net_mib->rx_packets = aggregate_mib[port_id].curr.rx_good_pkts +
							      net_mib->rx_dropped +
							      net_mib->rx_errors;
					net_mib->rx_bytes = aggregate_mib[port_id].curr.rx_good_bytes +
							    aggregate_mib[port_id].curr.rx_bad_bytes;
			        } else { /*ATM*/
			        	net_mib->tx_errors = aggregate_mib[port_id].curr.tx_drv_error_pkts;
					net_mib->tx_dropped = aggregate_mib[port_id].curr.tx_drv_drop_pkts;

					/*rx mib*/
					net_mib->rx_dropped = aggregate_mib[port_id].curr.rx_drop_pkts +
							      aggregate_mib[port_id].curr.rx_drv_drop_pkts;
					net_mib->rx_errors = aggregate_mib[port_id].curr.rx_drv_error_pkts;
					net_mib->rx_packets = net_mib->rx_dropped +
							      net_mib->rx_errors;
					net_mib->rx_bytes = 0;
			        }
			}
		}
	} else { /*get vap/subinterface based mib */
		DP_DEBUG(DP_DBG_FLAG_MIB, "dp_get_itf_mib vap %u.%u mib flag=0x%x\n",
			 subif->port_id, vap, flag);

		if (update_vap_mib_lower_lvl(subif, flag)) {
			PR_ERR("update_vap_mib_lower_lvl failed for port %d VAP=%d\n",
			       port_id, subif->subif);
			return -1;
		}

		if (!net_mib) return 0;

		memset(net_mib, 0, sizeof(*net_mib));
		net_mib->rx_packets = aggregate_mib[port_id].curr_vap[vap].rx_pkts;
		net_mib->rx_dropped = aggregate_mib[port_id].curr_vap[vap].rx_disc_pkts;
		net_mib->tx_packets = aggregate_mib[port_id].curr_vap[vap].tx_pkts;
		net_mib->tx_dropped = aggregate_mib[port_id].curr_vap[vap].tx_disc_pkts;
	}

	return 0;
}

/*Clear GSW Interface MIB  */
int clear_gsw_itf_mib(dp_subif_t *subif, u32 flag)
{
	int start, end;
	union gsw_var tmp;
	int i, itf_base;

	if (!subif ||
	    subif->port_id <= 0 ||
	    subif->port_id >= PMAC_MAX_NUM) {
		PR_ERR("Wrong subif\n");
		return -1;
	}

	if (subif->subif == -1) {
		start = 0;
		end = MAX_SUBIF_PER_PORT;
	} else {
		start = get_vap(subif->subif, 0);
		end = start + 1;
	}

	/*get this ports itf base */
	itf_base = get_gsw_interface_base(subif->port_id);

	if (itf_base < 0) {
		PR_ERR("Why dp_gsw_kioctl return failjure\n");
		return -1;
	}

	for (i = start; i < end; i++) {
		tmp.rmon.eRmonType = GSW_RMON_IF_TYPE;
		tmp.rmon.nRmonId = itf_base + i;

		if (tmp.rmon.nRmonId  >= MAX_RMON_ITF) {
			PR_ERR("Why Port[%d]'s interface ID %d so big\n",
			       subif->port_id, tmp.rmon.nRmonId);
			return -1;
		}

		dp_gsw_kioctl(GSWIP_R_DEV_NAME, GSW_RMON_CLEAR, (u32)&tmp);
	}

	return 0;
}

int dp_clear_netif_mib(dp_subif_t *subif, void *priv, u32 flag)
{
	unsigned int port_id, vap;
	union gsw_var tmp;

	if (!subif ||
	    subif->port_id < 0 ||
	    subif->port_id >= PMAC_MAX_NUM)
		return -1;

	port_id = subif->port_id;
	vap = get_vap(subif->subif, 0);

	if (port_id == 0) { /*special handling for port 0 */
		tmp.rmon.eRmonType = GSW_RMON_PORT_TYPE;
		tmp.rmon.nRmonId = port_id;
		dp_gsw_kioctl(GSWIP_L_DEV_NAME, GSW_RMON_CLEAR, (u32)&tmp);
		dp_gsw_kioctl(GSWIP_R_DEV_NAME, GSW_RMON_CLEAR, (u32)&tmp);
		return 0;
	}

	if ((flag & DP_STAT_MIB_SUBITF_ONLY)) { /*clear the specific interface mib counter */
		clear_gsw_itf_mib(subif, 0);
		dp_clear_mib(subif, 0);
		memset(&aggregate_mib[port_id].curr_vap[vap], 0, sizeof(aggregate_mib[port_id].curr_vap[vap]));
		//how about last[] ?
		return 0;
	}

	/*Clear port based RMON mib */
	DP_DEBUG(DP_DBG_FLAG_MIB, "dp_clear_netif_mib port %u mib flag=0x%x\n",
		 port_id, flag);

	memset(&aggregate_mib[port_id], 0, sizeof(aggregate_mib[port_id]));
	memset(&last[port_id], 0, sizeof(last[port_id]));

	if (port_id == WAN_EP) {
		/*reset GSWIP-R rmon counters*/
		tmp.rmon.eRmonType = GSW_RMON_PORT_TYPE;
		tmp.rmon.nRmonId = port_id;
		dp_gsw_kioctl(GSWIP_R_DEV_NAME, GSW_RMON_CLEAR, (u32)&tmp);

		/*reset GSWIP-R redirect counters*/
		tmp.rmon.eRmonType = GSW_RMON_REDIRECT_TYPE;
		tmp.rmon.nRmonId = 0;
		dp_gsw_kioctl(GSWIP_R_DEV_NAME, GSW_RMON_CLEAR, (u32)&tmp);

	} else if (port_id <= LAN_EP_MAX) {

		/*reset GSWIP-L/R rmon counters*/
		tmp.rmon.eRmonType = GSW_RMON_PORT_TYPE;
		tmp.rmon.nRmonId = port_id;
		dp_gsw_kioctl(GSWIP_L_DEV_NAME, GSW_RMON_CLEAR, (u32)&tmp);
		dp_gsw_kioctl(GSWIP_R_DEV_NAME, GSW_RMON_CLEAR, (u32)&tmp);
	} else { /*port 7 ~ 14 */
		tmp.rmon.eRmonType = GSW_RMON_PORT_TYPE;
		tmp.rmon.nRmonId = port_id;
		dp_gsw_kioctl(GSWIP_R_DEV_NAME, GSW_RMON_CLEAR, (u32)&tmp);
	}

	/*reset datapath's all vap mib */
	subif->subif = -1;
	dp_clear_mib(subif, 0);

	/*reset GSW interface mib */
	subif->subif = -1;
	clear_gsw_itf_mib(subif, 0);

	/*reset TMU mib counters via TMU HAL API*/
	return 0;
}

int dp_get_netif_stats(struct net_device *dev, dp_subif_t *subif_id,
		       struct rtnl_link_stats64 *stats, uint32_t flags)
{
	dp_subif_t subif;
	int res;

	if (!dev && !subif_id) {
		DP_DEBUG(DP_DBG_FLAG_MIB, "dp_get_netif_stats: dev/subif_id both NULL\n");
		return -1;
	}

	if (dev) {
		if ((res = dp_get_port_subitf_via_dev(dev,  &subif))) {
			DP_DEBUG(DP_DBG_FLAG_MIB, "%s is not registered yet to dp_get_netif_stats\n",
				 dev->name);
			return -1;
		}
	}

	DP_DEBUG(DP_DBG_FLAG_MIB, "dp_get_netif_stats for %s with port/vap %u.%u\n",
		 dev ? dev->name : "NULL",
		 subif.port_id, subif.subif);

	res = dp_get_itf_mib(&subif, NULL, stats, flags);
	return res;
}
EXPORT_SYMBOL(dp_get_netif_stats);

int32_t dp_clear_netif_stats(struct net_device *dev, dp_subif_t *subif_id, uint32_t flag)
{
	dp_subif_t subif;
	int res, i, j;
	int ep_start, ep_end;
	int vap_start, vap_end;

	if (dev) {
		if ((res = dp_get_port_subitf_via_dev(dev,  &subif))) {
			DP_DEBUG(DP_DBG_FLAG_MIB, "%s is not registered yet to dp_clear_netif_stats\n",
				 dev->name);
			return -1;
		}

		ep_start = subif.port_id;
		ep_end = ep_start + 1;
		vap_start = get_vap(subif.subif, 0);
		vap_end = vap_start + 1;
	} else if (subif_id) {
		ep_start = subif_id->port_id;
		ep_end = MAX_SUBIF_PER_PORT;

		if (subif_id->subif == -1) {
			vap_start = 0;
			vap_end = vap_start + 1;
		} else {
			vap_start = get_vap(subif_id->subif, 0);
			vap_end = vap_start + 1;
		}
	} else {
		ep_start = 0;
		ep_end = PMAC_MAX_NUM;
		vap_start = 0;
		vap_end = MAX_SUBIF_PER_PORT;
	}

	DP_DEBUG(DP_DBG_FLAG_MIB, "dp_clear_netif_stats for %s with port:%d-%d vap:%d-%d\n",
		 dev ? dev->name : "NULL",
		 ep_start, ep_end,
		 vap_start, vap_end);

	for (i = ep_start; i < ep_end; i++) {
		for (j = vap_start; j < vap_end; j++) {
			subif.port_id = i;
			subif.subif = set_vap(j, 0);
			res = dp_clear_netif_mib(&subif, NULL, flag);
		}
	}

	return res;
}
EXPORT_SYMBOL(dp_clear_netif_stats);

int proc_mib_port_dump(struct seq_file *s, int pos)
{
	int i = 0;
	dp_subif_t subif;
	struct rtnl_link_stats64 stats_mib;

	if (pos == 1) {
		memset(&subif, 0, sizeof(dp_subif_t));
		seq_printf(s, "%-12s %20s %20s %20s %20s\n", "PORT", "Tx_BYTES", "Tx_PKTS", "Tx_DROP_PKTS", "Tx_ERR_PKTS");
		seq_printf(s, "%-12s %20s %20s %20s %20s\n", "PORT", "Rx_BYTES", "Rx_PKTS", "Rx_DROP_PKTS", "Rx_ERR_PKTS");
		seq_printf(s, "-------------------------------------------------------------------------------------------------------\n");
	}

	i = pos;
	subif.port_id = i;

	if (dp_get_itf_mib(&subif, NULL, &stats_mib, 0) == 0) {
		seq_printf(s, "%-12d %20llu %20llu %20llu %20llu\n",
			   subif.port_id, stats_mib.tx_bytes, stats_mib.tx_packets, stats_mib.tx_dropped, stats_mib.tx_errors);
		seq_printf(s, "             %20llu %20llu %20llu %20llu\n",
			   stats_mib.rx_bytes, stats_mib.rx_packets, stats_mib.rx_dropped, stats_mib.rx_errors);
		seq_printf(s, "\n");
	}

	pos++;

	if (pos > (PMAC_MAX_NUM - 1))
		pos = -1;	/*end of the loop */

	return pos;
}

int proc_mib_port_start(void)
{
	return 1;
}

int proc_mib_vap_dump(struct seq_file *s, int pos)
{
	int j = 0;
	int ret = 0, f_newline = 0;
	dp_subif_t subif;
	struct rtnl_link_stats64 stats_mib;
	int itf_base;
	struct pmac_port_info *port;

	if ((pos > (PMAC_MAX_NUM - 1)) || (pos > proc_mib_vap_end_id)) {
		pos = -1;	/*end of the loop */
		return pos;
	}

	if (pos == proc_mib_vap_start_id) {
		memset(&subif, 0, sizeof(dp_subif_t));
		memset(&stats_mib, 0, sizeof(stats_mib));
		seq_printf(s, "%5s/%3s %5s %22s %22s %22s %22s\n", "Port", "VAP", "IfID", "Tx_PKTS", "Rx_PKTS", "Rx_DROP_PKTS", "Tx_DROP_PKTS\n");
	}

	port = get_port_info(pos);

	if (!port) //not allocated yet
		goto EXIT;

	for (j = 0 ; j <= (MAX_SUBIF_PER_PORT - 1); j++) {
		subif.port_id = pos;
		subif.subif = set_vap(j, 0);
		itf_base = get_gsw_interface_base(pos);

		if (itf_base < 0) //no GSW itf assigned
			continue;

		if (!port->subif_info[j].flags) //no registered yet
			continue;

		if (dp_get_itf_mib(&subif, NULL, &stats_mib, DP_STAT_MIB_SUBITF_ONLY) == 0) {
			seq_printf(s, "%5d/%03d %5d %22llu %22llu %22llu %22llu\n",
				   subif.port_id,
				   j,
				   itf_base + j,
				   stats_mib.rx_packets,
				   stats_mib.tx_packets,
				   stats_mib.rx_dropped,
				   stats_mib.tx_dropped);
			seq_printf(s, "\n");
			f_newline = 1;
		}
	}

	if (f_newline)
		ret = seq_printf(s, "\n");

	if (ret) /*buffer over flow and don't increase pos */
		return pos;

EXIT:
	pos++;
	return pos;
}

int proc_mib_vap_start(void)
{
	return proc_mib_vap_start_id;
}

ssize_t proc_mib_vap_write(struct file *file, const char *buf, size_t count,
			   loff_t *ppos)
{
	int len;
	char str[64];
	char *param_list[2] = { 0 };
	unsigned int num[2] = { -1 };
	len = (sizeof(str) > count) ? count : sizeof(str) - 1;
	len -= copy_from_user(str, buf, len);
	str[len] = 0;

	if (dp_split_buffer(str, param_list, ARRAY_SIZE(param_list)) < 2)
		goto help;

	if (param_list[0] && strlen(param_list[0]))
		num[0] = dp_atoi(param_list[0]);

	if (param_list[1] && strlen(param_list[1]))
		num[1] = dp_atoi(param_list[1]);

	set_start_end_id(num[0], num[1],
			 MAX_SUBIF_PER_PORT - 1, MAX_SUBIF_PER_PORT - 1,
			 0, -1, &proc_mib_vap_start_id, &proc_mib_vap_end_id);
	PRINTK("proc_mib_vap_start_id=%d, proc_mib_vap_end_id=%d\n",
	       proc_mib_vap_start_id, proc_mib_vap_end_id);
	return count;

help:
	PRINTK("usage: echo start_id end_id > /proc/dp/mib_vap\n");
	return count;
}

int dp_mib_init(void)
{
	spin_lock_init(&dp_mib_lock);
	memset(&aggregate_mib, 0, sizeof(aggregate_mib));
	memset(&last, 0, sizeof(last));
	memset(&last_vap, 0, sizeof(last_vap));

	init_timer_on_stack(&exp_timer);
	exp_timer.expires = jiffies + poll_interval;
	exp_timer.data = 0;
	exp_timer.function = mib_wraparound_timer_poll;
#if 1
	add_timer(&exp_timer);
	printk("dp_mib_init done\n");
#endif
	return 0;
}

void dp_mib_exit(void)
{
	del_timer(&exp_timer);
}

#if 0
Mode switching:
Bytes / Packets
GSW interface assignment:

DP and ethernet much early than PPA to initialize the based ? How to do ?

	ifInOctets : All Bytes Rx(64 - bit).
	ifInTotalPkts : GRX350 will target to provide total packets Rx Count.
	ifInDiscards : Rx Packet Drops
	ifInErrors : Rx Packet Errors

	ifOutOctets : Good Bytes Tx(64 - bit)
	ifOutTotalPkts : GRX350 will target to provide total packets Tx Count.
	ifOutDiscards : Tx Packet Drops
	ifOutErrors : Tx Packet Errors

	int32_t dp_get_netif_stats(struct net_device *if, dp_subif_t *subif_id, struct rtnl_link_stats64 *if_stats, uint32_t flag)
	int32_t dp_clear_netif_stats(struct net_device *if, dp_subif_t *subif_id, uint32_t flag)

	CBM Dequeue to bytesifInOctets : All Bytes Rx(64 - bit).
	ifInTotalPkts : GRX350 will target to provide total packets Rx Count.
	ifInDiscards : Rx Packet Drops
	ifInErrors : Rx Packet Errors

	ifOutOctets : Good Bytes Tx(64 - bit)
	ifOutTotalPkts : GRX350 will target to provide total packets Tx Count.
	ifOutDiscards : Tx Packet Drops
	ifOutErrors : Tx Packet Errors

	int32_t dp_get_netif_stats(struct net_device *if, dp_subif_t *subif_id, struct rtnl_link_stats64 *if_stats, uint32_t flag)
	int32_t dp_clear_netif_stats(struct net_device *if, dp_subif_t *subif_id, uint32_t flag)

	CBM Dequeue to bytes ?

	GSW Interface assignment

#endif
