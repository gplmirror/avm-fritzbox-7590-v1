/*
 * Copyright (c) 2015, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * author: mbahr@avm.de
 *
 *		Low-level functions for GPIO's by AVM 
 */		
#include <linux/err.h>
#include <linux/irqdomain.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/pinctrl/machine.h>
#include <linux/pinctrl/pinctrl.h>
#include <linux/pinctrl/pinmux.h>
#include <linux/pinctrl/pinconf.h>
#include <linux/pinctrl/pinconf-generic.h>
#include <linux/slab.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/irqchip/chained_irq.h>
#include <linux/spinlock.h>
#if defined(CONFIG_AVM_ENHANCED) && (CONFIG_GPIO_XRX500)
#include <lantiq_soc.h>
#include <asm/mach_avm.h>
#include <linux/simple_proc.h>
#include <linux/proc_fs.h>

#define PORTS			2
#define PINS			32	
#define PORT(x)                 (x / PINS)
#define PORT_PIN(x)             (x % PINS)

#define DBG_ERR(args...) printk(KERN_ERR args)
#define DBG_WRN(args...) printk(KERN_WARNING args)
/*--- #define DBG_TRC(args...) printk(KERN_INFO args) ---*/
#define DBG_TRC(args...)

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _ltq_avm_gpio_priv {
    int portmask;
	void __iomem *membase[PORTS];
    int sso_mask;
} ltq_avm_gpio;

/*--------------------------------------------------------------------------------*\
 * bank1: offset 0x0, bank2 offset 0x100
\*--------------------------------------------------------------------------------*/
struct _ltq_gpio_reg_x {
    volatile unsigned int out;      /*--- 0x00 ---*/
    volatile unsigned int in;       /*--- 0x04 ---*/
    volatile unsigned int dir;      /*--- 0x08 ---*/
    unsigned int reserved1[3];      /*--- 0x0c, 0x10, 0x14    ---*/
    volatile unsigned int exintcr0; /*--- 0x18 external interrupt control-register X0 ---*/
    volatile unsigned int exintcr1; /*--- 0x1c external interrupt control-register X1 ---*/
    volatile unsigned int irncr;    /*--- 0x20 interrupt node capture control ---*/
    volatile unsigned int irnicr;   /*--- 0x24 IRN interrupt control register ---*/
    volatile unsigned int irnen;    /*--- 0x28 IRN interrupt enable register ---*/
    volatile unsigned int irncfg;   /*--- 0x2c IRN interrupt configuration register ---*/
    volatile unsigned int irnenset; /*--- 0x30 interrupt enable set register ---*/
    volatile unsigned int irnenclr; /*--- 0x34 interrupt enable clr register ---*/
    unsigned int reserved2[2];      /*--- 0x38, 0x3c ---*/
    volatile unsigned int outset;   /*--- 0x40 output set register ---*/
    volatile unsigned int outclr;   /*--- 0x44 output clr register ---*/
    volatile unsigned int dirset;   /*--- 0x48 dirset ---*/
    volatile unsigned int dirclr;   /*--- 0x4c dirclr ---*/
};

static int grx_avm_gpio_sso_register(unsigned int gpio_pin);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int grx_avm_gpio_init(int port, void __iomem *iobase){
    struct _ltq_avm_gpio_priv *pltq_gpio_priv = &ltq_avm_gpio;
    DBG_TRC("%s: port=%d\n", __func__, port);
    if(port >= PORTS) {
        DBG_ERR("%s: failed\n", __func__);
        return -EINVAL;
    }
    DBG_TRC("%s: membase[%d]=%p\n", __func__, port, iobase);
    pltq_gpio_priv->membase[port] = iobase;
    mb();
    pltq_gpio_priv->portmask |= (0x1 << port);

    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int grx_avm_gpio_out_bit(unsigned int gpio_pin, int val){
	struct _ltq_avm_gpio_priv *pltq_gpio_priv = &ltq_avm_gpio;
    int pin, port;
	unsigned int offset;

    if((gpio_pin >= 240) && (gpio_pin < ARCH_NR_GPIOS)) { /* TODO: Start/Anzahl der SSO-LEDs aus dts bzw. dynamisch beziehen */
        /*--- Shift Register: ---*/
        grx_avm_gpio_sso_register(gpio_pin);
        __gpio_set_value(gpio_pin, val);
       return 0;
    }
    DBG_TRC("%s: gpio=%d val=%x\n", __func__, gpio_pin, val);
    port = PORT(gpio_pin);
	if((port >= PORTS) || ((pltq_gpio_priv->portmask & (1 << port)) == 0)) {
        DBG_ERR("%s: gpio_pin=%d - %s\n", __func__, gpio_pin, (port >= PORTS) ? "invalid port" : "uninitialized");
		return -EINVAL;
	}
    pin    = PORT_PIN(gpio_pin);
    if(val) {
        offset = offsetof(struct _ltq_gpio_reg_x, outset);
    } else {
        offset = offsetof(struct _ltq_gpio_reg_x, outclr);
    }
    ltq_w32(1 << pin, (unsigned char *)pltq_gpio_priv->membase[port] + offset);
    DBG_TRC("%s: gpio=%d val=%x pin=%d offset=%x, port=%d\n", __func__, gpio_pin, val, pin, offset, port);
    return 0;
}
EXPORT_SYMBOL(grx_avm_gpio_out_bit);
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int grx_avm_gpio_in_bit(unsigned int gpio_pin){
	struct _ltq_avm_gpio_priv *pltq_gpio_priv = &ltq_avm_gpio;
    int pin, port;
	unsigned int offset, val;

    port = PORT(gpio_pin);
	if((port >= PORTS) || ((pltq_gpio_priv->portmask & (1 << port)) == 0)) {
        DBG_ERR("%s: gpio_pin=%d ERROR - %s\n", __func__, gpio_pin, (port >= PORTS) ? "invalid port" : "device uninitialized");
		return -EINVAL;
	}
    pin    = PORT_PIN(gpio_pin);
    offset = offsetof(struct _ltq_gpio_reg_x, in);
    val = (ltq_r32((unsigned char *)pltq_gpio_priv->membase[port] + offset) >> pin) & 0x1;

    DBG_TRC("%s: gpio=%d val=%x pin=%d offset=%x, port=%d\n", __func__, gpio_pin, val, pin, offset, port);
    return val;
}
EXPORT_SYMBOL(grx_avm_gpio_in_bit);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int grx_avm_gpio_ctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode, enum _hw_gpio_direction pin_dir){
	struct _ltq_avm_gpio_priv *pltq_gpio_priv = &ltq_avm_gpio;
    int pin, port;
	unsigned int offset;

    if((gpio_pin >= 240) && (gpio_pin < ARCH_NR_GPIOS)) { /* TODO: Start/Anzahl der SSO-LEDs aus dts bzw. dynamisch beziehen */       
       return 0; /*--- Shift Register: ---*/
    }

    port = PORT(gpio_pin);
	if((port >= PORTS) || ((pltq_gpio_priv->portmask & (1 << port)) == 0)) {
        DBG_ERR("%s: gpio_pin=%d ERROR - %s\n", __func__, gpio_pin, (port >= PORTS) ? "invalid port" : "device uninitialized");
		return -EINVAL;
	}
    pin    = PORT_PIN(gpio_pin);
    if(pin_dir == GPIO_OUTPUT_PIN) {
        offset = offsetof(struct _ltq_gpio_reg_x, dirset);
        ltq_w32(1 << pin, (unsigned char *)pltq_gpio_priv->membase[port] + offset);
        DBG_TRC("%s: dirset gpio=%d dir_out pin=%d offset=%x, port=%d\n", __func__, gpio_pin, pin, offset, port);
    } else if(pin_dir == GPIO_INPUT_PIN) {
        offset = offsetof(struct _ltq_gpio_reg_x, dirclr);
        ltq_w32(1 << pin, (unsigned char *)pltq_gpio_priv->membase[port] + offset);
        DBG_TRC("%s: dirset gpio=%d dir_in pin=%d offset=%x, port=%d\n", __func__, gpio_pin, pin, offset, port);
    }
    if((pin_mode >= GPIO_PIN) && (pin_mode <= FUNCTION_PINMUX3)) {
        return grx_avm_pinctrl(gpio_pin, pin_mode);
    }
    return 0;
}
EXPORT_SYMBOL(grx_avm_gpio_ctrl);
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
enum _hw_gpio_direction grx_avm_gpio_get_dir(unsigned int gpio_pin){
	struct _ltq_avm_gpio_priv *pltq_gpio_priv = &ltq_avm_gpio;
    int pin, port, dir;
    port = PORT(gpio_pin);
	if((port >= PORTS) || ((pltq_gpio_priv->portmask & (1 << port)) == 0)) {
        DBG_ERR("%s: gpio_pin=%d ERROR - %s\n", __func__, gpio_pin, (port >= PORTS) ? "invalid port" : "device uninitialized");
		return -EINVAL;
	}
    pin    = PORT_PIN(gpio_pin);
    dir = ltq_r32((unsigned char *)pltq_gpio_priv->membase[port] + offsetof(struct _ltq_gpio_reg_x, dir));
    return ((dir >> pin) & 0x1) ? GPIO_OUTPUT_PIN :  GPIO_INPUT_PIN;
}
/*--------------------------------------------------------------------------------*\
 * piglet-Treiber benoetigt exklusiven Zugriff um TDM-Clocks zu messen
 * (Standardfunktionen zu langsam)
\*--------------------------------------------------------------------------------*/
unsigned char *grx_avm_gpio_get_membase_and_bit(unsigned int gpio_pin, unsigned int *bit){
	struct _ltq_avm_gpio_priv *pltq_gpio_priv = &ltq_avm_gpio;
    int port;

    port = PORT(gpio_pin);
	if((port >= PORTS) || ((pltq_gpio_priv->portmask & (1 << port)) == 0)) {
        DBG_ERR("%s: gpio_pin=%d ERROR - %s\n", __func__, gpio_pin, (port >= PORTS) ? "invalid port" : "device uninitialized");
		return NULL;
	}
	if(bit) *bit = PORT_PIN(gpio_pin);
	return (unsigned char *)pltq_gpio_priv->membase[port] + offsetof(struct _ltq_gpio_reg_x, in);
}
EXPORT_SYMBOL(grx_avm_gpio_get_membase_and_bit);

static struct proc_dir_entry *gpioprocdir;

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void grx_gpio_list(struct seq_file *seq, void *priv){
    int gpio_pin, port, pin;
	struct _ltq_avm_gpio_priv *pltq_gpio_priv = (struct _ltq_avm_gpio_priv *)priv;
    unsigned int dir, val;

    for(gpio_pin = 0; gpio_pin < 64; gpio_pin++) {

        port = PORT(gpio_pin);
        if((port >= PORTS) || ((pltq_gpio_priv->portmask & (1 << port)) == 0)) {
            return;
        }
        if(grx_avm_pinctrl_show_gpio(seq, gpio_pin)) {
            continue;
        }
        pin    = PORT_PIN(gpio_pin);
        dir = ltq_r32((unsigned char *)pltq_gpio_priv->membase[port] + offsetof(struct _ltq_gpio_reg_x, dir));
        if((dir >> pin) & 0x1) {
            val = ltq_r32((unsigned char *)pltq_gpio_priv->membase[port] + offsetof(struct _ltq_gpio_reg_x, out));
        } else {
            val = ltq_r32((unsigned char *)pltq_gpio_priv->membase[port] + offsetof(struct _ltq_gpio_reg_x, in));
        }
        seq_printf(seq,  " %s %x\n", ((dir >> pin) & 1) ? "OUT" : "IN ", (val >> pin) & 0x1);  
    }
}
/*--------------------------------------------------------------------------------*\
 * ret: negval -> not found/no range/no match 
\*--------------------------------------------------------------------------------*/
static int generic_gpio_param_parse(char *string, char *match, int maxval, char *matchstrg1, char *matchstrg2) {
    char *p = string;
    int ret = -1;
    if((p = strstr(string, match))) {
       p += strlen(match);
       while(*p == ' ' || *p == '\t') p++;
       if(matchstrg1 && strncmp(p, matchstrg1, strlen(matchstrg1)) == 0) {
           ret = 0;
       } else if(matchstrg2 && strncmp(p, matchstrg2, strlen(matchstrg2)) == 0) {
           ret = 1;
       } else if(*p) {
           sscanf(p, "%d", &ret);
           if(ret > maxval) {
               ret = -1;
           }
       }
    }
    return ret;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int grx_gpio_set(char *string, void *priv __maybe_unused) {
    int gpio, dir, set, pd, pu, od, mux, drive, slew;

    gpio  = generic_gpio_param_parse(string, "gpio=", 63, NULL, NULL);
    dir   = generic_gpio_param_parse(string, "dir=", 0, "in", "out");
    set   = generic_gpio_param_parse(string, "set=", 1, NULL, NULL);
    pd    = generic_gpio_param_parse(string, "pulldown=", 1, NULL, NULL);
    pu    = generic_gpio_param_parse(string, "pullup=", 1, NULL, NULL);
    od    = generic_gpio_param_parse(string, "od=", 1, NULL, NULL);
    drive = generic_gpio_param_parse(string, "drive=", 3, NULL, NULL);
    slew  = generic_gpio_param_parse(string, "slew=", 3, NULL, NULL);
    mux   = generic_gpio_param_parse(string, "mux=", 3, NULL, NULL);
    if((gpio < 0) || (strstr(string, "help"))) {
        printk(KERN_ERR "use: gpio=<val> dir=<in|out> set=<0|1> mux=<0..3> pullup=<0|1> pulldown=<0|1> od=<0|1> drive=<0..3> slew=<0|1>\n");
        return 0;
    }
    if(dir >= 0 || mux >= 0) {
        grx_avm_gpio_ctrl(gpio, (enum _hw_gpio_function) mux, (enum _hw_gpio_direction) dir);
    }
    if(set >= 0) {
        grx_avm_gpio_out_bit(gpio, set);
    }
    if(pd >= 0) {
        grx_avm_pinctrl_config(gpio, PINCONF_PARAM_PULLDOWN, pd);
    }
    if(pu >= 0) {
        grx_avm_pinctrl_config(gpio, PINCONF_PARAM_PULLUP, pu);
    }
    if(od >= 0) {
        grx_avm_pinctrl_config(gpio, PINCONF_PARAM_OPEN_DRAIN, od);
    }
    if(slew >= 0) {
        grx_avm_pinctrl_config(gpio, PINCONF_PARAM_SLEW_RATE, slew);
    }
    if(drive >= 0) {
        grx_avm_pinctrl_config(gpio, PINCONF_PARAM_DRIVE_CURRENT, drive);
    }
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init avm_gpioproc_init(void) {
#define PROC_GPIODIR   "avm/gpio"
	gpioprocdir = proc_mkdir(PROC_GPIODIR, NULL);
	if(gpioprocdir == NULL) {
        return 0;
	}
	add_simple_proc_file( "avm/gpio/list", NULL, grx_gpio_list, &ltq_avm_gpio);
	add_simple_proc_file( "avm/gpio/set", grx_gpio_set, NULL, NULL);
    return 0;
}
late_initcall(avm_gpioproc_init);
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) && (CONFIG_GPIO_XRX500) ---*/

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int grx_avm_gpio_sso_register(unsigned int gpio_pin) {
    int ret;
    struct _ltq_avm_gpio_priv *pltq_gpio_priv = &ltq_avm_gpio;

    if(pltq_gpio_priv->sso_mask & (1 << (gpio_pin & 0x1F)) ) {
        return 0;
    }
    ret = gpio_request(gpio_pin, "sso-xrx500");
    if(ret) {
        DBG_WRN("Request SSO GPIO %u returned %d\n", gpio_pin, ret);
    } else {
        pltq_gpio_priv->sso_mask |= 1 << (gpio_pin & 0x1F);
    }
    return ret;
}
