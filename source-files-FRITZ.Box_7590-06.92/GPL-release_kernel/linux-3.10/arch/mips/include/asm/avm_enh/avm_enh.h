#ifndef __avm_enh_h__
#define __avm_enh_h__

#include <asm/avm_enh/bugtable.h>
#include <asm/avm_enh/avm_watch.h>

/*--------------------------------------------------------------------------------*\
 * bthelper.c
\*--------------------------------------------------------------------------------*/
bool arch_trigger_all_cpu_backtrace(void);
char *get_user_symbol(char *txt, unsigned int maxttxlen, unsigned long gp, unsigned long addr);
void show_code_position_by_epc(char *prefix, struct pt_regs *regs);
int show_unaligned_position(struct pt_regs *regs);
#define arch_trigger_all_cpu_backtrace arch_trigger_all_cpu_backtrace

/*--------------------------------------------------------------------------------*\
 * kseg0_module.c
\*--------------------------------------------------------------------------------*/
enum _module_alloc_type_ {
    module_alloc_type_init,
    module_alloc_type_core,
    module_alloc_type_page,
    module_alloc_type_unknown
};
struct resource;
void __init module_alloc_bootmem_init(struct resource **res);
void *module_alloc(unsigned long size, char *name, enum _module_alloc_type_ type);
unsigned long module_alloc_size_list_alloc(unsigned long size, char *name, enum _module_alloc_type_ type);
int module_alloc_size_list_free(unsigned long addr);
char *module_alloc_find_module_name(char *buff, char *end, unsigned long addr);
int is_kernel_module_addr(unsigned long addr);

/**--------------------------------------------------------------------------------**\
 * kernel/time/tick-sched.c:
 * jiffies-correction in nmi-handler
\**--------------------------------------------------------------------------------**/
extern void avm_tick_jiffies_update(void);
#endif/*--- #ifndef __avm_enh_h__ ---*/
