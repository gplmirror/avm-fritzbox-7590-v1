#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/skbuff.h>

void kfree_skb(struct sk_buff *skb)
{
    pr_err("ERROR kfree_skb should not be called\n");
}
