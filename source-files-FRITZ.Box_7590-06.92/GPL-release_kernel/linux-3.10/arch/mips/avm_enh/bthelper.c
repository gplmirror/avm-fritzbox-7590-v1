/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2014 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 *
 *   mips-helper-functions for stackdump on smp etc.
\*------------------------------------------------------------------------------------------*/
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/mm.h>
#include <linux/rmap.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <asm/avm_enh/avm_enh.h>
#include <asm/mipsregs.h>
#include <asm/mipsmtregs.h>
#include <asm/mmu_context.h>
#include <asm/uaccess.h>
#include <asm/pgtable.h>
#include <asm/branch.h>
#include <asm/mmu_context.h>
#include <asm/types.h>
#include <asm/stacktrace.h>
#include <linux/simple_proc.h>
#include <asm/yield_context.h>
#include <asm/mach_avm.h>

extern void show_backtrace(struct task_struct *task, const struct pt_regs *regs);
#if defined(CONFIG_AVM_ENHANCED)
enum _monitor_ipi_type {
    monitor_nop = 0x0,
    monitor_bt  = 0x1,
};

struct _monitor_bt {
    unsigned int valid;
    struct pt_regs ptregs;
};

struct _monitor_ipi {
    enum _monitor_ipi_type type;
    union __monitor_ipi {
        struct _monitor_bt bt[YIELD_MONITOR_MAX_TC];
    } buf;
    unsigned int core;
    unsigned int init;
    atomic_t     ready;
    spinlock_t   lock; 
};
static atomic_t backtrace_busy;
static struct _monitor_ipi IPI_Monitor[YIELD_MONITOR_MAX_CORES];
static int monitor_ipi_yield_context(int signal __attribute__((unused)), void *handle);
static int __get_userinfo(char *buf, unsigned int maxbuflen, struct mm_struct *mmm, unsigned long addr);
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static void print_cp0_status(unsigned int cp0_status) {
	if (current_cpu_data.isa_level == MIPS_CPU_ISA_I) {
        printk(KERN_ERR"Status:  %08x %s%s%s%s%s\n", cp0_status, 
                                                (cp0_status & ST0_KUO) ? "KUo " : "",
                                                (cp0_status & ST0_IEO) ? "IEo " : "",
                                                (cp0_status & ST0_KUP) ? "KUp " : "",
                                                (cp0_status & ST0_IEP) ? "IEp " : "",
                                                (cp0_status & ST0_IEC) ? "IEc " : "");
	} else {
        printk(KERN_ERR"Status:  %08x %s%s%s%s%s%s%s\n", cp0_status, 
                                                (cp0_status & ST0_KX) ? "KX " : "",
                                                (cp0_status & ST0_SX) ? "SX " : "",
                                                (cp0_status & ST0_UX) ? "UX " : "",
                                                (cp0_status & ST0_KSU) == KSU_USER       ? "USER "       : 
                                                (cp0_status & ST0_KSU) == KSU_SUPERVISOR ? "SUPERVISOR " : 
                                                (cp0_status & ST0_KSU) == KSU_KERNEL     ? "KERNEL "     : "BAD MODE ",
                                                (cp0_status & ST0_ERL) ? "ERL " : "",
                                                (cp0_status & ST0_EXL) ? "EXL " : "",
                                                (cp0_status & ST0_IE)  ? "IE"  : "");
	}
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static struct _cpuid_to_mt_info {
    unsigned int core;
    unsigned int tc;
    unsigned int linuxos;
    char name[12];
} cpu_id_to_mt_info[NR_CPUS];

/*--------------------------------------------------------------------------------*\
 * collect info about which cpuid use core_x, tc_x and linux-mode
 * (needed for perfect backtrace)
\*--------------------------------------------------------------------------------*/
void avm_register_cpuid(char *name, unsigned int cpuid, unsigned int core, unsigned int tc) {
    struct _cpuid_to_mt_info *p_mt = &cpu_id_to_mt_info[cpuid];
    if(cpuid > NR_CPUS) {
        printk(KERN_ERR"%s: invalid cpu_id=%u\n", __func__, cpuid);
        return;
    }
    if(name && (strcmp(name, "LINUX") == 0)) {
        p_mt->linuxos = 1;
    } else {
        p_mt->linuxos = 0;
    }
    if(name) {
        snprintf(p_mt->name, sizeof(p_mt->name), name);
    } else {
        p_mt->name[0] = 0;
    }
    p_mt->core = core;
    p_mt->tc   = tc;
    printk(KERN_ERR"%s: cpu_id=%u: %s core=%u tc=%u\n", __func__, cpuid, name, core, tc);
    
}
/*--------------------------------------------------------------------------------*\
 * Request which (linux-)cpu_id  behind tc/core
 * ret:  cpu_id - if cpu_id == -1: no linux-os
 * name: name of OS on this CPU
\*--------------------------------------------------------------------------------*/
int get_cpuid_by_mt(unsigned int core, unsigned int tc, char **name) {
    unsigned int cpu;
    for(cpu = 0; cpu < NR_CPUS; cpu++) {
        struct _cpuid_to_mt_info *p_mt = &cpu_id_to_mt_info[cpu];
        if((p_mt->core == core) && (p_mt->tc == tc)) {
            if(name) *name = p_mt->name;
            return p_mt->linuxos ? cpu : -1;
        }
    }
    if(name)*name = "Other";
    return -1;
}
/*--------------------------------------------------------------------------------*\
 * Request which tc/core behind linux-cpu
 *
 * ret:   0      Linux-OS 
 *      < 0      invalid CPU_ID
 *      > 0      other
\*--------------------------------------------------------------------------------*/
int get_mt_by_cpuid(unsigned int cpu_id, unsigned int *core, unsigned int *tc) {
    struct _cpuid_to_mt_info *p_mt = &cpu_id_to_mt_info[cpu_id];
    if(cpu_id >= ARRAY_SIZE(cpu_id_to_mt_info)) {
        return -ERANGE;
    }
    *core = p_mt->core;
    *tc   = p_mt->tc;
    return p_mt->linuxos ? 0 : 1;
}
/*--------------------------------------------------------------------------------*\
  Absetzen eines Monitor-Befehls um synchron Infos von anderen Core zu erhalten
  ret: 0 ok 
\*--------------------------------------------------------------------------------*/
static int trigger_yield_monitor(unsigned int core, enum _monitor_ipi_type type, void *buf) {
    int ret = 0;
    unsigned long flags, retry = 1000;
    struct _monitor_ipi *m_ipi = &IPI_Monitor[core];
    if(core > YIELD_MONITOR_MAX_CORES) {
        return -EINVAL;
    }
    if(m_ipi->init == 0) {
        return -EACCES;
    }
    spin_lock_irqsave(&m_ipi->lock, flags);
    atomic_set(&m_ipi->ready, 0);
    m_ipi->type = type;
    wmb();
    if(cpu_id_to_mt_info[smp_processor_id()].core == core) {
        /*--- Umweg ueber yield nicht notwendig ---*/
        monitor_ipi_yield_context(0, m_ipi);
    } else {
        gic_trigger_irq(YIELD_IRQ_BY_ID(YIELD_MONITOR_IPI_ID(core)), 1);
    }
    while(retry && atomic_read(&m_ipi->ready) == 0) {
        udelay(1);
        retry--;
    }
    if(atomic_read(&m_ipi->ready)) {
       switch(type) {
            case monitor_bt:  
                memcpy(buf, &m_ipi->buf.bt, sizeof(m_ipi->buf.bt));
                break;
            case monitor_nop:  
            default:
                break;
        }
    } else {
        printk(KERN_ERR"%s: cpu=%u timeout_error", __func__, smp_processor_id());
        ret = -EACCES;
    }
    /*--- printk(KERN_ERR"%s: retry=%lu\n", __func__, retry); ---*/
    spin_unlock_irqrestore(&m_ipi->lock, flags);
    return ret;
}

#define READ_TCGPR(_ptregs, reg) (_ptregs)->regs[reg] = mftgpr(reg)

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void fill_bt_data_per_core(struct _monitor_bt bt[], unsigned el) {
	unsigned long haltval;
    unsigned int tc;

    for(tc = 0; tc < el; tc++) {
        settc(tc);
        if(!(read_tc_c0_tcstatus() & TCSTATUS_A)){
            bt[tc].valid = 0;
            continue;
        }
        if (read_tc_c0_tcbind() == read_c0_tcbind()) {
            /* Are we dumping ourself?  */
            haltval = 0; /* Then we're not halted, and mustn't be */
        } else {
            haltval = read_tc_c0_tchalt();
            write_tc_c0_tchalt(1);
        }
        READ_TCGPR(&bt[tc].ptregs, 1); READ_TCGPR(&bt[tc].ptregs, 2); READ_TCGPR(&bt[tc].ptregs, 3); 
        READ_TCGPR(&bt[tc].ptregs, 4); READ_TCGPR(&bt[tc].ptregs, 5); READ_TCGPR(&bt[tc].ptregs, 6);
        READ_TCGPR(&bt[tc].ptregs, 7); READ_TCGPR(&bt[tc].ptregs, 8); READ_TCGPR(&bt[tc].ptregs, 9); 
        READ_TCGPR(&bt[tc].ptregs,10); READ_TCGPR(&bt[tc].ptregs,11); READ_TCGPR(&bt[tc].ptregs,12); 
        READ_TCGPR(&bt[tc].ptregs,13); READ_TCGPR(&bt[tc].ptregs,14); READ_TCGPR(&bt[tc].ptregs,15); 
        READ_TCGPR(&bt[tc].ptregs,16); READ_TCGPR(&bt[tc].ptregs,17); READ_TCGPR(&bt[tc].ptregs,18); 
        READ_TCGPR(&bt[tc].ptregs,19); READ_TCGPR(&bt[tc].ptregs,20); READ_TCGPR(&bt[tc].ptregs,21); 
        READ_TCGPR(&bt[tc].ptregs,22); READ_TCGPR(&bt[tc].ptregs,23); READ_TCGPR(&bt[tc].ptregs,24); 
        READ_TCGPR(&bt[tc].ptregs,25); READ_TCGPR(&bt[tc].ptregs,26); READ_TCGPR(&bt[tc].ptregs,27); 
        READ_TCGPR(&bt[tc].ptregs,28); READ_TCGPR(&bt[tc].ptregs,29); READ_TCGPR(&bt[tc].ptregs,30); 
        READ_TCGPR(&bt[tc].ptregs,31);

        bt[tc].ptregs.cp0_status   = read_vpe_c0_status();
        bt[tc].ptregs.cp0_badvaddr = read_vpe_c0_badvaddr();
        bt[tc].ptregs.cp0_cause    = read_vpe_c0_cause();
        bt[tc].ptregs.cp0_epc      = read_tc_c0_tcrestart();
#ifdef CONFIG_MIPS_MT_SMTC
        bt[tc].ptregs.cp0_tcstatus = read_tc_c0_tcstatus();
#endif /* CONFIG_MIPS_MT_SMTC */
        bt[tc].valid = 1;
        if (!haltval) {
            write_tc_c0_tchalt(0);
        }
    }
}
/*--------------------------------------------------------------------------------*\
 * Yield-Context!
\*--------------------------------------------------------------------------------*/
static int monitor_ipi_yield_context(int signal __attribute__((unused)), void *handle){
    struct _monitor_ipi *m_ipi = (struct _monitor_ipi *)handle;

    gic_trigger_irq(YIELD_IRQ_BY_ID(YIELD_MONITOR_IPI_ID(m_ipi->core)), 0);
    if(atomic_read(&m_ipi->ready)) {
        return YIELD_HANDLED;
    }
    switch(m_ipi->type) {
            case monitor_bt:  
                fill_bt_data_per_core(m_ipi->buf.bt, ARRAY_SIZE(m_ipi->buf.bt));
                break;
            case monitor_nop:  
            default:  
                break;
    }
    wmb();
    atomic_set(&m_ipi->ready, 1);
    return YIELD_HANDLED;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init monitor_ipi_init(void) {
	int ret, core; 
    for(core = 0; core < YIELD_MONITOR_MAX_CORES; core++) {
        spin_lock_init(&IPI_Monitor[core].lock);
        IPI_Monitor[core].init = 0;
        IPI_Monitor[core].core = core;
        ret  = gic_map_setup(YIELD_CPU_BY_ID(YIELD_MONITOR_IPI_ID(core)), 
                             YIELD_IRQ_BY_ID(YIELD_MONITOR_IPI_ID(core)) , 
                             2 /*--- yield ---*/, 
                             YIELD_SIGNAL_BY_ID(YIELD_MONITOR_IPI_ID(core)));
        if(ret) {
            printk(KERN_ERR"%s: error %d on gic_map_setup(%u,%u)", __func__, ret, 
                                                                   YIELD_CPU_BY_ID(YIELD_MONITOR_IPI_ID(core)), 
                                                                   YIELD_IRQ_BY_ID(YIELD_MONITOR_IPI_ID(core)));
            continue;
        }
        ret = gic_map_irq_type(YIELD_IRQ_BY_ID(YIELD_MONITOR_IPI_ID(core)), IRQ_TYPE_EDGE_RISING);
        if(ret) {
            printk(KERN_ERR"%s: error %d on gic_map_irq_type(%u)", __func__, ret, 
                                                                  YIELD_IRQ_BY_ID(YIELD_MONITOR_IPI_ID(core)));
            continue;
        }
        ret = request_yield_handler_on(YIELD_CPU_BY_ID(YIELD_MONITOR_IPI_ID(core)), 
                                       YIELD_TC_BY_ID(YIELD_MONITOR_IPI_ID(core)),
                                       YIELD_SIGNAL_BY_ID(YIELD_MONITOR_IPI_ID(core)), 
                                       monitor_ipi_yield_context, &IPI_Monitor[core]);
        if(ret < 0) {
            printk(KERN_ERR"%s: error %d on request_yield_handler_on(%u,%u,%u)", __func__, ret, 
                                                                    YIELD_IRQ_BY_ID(YIELD_MONITOR_IPI_ID(core)), 
                                                                    YIELD_TC_BY_ID(YIELD_MONITOR_IPI_ID(core)), 
                                                                    YIELD_SIGNAL_BY_ID(YIELD_MONITOR_IPI_ID(core)));
            continue;
        }
        IPI_Monitor[core].init = 1;
        atomic_set(&backtrace_busy, 0);
        printk(KERN_ERR"%s: core %d initialized\n", __func__, core);
    }
	return ret;
}
late_initcall(monitor_ipi_init);

static struct _monitor_bt bt[YIELD_MONITOR_MAX_TC];
extern unsigned long kernelsp[NR_CPUS];

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline int is_yieldthread(unsigned int tc) {
    return (tc >= YIELD_FIRST_TC)  && (tc <= YIELD_LAST_TC);
}
/*--------------------------------------------------------------------------------*\
 * CPU0-CPU1: CORE0: VPE0, VPE1
 * CPU2-CPU3: CORE1: VPE0, VPE1
\*--------------------------------------------------------------------------------*/
bool arch_trigger_all_cpu_backtrace(void) {
	char txtbuf[2][64];
    unsigned long gp;
    struct task_struct *task;
    int act_cpu = smp_processor_id();
    unsigned int core;
    char *name;

    if(atomic_add_return(1, &backtrace_busy) > 1) {
        return 0;
    }
    for(core = 0; core < YIELD_MONITOR_MAX_CORES; core++) {
        unsigned int tc;
        memset(&bt,0, sizeof(bt));

        if(trigger_yield_monitor(core, monitor_bt, &bt)) {
            continue;
        }
        for(tc = 0; tc < ARRAY_SIZE(bt); tc++) {
            int cpuid; 
            struct pt_regs *ptregs = &bt[tc].ptregs;

            if(bt[tc].valid == 0) {
                continue;
            }
            cpuid = get_cpuid_by_mt(core, tc, &name);
            
            if((cpuid  < 0) && !is_yieldthread(tc)) {
                /*--- sowas wie MPE-Fw: keine Linux-konformes thread_info ---*/
                printk(KERN_ERR"---- CORE%u TC%u ----- (%s)\n", core, tc, name);
                print_cp0_status(ptregs->cp0_status);
                printk(KERN_ERR"[<%p>] %pS\n[<%p>] %pS\n\n", (void *)ptregs->cp0_epc, (void *)ptregs->cp0_epc,
                                                             (void *)ptregs->regs[31], (void *)ptregs->regs[31]);
                continue;
            }
            if((act_cpu == cpuid) && !user_mode(ptregs) && (in_nmi() || in_irq())) {
                if(get_irq_regs()) {
                    ptregs = get_irq_regs(); /*--- wir wollen nicht den aktuellen Irq/NMI/Exception-Kontext tracen ---*/
                } else {
                    printk(KERN_ERR"%s: tc=%u act_cpu=%u cpuid=%u get_irq_regs() == NULL - check genex.S\n", __func__, 
                                        tc, act_cpu, cpuid);
                }
            }
            if((cpuid >= 0) && user_mode(ptregs)) {
                /*--- wir sind im USER-Mode: Spezialbehandlung fuer gp-Ermittlung (da virtuelle Adressen) ---*/
                gp = kernelsp[cpuid] & ~THREAD_MASK; /*--- kseg0-gp ermitteln ---*/
            } else {
                gp = ptregs->regs[28]; /*--- kseg0-gp ermitteln ---*/
            }
            if(cpuid >= 0) {
                printk(KERN_ERR"---- CPU_ID=%u CORE%u TC%u ----- (%s)\n", cpuid, core, tc, name);
                print_cp0_status(ptregs->cp0_status);
            } else {
                printk(KERN_ERR"---- CORE%u TC%u ----- (YIELD-Thread)\n", core, tc);
            }
            if(virt_addr_valid(gp) && ((gp & THREAD_MASK) == 0)) { /*--- valid gp-address ? ---*/
                const int field = 2 * sizeof(unsigned long);
                struct thread_info *thread  = (struct thread_info *)gp;
                task = thread->task;
                if((!virt_addr_valid(task))   /*--- valide kseg0-task-address ? ---*/) {
                    printk(KERN_ERR"invalid task-pointer %p\n", task);
                    continue;
                }
                printk(KERN_ERR"current: %s (pid: %d, threadinfo=%p, task=%p, sp=%08lx tls=0x%0*lx)\n", 
                                task->comm, task->pid, thread, task, ptregs->regs[29], field, thread->tp_value);
                if (!user_mode(ptregs)) {
                    show_backtrace(task, ptregs);
                    continue;
                }
				printk(KERN_ERR"[<%08lx>] %s\n[<%08lx>] %s\n", 
                                            ptregs->cp0_epc,  
                                            get_user_symbol(txtbuf[0], sizeof(txtbuf[0]), gp, ptregs->cp0_epc), 
                                            ptregs->regs[31], 
                                            get_user_symbol(txtbuf[1], sizeof(txtbuf[1]), gp, ptregs->regs[31]));
            }
        }
    }
    atomic_set(&backtrace_busy, 0);
    return 1;
}
#else/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
bool arch_trigger_all_cpu_backtrace(void) {
    return 0;
}
#endif/*--- #else ---*//*--- #if defined(CONFIG_AVM_ENHANCED) ---*/

#define IS_MIPS16_EXTEND_OR_JAL(a) ((((a) >> (27 - 16)) == 30) | (((a) >> (27 - 16)) == 3)) /*--- Opcode EXTEND oder JAL ---*/
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int check_pc_adress(unsigned int __user *pc, unsigned int usermode) {

    if(((unsigned long)pc & (0xFF << 24)) == (PAGE_POISON << 24)) {
        return 1;
    }
    if(((unsigned long)pc & (0xFF << 24)) == (POISON_INUSE	<< 24)) {
        return 2;
    }
    if(((unsigned long)pc & (0xFF << 24)) == (POISON_FREE	<< 24)) {
        return 3;
    }
    if(((unsigned long)pc & (0xFF << 24)) == (POISON_END	<< 24)) {
        return 4;
    }
    if(((unsigned long)pc & (0xFF << 24)) == (POISON_FREE_INITMEM << 24)) {
        return 5;
    }
    if(!usermode && ((unsigned long)!kernel_text_address((unsigned long)pc))) {
        return 6;
    }
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int print_code_range(struct seq_file *seq, const char *prefix, unsigned int __user *pc, unsigned int mips16, 
                            unsigned int usermode, int left_offset, int right_offset, unsigned long *badaddr) {
    int ret;

    if((ret = check_pc_adress(pc, usermode))) {
        if(seq == NULL) printk(KERN_ERR "no code-dump, address could be %s (no memory at this address).\n", 
                                            ret == 1 ? "PAGE_POISON" :
                                            ret == 2 ? "POISON_INUSE" :
                                            ret == 3 ? "POISON_FREE" :
                                            ret == 4 ? "POISON_END" :
                                            ret == 5 ? "POISON_FREE_INITMEM" : "INVAL_KSEGx");
        return 0;
    }
    if(!mips16) {
        signed int i;
        unsigned long access_addr = (unsigned long)pc + sizeof(unsigned int) * left_offset;
        if(seq == NULL) printk(KERN_ERR"Code(0x%08lx):", access_addr);
        for(i = left_offset; i < right_offset; i++) {
            unsigned int pc_value;

            access_addr = (unsigned long)pc + sizeof(unsigned int) * i;
            if(usermode) {
                if(!unlikely(access_ok(VERIFY_READ, access_addr, 4))) {
                    if(seq == NULL)printk(KERN_ERR "[%s] illegal address 0x%lx (sigill)\n", prefix, access_addr);
                    *badaddr = (unsigned long)access_addr;
                    return -1; /*--- sigill; ---*/
                }
                if(__get_user(pc_value, (unsigned int __user *)access_addr)) {
                    if(seq == NULL)printk(KERN_ERR "[%s] load from address 0x%lx failed (sigbus)\n", prefix,
                                         access_addr);
                    *badaddr = (unsigned long)access_addr;
                    return -2; /*--- sigbus; ---*/
                }
            } else {
                pc_value = *((unsigned int *)access_addr);
            }
            if(seq) {
                seq_printf(seq, "%s %s0x%08x%s", i == left_offset ? prefix : "", i == 0 ? "<" : "", pc_value, 
                                                                                                    i == 0 ? ">" : "");
            } else {
                printk(" %s0x%08x%s", i == 0 ? "<" : "", pc_value, i == 0 ? ">" : "");
            }
        }
    } else {
        /*--- wegen EXT-Code nur step by step vorhangeln: ---*/
        unsigned short code0, code1;
        unsigned long pc_addr = (unsigned long)pc & ~0x1;
        unsigned long access_addr = (unsigned long)pc & ~0x1;
        unsigned long end_addr = ((unsigned long)pc & ~0x1) + sizeof(unsigned short) * right_offset;
    
        while(left_offset < 0) {
            if(usermode && !unlikely(access_ok(VERIFY_READ, access_addr, 4))) {
                if(seq == NULL)printk(KERN_ERR "[%s] illegal address 0x%lx (sigill)\n", prefix, access_addr);
                *badaddr = (unsigned long)access_addr;
                return -1; /*--- sigill; ---*/
            }
            if(__get_user(code1, (unsigned short __user *)(access_addr - sizeof(short)))) {
                if(seq == NULL)printk(KERN_ERR "[%s] load from 16 bit address 0x%lx failed (sigbus)\n", prefix, 
                                                                                                        access_addr);
                *badaddr = (unsigned long)access_addr;
                return -2; /*--- sigbus; ---*/
            }
            if(__get_user(code0, (unsigned short __user *)(access_addr - 2 * sizeof(short)))) {
                if(seq == NULL)printk(KERN_ERR "[%s] load from 16 bit address 0x%lx failed (sigbus)\n", prefix, 
                                                                                                        access_addr);
                *badaddr = (unsigned long)access_addr;
                return -2; /*--- sigbus; ---*/
            }
            if(IS_MIPS16_EXTEND_OR_JAL(code0)) {
                access_addr -= 2 * sizeof(short);
            } else {
                access_addr -= sizeof(short);
            }
            left_offset++;
        }
        if(seq) {
            seq_printf(seq, "%s", prefix);
        } else {
            printk(KERN_ERR"Code(0x%08lx):", access_addr);
        }
        while(access_addr < end_addr) {
            if(__get_user(code0, (unsigned short __user *)(access_addr))) {
                if(seq == NULL)printk(KERN_ERR "[%s] load from 16 bit address 0x%lx failed (sigbus)\n", prefix, 
                                                                                                        access_addr);
                *badaddr = (unsigned long)access_addr;
                return -2; /*--- sigbus; ---*/
            }
            if(access_addr == pc_addr) {
                if(IS_MIPS16_EXTEND_OR_JAL(code0)) {
                    access_addr += sizeof(short);
                    if(__get_user(code1, (unsigned short __user *)(access_addr))) {
                        if(seq == NULL)printk(KERN_ERR "[%s] load from 16 bit address 0x%lx failed (sigbus)\n",
                                                                                                prefix, access_addr);
                        *badaddr = (unsigned long)access_addr;
                        return -2; /*--- sigbus; ---*/
                    }
                    if(seq) {
                        seq_printf(seq, " <0x%04x %04x>", code0, code1);
                    } else {
                        printk(" <0x%04x %04x>", code0, code1);
                    }
                } else {
                    if(seq) {
                        seq_printf(seq, " <0x%04x>", code0);
                    } else {
                        printk(" <0x%04x>", code0);
                    }
                }
            } else {
                if(seq) {
                    seq_printf(seq, " 0x%04x", code0);
                } else {
                    printk(" 0x%04x", code0);
                }
            }
            access_addr += sizeof(short);
        }
    }
    if(seq == NULL) printk("\n");
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __get_userinfo(char *buf, unsigned int maxbuflen, struct mm_struct *mmm, unsigned long addr) {
    struct vm_area_struct *vm;
    unsigned int i = 0;
    if(mmm == NULL) {
        return 1;
    }
    vm = mmm->mmap;
    while(vm) {
        /*--- printk(KERN_INFO"%s[%x]:%p %x - %x vm_mm %p\n", __func__, addr, vm, vm->vm_start, vm->vm_end, vm->vm_mm); ---*/
        if((addr >= vm->vm_start) && (addr < vm->vm_end)) {
            snprintf(buf, maxbuflen,"seg=%3u of=0x%08lx/0x%lx [%s]", i, addr - (unsigned long)vm->vm_start, 
                     (unsigned long)vm->vm_end - (unsigned long)vm->vm_start,
                     (vm->vm_file && vm->vm_file->f_path.dentry) ? (char *)vm->vm_file->f_path.dentry->d_name.name : ""
                    );
            /*--- printk(KERN_INFO"%s", buf); ---*/
            return 0;
        }
        vm = vm->vm_next;
        i++;
    }
    return 1;
}
/*--------------------------------------------------------------------------------*\
 *  gp muss valide (nicht-virtuelle) Kerneladresse sein
 *  Schwachstelle: thread/task nicht mehr valid (in dem Moment freigegeben)
\*--------------------------------------------------------------------------------*/
char *get_user_symbol(char *txt, unsigned int maxttxlen, unsigned long gp, unsigned long addr) {
    unsigned long flags;
    struct task_struct *task = NULL;
    struct thread_info *thread  = (struct thread_info *)gp;
    txt[0] = 0;
    if(virt_addr_valid(gp) && ((gp & THREAD_MASK) == 0)) { /*--- valide gp-address ? ---*/
        return txt; 
    }
    local_irq_save(flags);
    task = thread->task;
    if(task == NULL) {
        local_irq_restore(flags);
        return txt; 
    }
    if(spin_trylock(&task->alloc_lock)) {
        /*--------------------------------------------------------------------------------*\
        kritisch: mmput()/get_task_mm() darf im Irq-Kontext nicht verwendet werden, 
        mmput hoffentlich nur im task_lock()
        \*--------------------------------------------------------------------------------*/
        __get_userinfo(txt, maxttxlen, task->mm, addr);
        spin_unlock(&task->alloc_lock);
    }
    local_irq_restore(flags);
    return txt; 
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int print_mem_config(struct mm_struct *mmm, unsigned long addr) {
    struct vm_area_struct *vm;
    unsigned int i = 0;
    if(mmm == NULL) 
        return 0;
    vm = mmm->mmap;
    while(vm) {
        if((addr >= vm->vm_start) && (addr < vm->vm_end)) {
            printk(KERN_ERR"Adresse-Segment(%d): 0x%lx: 0x%lx :0x%lx (offset 0x%lx)", 
                    i, vm->vm_start, addr, vm->vm_end, addr - vm->vm_start);
            if(vm->vm_file) {
                printk(" Path='%s'", vm->vm_file->f_path.dentry->d_name.name);
            }
            printk("\n");
            return 0;
        }
        vm = vm->vm_next;
        i++;
    }
    return 1;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void show_code_position_by_epc(char *prefix, struct pt_regs *regs) {
	unsigned int __user *pc;
    unsigned long addr = 0UL;

	pc = (unsigned int __user *) exception_epc(regs);
    printk(KERN_ERR "[%s] pc=0x%p(%pF) addr=0x%08lx task=%s pid=%d ra=0x%08lx(%pF)\n", 
            prefix, pc, pc, regs->cp0_badvaddr, current->comm, current->pid,
            regs->regs[31], (void *)regs->regs[31]
          );
    print_code_range(NULL, prefix, pc, regs->cp0_epc & 0x1, user_mode(regs), -2, 3, &addr);
	if (user_mode(regs)) {
        if(print_mem_config(current->active_mm, (unsigned long)pc))
            print_mem_config(current->mm, (unsigned long)pc);
    }
}

/*------------------------------------------------------------------------------------------*\
 * Unaligned-Helper
\*------------------------------------------------------------------------------------------*/
#define UNALIGNED_WARN      0x1 
#define UNALIGNED_BACKTRACE 0x8 
enum _unaligned_mode {
	UNALIGNED_ACTION_FIXUP             = 2,
	UNALIGNED_ACTION_FIXUP_WARN        = 3 | UNALIGNED_WARN, /*-- verodern eigentlich Dummy :-) ---*/
	UNALIGNED_ACTION_SIGNAL            = 4,
	UNALIGNED_ACTION_SIGNAL_WARN       = 5 | UNALIGNED_WARN, /*-- verodern eigentlich Dummy :-) ---*/
	UNALIGNED_ACTION_FIXUP_WARN_BT     = UNALIGNED_ACTION_FIXUP_WARN | UNALIGNED_WARN | UNALIGNED_BACKTRACE
};

static int ai_usermode	  = UNALIGNED_ACTION_FIXUP
		    		      /*--- | UNALIGNED_WARN ---*/
                                ;
static int ai_kernelmode =  UNALIGNED_ACTION_FIXUP
		    		      /*--- | UNALIGNED_WARN ---*/
                                ;
static unsigned long ai_user;
static unsigned long ai_sys;

#define UNALIGNED_MAX_SCORE_ENTRIES       8
#if defined(UNALIGNED_MAX_SCORE_ENTRIES)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _unaligned_access_score_entry {
    unsigned long unaligneds;
    unsigned long ts;
    unsigned short mips16;
    unsigned short caddr_of; /*--- > 0: pc-offset in codetable in bytes---*/
#define CODE_ENTRIES    5
    unsigned int code[CODE_ENTRIES];    /*--- Code muss gesichert werden, da evtl. Virtual Memory ---*/
    char shortcomm[8];                  /*--- Name mal sichern falls beendet ---*/
    pid_t pid;
    void *pc;
};
#define LEFT_CODE_OFFSET 2
#define RIGHT_CODE_OFFSET(a) min((a), CODE_ENTRIES - LEFT_CODE_OFFSET)
/*--------------------------------------------------------------------------------*\
 * 0: Fehler
 * sonst: liefert byte-offset zum pc
\*--------------------------------------------------------------------------------*/
static unsigned short cpy_code_range(unsigned int __user *pc, unsigned int usermode, unsigned int codetable[], 
                                                                        unsigned int code_entries, int left_offset) {
    signed int i;
    unsigned int idx = 0;
    int right_offset;
    unsigned short ret = 0;
    int mips16_offset = ((unsigned long)pc) & 0x2;
    unsigned long access_addr = ((unsigned long)pc) & ~0x3;

    right_offset = left_offset + code_entries;
    if(check_pc_adress(pc, usermode)) {
        memset(codetable, 0, code_entries * sizeof(unsigned int));
        return ret; 
    }
    access_addr += sizeof(unsigned int) * left_offset;
    for(i = left_offset; i < right_offset; i++) {
        unsigned int pc_value;

        if(usermode) {
            if(!unlikely(access_ok(VERIFY_READ, access_addr, 4))) {
                return ret;
            }
            if(__get_user(pc_value, (unsigned int __user *)access_addr)) {
                return ret;
            }
        } else {
            pc_value = *((unsigned int *)access_addr);
        }
        codetable[idx] = pc_value;
        if(i == 0) {
            ret = (unsigned char *)&codetable[idx] - (unsigned char *)&codetable[0] + mips16_offset;
        }
        idx++;
        access_addr += sizeof(unsigned int);
    }
    return ret;
}
/*--------------------------------------------------------------------------------*\
 * usermode: 0 Kernel
 *           1 MIPS32 
 *           2 MIPS16
\*--------------------------------------------------------------------------------*/
static void add_unaligned_access_to_table(void *pc, struct _unaligned_access_score_entry table[], 
                                                    unsigned int table_entries, unsigned int usermode) {
    unsigned int ts_idx = 0, ts_score = 0;
    unsigned int i;
    for(i = 0; i < table_entries; i++) {
        if(table[i].unaligneds) {
            if(usermode) {
                if(task_pid_nr(current) != table[i].pid) {
                    continue;
                }
            }
            if(pc == table[i].pc) {
                table[i].unaligneds++;
                /*--- printk(KERN_ERR "%s:[%u]add %s %s pid=%u pc=%p %lu\n", __func__, i, usermode ? "USER" : "KERNEL",  current->comm, table[i].pid, pc, table[i].unaligneds); ---*/
                return;
            }
            continue;
        }
        /*--- printk(KERN_ERR "%s:[%u]initial %s %s pc=%p pid=%u\n", __func__, i, usermode ? "USER" : "KERNEL",  current->comm, pc, table[i].pid); ---*/
        ts_idx = i;
        goto table_settings;
    }
    /*--- alle besetzt: bewerte per unaligneds / per time ---*/
    for(i = 0; i < table_entries; i++) {
        unsigned long diff = jiffies - table[i].ts;
        unsigned int score = diff / (table[i].unaligneds | 0x1);
        /*--- printk(KERN_ERR "%s:[%u]score %d diff %u unaligneds=%lu ts_score %u idx=%u\n", __func__,  i, score, diff, table[i].unaligneds, ts_score, ts_idx); ---*/
        if(score > ts_score) {
            ts_score = score;
            ts_idx = i;
        }
    }
    /*--- printk(KERN_ERR "%s:[%u]replace %s old: unaligneds=%lu pc=%p pid=%u ts_score=%u new=%s pc=%p\n", __func__, ts_idx, usermode ? "USER" : "KERNEL", table[ts_idx].unaligneds, table[ts_idx].pc, table[ts_idx].pid, ts_score, current->comm, pc); ---*/
table_settings:
    table[ts_idx].unaligneds = 1;
    table[ts_idx].pc         = pc;
    table[ts_idx].ts         = jiffies;
    table[ts_idx].pid        = task_pid_nr(current);
    table[ts_idx].mips16     = usermode & 0x2 ? 1 : 0;
    table[ts_idx].caddr_of   = cpy_code_range(pc, usermode, table[ts_idx].code, ARRAY_SIZE(table[ts_idx].code), 
                                                                                                    -LEFT_CODE_OFFSET);
    strncpy(table[ts_idx].shortcomm, current->comm, sizeof(table[ts_idx].shortcomm));
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned long show_unaligned_access_table(struct seq_file *seq, struct _unaligned_access_score_entry table[], 
                                                 unsigned int table_entries, unsigned int usermode) {
    char comm[sizeof(table[0].shortcomm) + 1];
    unsigned int i;
    unsigned long sum = 0;
    unsigned long address;

    seq_printf(seq, "%s:\nunaligneds\t unaligneds/hour\t\n", usermode ? "User-Scorelist" : "System-Scorelist");
    for(i = 0; i < table_entries; i++) {
        if(table[i].unaligneds) {
            unsigned int diff_hour = (jiffies - table[i].ts) / HZ / 3600;
            unsigned long unaligneds_per_hour = table[i].unaligneds;
            sum += table[i].unaligneds;
            if(diff_hour) {
                unaligneds_per_hour /= diff_hour; 
            }
            if(usermode) {
                struct pid *ppid;
                struct task_struct *tsk;
                char *pcomm; 
                if((ppid = find_get_pid(table[i].pid)) && (tsk = get_pid_task(ppid, PIDTYPE_PID))) {
                    pcomm = tsk->comm;
                } else {
                    memcpy(comm, table[i].shortcomm, sizeof(comm) - 1);
                    comm[sizeof(comm) - 1] = 0;
                    pcomm = comm;
                    tsk = NULL;
                }
                seq_printf(seq, "%10lu \t%10lu \t %s(%u) pc=0x%p ", table[i].unaligneds, unaligneds_per_hour, pcomm, 
                                                                    table[i].pid, table[i].pc);
                if(table[i].caddr_of) {
                    print_code_range(seq, ":", 
                            (unsigned int __user *)((unsigned long )&table[i].code + table[i].caddr_of), 
                            table[i].mips16, 0 /* use copy in kernel space */, -LEFT_CODE_OFFSET, 
                            RIGHT_CODE_OFFSET(2), &address);
                }
                if(tsk) {
                    put_task_struct(tsk);
                }
                if(ppid) {
                    put_pid(ppid);
                }
                seq_printf(seq, "\n");
            } else {
                seq_printf(seq, "%10lu \t%10lu \t 0x%p(%pF) ", table[i].unaligneds, unaligneds_per_hour,
                                                               table[i].pc, table[i].pc);
                if(table[i].caddr_of) {
                    print_code_range(seq, ":", 
                            (unsigned int __user *)((unsigned long )&table[i].code + table[i].caddr_of),
                            table[i].mips16, 0, -LEFT_CODE_OFFSET, RIGHT_CODE_OFFSET(2), &address);
                }
                seq_printf(seq, "\n");
            }
        }
    }
    return sum;
}
static struct _unaligned_access_score_entry user_score_table[UNALIGNED_MAX_SCORE_ENTRIES], 
                                            sys_score_table[UNALIGNED_MAX_SCORE_ENTRIES];
#endif/*--- #if defined(UNALIGNED_MAX_SCORE_ENTRIES) ---*/

/*--------------------------------------------------------------------------------*\
 * ret: 0 ok, sonst -1: sigill, -2 sigbus ausloesen
\*--------------------------------------------------------------------------------*/
int show_unaligned_position(struct pt_regs *regs){
    unsigned long addr = 0UL;
	unsigned int __user *pc;
	pc = (unsigned int __user *) exception_epc(regs);

    if (!user_mode(regs)) {
        ai_sys++;
#if defined(UNALIGNED_MAX_SCORE_ENTRIES)
        add_unaligned_access_to_table(pc, sys_score_table, ARRAY_SIZE(sys_score_table), 0);
#endif/*--- #if defined(UNALIGNED_MAX_SCORE_ENTRIES) ---*/
        if(ai_kernelmode & UNALIGNED_WARN) {
            int ret;
            printk(KERN_ERR "[kernel-unaligned %lu] pc=0x%p(%pF) addr=0x%08lx task=%s pid=%d ra=0x%08lx(%pF)\n", 
                  ai_sys, pc, pc, regs->cp0_badvaddr, current->comm, current->pid,
                  regs->regs[31], (void *)regs->regs[31]
            );
            ret = print_code_range(NULL, "kernel-unaligned", pc, regs->cp0_epc & 0x1, user_mode(regs), -2, 3, &addr);
            if(ret) {
				return ret;
            }
	        if(print_mem_config(current->active_mm, (unsigned long)pc))
	            print_mem_config(current->mm, (unsigned long)pc);

            if(ai_kernelmode & UNALIGNED_BACKTRACE) {
                show_backtrace(current, regs);
            }
        }
		return (ai_kernelmode & UNALIGNED_ACTION_SIGNAL) ? -2 : 0;
	}
	ai_user++;
#if defined(UNALIGNED_MAX_SCORE_ENTRIES)
    add_unaligned_access_to_table(pc, user_score_table, ARRAY_SIZE(user_score_table), 1 + (regs->cp0_epc & 0x1));
#endif/*--- #if defined(UNALIGNED_MAX_SCORE_ENTRIES) ---*/

	if(ai_usermode & UNALIGNED_WARN) {
		int ret;
		printk(KERN_ERR"Alignment trap: %s (%d) PC=0x%p Address=0x%08lx\n", current->comm, task_pid_nr(current), pc, 
                                                                            regs->cp0_badvaddr);
		ret = print_code_range(NULL, "kernel-unaligned", pc, regs->cp0_epc & 0x1, user_mode(regs), -2, 3, &addr);
		if(ret) {
			return ret;
		}
		if(print_mem_config(current->active_mm, (unsigned long)pc)) {
			print_mem_config(current->mm, (unsigned long)pc);
		}
		/*--- show_registers(regs); ---*/
	}
	return (ai_usermode & UNALIGNED_ACTION_SIGNAL) ? -2 : 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static const char *map_mode(enum _unaligned_mode mode) {
    switch(mode) {
        case UNALIGNED_ACTION_FIXUP:            return "(fixup)      ";
        case UNALIGNED_ACTION_FIXUP_WARN:       return "(fixup+warn) ";
        case UNALIGNED_ACTION_SIGNAL:           return "(signal)     ";
        case UNALIGNED_ACTION_SIGNAL_WARN:      return "(signal+warn)";
        case UNALIGNED_ACTION_FIXUP_WARN_BT:    return "(fixup+warn+backtrace)";
    }
    return "";
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void unalignment_proc_read(struct seq_file *seq, void *ref __maybe_unused){
	seq_printf(seq, "User:\t\t%lu\n", ai_user);
	seq_printf(seq, "System:\t\t%lu\n", ai_sys);
	seq_printf(seq, "User faults:\t%i %s\n", ai_usermode, map_mode(ai_usermode));
	seq_printf(seq, "Kernel faults:\t%i %s\n", ai_kernelmode, map_mode(ai_kernelmode));
#if defined(UNALIGNED_MAX_SCORE_ENTRIES)
    if(ai_user) {
        if(show_unaligned_access_table(seq,  user_score_table, ARRAY_SIZE(user_score_table), 1) != ai_user) {
            seq_printf(seq, "... only the newest user-unaligneds shown\n");
        }
    }
    if(ai_sys) {
        if(show_unaligned_access_table(seq,  sys_score_table, ARRAY_SIZE(sys_score_table), 0) != ai_sys) {
            seq_printf(seq, "... only the newest kernel-unaligneds shown\n");
        }
    }
#endif/*--- #if defined(UNALIGNED_MAX_SCORE_ENTRIES) ---*/
}
/*--- #define DSP_ASE_UNALIGNED_CHECK ---*/
#if defined(DSP_ASE_UNALIGNED_CHECK)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned int lwx_unaligned_check(void *addr, unsigned int index) {
    unsigned int erg;
    __asm__ __volatile__ (".set\tnoat\n"
                          "LWX %0, %1(%2) \n"
                            : "=r" (erg)
                            : "r" (index), "r" ((unsigned long)addr)
                            : "cc");
    return erg;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline signed int lhx_unaligned_check(void *addr, unsigned int index) {
    signed int erg;
    __asm__ __volatile__ (".set\tnoat\n"
                          "LHX %0, %1(%2) \n"
                            : "=r" (erg)
                            : "r" (index), "r" ((unsigned long)addr)
                            : "cc");
    return erg;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned int test_table[] = {
      0x1 <<  0, 0xF1 <<  0, 0x3 <<  0, 0xFF <<  0,
      0x1 <<  8, 0xF1 <<  8, 0x3 <<  8, 0xFF <<  8, 
      0x1 << 16, 0xF1 << 16, 0x3 << 16, 0xFF << 16, 
      0x1 << 24, 0xF1 << 24, 0x3 << 24, 0xFF << 24 
};
static signed short test2_table[] = {
      0x1 <<  0, 0xF1 <<  0, 0x3 <<  0, 0xFF <<  0,
      0x1 <<  8, 0xF1 <<  8, 0x3 <<  8, 0xFF <<  8, 
};
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void check_dsp_ase_unaligneds(void) {
    unsigned int i;
    unsigned int unalignedtable[16 + 1];
    unsigned int *p = (unsigned int *)((unsigned char *)unalignedtable +1);

    memcpy(p, test_table, sizeof(test_table));
    for(i = 0; i < ARRAY_SIZE(test_table); i++) {
        unsigned int val = lwx_unaligned_check(p, i * 4);
        if(val != test_table[i]) {
            printk(KERN_ERR"%s:#1 error: val(%x)  != table[%u] (%x)\n", __func__, val, i, test_table[i]);
        }
    }
    for(i = 0; i < ARRAY_SIZE(test_table); i++) {
        unsigned int val = lwx_unaligned_check(unalignedtable, (i * 4) + 1);
        if(val != test_table[i]) {
            printk(KERN_ERR"%s:#2 error: val(%x)  != table[%u] (%x)\n", __func__, val, i, test_table[i]);
        }
    }
    memcpy(p, test2_table, sizeof(test2_table));
    for(i = 0; i < ARRAY_SIZE(test2_table); i++) {
        signed int val = lhx_unaligned_check(p, i * 2);
        if(val != (signed int)test2_table[i]) {
            printk(KERN_ERR"%s:#3 error: val(%x)  != table[%u] (%x)\n", __func__, val, i, (signed int)test2_table[i]);
        }
    }
    for(i = 0; i < ARRAY_SIZE(test2_table); i++) {
        signed int val = lhx_unaligned_check(unalignedtable, (i * 2) + 1);
        if(val != (signed int)test2_table[i]) {
            printk(KERN_ERR"%s:#4 error: val(%x)  != table[%u] (%x)\n", __func__, val, i, (signed int)test2_table[i]);
        }
    }
}
#endif/*--- #if defined(DSP_ASE_UNALIGNED_CHECK) ---*/
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int unalignment_proc_write(char *cmd, void *ref __maybe_unused) {
	int mode = cmd[0];
#if defined(DSP_ASE_UNALIGNED_CHECK)
    if(mode == 'T') {
        check_dsp_ase_unaligneds();
        return 0;
    }
#endif/*--- #if defined(DSP_ASE_UNALIGNED_CHECK) ---*/
	if (mode >= '2' && mode <= '5') {
		ai_usermode = mode - '0';
		printk(KERN_ERR "set user unaligned-mode: %s\n", map_mode(ai_usermode));
	} else if (mode >= '6' && mode <= '8') {
		ai_kernelmode = mode == '6' ? UNALIGNED_ACTION_FIXUP : 
                        mode == '7' ? UNALIGNED_ACTION_FIXUP_WARN : UNALIGNED_ACTION_FIXUP_WARN_BT;
		printk(KERN_ERR "set kernel unaligned-mode: %s\n", map_mode(ai_kernelmode));
	} else {
		printk(KERN_ERR "parameter: user   '2' %s '3' %s '4' %s '5' %s \n", map_mode(UNALIGNED_ACTION_FIXUP), 
                                                                            map_mode(UNALIGNED_ACTION_FIXUP_WARN), 
                                                                            map_mode(UNALIGNED_ACTION_SIGNAL), 
                                                                            map_mode(UNALIGNED_ACTION_SIGNAL_WARN));
		printk(KERN_ERR "           system '6' %s '7' %s '8' %s\n", map_mode(UNALIGNED_ACTION_FIXUP), 
                                                                    map_mode(UNALIGNED_ACTION_FIXUP_WARN), 
                                                                    map_mode(UNALIGNED_ACTION_FIXUP_WARN_BT));
	}
	return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init alignment_init(void) {
	int ret; 
	proc_mkdir("cpu", NULL);
	ret = add_simple_proc_file(
			"cpu/alignment", 
			unalignment_proc_write,
			unalignment_proc_read,
			NULL);
	return ret;
}
__initcall(alignment_init);
