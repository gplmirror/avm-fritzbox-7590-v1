#ifndef _logging_h
#define _logging_h

/**
 *
 * Logging functions, to be used in kernel- and userspace
 */

void libvorbis_open_stdout (void);
void libvorbis_writef (int level, const char * s, ...);

/* TODO: this should go into config.h */
#define LOGPREFIX "libvorbis:"

#ifndef LOGLEVEL
#warning "LOGLEVEL not defined. Defaulting to 1"
#define LOGLEVEL 1
#endif

/*
 * Use NOTRACE to disable trace-messages in certain modules.
 */
#if LOGLEVEL > 4 && !defined(NOTRACE)
#if defined(__GNUC__)
#define print_trace(_fmt, ...) libvorbis_writef (5, LOGPREFIX "TRC:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else  /* __GNUC__ */
#define print_trace(_fmt, ...) libvorbis_writef (5, LOGPREFIX "TRC:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, __VA_ARGS__)
#endif
#else
#define print_trace(_fmt, ...) do {} while(0)
#endif

#if LOGLEVEL > 3 && !defined(NODEBUG)
#if defined(__GNUC__)
#define print_debug(_fmt, ...) libvorbis_writef (4, LOGPREFIX "DBG:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else  /* __GNUC__ */
#define print_debug(_fmt, ...) libvorbis_writef (4, LOGPREFIX "DBG:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, __VA_ARGS__)
#endif
#else
#define print_debug(_fmt, ...) do {} while(0)
#endif

#if LOGLEVEL > 2
#if defined(__GNUC__)
#define print_info(_fmt, ...)  libvorbis_writef (3, LOGPREFIX "INF:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else  /* __GNUC__ */
#define print_info(_fmt, ...)  libvorbis_writef (3, LOGPREFIX "INF:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, __VA_ARGS__)
#endif
#else
#define print_info(_fmt, ...) do {} while(0)
#endif

#if LOGLEVEL > 1
#if defined(__GNUC__)
#define print_warn(_fmt, ...)  libvorbis_writef (2, LOGPREFIX "WRN:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else  /* __GNUC__ */
#define print_warn(_fmt, ...)  libvorbis_writef (2, LOGPREFIX "WRN:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, __VA_ARGS__)
#endif
#else
#define print_warn(_fmt, ...) do {} while(0)
#endif

#if LOGLEVEL > 0
#if defined(__GNUC__)
#define print_error(_fmt, ...)  libvorbis_writef (1, LOGPREFIX "ERR:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else  /* __GNUC__ */
#define print_error(_fmt, ...)  libvorbis_writef (1, LOGPREFIX "ERR:%s:%d: " _fmt "\n", __FUNCTION__, __LINE__, __VA_ARGS__)
#endif
#else
#define print_error(_fmt, ...) do {} while(0)
#endif

#if LOGLEVEL > 0
#if defined(__GNUC__)
#define print_raw(_fmt, ...)  libvorbis_writef (0, LOGPREFIX _fmt "\n", ##__VA_ARGS__)
#else  /* __GNUC__ */
#define print_raw(_fmt, ...)  libvorbis_writef (0, LOGPREFIX _fmt "\n", __VA_ARGS__)
#endif
#else
#define print_raw(_fmt, ...) do {} while(0)
#endif

#endif /* _logging_h */
