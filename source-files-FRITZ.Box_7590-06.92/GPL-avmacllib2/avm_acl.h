/* 
 * - User Access Control
 *
 * Copyright 2009 by AVM GmbH <info@avm.de>
 *
 * This software contains free software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License ("License") as published by the Free Software Foundation 
 * (version 2 of the License). This software is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
 * A PARTICULAR PURPOSE. See the copy of the License you received along with this software for more details.
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

/* this is the common path prefix of all real paths (linux file system view) that can be accessed */
#define AVM_ACL_COMMON_REAL_PREFIX "/var/media/ftp"


int acl_set_user_context(char *username, int access_from_internet);
int acl_set_user_context_uid(unsigned uid, int access_from_internet);

/**
 * @brief acl_set_full_access_user_context
 * Set write and read rights for "/" root direcotry.
 * for internal admin tasks ("root access")
 *
 * @return 0
 */
int acl_set_full_access_user_context(void);

char *acl_get_user_common_real_dir(void);

int acl_is_allowed_path(const char *path, int *pwrite_access);

void acl_set_samba_mode(void);

void acl_reconfig(void);

int acl_chmod_allowed(void);

int acl_force_secured_internet_access(void);

struct acl_string_list
{
	struct acl_string_list *next;
	char *s;
};

// get all real filesystems path the current user has at least read access to
// the caller must free the list
struct acl_string_list *acl_get_user_all_real_paths_with_read_access(void);

#ifdef __cplusplus
}
#endif




