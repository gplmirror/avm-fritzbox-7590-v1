/*
 * - User Access Control
 *
 * Copyright 2009-2017 by AVM GmbH <info@avm.de>
 *
 * This software contains free software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License ("License") as published by the Free Software Foundation 
 * (version 2 of the License). This software is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
 * A PARTICULAR PURPOSE. See the copy of the License you received along with this software for more details.
 *
 */

#ifdef HAVE_CONFIG_H
# include <autoconfig.h>
#endif

/* memory debugging
 * #include "config.h"
 */
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <ctype.h> /* isspace () */
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <assert.h>
#include <stdarg.h>

#include "avm_acl.h"

// #include "extern.h"
#define ACL_BOX_USER_ID_COMPATMODE 100
#define ACL_BOX_USER_ID_SKIP_AUTH_FROM_HOMENET 101

#if defined(COMPILE_HOSTTOOLS)
static void _fLog(char *fmt, ...)
{
	FILE *fp = fopen("/dev/console", "w");
	if (!fp) fp = fopen("/var/tmp/acl.debug", "a"); // NOTE that you have to "chmod 777 /var/tmp" before
	if (fp) {
		va_list ap;
		fprintf(fp, "acl(%d): ", getpid());
		va_start(ap, fmt);
		vfprintf(fp, fmt, ap);
		va_end(ap);
		fprintf(fp, "\n");
		fclose(fp);
	}
}
#define Log(x) _fLog x

#undef  AVM_ACL_COMMON_REAL_PREFIX
#define AVM_ACL_COMMON_REAL_PREFIX "/var/tmp"

#else
#define Log(x)
#endif

static char *g_username = 0;
static unsigned g_uid;
static short g_access_from_internet = 0;
static short g_bReconfig = 0;

static int g_is_samba = 0;

struct acl_directory
{
	struct acl_directory *next;

	char *real_path;    /* the user always has read access in this path and all files/folders below */
	int real_path_len;  /* calc and store the len once to be faster */
	short write_access;

	int common_path_len; // number of chars in real_path for which another acl_directory exists
};

static struct user_context 
{
	int logged_in;
//	int access_from_internet;

	struct acl_directory *acl;

	char *real_root;
	int real_root_len;
} userctx = { 0, NULL, NULL, 0 };


static int FixUserContext(void)
{
	if (!g_bReconfig) return 0;
	g_bReconfig = 0;

	if (!userctx.acl) return -1;
	// reconfig with updated users.acl file
	if (g_username) {
		acl_set_user_context(g_username, g_access_from_internet);
	} else {
		acl_set_user_context_uid(g_uid, g_access_from_internet);
	}
	if (!userctx.acl) return -1;

	return 0;
}


/* ------------------------------------------------------------------------ */

// Nutzung macht nur Sinn, wenn der Interne Speicher fuer Netzwerkzugriff nicht aktiviert ist.
static int is_mountpoint(const char *path)
{
	const char *p;
	int len = strlen(path);
	
	if (len < strlen(AVM_ACL_COMMON_REAL_PREFIX)) {
		return 0;
	}

	if (path != strstr(path, AVM_ACL_COMMON_REAL_PREFIX)) return 0;

	p = path + strlen(AVM_ACL_COMMON_REAL_PREFIX);
	if (*p != '/') return 0;
	p++;
	if (*p == '\0') return 0;
	// kein / mehr oder nur am ende
	while(*p != '\0') {
		if (*p == '/') {
			if (*(p+1) != '\0') return 0;
		}
		p++;
	}
	return 1;
}

static int InternalMemoryActiveForNetwork(int reset)
{
	static int known = 0;
	static int active;

	if (reset) {
		// just trigger reload
		known = 0;
		return 0;
	}

	if (!known) {
		char buf[PATH_MAX];
		known = 1;
		if (0 == realpath("/var/InternerSpeicher", buf)) {
			active = 0;
		} else {
			// as a "trick" we use the "old" /var/mountpoint_excluded_for_network
			if (0 == access("/var/mountpoint_excluded_for_network", R_OK)) {
				active = 0;
			} active = 1;
		}
	}

	return active;
}

/**
 * Block access to listed files.
 * Only needed for files in one of the directories:
 * /var/InternerSpeicher
 * /var/media/ftp (AVM_ACL_COMMON_REAL_PREFIX)
 *
 * Other locations of bpjm.data do not need to be blocked here, because the directories are out of access scope.
 */
static const char *blockedpaths[] = {
	"/var/InternerSpeicher/FRITZ/bpjm.data",
	AVM_ACL_COMMON_REAL_PREFIX"/FRITZ/bpjm.data"
};

#define DIM(array) (sizeof(array)/sizeof((array)[0]))

static bool IsBlockedPath(const char *path)
{
	unsigned int u;

	for (u = 0; u < DIM(blockedpaths); u++) {
		char buf[PATH_MAX];
		errno = 0;
		if (0 != realpath(blockedpaths[u], buf)) {
			Log(("strcmp(%s, %s)", path, buf));
			if (!strcmp(path, buf)) {
				return true;
			}
		} else {
			Log(("realpath(%s) failed, %s.", blockedpaths[u], strerror(errno)));
		}
	}

	return false;
}

static int IsHiddenLostAndFound(const char *path)
{
	static char *internal_lost_and_found = (char *)-1;
	static int internal_lost_and_found_len;
	if ((char *)-1 == internal_lost_and_found) {
		char buf[PATH_MAX];
		if (0 == realpath("/var/InternerSpeicher", buf)) {
			internal_lost_and_found = 0;
			return 0;
		} else {
			internal_lost_and_found_len = strlen(buf) + strlen("/lost+found");
			internal_lost_and_found = (char*)malloc(internal_lost_and_found_len + 1);
			if (!internal_lost_and_found) return 0;
			sprintf(internal_lost_and_found, "%s/lost+found", buf);
		}
	}
	if (0 == internal_lost_and_found) return 0;

	if (path != strstr(path, internal_lost_and_found)) return 0;

	if (path[internal_lost_and_found_len] != '/' && path[internal_lost_and_found_len] != '\0') return 0;
	return 1;
}

/* ------------------------------------------------------------------------ */

/**
 * @brief acl_is_allowed_path
 * check if access to a given real path is allowed for the client
 * all path components must exist
 * Return 0 if path is empty.
 * Return 0 if no user logged in.
 * Return 0 if path contains "..". TODO details
 * Return 1 if used by smbd and trying to read $IPC.
 * @param path_ complete filesystem path
 * @param pwrite_access out parameter 1 if write access else 0
 *
 * @return 1 on success else 0 on error
 */
int acl_is_allowed_path(const char *path_, int *pwrite_access)
{
	struct acl_directory *a = userctx.acl;
	char buf[PATH_MAX];
	char *path;

	if (!path_) {
		return 0;
	}

	path = strdup(path_);
	if (!path) {
		return 0;
	}

	Log(("acl_is_allowed_path %s", path));

	if (g_is_samba && (!InternalMemoryActiveForNetwork(0) || !userctx.logged_in) && !strcmp(path, AVM_ACL_COMMON_REAL_PREFIX)) {
		// our path of $IPC service
		*pwrite_access = 0;
		free(path);
		Log(("acl_is_allowed_path - Read-Only for $IPC"));
		return 1;
	}

	if (!userctx.logged_in) {
		free(path);
		Log(("acl_is_allowed_path - not logged in"));
		return 0;
	}

	if (*path == '\0') {
		/* realpath() with empty string would fail */
		if (0 == getcwd(buf, sizeof(buf))) {
			free(path);
			Log(("acl_is_allowed_real_path - getcwd failed"));
			return 0;
		}
		buf[sizeof(buf)-1] = '\0';
	} else {
		if (buf != realpath(path, buf)) {
			int len, buf_remaining;
			// Check if parent exists
			// and construct realpath
			char *p = &path[strlen(path)];
			while (p > path && *p != '/') p--;
			if (*p != '/') {
				Log(("realpath failed and path=%s has no / p=%s", path, p));
				if (buf != realpath(".", buf)) {
					// parent does not exists
					free(path);
					Log(("realpath of current dir failed"));
					return 0;
				}
				len = strlen(buf);
				if (0 == len) {
					free(path);
					return 0;
				}
				buf_remaining = sizeof(buf) - len - 1;
				if (buf[len-1] != '/') {
					if (buf_remaining < 1) {
						free(path);
						return 0;
					}
					strncat(buf, "/", 1);
					buf_remaining--;
				}
				len = strlen(path);
				if (buf_remaining < len) {
					free(path);
					return 0;
				}
				strncat(buf, path, len);
			} else {
				if (!strcmp(p+1, "..")) {
					free(path);
					return 0;
				}
				*p = '\0';
				if (buf != realpath(path, buf)) {
					// parent does not exists
					Log(("realpath of parent=%s failed", path));
					*p = '/';
					free(path);
					return 0;
				}
				len = strlen(buf);
				if (0 == len) {
					free(path);
					return 0;
				}
				buf_remaining = sizeof(buf) - len - 1;
				*p = '/';
				if (buf[len-1] == '/') {
					len = strlen(p+1);
					if (buf_remaining < len) {
						free(path);
						return 0;
					}
					strncat(buf, p+1, len);
				} else {
					len = strlen(p);
					if (buf_remaining < len) {
						free(path);
						return 0;
					}
					strncat(buf, p, len);
				}
			}
			Log(("acl_is_real_path - constructed realpath %s", buf));
		}
	}

	Log(("checking access to %s", buf));

	if (IsHiddenLostAndFound(buf)) {
		Log(("acl_is_allowed_path - path %s - not allowed cause its in lost+found in internal flash", buf));
		free(path);
		return 0;
	}

	if (IsBlockedPath(buf)) {
		Log(("path %s is blocked", path));
		free(path);
		return 0;
	}

	if (FixUserContext()) {
		Log(("acl_is_allowed_path - path %s - not allowed cause FixUserContext failed", buf));
		free(path);
		return 0;
	}

	while(a) {
		if (buf == strstr(buf, a->real_path)) {
			char c = buf[a->real_path_len];
			if (c == '\0' || c == '/') break; // found
		}
		a = a->next;
	}
	if (a) {
		if (InternalMemoryActiveForNetwork(0)) {
			// /var/media/ftp is the internal memory
			// and all mountpoints are within the internal memory
			*pwrite_access = a->write_access  ? 1 : 0;
		} else {
			// im smbd gibts sonst Probleme mit loeschen/umbennenn
			// direkt unterhalb des mountpoints
			if (!g_is_samba && is_mountpoint(buf)) {
				*pwrite_access = 0;
			} else 
				*pwrite_access = a->write_access  ? 1 : 0;
		}
	} else {
		// check for path component to a shared "partition home"
		// eg. the following  paths are configured:
		//  /var/media/ftp/STICK/home/sub1/sub2/sub3
		//  /var/media/ftp/STICK/home/sub4/sub5
		// --> now we also have to allow readonly access to exactly 
		//  /var/media/ftp/STICK/home
		//  /var/media/ftp/STICK/home/sub1
		//  /var/media/ftp/STICK/home/sub1/sub2
		//  /var/media/ftp/STICK/home/sub4
		// otherwise the user would not be able to reach 
		// /var/media/ftp/STICK/home/sub1/sub2/sub3 or
		// /var/media/ftp/STICK/home/sub4/sub5 
		// from his "partition root" /var/media/ftp/STICK/home
		int len = strlen(buf);
		for (a = userctx.acl; a; a = a->next) {
			char c;
			// Log(("checking intermediate path %s against allowed %s", buf, a->real_path));
			if (0 == a->common_path_len) continue;
			if (len < a->common_path_len) continue; // ist unterhalb der "partition root"
			c = buf[a->common_path_len];
			if (c != '\0' && c != '/') continue;
			assert(strlen(buf) >= a->common_path_len);
			if (memcmp(a->real_path, buf, a->common_path_len)) continue;
			if (a->real_path != strstr(a->real_path, buf)) continue;
			c = a->real_path[len];
			if (c == '/') {
				Log(("intermediate path %s IS ALLOWED against allowed %s", buf, a->real_path));
				break; // found
			}
		}
		/* Bei Freigabe eines Unterverzeichnisses wird dennoch als einziger! Samba-Share
		 * immer /var/media/ftp freigegeben.
		 * Ein User muss daher auf alle Verzeichnisse auf dem Weg zu seinem user-spezifischen Real-Root
		 * mit Read-Only-Rechten zugreifen duerfen.
		 * Beispiel: Freigabe es Verzeichnisses /Musik/Pop/neu fuer den User
		 * --> user-spezifischer Real-Root ist /var/media/ftp/Musik/Pop/neu
		 * --> Leserecht auch auf folgende Verzeichnisse noetig:
		 * /var/media/ftp
		 * /var/media/ftp/Musik
		 * /var/media/ftp/Pop
		 * (aber auf /var/media/ftp/Musik/Pop/neu ggf. auch Schreibrechte!)
		 */
		if (!a && g_is_samba && len >= strlen(AVM_ACL_COMMON_REAL_PREFIX)) {
			for (a = userctx.acl; a; a = a->next) {
				int rl;
				char *p;
				// Log(("samba: checking for parent path %s against allowed %s", buf, a->real_path));
				p = strrchr(a->real_path, '/'); // remove last dir
				if (!p) continue;
				rl = p - a->real_path;
				if (len > rl) continue; // buf ist auf gleichem Level oder subdir von a->real_path
				if (a->real_path[len] != '/') continue;
				if (!memcmp(buf, a->real_path, len)) {
					Log(("samba: parent path %s IS ALLOWED (read only) against allowed %s", buf, a->real_path));
					break;
				}
			}
		}
		if (a) {
			*pwrite_access = 0;
		}
	}

	if (!a) {
		Log(("acl_is_allowed_path - path %s - not in any allow path for this user ", buf));
		free(path);
		return 0;
	}

	Log(("access to %s allowed write_access=%d ret=1", buf, *pwrite_access));
	free(path);

	return 1;
}

/* ------------------------------------------------------------------------ */


static void add_virt_path(char *path, int write_access)
{
	struct acl_directory **pp;
	struct acl_directory *a;
	struct acl_directory *a2;
	Log(("add_virt_path %s write_access:%d", path, write_access));

	if (path[0] != '/') return;
	
	if (path[1] == '\0') path = "";

	a = calloc(1, sizeof(struct acl_directory));
	if (!a) return;
	a->real_path = malloc(strlen(AVM_ACL_COMMON_REAL_PREFIX) + strlen(path) + 1);
	if (!a->real_path) {
		free(a);
		return;
	}
	strcpy(a->real_path, AVM_ACL_COMMON_REAL_PREFIX);
	strcat(a->real_path, path);

	a->real_path_len = strlen(a->real_path);
	a->write_access = write_access;

	// calc common_path_len -> bis zum letzten gemeinsamen Verzeichnis vom Pfad-Anfang
	for (a2 = userctx.acl; a2; a2 = a2->next) {
		int len;
		char *p1 = a->real_path;
		char *p2 = a2->real_path;
		while(*p1 == *p2 && *p1 != '\0') { p1++; p2++; }
		if ((*p1 == '\0' || *p1 == '/') && (*p2 == '\0' || *p2 == '/')) {
		} else {
			p1--;
			while(*p1 != '/') p1--;
		}
		// match bis einschliesslich p1-1
		len = p1 - a->real_path;
		if (len > a->common_path_len) {
Log(("common_path_len of new %s set to %u", a->real_path, len));
			a->common_path_len = len;
		}
		if (len > a2->common_path_len) {
Log(("common_path_len of existing %s set to %u", a2->real_path, len));
			a2->common_path_len = len;
		}
	}

	pp = &userctx.acl;
	while(*pp) {
		pp = &((*pp)->next);
	}
	*pp = a;
}


/* ----------------------------------------------------------------------- */
enum eToken { TOKEN_EOF, 
			TOKEN_STRING,
			TOKEN_USER,
			TOKEN_ID,
			TOKEN_DIR,
			TOKEN_ACCESS,
			TOKEN_LOCAL_READ,
			TOKEN_LOCAL_WRITE,
			TOKEN_INTERNET_READ,
			TOKEN_INTERNET_WRITE,
			TOKEN_INTERNET_SECURED,
			TOKEN_APP,
			TOKEN_APP_USERID,
			TOKEN_APP_NAS_RIGHTS,
			TOKEN_APP_INTERNET_RIGHTS
};

static struct defined_token
{
	char *string;
	enum eToken token;
} tokens[] = {
	{ "user", TOKEN_USER },
	{ "id", TOKEN_ID },
	{ "dir", TOKEN_DIR },
	{ "access", TOKEN_ACCESS },
	{ "read", TOKEN_LOCAL_READ },
	{ "write", TOKEN_LOCAL_WRITE },
	{ "internet-read", TOKEN_INTERNET_READ },
	{ "internet-write", TOKEN_INTERNET_WRITE },
	{ "internet-secured", TOKEN_INTERNET_SECURED },
	{ "app", TOKEN_APP },
	{ "userid", TOKEN_APP_USERID },
	{ "nas-rights", TOKEN_APP_NAS_RIGHTS },
	{ "internet-rights", TOKEN_APP_INTERNET_RIGHTS },

	{ 0, TOKEN_EOF }
};

struct UserACLReader {
	FILE *fp;

	char *string; // current token
	size_t string_len;
	size_t string_buf_size;
};

static void add_char(struct UserACLReader *r, int c)
{
	if (!r->string) return;
	if (r->string_len >= r->string_buf_size-2) return;
	r->string[r->string_len] = c;
	r->string_len++;
}

static char *GetString(struct UserACLReader *r)
{
	return r->string;
}

static enum eToken UserACLFile_GetToken(struct UserACLReader *r)
{
	int c;
	short bQuoted;
	short bEscaped = 0;
	if (!r->fp) return TOKEN_EOF;
	do {
		c = fgetc(r->fp);	
		if (c == EOF) {
			fclose(r->fp);
			r->fp = 0;
			return TOKEN_EOF;
		}
	} while(isspace(c));

	if (!r->string) {
		r->string_buf_size = 256;
		r->string = (char *)malloc(r->string_buf_size);
		if (!r->string) return TOKEN_EOF;
	}
	r->string_len = 0;
	if (c == '\"') {
		bQuoted = 1;
	} else { 
		bQuoted = 0;
		add_char(r, c);
	}

	do {
		c = fgetc(r->fp);
		if (c == EOF) break;

		if (bQuoted) {
			// end at "
			if (bEscaped) {
				bEscaped = 0;
				add_char(r, c);
			} else { 
				if (c == '\\') bEscaped = 1;
				else if (c == '\"') break; // string end
				else add_char(r, c);
			}
		} else {
			// end at white space
			if (isspace(c)) break; // stringend
			add_char(r, c);
		}
	} while(1);

	r->string[r->string_len++] = '\0';

	if (!bQuoted) {
		struct defined_token *dt;
		for (dt = &tokens[0]; dt->string; dt++) {
			if (!strcmp(r->string, dt->string)) return dt->token;
		}
	}
	return TOKEN_STRING;
}

static struct UserACLReader *UserACLFile_ReadOpen(void)
{
	struct UserACLReader *r = (struct UserACLReader *)calloc(1, sizeof(struct UserACLReader));
	if (!r) return 0;
	r->fp = fopen("/var/tmp/users.acl", "rt");
	if (!r->fp) {
		free(r);
		return 0;
	}
	return r;
}

static void UserACLFile_ReadClose(struct UserACLReader *r)
{
	if (!r) return;
	free(r->string);
	if (r->fp) fclose(r->fp);

	free(r);
}


/* 
user "Udo G" id 70
dir "/x"
access write
*/

static int ReadUser(struct UserACLReader *r, int access_from_internet, int allow_write_access)
{
	enum eToken t;
	short ungot_token = 0;
	do {
		short read_access = 0;    // effective read right to this path
		short write_access = 0;   // effective write right to this path
		char *path;
		if (!ungot_token) t = UserACLFile_GetToken(r);
		ungot_token = 0;
		if (t != TOKEN_DIR) break;
		t = UserACLFile_GetToken(r);
		if (t != TOKEN_STRING) break;

		path = strdup(GetString(r));
		t = UserACLFile_GetToken(r);
		// if (!access_from_internet) read_access = 1;
		if (t == TOKEN_ACCESS) {
			short end = 0;
			do {
				t = UserACLFile_GetToken(r);
				switch(t) {
				case TOKEN_LOCAL_READ:
					if (!access_from_internet) read_access = 1;
					break;
				case TOKEN_LOCAL_WRITE:
					if (!access_from_internet) { write_access = 1; read_access = 1; }
					break;
				case TOKEN_INTERNET_READ:
					if (access_from_internet) read_access = 1;
					break;
				case TOKEN_INTERNET_WRITE:
					if (access_from_internet) { write_access = 1; read_access = 1; }
					break;
				default:
					ungot_token = 1;
					end = 1;
				}
			} while(!end);
		} else {
			ungot_token = 1;
		}
		if (read_access && path && path[0] == '/') {
			add_virt_path(path, (allow_write_access && write_access) ? 1 : 0);
		}
		free(path);
	} while(1);

	UserACLFile_ReadClose(r);

	return 0;
}

static int ReadUsersACLFileByID(unsigned id, int access_from_internet, int allow_write_access)
{
	enum eToken t;
	struct UserACLReader *r = UserACLFile_ReadOpen();
	if (!r) return -1;

	t = UserACLFile_GetToken(r);

	while(t != TOKEN_EOF) {
		if (t == TOKEN_USER) {
			do {
				t = UserACLFile_GetToken(r);
			} while(t == TOKEN_STRING);
				if (t == TOKEN_ID) {
					t = UserACLFile_GetToken(r);
					if (t == TOKEN_STRING) {
						if (atoi(GetString(r)) == id) break;
					}
				}
		} else if (t == TOKEN_APP) {
			/* app by id not supported here */
		}
		t = UserACLFile_GetToken(r);
	}

	if (t == TOKEN_EOF) goto fail;

	return ReadUser(r, access_from_internet, allow_write_access);

fail:
	UserACLFile_ReadClose(r);
	return -1;
}

static int ReadUsersACLFileByFriendlyName(char *username, int access_from_internet)
{
	enum eToken t;
	int appid = 0;
	int app_userid;
	int app_internet_rights;
	int app_write_access;
	struct UserACLReader *r = UserACLFile_ReadOpen();
	if (!r) return -1;

	t = UserACLFile_GetToken(r);

	while(t != TOKEN_EOF) {
		if (t == TOKEN_USER) {
			short bFound = 0;
			do {
				t = UserACLFile_GetToken(r);
				if (!bFound && t == TOKEN_STRING && !strcasecmp(GetString(r), username)) bFound = 1;
			} while (t == TOKEN_STRING);
			if (bFound) {
				if (t != TOKEN_ID) goto fail;
				t = UserACLFile_GetToken(r);
				if (t != TOKEN_STRING) goto fail;
				break;
			}
		} else if (t == TOKEN_APP) {
			short bFound = 0;
			do {
				t = UserACLFile_GetToken(r);
				if (!bFound && t == TOKEN_STRING && !strcasecmp(GetString(r), username)) bFound = 1;
			} while (t == TOKEN_STRING);
			if (bFound) {
				if (t != TOKEN_ID) goto fail;
				t = UserACLFile_GetToken(r);
				if (t != TOKEN_STRING) goto fail;
				appid = atoi(GetString(r));
				if (appid <= 0) goto fail;
				t = UserACLFile_GetToken(r);
				if (t != TOKEN_APP_USERID) goto fail;
				t = UserACLFile_GetToken(r);
				if (t != TOKEN_STRING) goto fail;
				app_userid = atoi(GetString(r));
				if (app_userid <= 0) goto fail;
				t = UserACLFile_GetToken(r);
				if (t != TOKEN_APP_NAS_RIGHTS) goto fail;
				t = UserACLFile_GetToken(r);
				switch(t) {
				case TOKEN_LOCAL_READ:  app_write_access = 0; break;  // read-only
				case TOKEN_LOCAL_WRITE: app_write_access = 1; break;  // read-write  
				default: goto fail;
				}
				t = UserACLFile_GetToken(r);
				if (t != TOKEN_APP_INTERNET_RIGHTS) goto fail;
				t = UserACLFile_GetToken(r);
				if (t != TOKEN_STRING) goto fail;
				app_internet_rights = atoi(GetString(r));
				if (app_internet_rights < 0) goto fail;
				break;
			}
		}
		t = UserACLFile_GetToken(r);
	}

	if (t == TOKEN_EOF) goto fail;

	if (0 != appid) {
		int ret;
		UserACLFile_ReadClose(r);
		if (access_from_internet && 0 == app_internet_rights) return 0;
		if (app_internet_rights &&
			(app_userid == ACL_BOX_USER_ID_COMPATMODE || app_userid == ACL_BOX_USER_ID_SKIP_AUTH_FROM_HOMENET)) {
			access_from_internet = 0; // this way internet rights are added to the app
		}
		ret = ReadUsersACLFileByID(app_userid, access_from_internet, app_write_access);
		return ret;
	} else {
		return ReadUser(r, access_from_internet, 1/*allow write access*/);
	}

fail:
	UserACLFile_ReadClose(r);
	return -1;
}

/* ----------------------------------------------------------------------- */

static void acl_unset_user_context(void)
{
Log(("acl_unset_user_context"));
	while(userctx.acl) {
		struct acl_directory *a = userctx.acl;
		free(a->real_path);
		userctx.acl = a->next;
		free(a);
	}

	free(userctx.real_root);

	memset(&userctx, 0, sizeof(userctx));
}


static int calc_real_root(void)
{
	int root_path_len;
	struct acl_directory *root = userctx.acl;
Log(("calc_real_root"));
	// calc real root (what is virtual /) for this user
	// the real root is the longest common parent path of all real directories the user can access (at least read)
// ACHTUNG: TODO Das Home-Dir in /etc/passwd muss fuer Local Access als auch fuer Access aus dem Internet gelten!!!!!!!
//           bzw der ftp muss das homedir hieraus berechnen und nicht aus /etc/passwd lesen!

	if (!root) return -1;
	while(root->next) root = root->next; // note that longer path with common parent are first in list, so we go to the last

	root_path_len = root->real_path_len;

	do { 
		// check obs gemeinsamer parent ist
		struct acl_directory *a = userctx.acl;
		char *p;

		if (root_path_len == 1) break; // "/" passt immer
		while(a != root) {
			char c;
			if (a->real_path_len < root_path_len || memcmp(a->real_path, root->real_path, root_path_len)) break; // passt nicht
			c = a->real_path[root_path_len];
			if (c != '/' && c != '\0') break; // passt nicht
			a = a->next;
		}
		if (a == root) break; // passt!

		// shorten root - go up one dir
		if (root_path_len == strlen(AVM_ACL_COMMON_REAL_PREFIX)) break; // cant shorten more!

		p = root->real_path + root_path_len - 1;
		while(*p != '/' && p > root->real_path) p--;
		if (p == root->real_path) {
			root_path_len = 1; // '/' - should not happen
			break;
		}
		root_path_len = p - root->real_path;
	} while(1);

	userctx.real_root_len = root_path_len;
	userctx.real_root = malloc(root_path_len+1);
	if (!userctx.real_root) return -1;
	memcpy(userctx.real_root, root->real_path, root_path_len);
	userctx.real_root[root_path_len] = '\0';

	Log(("calc_real_root: real_root is %s", userctx.real_root));

	return 0;
}

/**
 * @brief acl_set_user_context
 * Returns  0 if no username is given.
 * Returns -1 if no access for given user.
 * Returns  0 if some access is configured for given user.
 *
 * @param username may be NULL
 * @param access_from_internet 0 for (W)LAN access else WAN access
 *
 * @return 0 or -1
 */
int acl_set_user_context(char *username, int access_from_internet)
{
	char *old_name;
	Log(("acl_set_user_context username %s access_from_internet:%d", username ? username : "<null>", access_from_internet));
Log(("sizeof(off_t)=%u sizeof(struct stat)=%u _FILE_OFFSET_BITS=%u", sizeof(off_t), sizeof(struct stat), _FILE_OFFSET_BITS));

	acl_unset_user_context();
	if (!username) {
		return 0;
	}

	if (ReadUsersACLFileByFriendlyName(username, access_from_internet)) {
		// no access at all
		goto bad;
	}

#if 0 // TEST
	// laengere pfade mit gleichen Parent-Pfaden vorher anfuegen!
	// add_virt_path("/JetFlash-TS8GJF160-01/readonly", 0/* READ ONLY */);
	// add_virt_path("/JetFlash-TS8GJF160-01", 1/* write access to everything */);
#endif

	// parameter username may be the same pointer as g_username 
	old_name = g_username;
	g_username = strdup(username);
	free(old_name);
	g_uid = 0;
	g_access_from_internet = access_from_internet;

	userctx.logged_in = 1;
	return 0;

bad:
	acl_unset_user_context();
	Log(("acl_set_user_context failed"));
	return -1;
}

int acl_set_user_context_uid(unsigned uid, int access_from_internet)
{
	Log(("acl_set_user_context_uid uid:%u access_from_internet:%d", uid, access_from_internet));

	acl_unset_user_context();

	if (uid <= 1000) return -1;

	if (ReadUsersACLFileByID(uid - 1000, access_from_internet, 1/*allow write access*/)) {
		// no access at all
		goto bad;
	}

	free(g_username);
	g_username = 0;
	g_uid = uid;
	g_access_from_internet = access_from_internet;
	userctx.logged_in = 1;

	return 0;

bad:
	acl_unset_user_context();
	Log(("acl_set_user_context_uid failed"));
	return -1;
}

/**
 * @brief acl_set_full_access_user_context
 * Set write and read rights for "/" root direcotry.
 * for internal admin tasks ("root access")
 *
 * @return 0
 */
int acl_set_full_access_user_context(void)
{
Log(("acl_set_full_user_context()"));
	acl_unset_user_context();
	add_virt_path("/", 1/* write access to everything */);
	userctx.logged_in = 1;
	return 0;
}

char *acl_get_user_common_real_dir(void)
{
Log(("acl_get_user_common_real_dir"));
	if (!userctx.acl) return 0;
	if (!userctx.real_root) {
		if (calc_real_root()) return 0; // failed
	}
	return userctx.real_root;
}


struct acl_string_list *acl_get_user_all_real_paths_with_read_access(void)
{
	struct acl_string_list *head = 0;
	struct acl_string_list *tail = 0;

	struct acl_directory *a;
	for (a = userctx.acl; a && a->real_path; a = a->next) {
		struct acl_string_list *sl = (struct acl_string_list *)malloc(sizeof(struct acl_string_list));
		if (!sl) break;
		sl->s = strdup(a->real_path);
		if (!sl->s) { free(sl); break; };
		sl->next = 0;
		if (!head) {
			head = sl;
		} else {
			tail->next = sl;
		}
		tail = sl;
	}

	return head;
}

void acl_set_samba_mode(void)
{
Log(("acl_set_samba_mode"));
	g_is_samba = 1;
}

void acl_reconfig(void)
{
	InternalMemoryActiveForNetwork(1/*reset*/);
	g_bReconfig = 1;
}

int acl_chmod_allowed(void)
{
	static int is_tcom = 2;

	if (2 == is_tcom) {
		char *s = getenv("OEM");
		is_tcom = s && (!strcmp(s, "tcom") || !strcmp(s, "congstar"));
	}
	return !is_tcom;
}

int acl_force_secured_internet_access(void)
{
	enum eToken t;
	int secured = 0;
	struct UserACLReader *r = UserACLFile_ReadOpen();
	if (!r) return 0;

	t = UserACLFile_GetToken(r);

	while(t != TOKEN_EOF && t != TOKEN_USER) {
		if (t == TOKEN_INTERNET_SECURED) {
			secured = 1;
			break;
		}
		t = UserACLFile_GetToken(r);
	}
	UserACLFile_ReadClose(r);
	return secured;
}


