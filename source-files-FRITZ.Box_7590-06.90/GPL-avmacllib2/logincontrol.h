/*
 * - User Login Control / Event Logging
 *
 * Copyright 2009 by AVM GmbH <info@avm.de>
 *
 * This software contains free software; you can redistribute it and/or modify it under the terms 
 * of the GNU General Public License ("License") as published by the Free Software Foundation 
 * (version 2 of the License). This software is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
 * A PARTICULAR PURPOSE. See the copy of the License you received along with this software for more details.
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

int LoginControlIsAllowed(const char *display_name);

void LoginControlWrongPassword(const char *display_name, int from_internet);
void LoginControlPasswordOk(const char *display_name, int from_internet);

void LoginControlLogout(const char *display_name, int from_internet);

void LoginControlReset(const char *display_name);

// check all for expired blocking
// return time till next expire
time_t  LoginControlCleanup(void);

#ifdef __cplusplus
}
#endif

