/* 
   Unix SMB/CIFS implementation.

   FRITZ!OS Event Logging

   Copyright (C) 2016 AVM 
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef _AVM_EVENTLOG_H_
#define _AVM_EVENTLOG_H_

#ifndef AVM_FRITZBOX
#error include this file for AVM FRITZ!Box only!
#endif

#define AVM_EVENT_ID_SAMBA_LOGIN_OK         582
#define AVM_EVENT_ID_SAMBA_LOGIN_FAILED     583
#define AVM_EVENT_ID_SAMBA_APP_LOGIN_OK     586
#define AVM_EVENT_ID_SAMBA_APP_LOGIN_FAILED 587

void AccessEventLog(unsigned event_id, const char *name, const char *addr_str);

char *GetAppDisplayName(const char *username);

#endif /*  __AVM_EVENTLOG_H__ */
