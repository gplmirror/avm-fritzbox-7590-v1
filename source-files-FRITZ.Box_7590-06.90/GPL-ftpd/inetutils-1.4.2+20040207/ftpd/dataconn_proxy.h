#ifndef PUMA6_ATOM
#error dataconn_proxy is used on PUMA6 ATOM only
#endif

int dataconn_proxy_create(struct sockaddr_storage *atom_addr, /*OUT*/unsigned *arm_port);
void dataconn_proxy_destroy(void);
