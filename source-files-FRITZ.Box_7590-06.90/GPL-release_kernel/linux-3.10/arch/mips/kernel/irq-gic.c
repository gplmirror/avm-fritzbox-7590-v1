/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 2008 Ralf Baechle (ralf@linux-mips.org)
 * Copyright (C) 2012 MIPS Technologies, Inc.  All rights reserved.
 */
#include <linux/bitmap.h>
#include <linux/init.h>
#include <linux/smp.h>
#include <linux/irq.h>
#include <linux/clocksource.h>
#include <linux/io.h>

#include <asm/gic.h>
#include <asm/setup.h>
#include <asm/traps.h>
#include <asm/gcmpregs.h>
#include <linux/hardirq.h>
#include <asm-generic/bitops/find.h>

#if defined(CONFIG_AVM_ENHANCED)
#include <linux/proc_fs.h>
#include <linux/simple_proc.h>
#include <linux/kernel_stat.h>
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
#include <linux/avm_profile.h>
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/ 
#if defined(CONFIG_AVM_POWER) 
#include <linux/avm_power.h>
#endif /*--- #if defined(CONFIG_AVM_POWER) ---*/ 

/* The following defintions must match the static interrupt routing table */
#define GIC_VI2_NUM_INTRS	64
#define GIC_VI3_NUM_INTRS	64
#define GIC_VI4_NUM_INTRS	32
#define GIC_VI5_NUM_INTRS	32
#define GIC_VI6_NUM_INTRS	64

#define GIC_VI2_INTRS_BASE	0
#define GIC_VI3_INTRS_BASE	GIC_VI2_NUM_INTRS
#define GIC_VI4_INTRS_BASE	(GIC_VI2_NUM_INTRS + GIC_VI3_NUM_INTRS)
#define GIC_VI5_INTRS_BASE	(GIC_VI2_NUM_INTRS + GIC_VI3_NUM_INTRS \
					+ GIC_VI4_NUM_INTRS)
#define GIC_VI6_INTRS_BASE	(GIC_VI2_NUM_INTRS + GIC_VI3_NUM_INTRS \
					+ GIC_VI4_NUM_INTRS + GIC_VI5_NUM_INTRS)

/* offset = (irq_base /32) *4 = irq_base >> 3 */
#define GIC_VI2_SH_PEND		(GIC_SH_PEND_31_0_OFS \
					+ (GIC_VI2_INTRS_BASE >> 3))
#define GIC_VI2_SH_MASK		(GIC_SH_MASK_31_0_OFS \
					+ (GIC_VI2_INTRS_BASE >> 3))

#define GIC_VI3_SH_PEND		(GIC_SH_PEND_31_0_OFS \
					+ (GIC_VI3_INTRS_BASE >> 3))
#define GIC_VI3_SH_MASK		(GIC_SH_MASK_31_0_OFS \
					+ (GIC_VI3_INTRS_BASE >> 3))

#define GIC_VI4_SH_PEND		(GIC_SH_PEND_31_0_OFS \
					+ (GIC_VI4_INTRS_BASE >> 3))
#define GIC_VI4_SH_MASK		(GIC_SH_MASK_31_0_OFS \
					+ (GIC_VI4_INTRS_BASE >> 3))

#define GIC_VI5_SH_PEND		(GIC_SH_PEND_31_0_OFS \
					+ (GIC_VI5_INTRS_BASE >> 3))
#define GIC_VI5_SH_MASK		(GIC_SH_MASK_31_0_OFS \
					+ (GIC_VI5_INTRS_BASE >> 3))

#define GIC_VI6_SH_PEND		(GIC_SH_PEND_31_0_OFS \
					+ (GIC_VI6_INTRS_BASE >> 3))
#define GIC_VI6_SH_MASK		(GIC_SH_MASK_31_0_OFS \
					+ (GIC_VI6_INTRS_BASE >> 3))

struct gic_pcpu_mask {
	DECLARE_BITMAP(pcpu_mask, GIC_NUM_INTRS);
};


/*--- #define DBG_IRQ_GIC_TRC(args...) printk(KERN_ERR args) ---*/
#define DBG_IRQ_GIC_TRC(args...)
/*--- #define DBG_TRC_IRQ_NMB     59 ---*/
#if defined(DBG_TRC_IRQ_NMB)
#define DBG_IRQ(irq, args...) if(irq == DBG_TRC_IRQ_NMB) printk(KERN_ERR args)
#else
#define DBG_IRQ(irq, args...)
#endif/*--- #if defined(DBG_TRC_IRQ_NMB) ---*/


unsigned int gic_frequency;
unsigned int gic_present;
unsigned long _gic_base;
unsigned int gic_irq_base;
unsigned int gic_irq_flags[GIC_NUM_INTRS];
static int gic_vpes;
static DEFINE_SPINLOCK(gic_lock);
/* The index into this array is the vector # of the interrupt. */
struct gic_shared_intr_map gic_shared_intr_map[GIC_NUM_INTRS];

static struct gic_pcpu_mask pcpu_masks[NR_CPUS];

static inline bool gic_is_local_irq(unsigned int hwirq)
{
	return hwirq >= GIC_NUM_INTRS;
}

static inline unsigned int gic_hw_to_local_irq(unsigned int hwirq)
{
	return hwirq - GIC_NUM_INTRS;
}

static inline unsigned int gic_local_to_hw_irq(unsigned int irq)
{
	return irq + GIC_NUM_INTRS;
}

static inline bool local_irq_is_legacy(unsigned int local_irq)
{
	if ((local_irq == GIC_LOCAL_INT_TIMER)
		|| (local_irq == GIC_LOCAL_INT_PERFCTR))
		return true;
	else
		return false;
}


#if defined(CONFIG_CSRC_GIC) || defined(CONFIG_CEVT_GIC)
cycle_t gic_read_count(void)
{
	unsigned int hi, hi2, lo;

	do {
		GICREAD(GIC_REG(SHARED, GIC_SH_COUNTER_63_32), hi);
		GICREAD(GIC_REG(SHARED, GIC_SH_COUNTER_31_00), lo);
		GICREAD(GIC_REG(SHARED, GIC_SH_COUNTER_63_32), hi2);
	} while (hi2 != hi);

	return (((cycle_t) hi) << 32) + lo;
}

void gic_write_compare(cycle_t cnt)
{
	GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_COMPARE_HI),
				(int)(cnt >> 32));
	GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_COMPARE_LO),
				(int)(cnt & 0xffffffff));
}

cycle_t gic_read_compare(void)
{
	unsigned int hi, lo;

	GICREAD(GIC_REG(VPE_LOCAL, GIC_VPE_COMPARE_HI), hi);
	GICREAD(GIC_REG(VPE_LOCAL, GIC_VPE_COMPARE_LO), lo);

	return (((cycle_t) hi) << 32) + lo;
}
#endif

unsigned int gic_get_timer_pending(void)
{
	unsigned int vpe_pending;

	GICREAD(GIC_REG(VPE_LOCAL, GIC_VPE_PEND), vpe_pending);
	return vpe_pending & GIC_VPE_PEND_TIMER_MSK;
}

void gic_bind_eic_interrupt(int irq, int set)
{
	/* Convert irq vector # to hw int # */
	irq -= GIC_PIN_TO_VEC_OFFSET;

	/* Set irq to use shadow set */
	GICWRITE(GIC_REG_ADDR(VPE_LOCAL, GIC_VPE_EIC_SS(irq)), set);
}

void gic_send_ipi(unsigned int intr)
{
	GICWRITE(GIC_REG(SHARED, GIC_SH_WEDGE), 0x80000000 | intr);
}
#if defined(CONFIG_AVM_ENHANCED)
static unsigned int is_irq_pending(unsigned int intr);
static unsigned int local_irq_to_map_off(unsigned int intr);
/*--------------------------------------------------------------------------------*\
 * trigger works only if irq configured in edge-mode
\*--------------------------------------------------------------------------------*/
void gic_trigger_irq(unsigned int irq, unsigned int set) {
	int intr = irq - gic_irq_base;

    DBG_IRQ(irq, "%s(%u)%s -> %s\n", __func__, irq, gic_is_local_irq(intr) ? "-> local" : "", set ? "set" : "unset");
    if((intr < 0) || (intr > GIC_NUM_INTRS)){
		return;
    }
    if(set) {
        set = 1U << 31;
    }
	GICWRITE(GIC_REG(SHARED, GIC_SH_WEDGE), set | intr);
}
EXPORT_SYMBOL(gic_trigger_irq);
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/

static void gic_eic_irq_dispatch(void)
{
	unsigned int cause = read_c0_cause();
	int irq;

	irq = (cause & ST0_IM) >> STATUSB_IP2;
	if (irq == 0)
		irq = -1;

	if (irq >= 0) {
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
		avm_simple_profiling_enter_irq(gic_irq_base + irq);
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/
		do_IRQ(gic_irq_base + irq);
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
		avm_simple_profiling_leave_irq(gic_irq_base + irq);
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/
	 } else 
		spurious_interrupt();
}

unsigned int gic_compare_int(void)
{
	unsigned int pending;

	GICREAD(GIC_REG(VPE_LOCAL, GIC_VPE_PEND), pending);
	if (pending & GIC_VPE_PEND_CMP_MSK)
		return 1;
	else
		return 0;
}

static bool gic_local_irq_is_routable(int intr)
{
	u32 vpe_ctl;

	/* All local interrupts are routable in EIC mode. */
	if (cpu_has_veic)
		return true;

	GICREAD(GIC_REG(VPE_LOCAL, GIC_VPE_CTL), vpe_ctl);
	switch (intr) {
	case GIC_LOCAL_INT_TIMER:
		return vpe_ctl & GIC_VPE_CTL_TIMER_RTBL_MSK;
	case GIC_LOCAL_INT_PERFCTR:
		return vpe_ctl & GIC_VPE_CTL_PERFCNT_RTBL_MSK;
	case GIC_LOCAL_INT_FDC:
		return vpe_ctl & GIC_VPE_CTL_FDC_RTBL_MSK;
	case GIC_LOCAL_INT_SWINT0:
	case GIC_LOCAL_INT_SWINT1:
		return vpe_ctl & GIC_VPE_CTL_SWINT_RTBL_MSK;
	default:
		return true;
	}
}

int gic_get_c0_compare_int(void)
{
	if (!gic_local_irq_is_routable(GIC_LOCAL_INT_TIMER))
		return MIPS_CPU_IRQ_BASE + cp0_compare_irq;
	return MIPS_GIC_LOCAL_IRQ_BASE + GIC_LOCAL_INT_TIMER;
}

int gic_get_c0_perfcount_int(void)
{
	if (!gic_local_irq_is_routable(GIC_LOCAL_INT_PERFCTR)) {
		/* Is the erformance counter shared with the timer? */
		if (cp0_perfcount_irq < 0)
			return -1;
		return MIPS_CPU_IRQ_BASE + cp0_perfcount_irq;
	}
	return MIPS_GIC_LOCAL_IRQ_BASE + GIC_LOCAL_INT_PERFCTR;
}

void gic_shared_irq_dispatch(void)
{
	unsigned int i, intr;
	unsigned long *pcpu_mask;
	unsigned long *pending_abs, *intrmask_abs;
	DECLARE_BITMAP(pending, GIC_NUM_INTRS);
	DECLARE_BITMAP(intrmask, GIC_NUM_INTRS);

	/* Get per-cpu bitmaps */
	pcpu_mask = pcpu_masks[smp_processor_id()].pcpu_mask;

	pending_abs = (unsigned long *) GIC_REG_ABS_ADDR(SHARED,
							 GIC_SH_PEND_31_0_OFS);
	intrmask_abs = (unsigned long *) GIC_REG_ABS_ADDR(SHARED,
							  GIC_SH_MASK_31_0_OFS);

	for (i = 0; i < BITS_TO_LONGS(GIC_NUM_INTRS); i++) {
		GICREAD(*pending_abs, pending[i]);
		GICREAD(*intrmask_abs, intrmask[i]);
		pending_abs++;
		intrmask_abs++;
	}
#if defined(DBG_TRC_IRQ_NMB)
    if(pending[BITS_TO_LONGS(DBG_TRC_IRQ_NMB)] & (1 << GIC_INTR_BIT(DBG_TRC_IRQ_NMB))) {
        DBG_IRQ(DBG_TRC_IRQ_NMB, "%s(%u) pending %s\n", __func__, DBG_TRC_IRQ_NMB, intrmask[BITS_TO_LONGS(DBG_TRC_IRQ_NMB)] & (1 << GIC_INTR_BIT(DBG_TRC_IRQ_NMB)) ? "(enabled)" : "(disabled)");
    }
#endif/*--- #if defined(DBG_TRC_IRQ_NMB) ---*/
	bitmap_and(pending, pending, intrmask, GIC_NUM_INTRS);
	bitmap_and(pending, pending, pcpu_mask, GIC_NUM_INTRS);

#if defined(CONFIG_AVM_ENHANCED)
	/*--------------------------------------------------------------------------------*\
	 * we have to ignore NMI and YIELD Events
	 \*--------------------------------------------------------------------------------*/
	for(;;) {
		unsigned int pinmap;
		intr = find_first_bit(pending, GIC_NUM_INTRS);
		if(intr >= GIC_NUM_INTRS) {
			return;
		}
		GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), pinmap);
		if(!(pinmap & GIC_MAP_TO_PIN_MSK)) {
			/*--- NMI or YIELD -> do not signalize! ---*/
			DBG_IRQ_GIC_TRC("%s: cpu=%d intr=%u pinmap=%x do not signalize as pending\n", __func__, smp_processor_id(), intr, pinmap);
		} else {
            unsigned int irq = gic_irq_base + intr;
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
		    avm_simple_profiling_enter_irq(irq);
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/
			do_IRQ(irq);
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
		    avm_simple_profiling_leave_irq(irq);
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/
		}
		bitmap_clear(pending, intr, 1);
	}
#else/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
	intr = find_first_bit(pending, GIC_NUM_INTRS);
	while (intr != GIC_NUM_INTRS) {
		do_IRQ(gic_irq_base + intr);

		/* go to next pending bit */
		bitmap_clear(pending, intr, 1);
		intr = find_first_bit(pending, GIC_NUM_INTRS);
	}
#endif/*--- #else ---*//*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
}
#if !defined(CONFIG_AVM_POWER)
#define avm_cpu_wait_end(); 
#endif/*--- #if !defined(CONFIG_AVM_POWER)  ---*/


#if defined(CONFIG_AVM_ENHANCED)
/*--------------------------------------------------------------------------------*\
 * Wichtiger Fix!
 * Der Originalcode hat den entscheidenen Fehler alle Pending-Irqs vorneweg lokal auszulesen und nacheinander abzuarbeiten 
 * falls in do_IRQ() die Irqs wieder angeschaltet werden, kann die Irq-Routine unterbrochen werden (Nested-Irq). Sie liest dann ebenfalls 
 * wieder die Pending-Irqs. Somit werden - je nachdem wo unterbrochen wurde - u.U. Pending-Irqs doppelt abgearbeitet (z.B. fuer PCMLINK_INT kontraproduktiv)
 * deshalb: nach jedem do_IRQ() muessen die Pending-Irqs wieder aktualisiert werden
 * zusaetzlich wurde der Code durch Verzicht auf BITMAP stark optimiert!
\*--------------------------------------------------------------------------------*/
static const struct _vix_intr {
    const unsigned long vix_intrs_base;
    const unsigned long vix_num_intrs;
    const unsigned long vix_sh_pend;
    const unsigned long vix_sh_mask;
}  vix_int[] = {
    [2] = { .vix_intrs_base = GIC_VI2_INTRS_BASE, .vix_num_intrs = GIC_VI2_NUM_INTRS , .vix_sh_pend = GIC_VI2_SH_PEND, .vix_sh_mask = GIC_VI2_SH_MASK },
    [3] = { .vix_intrs_base = GIC_VI3_INTRS_BASE, .vix_num_intrs = GIC_VI3_NUM_INTRS , .vix_sh_pend = GIC_VI3_SH_PEND, .vix_sh_mask = GIC_VI3_SH_MASK },
    [4] = { .vix_intrs_base = GIC_VI4_INTRS_BASE, .vix_num_intrs = GIC_VI4_NUM_INTRS , .vix_sh_pend = GIC_VI4_SH_PEND, .vix_sh_mask = GIC_VI4_SH_MASK },
    [5] = { .vix_intrs_base = GIC_VI5_INTRS_BASE, .vix_num_intrs = GIC_VI5_NUM_INTRS , .vix_sh_pend = GIC_VI5_SH_PEND, .vix_sh_mask = GIC_VI5_SH_MASK },
    [6] = { .vix_intrs_base = GIC_VI6_INTRS_BASE, .vix_num_intrs = GIC_VI6_NUM_INTRS , .vix_sh_pend = GIC_VI6_SH_PEND, .vix_sh_mask = GIC_VI6_SH_MASK },
}; 
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void gic_irq_vi(const unsigned int irq_group) {
    unsigned int i, pending, intrmask, intr, irq;
	unsigned long *pending_abs, *intrmask_abs;
	unsigned long *pcpu_mask;

    pending_abs  = (unsigned long *) GIC_REG_ABS_ADDR(SHARED, vix_int[irq_group].vix_sh_pend);
    intrmask_abs = (unsigned long *) GIC_REG_ABS_ADDR(SHARED,vix_int[irq_group].vix_sh_mask);
	pcpu_mask    = (unsigned long *) &pcpu_masks[smp_processor_id()].pcpu_mask[vix_int[irq_group].vix_intrs_base >> 5];
    
	for (i = 0; i < BITS_TO_LONGS(vix_int[irq_group].vix_num_intrs); i++) {
        for(;;) {
            GICREAD(*pending_abs, pending);
            GICREAD(*intrmask_abs, intrmask);
            pending &= intrmask;
            pending &= pcpu_mask[i];
            if((intr = ffs(pending)) == 0) {
                /*--- nothing pending on this irq-part ---*/
                break;
            }
            irq = ((intr - 1) + (i * BITS_PER_LONG)) + gic_irq_base + vix_int[irq_group].vix_intrs_base;
#if defined(CONFIG_AVM_SIMPLE_PROFILING)
            avm_simple_profiling_enter_irq(irq);
#endif/*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/
            do_IRQ(irq);
#if defined(CONFIG_AVM_SIMPLE_PROFILING)
            avm_simple_profiling_leave_irq(irq);
#endif/*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/
        }
		pending_abs++;
		intrmask_abs++;
    }
}    
#define GIC_VIx_IRQ_DISPATCH(x)						\
void gic_shared_irq_vi ## x ##_dispatch(void)				\
{									\
    avm_cpu_wait_end(); \
    gic_irq_vi(x); \
}
#else/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
/*--------------------------------------------------------------------------------*\
 * do not use - do not use - do not use - do not use - do not use - do not use
\*--------------------------------------------------------------------------------*/
#define GIC_SHARED_IRQ_DISPATCH(X)					\
do {									\
	unsigned int i, intr;						\
	unsigned long *pcpu_mask;					\
	unsigned long *pending_abs, *intrmask_abs;			\
	DECLARE_BITMAP(pending, GIC_VI##X##_NUM_INTRS);			\
	DECLARE_BITMAP(intrmask, GIC_VI##X##_NUM_INTRS);		\
									\
	/* Get per-cpu bitmaps */					\
	pcpu_mask = pcpu_masks[smp_processor_id()].pcpu_mask;		\
	pending_abs = (unsigned long *) GIC_REG_ABS_ADDR(SHARED,	\
						GIC_VI##X##_SH_PEND);	\
	intrmask_abs = (unsigned long *) GIC_REG_ABS_ADDR(SHARED,	\
						GIC_VI##X##_SH_MASK);	\
	for (i = 0; i < BITS_TO_LONGS(GIC_VI##X##_NUM_INTRS); i++) {	\
		GICREAD(*pending_abs, pending[i]);			\
		GICREAD(*intrmask_abs, intrmask[i]);			\
		pending_abs++;						\
		intrmask_abs++;						\
	}								\
	bitmap_and(pending, pending, intrmask, GIC_VI##X##_NUM_INTRS);	\
	bitmap_and(pending, pending, pcpu_mask				\
		+ (GIC_VI##X##_INTRS_BASE >> 5), GIC_VI##X##_NUM_INTRS);\
	intr = find_first_bit(pending, GIC_VI##X##_NUM_INTRS);		\
	while (intr != GIC_VI##X##_NUM_INTRS) {				\
		do_IRQ(gic_irq_base + GIC_VI##X##_INTRS_BASE + intr);	\
		bitmap_clear(pending, intr, 1);				\
		intr = find_first_bit(pending, GIC_VI##X##_NUM_INTRS);	\
	}								\
} while (0)
    

#define GIC_VIx_IRQ_DISPATCH(x)						\
void gic_shared_irq_vi ## x ##_dispatch(void)				\
{									\
    avm_cpu_wait_end(); \
    GIC_SHARED_IRQ_DISPATCH(x); \
}
#endif/*--- #else ---*//*--- #if defined(CONFIG_AVM_ENHANCED) ---*/

GIC_VIx_IRQ_DISPATCH(2)
GIC_VIx_IRQ_DISPATCH(3)
GIC_VIx_IRQ_DISPATCH(4)
GIC_VIx_IRQ_DISPATCH(5)
GIC_VIx_IRQ_DISPATCH(6)


void gic_local_irq_dispatch(void)
{
	unsigned int intr;
	unsigned long pending, masked;

#if defined(CONFIG_AVM_POWER)
	avm_cpu_wait_end();  /*--- auch wenn es r4k_wait_irqoff gibt: trotzdem aufrufen, um system-load-Ausgabe zu triggern  ---*/ 
#endif/*--- #if defined(CONFIG_AVM_POWER)  ---*/

	GICREAD(GIC_REG(VPE_LOCAL, GIC_VPE_PEND), pending);
	GICREAD(GIC_REG(VPE_LOCAL, GIC_VPE_MASK), masked);

	bitmap_and(&pending, &pending, &masked, GIC_NUM_LOCAL_INTRS);

#if defined(CONFIG_AVM_ENHANCED)
	/*--------------------------------------------------------------------------------*\
	 * we have to ignore NMI and YIELD Events
	 \*--------------------------------------------------------------------------------*/
	for(;;) {
		unsigned int pinmap;
		intr = find_first_bit(&pending, GIC_NUM_LOCAL_INTRS);
		if(intr >= GIC_NUM_LOCAL_INTRS) {
			return;
		}
		GICREAD(GIC_REG_ADDR(VPE_LOCAL, local_irq_to_map_off(intr)), pinmap);
		if(!(pinmap & GIC_MAP_TO_PIN_MSK)) {
			/*--- NMI or YIELD -> do not signalize! ---*/
			DBG_IRQ_GIC_TRC("%s: cpu=%d intr=%u pinmap=%x do not signalize as pending\n", __func__, smp_processor_id(), intr, pinmap);
		} else {
            unsigned int irq = MIPS_GIC_LOCAL_IRQ_BASE + intr;
#if defined(CONFIG_AVM_SIMPLE_PROFILING)
		    avm_simple_profiling_enter_irq(irq);
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/
			do_IRQ(irq);
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
		    avm_simple_profiling_leave_irq(irq);
#endif /*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING) ---*/
		}
		bitmap_clear(&pending, intr, 1);
	}
#else/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
	intr = find_first_bit(&pending, GIC_NUM_LOCAL_INTRS);
	while (intr != GIC_NUM_LOCAL_INTRS) {
		do_IRQ(MIPS_GIC_LOCAL_IRQ_BASE + intr);
		/* go to next pending bit */
		bitmap_clear(&pending, intr, 1);
		intr = find_first_bit(&pending, GIC_NUM_LOCAL_INTRS);
	}
#endif/*--- #else ---*//*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
}

static void gic_mask_local_irq(int irq)
{
	int local_irq = gic_hw_to_local_irq(irq);

	if (local_irq_is_legacy(local_irq)) {
		int i;
		unsigned long flags;

		spin_lock_irqsave(&gic_lock, flags);
		for (i = 0; i < gic_vpes; i++) {
			if (ltq_vpe_run_linux_os(i)) {
				GICWRITE(GIC_REG(VPE_LOCAL,
					GIC_VPE_OTHER_ADDR), i);
				GICWRITE(GIC_REG(VPE_OTHER,
					GIC_VPE_RMASK), 1 << local_irq);
			}
		}
		spin_unlock_irqrestore(&gic_lock, flags);
	} else
		GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_RMASK), 1 << local_irq);
}

static void gic_unmask_local_irq(int irq)
{
	int local_irq = gic_hw_to_local_irq(irq);

	if (local_irq_is_legacy(local_irq)) {
		int i;
		unsigned long flags;

		spin_lock_irqsave(&gic_lock, flags);
		for (i = 0; i < gic_vpes; i++) {
			if (ltq_vpe_run_linux_os(i)) {
				GICWRITE(GIC_REG(VPE_LOCAL,
					GIC_VPE_OTHER_ADDR), i);
				GICWRITE(GIC_REG(VPE_OTHER,
					GIC_VPE_SMASK), 1 << local_irq);
			}
		}
		spin_unlock_irqrestore(&gic_lock, flags);
	} else
		GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_SMASK), 1 << local_irq);
}

static void gic_mask_irq(struct irq_data *d)
{
	unsigned int irq = d->irq - gic_irq_base;
    DBG_IRQ(d->irq, "%s(%u)%s\n", __func__, d->irq, gic_is_local_irq(irq) ? "-> local" : "");

	if (gic_is_local_irq(irq))
		gic_mask_local_irq(irq);
	else
		GIC_CLR_INTR_MASK(irq);
}

static void gic_unmask_irq(struct irq_data *d)
{
	unsigned int irq = d->irq - gic_irq_base;
    DBG_IRQ(d->irq, "%s(%u)%s\n", __func__, d->irq, gic_is_local_irq(irq) ? "-> local" : "");

	if (gic_is_local_irq(irq))
		gic_unmask_local_irq(irq);
	else {
#ifdef CONFIG_PCIE_LANTIQ
		pcie_intx_war(irq);
#endif /* CONFIG_PCIE_LANTIQ */
		GIC_SET_INTR_MASK(irq);
	}
}

void __weak gic_irq_ack(struct irq_data *d)
{
	unsigned int irq = d->irq - gic_irq_base;
    DBG_IRQ(d->irq, "%s(%u)%s%s\n", __func__, d->irq, gic_is_local_irq(irq) ? "-> local" : "", gic_irq_flags[irq] & GIC_TRIG_EDGE ? " (edge)" : "");

	if (gic_is_local_irq(irq))
		gic_mask_local_irq(irq);
	else {
#if !defined(CONFIG_AVM_ENHANCED)
		GIC_CLR_INTR_MASK(irq);
#else/*--- #if !defined(CONFIG_AVM_ENHANCED) ---*/
        /*--------------------------------------------------------------------------------*\
        mbahr@avm: nonsens to mask irq when the generic handle_edge_irq() installed, because
                   nobody unmask the irq after !
        \*--------------------------------------------------------------------------------*/
#endif/*--- #if !defined(CONFIG_AVM_ENHANCED) ---*/

		/* Clear edge detector */
		if (gic_irq_flags[irq] & GIC_TRIG_EDGE) {
			GICWRITE(GIC_REG(SHARED, GIC_SH_WEDGE), irq);
        }
	}
}

void __weak gic_finish_irq(struct irq_data *d)
{
	unsigned int irq = d->irq - gic_irq_base;
    DBG_IRQ(d->irq, "%s(%u)%s%s%s\n", __func__, d->irq, gic_is_local_irq(irq) ? "-> local" : "", irqd_irq_disabled(d) ? " (disabled)" : "",  irqd_irq_masked(d) ? " (masked)" : "");

	if (gic_is_local_irq(irq))
		gic_unmask_local_irq(irq);
	else {
#ifdef CONFIG_PCIE_LANTIQ
		pcie_intx_war(irq);
#endif /* CONFIG_PCIE_LANTIQ */
		/* Fixed the disable_irq /enable_irq with percpu interrupt */
		if (!(irqd_irq_disabled(d) && irqd_irq_masked(d)))
			GIC_SET_INTR_MASK(irq);
	}
}

static int gic_set_type(struct irq_data *d, unsigned int type)
{
	unsigned int irq = d->irq - gic_irq_base;
	unsigned long flags;
	bool is_edge;

	if (gic_is_local_irq(irq))
		return -EINVAL;
    DBG_IRQ_GIC_TRC("%s intr=%3d type %s\n", __func__, irq, 
        (type & IRQ_TYPE_SENSE_MASK) == IRQ_TYPE_EDGE_FALLING ? "IRQ_TYPE_EDGE_FALLING" :  
        (type & IRQ_TYPE_SENSE_MASK) == IRQ_TYPE_EDGE_RISING  ? "IRQ_TYPE_EDGE_RISING " :  
        (type & IRQ_TYPE_SENSE_MASK) == IRQ_TYPE_EDGE_BOTH    ? "IRQ_TYPE_EDGE_BOTH   " :  
        (type & IRQ_TYPE_SENSE_MASK) == IRQ_TYPE_LEVEL_LOW    ? "IRQ_TYPE_LEVEL_LOW   " : 
                                                                "IRQ_TYPE_LEVEL_HIGH  ");
	spin_lock_irqsave(&gic_lock, flags);
	switch (type & IRQ_TYPE_SENSE_MASK) {
	case IRQ_TYPE_EDGE_FALLING:
		GIC_SET_POLARITY(irq, GIC_POL_NEG);
		GIC_SET_TRIGGER(irq, GIC_TRIG_EDGE);
		GIC_SET_DUAL(irq, GIC_TRIG_DUAL_DISABLE);
		is_edge = true;
		break;
	case IRQ_TYPE_EDGE_RISING:
		GIC_SET_POLARITY(irq, GIC_POL_POS);
		GIC_SET_TRIGGER(irq, GIC_TRIG_EDGE);
		GIC_SET_DUAL(irq, GIC_TRIG_DUAL_DISABLE);
		is_edge = true;
		break;
	case IRQ_TYPE_EDGE_BOTH:
		/* polarity is irrelevant in this case */
		GIC_SET_TRIGGER(irq, GIC_TRIG_EDGE);
		GIC_SET_DUAL(irq, GIC_TRIG_DUAL_ENABLE);
		is_edge = true;
		break;
	case IRQ_TYPE_LEVEL_LOW:
		GIC_SET_POLARITY(irq, GIC_POL_NEG);
		GIC_SET_TRIGGER(irq, GIC_TRIG_LEVEL);
		GIC_SET_DUAL(irq, GIC_TRIG_DUAL_DISABLE);
		is_edge = false;
		break;
	case IRQ_TYPE_LEVEL_HIGH:
	default:
		GIC_SET_POLARITY(irq, GIC_POL_POS);
		GIC_SET_TRIGGER(irq, GIC_TRIG_LEVEL);
		GIC_SET_DUAL(irq, GIC_TRIG_DUAL_DISABLE);
		is_edge = false;
		break;
	}

	if (is_edge) {
		gic_irq_flags[irq] |= GIC_TRIG_EDGE;
		__irq_set_handler_locked(d->irq, handle_edge_irq);
	} else {
		gic_irq_flags[irq] &= ~GIC_TRIG_EDGE;
		__irq_set_handler_locked(d->irq, handle_level_irq);
	}
	spin_unlock_irqrestore(&gic_lock, flags);

	return 0;
}

#ifdef CONFIG_SMP
static int gic_set_affinity(struct irq_data *d, const struct cpumask *cpumask,
			    bool force)
{
	unsigned int irq = (d->irq - gic_irq_base);
	cpumask_t	tmp = CPU_MASK_NONE;
	unsigned long	flags;
	int		cpu;
    DBG_IRQ(d->irq, "%s(%u)\n", __func__, d->irq);

	if (gic_is_local_irq(irq))
		return -EINVAL;

	cpumask_and(&tmp, cpumask, cpu_online_mask);
	if (cpus_empty(tmp))
		return -EINVAL;

	/* Assumption : cpumask refers to a single CPU */
	spin_lock_irqsave(&gic_lock, flags);

	/* Re-route this IRQ */
	GIC_SH_MAP_TO_VPE_SMASK(irq, first_cpu(tmp));

	/* Update the pcpu_masks */
	for_each_possible_cpu(cpu)
		clear_bit(irq, pcpu_masks[cpu].pcpu_mask);
	set_bit(irq, pcpu_masks[first_cpu(tmp)].pcpu_mask);

	cpumask_copy(d->affinity, cpumask);
	spin_unlock_irqrestore(&gic_lock, flags);

	return IRQ_SET_MASK_OK_NOCOPY;
}
#endif

static struct irq_chip gic_irq_controller = {
	.name			=	"MIPS GIC",
	.irq_enable		=	gic_unmask_irq,
#if !defined(CONFIG_AVM_ENHANCED)
	.irq_disable	=	gic_irq_ack, /* mask, Yield war */
#else/*--- #if !defined(CONFIG_AVM_ENHANCED) ---*/
	.irq_disable    =	gic_mask_irq,
#endif/*--- #else ---*//*--- #if !defined(CONFIG_AVM_ENHANCED) ---*/
	.irq_ack		=	gic_irq_ack,
	.irq_mask		=	gic_mask_irq,
	.irq_mask_ack		=	gic_mask_irq,
	.irq_unmask		=	gic_unmask_irq,
	.irq_set_type   =	gic_set_type,
	.irq_eoi		=	gic_finish_irq,
#ifdef CONFIG_SMP
	.irq_set_affinity	=	gic_set_affinity,
#endif
};

int gic_yield_setup(unsigned int cpu, unsigned int pin, unsigned int intr)
{
	int cpux;
    int irq;
	unsigned long flags;
#define GIC_YIELD_IRQ_START	192
#define GIC_YIELD_IRQ_END	199
	/* Sanity check */
    if ((cpu >= nr_cpu_ids) || (pin > 0xF) || (intr < MIPS_GIC_IRQ_BASE))
		return -EINVAL;

    irq = intr - MIPS_GIC_IRQ_BASE;
#if 0
    if (irq < GIC_YIELD_IRQ_START || irq > GIC_YIELD_IRQ_END)
		return -EINVAL;
#endif
	spin_lock_irqsave(&gic_lock, flags);
    GICWRITE(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(irq)),
        GIC_MAP_TO_YQ_MSK | pin);
	/* Setup Intr to CPU mapping */
    GIC_SH_MAP_TO_VPE_SMASK(irq, cpu);

	/* Clear all yield related percpu mask */
	for_each_possible_cpu(cpux)
        clear_bit(irq, pcpu_masks[cpux].pcpu_mask);
	spin_unlock_irqrestore(&gic_lock, flags);
	return 0;
}
#if defined(CONFIG_AVM_ENHANCED)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void __gic_enable_mask_for_cpu(int cpu, int intr) {

    if(gic_is_local_irq(intr)) {
        unsigned int val __maybe_unused;
        int local_irq = gic_hw_to_local_irq(intr);
		GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_OTHER_ADDR), cpu);
		GICWRITE(GIC_REG(VPE_OTHER, GIC_VPE_SMASK), 1 << local_irq);
        wmb();
		GICREAD(GIC_REG(VPE_OTHER, GIC_VPE_MASK), val);
        DBG_IRQ_GIC_TRC("%s: intr=%d(local_irq=%d) cpu=%x GIC_VPE_MASK=%x\n", __func__, intr, local_irq, cpu, val);
    } else {
        GIC_SET_INTR_MASK(intr);
        wmb();
    }
}
/*--------------------------------------------------------------------------------*\
 * never use from irq-context (but from yield)
\*--------------------------------------------------------------------------------*/
void gic_enable_mask_for_cpu(int cpu, int irq) {
    int intr = irq - gic_irq_base;
    __raw_spin_lock(&gic_lock.rlock);
    __gic_enable_mask_for_cpu(cpu, intr);
    __raw_spin_unlock(&gic_lock.rlock);
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void __gic_disable_mask_for_cpu(int cpu, int intr) {

    if(gic_is_local_irq(intr)) {
        unsigned int val __maybe_unused;
        int local_irq = gic_hw_to_local_irq(intr);
		GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_OTHER_ADDR), cpu);
		GICWRITE(GIC_REG(VPE_OTHER, GIC_VPE_RMASK), 1 << local_irq);
        wmb();
		GICREAD(GIC_REG(VPE_OTHER, GIC_VPE_MASK), val);
        DBG_IRQ_GIC_TRC("%s: intr=%d(local_irq=%d) cpu=%x GIC_VPE_MASK=%x\n", __func__, intr, local_irq, cpu, val);
    } else {
        GIC_CLR_INTR_MASK(intr);
        wmb();
    }
}
/*--------------------------------------------------------------------------------*\
 * never use from irq-context (but from yield)
\*--------------------------------------------------------------------------------*/
void gic_write_compare_for_cpu(int cpu, cycle_t cnt) {
    __raw_spin_lock(&gic_lock.rlock);
	GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_OTHER_ADDR), cpu);
	GICWRITE(GIC_REG(VPE_OTHER, GIC_VPE_COMPARE_HI), (int)(cnt >> 32));
	GICWRITE(GIC_REG(VPE_OTHER, GIC_VPE_COMPARE_LO), (int)(cnt & 0xffffffff));
    __raw_spin_unlock(&gic_lock.rlock);
}
/*--------------------------------------------------------------------------------*\
 * never use from irq-context (but from yield)
\*--------------------------------------------------------------------------------*/
void gic_disable_mask_for_cpu(int cpu, int irq) {
    int intr = irq - gic_irq_base;
    __raw_spin_lock(&gic_lock.rlock);
    __gic_disable_mask_for_cpu(cpu, intr);
    __raw_spin_unlock(&gic_lock.rlock);
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void gic_map_pin(int cpu, int intr, unsigned int pin) {
    unsigned int val __maybe_unused;

    if(gic_is_local_irq(intr)) {
        unsigned int local_irq = gic_hw_to_local_irq(intr);

        GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_OTHER_ADDR), cpu);
        GICWRITE(GIC_REG_ADDR(VPE_OTHER, local_irq_to_map_off(local_irq)), pin);

		GICREAD(GIC_REG_ADDR(VPE_OTHER, local_irq_to_map_off(local_irq)), val);
        DBG_IRQ_GIC_TRC("%s: intr=%d (local_irq=%d) cpu=%x pin=%x reg_off:*%x=%x\n", __func__, 
                                                                                intr, local_irq, cpu, pin,
                                                                                local_irq_to_map_off(local_irq), val);
    } else {
        GICWRITE(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), pin);
        /* Setup Intr to CPU mapping */
        GIC_SH_MAP_TO_VPE_SMASK(intr, cpu);

         GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), val);
         DBG_IRQ_GIC_TRC("%s: intr=%d pin=%x reg:*%lx=%x\n", __func__, intr, pin,
                                                             GIC_REG_ABS_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), val);

        GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpu)), val);
        DBG_IRQ_GIC_TRC("%s: intr=%d cpu=%d reg(of=0x%x):*%lx=%x\n", __func__, intr, cpu,
                                                             GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpu),
                                                             GIC_REG_ABS_ADDR(SHARED, GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpu)), val);
    }
}
/*--------------------------------------------------------------------------------*\
 * type:    IRQ_TYPE_EDGE_FALLING
 *          IRQ_TYPE_EDGE_RISING 
 *          IRQ_TYPE_EDGE_BOTH
 *          IRQ_TYPE_LEVEL_LOW  
 *          IRQ_TYPE_LEVEL_HIGH
 *
 *  ret != 0 error
\*--------------------------------------------------------------------------------*/
int gic_map_irq_type(unsigned int irq, unsigned int type) {
    struct irq_data *d = irq_get_irq_data(irq);
    if(!d) {
        return -EINVAL;
    }
    return gic_set_type(d, type);
}
EXPORT_SYMBOL(gic_map_irq_type);
/*--------------------------------------------------------------------------------*\
 * cpu:  cpu to bind
 * irq   (linux-)irqnmb
 * mode: 0 irq
 *       1 nmi
 *       2 yield
 * pin:  if mode==2 (yield): signal (0-15)
 *       if mode==0 (irq):   non-eic: ip0-ip5
\*--------------------------------------------------------------------------------*/
int gic_map_setup(unsigned int cpu, unsigned int irq, unsigned int mode, unsigned int pin) {
	int cpux;
	int intr;
	unsigned int val __maybe_unused;
	unsigned long flags;

    DBG_IRQ_GIC_TRC("%s: cpu=%u irq=%d mode=%s pin=0x%x\n", __func__, cpu, irq, mode == 0 ? "irq" : 
                                                                                mode == 1 ? "nmi" : 
                                                                                mode == 2 ? "yield" : "?" , pin);
	/* Sanity check */
	if (cpu >= nr_cpu_ids) {
        DBG_IRQ_GIC_TRC("%s: -einval cpu=%d\n", __func__, cpu);
		return -EINVAL;
    }
	intr = irq - gic_irq_base;
    if(intr < 0 || (intr > GIC_NUM_INTRS + GIC_NUM_LOCAL_INTRS)) {
        DBG_IRQ_GIC_TRC("%s: -einval intr=%d\n", __func__, intr);
		return -EINVAL;
    }
	switch(mode) {
        case 0: /*IRQ */
            pin = GIC_MAP_TO_PIN_MSK | (pin & 0x3F);
            break;
        case 1: /*NMI */
            pin = GIC_MAP_TO_NMI_MSK;
            break;
        case 2:/* YIELD */
            if(pin > 15) {
                return -EINVAL;
            }
            pin = GIC_MAP_TO_YQ_MSK | pin;
            break;
        default: 
            DBG_IRQ_GIC_TRC("%s: -einval mode=%d\n", __func__, 2);
            return -EINVAL;
    }
	spin_lock_irqsave(&gic_lock, flags);
    gic_map_pin(cpu, intr, pin);

    /* Clear all yield related percpu mask */
    for_each_possible_cpu(cpux) {
        clear_bit(intr, pcpu_masks[cpux].pcpu_mask);
    }
	switch(mode) {
        case 0: /*IRQ */
            /*--- interrupt-mask etc. should be set with default-irq-function  ---*/
            set_bit(intr, pcpu_masks[cpu].pcpu_mask);
            /*--- for irq we have to disable interrupt-mask ---*/
            __gic_disable_mask_for_cpu(cpu, intr);
            break;
        case 1: /*NMI */
        case 2: /* YIELD */
            /*--- also for yield/nmi we have to set interrupt-mask ---*/
            __gic_enable_mask_for_cpu(cpu, intr);
            break;
    }
	spin_unlock_irqrestore(&gic_lock, flags);
    return 0;
}
EXPORT_SYMBOL(gic_map_setup);
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/

static void __init gic_setup_intr(unsigned int intr, unsigned int cpu,
	unsigned int pin, unsigned int polarity, unsigned int trigtype,
	unsigned int flags)
{
    unsigned int val __maybe_unused;
	struct gic_shared_intr_map *map_ptr;
    DBG_IRQ_GIC_TRC("%s: intr=%d cpu=%u pin=%x %s%s%s polarity=%x(%s) trigtype=%x(%s) flags=%x\n", __func__, 
        intr, cpu, pin, 
        pin & GIC_MAP_TO_PIN_MSK            ? "IRQ   "   : "", 
        pin & GIC_MAP_TO_YQ_MSK             ? "YIELD "   : "",
        pin & GIC_MAP_TO_NMI_MSK            ? "NMI   "   : "",
        polarity, polarity == GIC_POL_POS   ? "POS"   : polarity == GIC_POL_NEG    ? "NEG"   : "?  ", 
        trigtype, trigtype == GIC_TRIG_EDGE ? "EDGE " : trigtype == GIC_TRIG_LEVEL ? "LEVEL" : "?    ", 
        flags);
	/* 1 is hardcoded Voice FW VPE1/core0 */
	if (!ltq_vpe_run_linux_os(1) && (flags == GIC_FLAG_MUX))
		return;

	/* Setup Intr to Pin mapping */
	if (pin & GIC_MAP_TO_NMI_MSK) {
		int i;

		GICWRITE(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), pin);
		/* FIXME: hack to route NMI to all cpu's */
		for (i = 0; i < num_possible_cpus(); i += 32) {
			GICWRITE(GIC_REG_ADDR(SHARED,
					  GIC_SH_MAP_TO_VPE_REG_OFF(intr, i)),
				 0xffffffff);
		}
	} else if (pin & GIC_MAP_TO_YQ_MSK) {
		GICWRITE(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), pin);
		/* Setup Intr to CPU mapping */
		GIC_SH_MAP_TO_VPE_SMASK(intr, cpu);

        GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), val);
        DBG_IRQ_GIC_TRC("%s: yield-mapping: intr=%d pin=%x:*%lx=%x\n", __func__, intr, pin,
                                                         GIC_REG_ABS_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), val);

        GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpu)), val);
        DBG_IRQ_GIC_TRC("%s: yield-cpu-mapping: intr=%d cpu=%d reg(of=0x%x):*%lx=%x\n", __func__, intr, cpu,
                                                            GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpu),
                                                            GIC_REG_ABS_ADDR(SHARED, GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpu)), val);
	} else {
		GICWRITE(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)),
			 GIC_MAP_TO_PIN_MSK | pin);
		/* Setup Intr to CPU mapping */
		GIC_SH_MAP_TO_VPE_SMASK(intr, cpu);

        GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), val);
        DBG_IRQ_GIC_TRC("%s: irq-mapping: intr=%d pin=%x reg:*%lx=%x\n", __func__, intr, pin,
                                                         GIC_REG_ABS_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), val);

        GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpu)), val);
        DBG_IRQ_GIC_TRC("%s: irq-cpu-mapping: intr=%d cpu=%d reg(of=0x%x):*%lx=%x\n", __func__, intr, cpu,
                                                            GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpu),
                                                            GIC_REG_ABS_ADDR(SHARED, GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpu)), val);
		if (cpu_has_veic) {
			set_vi_handler(pin + GIC_PIN_TO_VEC_OFFSET,
				gic_eic_irq_dispatch);
			map_ptr = &gic_shared_intr_map
				[pin + GIC_PIN_TO_VEC_OFFSET];
			if (map_ptr->num_shared_intr >= GIC_MAX_SHARED_INTR)
				BUG();
			map_ptr->intr_list[map_ptr->num_shared_intr++] = intr;
		}
	}

	/* Setup Intr Polarity */
	GIC_SET_POLARITY(intr, polarity);

	/* Setup Intr Trigger Type */
	GIC_SET_TRIGGER(intr, trigtype);

	/* Init Intr Masks */
	GIC_CLR_INTR_MASK(intr);

	/* Initialise per-cpu Interrupt software masks */
	set_bit(intr, pcpu_masks[cpu].pcpu_mask);

	if ((flags & GIC_FLAG_TRANSPARENT) && (cpu_has_veic == 0))
		GIC_SET_INTR_MASK(intr);

	if (trigtype == GIC_TRIG_EDGE)
		gic_irq_flags[intr] |= GIC_TRIG_EDGE;
}

static unsigned int local_irq_to_map_off(unsigned int intr)
{
	switch (intr) {
	case GIC_LOCAL_INT_PERFCTR:
		return GIC_VPE_PERFCTR_MAP_OFS;
	case GIC_LOCAL_INT_SWINT0:
		return GIC_VPE_SWINT0_MAP_OFS;
	case GIC_LOCAL_INT_SWINT1:
		return GIC_VPE_SWINT1_MAP_OFS;
	case GIC_LOCAL_INT_FDC:
		return GIC_VPE_FDC_MAP_OFS;
	default:
		return GIC_VPE_MAP_OFS + 4 * (intr);
	}
}

static void __init gic_setup_local_intr(unsigned int intr, unsigned int pin,
	unsigned int flags)
{
	int i;
	struct gic_shared_intr_map *map_ptr;
	unsigned int local_irq = gic_hw_to_local_irq(intr);

	/* Setup Intr to Pin mapping */
	for (i = 0; i < nr_cpu_ids; i++) {
		if (ltq_vpe_run_linux_os(i)) {
			GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_OTHER_ADDR), i);
			if (pin & GIC_MAP_TO_NMI_MSK) {
				GICWRITE(GIC_REG_ADDR(VPE_OTHER,
					local_irq_to_map_off(local_irq)), pin);
			} else {
				GICWRITE(GIC_REG_ADDR(VPE_OTHER,
					local_irq_to_map_off(local_irq)),
						GIC_MAP_TO_PIN_MSK | pin);
			}
			if (!gic_local_irq_is_routable(local_irq))
				continue;
			/* Init Intr Masks */
			GICWRITE(GIC_REG(VPE_OTHER, GIC_VPE_RMASK),
				1 << local_irq);
		}
	}

	if (!(pin & GIC_MAP_TO_NMI_MSK) && cpu_has_veic) {
		set_vi_handler(pin + GIC_PIN_TO_VEC_OFFSET,
			gic_eic_irq_dispatch);
		map_ptr = &gic_shared_intr_map[pin + GIC_PIN_TO_VEC_OFFSET];
		if (map_ptr->num_shared_intr >= GIC_MAX_SHARED_INTR)
			BUG();
		map_ptr->intr_list[map_ptr->num_shared_intr++] = intr;
	}
}

static void __init gic_basic_init(int numintrs, int numvpes,
			struct gic_intr_map *intrmap, int mapsize)
{
	unsigned int i, cpu;
	unsigned int pin_offset = 0;

	board_bind_eic_interrupt = &gic_bind_eic_interrupt;

	/* Setup defaults */
	for (i = 0; i < numintrs; i++) {
		/* Bypass VMB/FW IPI initialed by u-boot/IBL */
		if ((i < GIC_NUM_INTRS) && (i < mapsize)) {
			if (intrmap[i].flags == GIC_FLAG_VMB_IPI)
				continue;
		}
		GIC_SET_POLARITY(i, GIC_POL_POS);
		GIC_SET_TRIGGER(i, GIC_TRIG_LEVEL);
		GIC_CLR_INTR_MASK(i);
		if (i < GIC_NUM_INTRS) {
			gic_irq_flags[i] = 0;
			gic_shared_intr_map[i].num_shared_intr = 0;
			gic_shared_intr_map[i].local_intr_mask = 0;
		}
	}
	/*
	 * In EIC mode, the HW_INT# is offset by (2-1). Need to subtract
	 * one because the GIC will add one (since 0=no intr).
	 */
	if (cpu_has_veic)
		pin_offset = (GIC_CPU_TO_VEC_OFFSET - GIC_PIN_TO_VEC_OFFSET);
	/* Setup specifics */
	for (i = 0; i < mapsize; i++) {
		cpu = intrmap[i].cpunum;
		if (cpu == GIC_UNUSED)
			continue;
		if (gic_is_local_irq(i))
			gic_setup_local_intr(i,
				intrmap[i].pin + pin_offset,
				intrmap[i].flags);
		else
			gic_setup_intr(i,
				intrmap[i].cpunum,
				intrmap[i].pin + pin_offset,
				intrmap[i].polarity,
				intrmap[i].trigtype,
				intrmap[i].flags);
	}
}

void __init gic_init(unsigned long gic_base_addr,
		     unsigned long gic_addrspace_size,
		     struct gic_intr_map *intr_map, unsigned int intr_map_size,
		     unsigned int irqbase)
{
	unsigned int gicconfig;
	int numintrs;

	_gic_base = (unsigned long) ioremap_nocache(gic_base_addr,
						    gic_addrspace_size);
	gic_irq_base = irqbase;

	GICREAD(GIC_REG(SHARED, GIC_SH_CONFIG), gicconfig);
	numintrs = (gicconfig & GIC_SH_CONFIG_NUMINTRS_MSK) >>
		   GIC_SH_CONFIG_NUMINTRS_SHF;
	numintrs = ((numintrs + 1) * 8);

	gic_vpes = (gicconfig & GIC_SH_CONFIG_NUMVPES_MSK) >>
		  GIC_SH_CONFIG_NUMVPES_SHF;
	gic_vpes = gic_vpes + 1;
	gic_basic_init(numintrs, gic_vpes, intr_map, intr_map_size);

	gic_platform_init(GIC_NUM_INTRS + GIC_NUM_LOCAL_INTRS,
		&gic_irq_controller);
}
#if defined(CONFIG_AVM_ENHANCED)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
char *name_of_local_irq(int local_irq) {
    switch(local_irq) {
        case GIC_LOCAL_INT_WD:      return "GIC_LOCAL_INT_WD     ";	/* GIC watchdog */
        case GIC_LOCAL_INT_COMPARE: return "GIC_LOCAL_INT_COMPARE"; /* GIC count and compare timer */
        case GIC_LOCAL_INT_TIMER:   return "GIC_LOCAL_INT_TIMER  "; /* CPU CP0 timer interrupt */
        case GIC_LOCAL_INT_PERFCTR: return "GIC_LOCAL_INT_PERFCTR"; /* CPU performance counter */
        case GIC_LOCAL_INT_SWINT0:  return "GIC_LOCAL_INT_SWINT0 "; /* CPU software interrupt 0 */
        case GIC_LOCAL_INT_SWINT1:  return "GIC_LOCAL_INT_SWINT1 "; /* CPU software interrupt 1 */
        case GIC_LOCAL_INT_FDC:     return "GIC_LOCAL_INT_FDC    "; /* CPU fast debug channel */
    }
    return "GIC_LOCAL_?";
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void show_local_irq_status(struct seq_file *seq) {
    unsigned int local_irq, cpu, pin_map[NR_CPUS][GIC_NUM_LOCAL_INTRS], pending[NR_CPUS], mask[NR_CPUS];
    unsigned long flags; 

    for(cpu = 0; cpu < NR_CPUS; cpu++) {
        spin_lock_irqsave(&gic_lock, flags);
        GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_OTHER_ADDR), cpu);
        for(local_irq = 0; local_irq < GIC_NUM_LOCAL_INTRS; local_irq++) {	
            GICREAD(GIC_REG_ADDR(VPE_OTHER, local_irq_to_map_off(local_irq)), pin_map[cpu][local_irq]);
        }
        GICREAD(GIC_REG(VPE_OTHER, GIC_VPE_PEND), pending[cpu]);
        GICREAD(GIC_REG(VPE_OTHER, GIC_VPE_MASK), mask[cpu]);
        spin_unlock_irqrestore(&gic_lock, flags);
     }
     for(local_irq = 0; local_irq < GIC_NUM_LOCAL_INTRS; local_irq++) {	
         seq_printf(seq,  "%s(%u) ", name_of_local_irq(local_irq), local_irq);
         for(cpu = 0; cpu < NR_CPUS; cpu++) {
             seq_printf(seq,  "CPU%x: %s%s%s(0x%x) %s %s ", cpu,
                    pin_map[cpu][local_irq] & GIC_MAP_TO_PIN_MSK      ? "IRQ   "   : "",
                    pin_map[cpu][local_irq] & GIC_MAP_TO_YQ_MSK       ? "YIELD "   : "",
                    pin_map[cpu][local_irq] & GIC_MAP_TO_NMI_MSK      ? "NMI   "   : "",
                    pin_map[cpu][local_irq] & GIC_MAP_MSK, 
                    (mask[cpu] >> local_irq) & 1                      ? "EN "      : "DIS",
                    (pending[cpu] >> local_irq) & 1                   ? "PEND"     : "    ");
         }
         seq_printf(seq,  "\n");
                                                                 
     }
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned int is_irq_pending(unsigned int intr) {
    unsigned int pending;

    GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_PEND_31_0_OFS + GIC_INTR_OFS(intr)), pending);
    return (pending & (1 <<  GIC_INTR_BIT(intr)))  ? 1 : 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned int is_irq_enabled(unsigned int intr) {
    unsigned int mask;

    GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MASK_31_0_OFS + GIC_INTR_OFS(intr)), mask);
    return (mask & (1 <<  GIC_INTR_BIT(intr))) ? 1 : 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void show_irq_status(unsigned int intr, struct seq_file *seq) {
    unsigned int pin_map, cpux, val, polarity, trigger, dual_en;
    char cpumap[NR_CPUS+1];

    memset(cpumap, 0, sizeof(cpumap));

    GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), pin_map);
    for_each_possible_cpu(cpux) {
        GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_VPE_REG_OFF(intr, cpux)), val);
        cpumap[cpux] = val & GIC_SH_MAP_TO_VPE_REG_BIT(cpux) ?  '0' + cpux : ' ';
    }
    GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_SET_POLARITY_OFS + GIC_INTR_OFS(intr)), polarity);
    GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_SET_TRIGGER_OFS + GIC_INTR_OFS(intr)), trigger);
    GICREAD(GIC_REG_ADDR(SHARED, GIC_SH_SET_DUAL_OFS + GIC_INTR_OFS(intr)), dual_en);
#if 0
printk(KERN_INFO"POL:%x TRIG.%x DUAL %x MASK %x PEND %x\n", 
                    GIC_SH_SET_POLARITY_OFS + GIC_INTR_OFS(intr),
                    GIC_SH_SET_TRIGGER_OFS + GIC_INTR_OFS(intr),
                    GIC_SH_SET_DUAL_OFS + GIC_INTR_OFS(intr),
                    GIC_SH_MASK_31_0_OFS + GIC_INTR_OFS(intr),
                    GIC_SH_PEND_31_0_OFS + GIC_INTR_OFS(intr));
#endif

    seq_printf(seq,  "%3u(%3u) %s %s%s%s(0x%x) %s %s %s %s %s\n", intr + gic_irq_base, intr, 
            cpumap,
            pin_map & GIC_MAP_TO_PIN_MSK                                    ? "IRQ  "    : "",
            pin_map & GIC_MAP_TO_YQ_MSK                                     ? "YIELD"    : "",
            pin_map & GIC_MAP_TO_NMI_MSK                                    ? "NMI  "    : "",
            pin_map & GIC_MAP_MSK, 
            ((polarity >> GIC_INTR_BIT(intr)) & 1) == GIC_POL_POS           ? "POS"      : "NEG",
            ((trigger >> GIC_INTR_BIT(intr)) & 1) == GIC_TRIG_EDGE          ? "EDGE "    : "LEVEL",
            ((dual_en >> GIC_INTR_BIT(intr)) & 1) == GIC_TRIG_DUAL_ENABLE   ? "DUAL"     : "    ",
            is_irq_enabled(intr)                                            ? "ENABLED " : "DISABLED",
            is_irq_pending(intr)                                            ? " PEND"    : "    "
            );
}
/*--------------------------------------------------------------------------------*\
 * ret: negval -> not found/no range/no match 
\*--------------------------------------------------------------------------------*/
static int generic_irq_param_parse(char *string, char *match, int maxval, char *matchstrg1, char *matchstrg2, char *matchstrg3) {
    char *p = string;
    int ret = -1;
    if((p = strstr(string, match))) {
       p += strlen(match);
       while(*p == ' ' || *p == '\t') p++;
       if(matchstrg1 && strncmp(p, matchstrg1, strlen(matchstrg1)) == 0) {
           ret = 0;
       } else if(matchstrg2 && strncmp(p, matchstrg2, strlen(matchstrg2)) == 0) {
           ret = 1;
       } else if(matchstrg3 && strncmp(p, matchstrg3, strlen(matchstrg3)) == 0) {
           ret = 2;
       } else if(*p) {
           sscanf(p, "%d", &ret);
           if(ret > maxval) {
               ret = -1;
           }
       }
    }
    return ret;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int grx_irq_set(char *string, void *priv __maybe_unused) {
    int irq, intr, trigger, mask, pol, dual_en, pinmap, signal, cpu, wedge;
    int local_irq = -1;
    unsigned long flags;
	char buf[768];
	struct seq_file seq;

    irq   = generic_irq_param_parse(string, "irq=", GIC_NUM_INTRS + gic_irq_base, NULL, NULL, NULL);
    if(irq < 0) {
        intr = generic_irq_param_parse(string, "intr=", GIC_NUM_INTRS + GIC_NUM_INTRS, NULL, NULL, NULL);
    } else if(irq >= gic_irq_base) {
        intr = irq - gic_irq_base;
    } else {
        local_irq = irq;
        intr      = -1;
    }
    trigger = generic_irq_param_parse(string, "trigger=", 1, "edge", "level", NULL);
    pol     = generic_irq_param_parse(string, "pol=",     1, "neg", "pos", NULL);
    wedge   = generic_irq_param_parse(string, "wedge=",   1, NULL, NULL, NULL);
    dual_en = generic_irq_param_parse(string, "dual=",    1, NULL, NULL, NULL);
    mask    = generic_irq_param_parse(string, "mask=",    1, NULL, NULL, NULL);
    pinmap  = generic_irq_param_parse(string, "pinmap=",  2, "irq", "nmi", "yield");
    signal  = generic_irq_param_parse(string, "signal=", 15, NULL, NULL, NULL);
    cpu     = generic_irq_param_parse(string, "cpu=", NR_CPUS - 1, NULL, NULL, NULL);
    if(((local_irq < 0) && (intr < 0)) || (strstr(string, "help"))) {
        printk(KERN_ERR "use: irq=<val> trigger=<edge|level> mask=<0|1> dual=<0|1> pol=<neg|pos> pinmap=<irq|nmi|yield> cpu=<nr> wedge=<0|1>\n"); 
        printk(KERN_ERR "also use: intr=<val> without MIPS_GIC_IRQ_BASE-offset(%u)) \n", gic_irq_base);
        printk(KERN_ERR "irq < %d: local irq\n", gic_irq_base);
        return 0;
    }
    if(intr >= 0) {
        if(trigger >= 0) {
           GIC_SET_TRIGGER(intr, trigger);
        }
        if(pol >= 0) {
           GIC_SET_POLARITY(intr, pol);
        }
        if(dual_en >= 0) {
           GIC_SET_DUAL(intr, dual_en);
        }
    }
    if(mask >= 0) {
        if(intr >= 0) {
            if(mask) GIC_SET_INTR_MASK(intr); else GIC_CLR_INTR_MASK(intr); 
        } else {
            spin_lock_irqsave(&gic_lock, flags);
            GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_OTHER_ADDR), cpu);
            if(mask) {
                GICWRITE(GIC_REG(VPE_OTHER, GIC_VPE_SMASK), 1 << local_irq);
            } else {
                GICWRITE(GIC_REG(VPE_OTHER, GIC_VPE_RMASK), 1 << local_irq);
            }
            spin_unlock_irqrestore(&gic_lock, flags);
        }
    }
    if(pinmap >= 0) {
        pinmap = (pinmap == 0) ? GIC_MAP_TO_PIN_MSK : 
                 (pinmap == 1) ? GIC_MAP_TO_NMI_MSK : GIC_MAP_TO_YQ_MSK;
        if(signal >= 0) {
            pinmap |= signal;
        }
        if(intr >= 0) {
            GICWRITE(GIC_REG_ADDR(SHARED, GIC_SH_MAP_TO_PIN(intr)), pinmap);
        } else {
            if(cpu < 0) {
                cpu = smp_processor_id();
            }
            spin_lock_irqsave(&gic_lock, flags);
            GICWRITE(GIC_REG(VPE_LOCAL, GIC_VPE_OTHER_ADDR), cpu);
			GICWRITE(GIC_REG_ADDR(VPE_OTHER,  local_irq_to_map_off(local_irq)), pinmap);
            spin_unlock_irqrestore(&gic_lock, flags);
        }
    }
    if(intr >= 0) {
        if(cpu >= 0) {
            GIC_SH_MAP_TO_VPE_SMASK(intr, cpu);
        }
        if(wedge >= 0) {
            GICWRITE(GIC_REG(SHARED, GIC_SH_WEDGE), intr | ((wedge) << 31));
        }
    }
    memset(&seq, 0, sizeof(seq));
	buf[0] = 0;
	seq.buf = buf;
	seq.size = sizeof(buf);
    if(intr >= 0) {
        show_irq_status(intr, &seq);
    } else {
        show_local_irq_status(&seq);
    }
	printk(KERN_ERR"\n%s\n", seq.buf);
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void grx_irq_list(struct seq_file *seq, void *priv){
    int intr;

    show_local_irq_status(seq);
    for(intr = 0; intr < GIC_NUM_INTRS; intr++) {
        show_irq_status(intr, seq);
    }
}
static int nmi_timer_notify(struct notifier_block *self, unsigned long dummy, void *param);
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static struct notifier_block nmi_timer_nb = {
	.notifier_call = nmi_timer_notify,
    .priority	   = INT_MAX - 1,
};
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
void dump_irq_status(void) {
	struct irq_desc *desc;
    int intr, cpu;
	struct seq_file seq;
	char buf[256];
    printk(KERN_ERR"HW-Interrupt Status:\n");
    for(intr = 0; intr < GIC_NUM_INTRS; intr++) {
        int any_count = 0;
        memset(&seq, 0, sizeof(seq));
        seq.buf  = buf;
        buf[0]   = 0;
        seq.size = sizeof(buf);
        desc = irq_to_desc(intr + gic_irq_base);
        if (!desc) {
            continue;
        }
        for_each_online_cpu(cpu) {
            any_count += kstat_irqs_cpu(intr + gic_irq_base, cpu);
        }
        if (!any_count) {
            continue;
        }
        show_irq_status(intr, &seq);
        printk(KERN_ERR"%-20s %10u %s", desc->action ? desc->action->name : "", any_count, seq.buf);
    }
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int nmi_timer_notify(struct notifier_block *self __maybe_unused, unsigned long dummy __maybe_unused, void *param __maybe_unused) {
    dump_irq_status();
	return NOTIFY_OK;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init small_irq_statistic_init(void) {
    /*--- printk(KERN_ERR"%s: init\n", __func__); ---*/
    register_nmi_notifier(&nmi_timer_nb);
    return 0;
}
late_initcall(small_irq_statistic_init);

static struct proc_dir_entry *irqprocdir;
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init avm_irqproc_init(void) {
#define PROC_GPIODIR   "avm/irq"
	irqprocdir = proc_mkdir(PROC_GPIODIR, NULL);
	if(irqprocdir == NULL) {
        return 0;
	}
	add_simple_proc_file( "avm/irq/list", NULL, grx_irq_list, NULL);
	add_simple_proc_file( "avm/irq/set", grx_irq_set, NULL, NULL);
    return 0;
}
late_initcall(avm_irqproc_init);
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
