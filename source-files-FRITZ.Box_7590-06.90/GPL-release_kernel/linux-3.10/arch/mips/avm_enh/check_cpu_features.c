/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 *
 *   checker-function
\*------------------------------------------------------------------------------------------*/
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>

#define AVM_OPTION_CHECK(options, mips_option, cpu_option) if(!!((options) & (mips_option)) != !!(cpu_option)) printk(KERN_ERR"%s: ERROR: option missmatch cpu_has_... : 0x%lx & %s != %s\n", __func__, (unsigned long)options, #mips_option, #cpu_option);

/*--------------------------------------------------------------------------------*\
 * aus Performance-Gruenden (->Compiler-Optimierung wirksam) werden diverse CPU-Features
 * (z.B. cpu_has_llsc) fest gesetzt in cpu-features-override.h
 *  hier wird gecheckt ob diese auch korrekt gesetzt
\*--------------------------------------------------------------------------------*/
static __init int avm_check_cpu_features(void) {
	struct cpuinfo_mips *c = &cpu_data[0];

    AVM_OPTION_CHECK(c->options, MIPS_CPU_TLB		       , cpu_has_tlb       );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_4KEX		       , cpu_has_4kex      );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_3K_CACHE	       , cpu_has_3k_cache  );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_4K_CACHE	       , cpu_has_4k_cache  );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_TX39_CACHE       , cpu_has_tx39_cache);
    AVM_OPTION_CHECK(c->options, MIPS_CPU_FPU		       , cpu_has_fpu       );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_32FPR		       , cpu_has_32fpr     );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_COUNTER	       , cpu_has_counter   );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_WATCH		       , cpu_has_watch     );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_DIVEC		       , cpu_has_divec     );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_VCE		       , cpu_has_vce       );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_CACHE_CDEX_P     , cpu_has_cache_cdex_p);
    AVM_OPTION_CHECK(c->options, MIPS_CPU_CACHE_CDEX_S     , cpu_has_cache_cdex_s);
    AVM_OPTION_CHECK(c->options, MIPS_CPU_MCHECK	       , cpu_has_mcheck    );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_EJTAG	    	   , cpu_has_ejtag     );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_NOFPUEX	       , cpu_has_nofpuex   );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_LLSC		       , cpu_has_llsc      );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_INCLUSIVE_CACHES , cpu_has_inclusive_pcaches);
    AVM_OPTION_CHECK(c->options, MIPS_CPU_PREFETCH	       , cpu_has_prefetch  );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_VINT		       , cpu_has_vint      );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_VEIC		       , cpu_has_veic      );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_ULRI		       , cpu_has_userlocal );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_PCI		       , cpu_has_perf_cntr_intr_bit);
    AVM_OPTION_CHECK(c->options, MIPS_CPU_RIXI		       , cpu_has_rixi      );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_MICROMIPS	       , cpu_has_mmips     );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_SEGMENTS         , cpu_has_segments  );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_EVA              , cpu_has_eva       );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_TLBINV           , cpu_has_tlbinv    );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_CM2              , cpu_has_cm2       );
    AVM_OPTION_CHECK(c->options, MIPS_CPU_CM2_L2SYNC       , cpu_has_cm2_l2sync);

    AVM_OPTION_CHECK(c->icache.flags, MIPS_CACHE_VTAG        , cpu_has_vtag_icache);
    AVM_OPTION_CHECK(c->icache.flags, MIPS_CACHE_ALIASES     , cpu_has_ic_aliases );
    AVM_OPTION_CHECK(c->icache.flags, MIPS_CACHE_IC_F_DC     , cpu_has_ic_fills_f_dc);
    AVM_OPTION_CHECK(c->icache.flags, MIPS_IC_SNOOPS_REMOTE  , cpu_icache_snoops_remote_store);

    AVM_OPTION_CHECK(c->dcache.flags, MIPS_CACHE_VTAG    , cpu_has_vtag_dcache);
    AVM_OPTION_CHECK(c->dcache.flags, MIPS_CACHE_ALIASES , cpu_has_dc_aliases);
    AVM_OPTION_CHECK(c->dcache.flags, MIPS_CACHE_PINDEX  , cpu_has_pindexed_dcache);

    AVM_OPTION_CHECK(c->isa_level, MIPS_CPU_ISA_I   , cpu_has_mips_1);
    AVM_OPTION_CHECK(c->isa_level, MIPS_CPU_ISA_II  , cpu_has_mips_2);
    AVM_OPTION_CHECK(c->isa_level, MIPS_CPU_ISA_III , cpu_has_mips_3);
    AVM_OPTION_CHECK(c->isa_level, MIPS_CPU_ISA_IV  , cpu_has_mips_4);
    AVM_OPTION_CHECK(c->isa_level, MIPS_CPU_ISA_V   , cpu_has_mips_5);

    AVM_OPTION_CHECK(c->isa_level, MIPS_CPU_ISA_M32R1 , cpu_has_mips32r1);
    AVM_OPTION_CHECK(c->isa_level, MIPS_CPU_ISA_M32R2 , cpu_has_mips32r2);

    AVM_OPTION_CHECK(c->isa_level, MIPS_CPU_ISA_M64R1 , cpu_has_mips64r1);
    AVM_OPTION_CHECK(c->isa_level, MIPS_CPU_ISA_M64R2 , cpu_has_mips64r2);

    AVM_OPTION_CHECK(c->ases, MIPS_ASE_VZ,     cpu_has_vz);
    AVM_OPTION_CHECK(c->ases, MIPS_ASE_DSP,    cpu_has_dsp);
    AVM_OPTION_CHECK(c->ases, MIPS_ASE_DSP2P,  cpu_has_dsp2);
    AVM_OPTION_CHECK(c->ases, MIPS_ASE_MIPSMT, cpu_has_mipsmt);
    printk(KERN_ERR"%s: mips-options: 0x%08lx icache.flags 0x%08x dcache.flags 0x%08x isa_level 0x%08x ases %08lx\n", __func__, c->options, c->icache.flags, c->dcache.flags, c->isa_level, c->ases);
    return 0;
}
arch_initcall(avm_check_cpu_features);
