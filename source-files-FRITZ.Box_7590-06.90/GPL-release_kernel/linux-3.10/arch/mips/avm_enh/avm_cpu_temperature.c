#include <linux/avm_power.h>
#include <asm/mach_avm.h>

static void *handle=0;

int GrxTemperaturSensorReadTemperatureCallback(void * handle, void *context, int *value) {
    int temp;
    temp = ( grx_get_chip_temperature(0) + grx_get_chip_temperature(1) ) / 2;
    temp *= 10;
    temp /= 4;
    *value=temp;
    return 0;
}

static int __init register_sensor(void) {
    handle = TemperaturSensorRegister("CPU", GrxTemperaturSensorReadTemperatureCallback, 0);
    if(!handle) {
        printk(KERN_ERR "Could not register CPU Temperature sensor to AVM Power driver.\n");
    }
    return 0;
}
static void __exit deregister_sensor(void) {
    if(handle) {
        TemperaturSensorDeregister(handle);
    }
}
__initcall(register_sensor);
__exitcall(deregister_sensor);
