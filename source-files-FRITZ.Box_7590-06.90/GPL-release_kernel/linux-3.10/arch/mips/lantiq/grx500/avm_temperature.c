#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/delay.h>
#include <lantiq.h>
#include <asm/addrspace.h>
#include <asm/mach_avm.h>



struct temp_mapping{
    uint32_t raw_value;
    int32_t temperature;
};

struct temp_mapping mapping_table[]={
    {3421 ,125 },
    {3437 ,120 },
    {3452 ,115 },
    {3467 ,110 },
    {3482 ,105 },
    {3496 ,100 },
    {3510 ,95 },
    {3524 ,90 },
    {3537 ,85 },
    {3550 ,80 },
    {3563 ,75 },
    {3575 ,70 },
    {3588 ,65 },
    {3600 ,60 },
    {3611 ,55 },
    {3623 ,50 },
    {3634 ,45 },
    {3645 ,40 },
    {3656 ,35 },
    {3667 ,30 },
    {3678 ,25 },
    {3688 ,20 },
    {3698 ,15 },
    {3708 ,10 },
    {3718 ,5 },
    {3728 ,0 },
    {3737 ,-5 },
    {3747 ,-10 },
    {3756 ,-15 },
    {3765 ,-20 },
    {3774 ,-25 },
    {3783 ,-30 },
    {3792 ,-35 },
    {3800 ,-40 }
};
//returns the temperature as fixed point with 2 bits after .
uint32_t convert_raw_to_temperature(struct seq_file *m, uint32_t raw_value){
    int i;
    if(raw_value < 3421){
        if(m){
            seq_printf(m, "[%s] Raw Value=%u smaller than 3421, cannot convert temperature, temperature is above 125°C\n", __FUNCTION__, raw_value); 
        }
        else{
            pr_err("[%s] Raw Value=%u smaller than 3421, cannot convert temperature, temperature is above 125°C\n", __FUNCTION__, raw_value); 
        }
        return 125 << 2;
    }
    if(raw_value > 3800){
        if(m){
            seq_printf(m, "[%s] Raw Value=%u higher than 3800, cannot convert temperature, temperature is below -40°C\n", __FUNCTION__, raw_value); 
        }
        else{
            pr_err("[%s] Raw Value=%u higher than 3800, cannot convert temperature, temperature is below -40°C\n", __FUNCTION__, raw_value); 
        }
        return -40 << 2;
    }
    for(i = 0; i < sizeof(mapping_table)/sizeof(struct temp_mapping); i++){
        //exact match
        if(raw_value == mapping_table[i].raw_value){
            return mapping_table[i].temperature << 2;
        }
        else if(raw_value < mapping_table[i].raw_value){
            //we break here, but need to check afterwards if the next entry might be a potiential exact match
            break;
        }
    }

    if(raw_value == mapping_table[i - 1].raw_value){
        return mapping_table[i - 1].temperature;
    }
    else{
        int32_t diff;
        int32_t diff2;
        uint32_t factor;
        int32_t ret;
        int32_t add;
        //we didn't have an exact match-> interpolate linearly
        diff = mapping_table[i].raw_value - mapping_table[i - 1].raw_value;
        /*--- seq_printf(m, "diff is %d\n", diff); ---*/
        diff2 = mapping_table[i].raw_value - raw_value;
        /*--- seq_printf(m, "diff2 is %d\n", diff2); ---*/
        diff2 = diff2 << 16;
        /*--- seq_printf(m, "diff2 is %d\n", diff2); ---*/
        factor = diff2  / diff;
        /*--- seq_printf(m, "factor is %d\n", factor); ---*/
        add = factor * (mapping_table[i - 1].temperature - mapping_table[i].temperature);
        /*--- seq_printf(m, "add is %d\n", add); ---*/
        add = add >> 14;
        /*--- seq_printf(m, "add is %d\n", add); ---*/
        ret = (mapping_table[i].temperature << 2) + add;
        /*--- seq_printf(m, "ret is %d\n", ret); ---*/
        return ret; 
    }

}

#define LTQ_TEMP_SENSOR                  ((u32 *)(KSEG1 + 0x16080100))
#define TEMP_SENSOR_SOC                  (1 << 0)
#define TEMP_SENSOR_AVG_SEL_SHIFT        1
#define TEMP_SENSOR_AVG_SEL_MASK         0x3
#define TEMP_SENSOR_TSOVH_INT_EN         (1 << 3)
#define TEMP_SENSOR_CH_SEL_SHIFT         8
#define TEMP_SENSOR_CH_SEL_MASK          0x7
#define TEMP_SENSOR_TS                   (1 << 11)
#define TEMP_SENSOR_INT_LVL_SHIFT        16
#define TEMP_SENSOR_INT_LVL_MASK         0xFFFF

#define LTQ_TEMP_SENSOR_DATA ((u32 *)(KSEG1 + 0x16080104))
#define TEMP_SENSOR_DATA_TS_DV  ( 1 << 31)

#define TEMP_SENSOR_DATA_TS(a) ((a) & 0xFFF)


#define LTQ_TEMP_SENSOR_TBIS ((u32 *)(KSEG1 + 0x16080108))

#define MAX_TEMP_CHANNEL        8
static DEFINE_SEMAPHORE(avm_temperature_sema);


/*--- #define DEBUG_AVM_TEMP ---*/
#if defined(DEBUG_AVM_TEMP)
#define DBG_TRC(args...)    printk(KERN_INFO args)
#else/*--- #if defined(DEBUG_AVM_TEMP) ---*/
#define DBG_TRC(args...)
#endif/*--- #else ---*//*--- #if defined(DEBUG_AVM_TEMP) ---*/

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void start_temp_sensor(unsigned int channel) {
    uint32_t sensor_config;  

    DBG_TRC("%s: chan=%u %x data=%x\n", __func__, channel, ltq_r32(LTQ_TEMP_SENSOR), ltq_r32(LTQ_TEMP_SENSOR_DATA));
    ltq_w32(0, LTQ_TEMP_SENSOR_DATA);              /*--- loesche data und ready-bit ---*/

    sensor_config = ltq_r32(LTQ_TEMP_SENSOR);
    sensor_config &= ~((TEMP_SENSOR_AVG_SEL_MASK << TEMP_SENSOR_AVG_SEL_SHIFT) | (TEMP_SENSOR_CH_SEL_MASK << TEMP_SENSOR_CH_SEL_SHIFT)); /*--- reset channel, reset AVG ---*/
    sensor_config |= (channel << TEMP_SENSOR_CH_SEL_SHIFT) | ( 3 << TEMP_SENSOR_AVG_SEL_SHIFT);         /*--- select channel and avg16 ---*/
    sensor_config &= ~TEMP_SENSOR_TS;          /*--- enable sensor ---*/
    sensor_config |=  TEMP_SENSOR_SOC;         /*--- start data-conversion ---*/
    ltq_w32(sensor_config, LTQ_TEMP_SENSOR);

    DBG_TRC("%s: chan=%u %x data=%x done\n", __func__, channel, ltq_r32(LTQ_TEMP_SENSOR), ltq_r32(LTQ_TEMP_SENSOR_DATA));
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void end_temp_sensor(void) {
    uint32_t sensor_config;  
    DBG_TRC("%s: %x data=%x\n", __func__, ltq_r32(LTQ_TEMP_SENSOR), ltq_r32(LTQ_TEMP_SENSOR_DATA));
    /*--- 	disable SOC, SHUT TS ---*/
    sensor_config = ltq_r32(LTQ_TEMP_SENSOR);
    sensor_config &= ~TEMP_SENSOR_SOC;
    sensor_config |=  TEMP_SENSOR_TS;

    ltq_w32(sensor_config, LTQ_TEMP_SENSOR);
    ltq_w32(0, LTQ_TEMP_SENSOR_DATA);              /*--- loesche data und ready-bit ---*/

    DBG_TRC("%s: %x data=%x done\n", __func__, ltq_r32(LTQ_TEMP_SENSOR), ltq_r32(LTQ_TEMP_SENSOR_DATA));
}
/*--------------------------------------------------------------------------------*\
 * oberste Bit gesetzt: ready
 * ABER: dies ist nur fuer SEHR (0.3.us) kurze Zeit gesetzt 
 * -> deshalb sollte es nicht ausgewertet werden
 * der eigentliche Wert ist aber auch danach noch gesetzt - also einfach nur auf Wert ungleich 0 testen ...
\*--------------------------------------------------------------------------------*/
static uint32_t read_temp_sensor(void) {
    return ltq_r32(LTQ_TEMP_SENSOR_DATA);
}
/*--------------------------------------------------------------------------------*\
 * das Busy-Wait dauert relativ lange (400 us), deshalb mit msleep() relaxed
 * weiteres Problem: das Data-Ready-Bit ist dann nur fuer sehr kurze Zeit gesetzt (0.3 us)
 * der eigentliche Wert ist aber auch danach noch gesetzt - also einfach nur auf Wert ungleich 0 testen ...
\*--------------------------------------------------------------------------------*/
static int grx_get_chip_temperature_raw(int channel) {
    uint32_t sensor_data;  
#if defined(DEBUG_AVM_TEMP)
    unsigned int ta, te = 0;
#endif/*--- #if defined(DEBUG_AVM_TEMP) ---*/

    if(channel >= MAX_TEMP_CHANNEL){
        //return maximum temperature on wrong channel
        pr_err("[%s] Error: Channel Number must not be greater than %u, returning 125 °C\n", __FUNCTION__, MAX_TEMP_CHANNEL - 1);
        return 3421;
    }
	down(&avm_temperature_sema);

#if defined(DEBUG_AVM_TEMP)
    ta = avm_get_cycles();
#endif/*--- #if defined(DEBUG_AVM_TEMP) ---*/
    for(;;){
        start_temp_sensor(channel); /*--- immer neu aufsetzen: falls uns ltq_temp dazwischen gefunkt hat: eigentlich sollte der Treiber aber raus! ---*/
        msleep(1);  /*--- es dauert etwa 400 us bis Temperatur anliegt ---*/
        sensor_data = read_temp_sensor();
        if(TEMP_SENSOR_DATA_TS(sensor_data)){
            /*--- wir schauen nicht auf das obere Bit da dieses nur kurz anliegend ---*/
#if defined(DEBUG_AVM_TEMP)
            te = avm_get_cycles();
#endif/*--- #if defined(DEBUG_AVM_TEMP) ---*/
            break;
        }
        end_temp_sensor();
    }
    end_temp_sensor();
    DBG_TRC("%s(%u) sensor_data=%x %u\n", __FUNCTION__, channel, sensor_data, te - ta);
    up(&avm_temperature_sema);
    return TEMP_SENSOR_DATA_TS(sensor_data);
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init grx_chip_temperature_init(void) {
    uint32_t sensor_config;
    DBG_TRC("%s()\n", __FUNCTION__);
    sensor_config = ltq_r32(LTQ_TEMP_SENSOR);
    /*--- workaround: TS_EN needs to be toggled after reset. ---*/
    sensor_config |= TEMP_SENSOR_TS;
    ltq_w32(sensor_config, LTQ_TEMP_SENSOR);

	sensor_config &= ~(TEMP_SENSOR_TS  | TEMP_SENSOR_SOC | TEMP_SENSOR_TSOVH_INT_EN);
    ltq_w32(sensor_config, LTQ_TEMP_SENSOR);
    return 0;
}
arch_initcall(grx_chip_temperature_init);

/*--------------------------------------------------------------------------------*\
returns the temperature in fixed point float with two bits behind the comma
\*--------------------------------------------------------------------------------*/
int grx_get_chip_temperature(int channel) {
    int32_t raw_temperature, temp;
    if(channel >= MAX_TEMP_CHANNEL){
        //return maximum temperature on wrong channel
        pr_err("[%s] Error: Channel Number must not be greater than %u, returning 125 °C\n", __FUNCTION__, MAX_TEMP_CHANNEL - 1);
        return 125 << 2;
    }
    raw_temperature = grx_get_chip_temperature_raw(channel);
    temp=convert_raw_to_temperature(NULL, raw_temperature);
    return temp;
}
#ifdef CONFIG_PROC_FS
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int temperature_proc_show(struct seq_file *m, void *v) {
    uint32_t channel;
    int32_t raw_temperature, temp, temp_points;
    seq_printf(m, "Reading Temperatures:\n");
    //enable sensor
    //sensor_config = ltq_r32(LTQ_TEMP_SENSOR);
    for(channel = 0; channel < 2; channel++){
        raw_temperature = grx_get_chip_temperature_raw(channel);
        temp=convert_raw_to_temperature(NULL, raw_temperature);
        //split the fixed point for printout
        temp_points = (temp & 0x3) * 25;
        temp       /= 4;
        seq_printf(m, "Channel %d: %d.%02d °C (Raw Value: %d)\n", channel, temp, temp_points, raw_temperature);
    } 
    return 0;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int temperature_proc_open(struct inode *inode, struct  file *file) {
  return single_open(file, temperature_proc_show, NULL);
}

static const struct file_operations temperature_proc_fops = {
  .owner = THIS_MODULE,
  .open = temperature_proc_open,
  .read = seq_read,
  .llseek = seq_lseek,
  .release = single_release,
};

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init temperature_proc_init(void) {
  proc_create("chip_temperature", 0, NULL, &temperature_proc_fops);
  return 0;
}
late_initcall(temperature_proc_init);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void __exit temperature_proc_exit(void) {
  remove_proc_entry("chip_temperature", NULL);
}
#endif
