#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <asm/prom.h>
#include <linux/avm_hw_config.h>

#include <asm/mach-lantiq/common/ifx_board.h>
#include <asm/mach-lantiq/common/ifx_gpio.h>


#if defined(CONFIG_EARLY_PRINTK)
extern void prom_printf(const char * fmt, ...);
#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_current_hw_config = NULL;
EXPORT_SYMBOL(avm_current_hw_config);


#define GPIO_BOARD_CONFIG_SIZE 100  /*--- Last is reserved for marking end of config ---*/
struct ifx_gpio_ioctl_pin_config g_board_gpio_pin_map[GPIO_BOARD_CONFIG_SIZE];
EXPORT_SYMBOL(g_board_gpio_pin_map);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_get_hw_config_table(void)
{
    //set externally to table generated by device tree
    if(avm_current_hw_config) {
        return avm_current_hw_config;
    }
    pr_err("Error: GPIO Config Table not set from Device Tree\n");
#if defined(CONFIG_EARLY_PRINTK)
    prom_printf("[%s] error: No hardware configuration found\n", __FUNCTION__);
#endif
    BUG_ON(1);
    return NULL;
}
EXPORT_SYMBOL(avm_get_hw_config_table);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int init_gpio_config(void) 
{
    int index;
    struct _avm_hw_config *config;
    
    config = avm_get_hw_config_table();
    if(!config) {
        return -1; // error: no hardware config found!
    }

    index = 0;
    while(config->name) {
        if(config->manufactor_hw_config.manufactor_lantiq_gpio_config.module_id > 0) {
            if(index >= GPIO_BOARD_CONFIG_SIZE - 1) {
#if defined(CONFIG_EARLY_PRINTK)
                prom_printf("[%s] error: Number of GPIOs in hardware config exceeds limit of %d\n", __FUNCTION__, GPIO_BOARD_CONFIG_SIZE);
#endif
                return -1;
            }
            g_board_gpio_pin_map[index].pin       = config->value;
            g_board_gpio_pin_map[index].module_id = config->manufactor_hw_config.manufactor_lantiq_gpio_config.module_id;
            g_board_gpio_pin_map[index].config    = config->manufactor_hw_config.manufactor_lantiq_gpio_config.config;
            /*--- prom_printf("[%s] %d: GPIO config for %s: pin=%d module_id=0x%x config=0x%x\n", __FUNCTION__, index, config->name, config->value, ---*/
                    /*--- g_board_gpio_pin_map[index].module_id,  g_board_gpio_pin_map[index].config); ---*/
            index++;
        }
        config++;
    }

    /*--- writing the end-of-gpio-config marker ---*/
    g_board_gpio_pin_map[index].module_id = IFX_GPIO_PIN_AVAILABLE;

    return 0;
}

