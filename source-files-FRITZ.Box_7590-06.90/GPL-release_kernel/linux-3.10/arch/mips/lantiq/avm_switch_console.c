#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <lantiq.h>
#include <asm/addrspace.h>
#include <linux/uaccess.h>
#ifdef CONFIG_SOC_GRX500_BOOTCORE
#include <grx500_bootcore_cnfg.h>
#include <grx500_bootcore_chipreg.h>
#define IFMUX_CFG 0xb6080120
#endif

#ifdef CONFIG_SOC_GRX500
#define IFMUX_CFG ((u32 __iomem *)(KSEG1 + 0x16080120))
#endif

#define BBTEPUART_EN_SHIFT   (10)
#define VCODEC_UART_EN_SHIFT (12)

#ifdef CONFIG_PROC_FS
static int console_proc_write(struct file *file, const char __user *buf,
				   size_t count, loff_t *ppos) {
    uint32_t tmp;
    char c;
    
    if (!count)
        return -EINVAL;
    if (get_user(c, buf))
        return -EFAULT;
    if (c != '1')
        return -EINVAL;

    pr_warn("Switchting Console\n");
#ifdef CONFIG_SOC_GRX500
    tmp = ltq_r32(IFMUX_CFG);
#endif
#ifdef CONFIG_SOC_GRX500_BOOTCORE
     tmp = MT_RdReg(IFMUX_CFG, 0);
#endif
    tmp &= ~( 0x3 << VCODEC_UART_EN_SHIFT);
#ifdef CONFIG_SOC_GRX500
    tmp |= ( 0x2 << VCODEC_UART_EN_SHIFT);
    ltq_w32(tmp, IFMUX_CFG);
#endif
#ifdef CONFIG_SOC_GRX500_BOOTCORE
    MT_WrReg(IFMUX_CFG, 0, tmp);
#endif
    return count;
}

static const struct file_operations console_proc_fops = {
  .owner = THIS_MODULE,
  .write = console_proc_write,
};

static int __init console_proc_init(void) {
  proc_create("switch_console", S_IWUSR, NULL, &console_proc_fops);
  return 0;
}
late_initcall(console_proc_init);

static void __exit console_proc_exit(void) {
  remove_proc_entry("switch_console", NULL);
}
#endif
