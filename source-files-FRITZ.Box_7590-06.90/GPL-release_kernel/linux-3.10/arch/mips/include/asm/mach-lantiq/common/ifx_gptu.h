#ifndef IFX_GPTU_H
#define IFX_GPTU_H
extern unsigned int ifx_gptu_timer_yield_ack( unsigned int timer, unsigned int reload_random_percent);
extern void ifx_gptu_timer_yield_set_reload(unsigned int timer, unsigned int reload_val, unsigned int restart_timer);
 
#endif /* IFX_GPTU_H */
