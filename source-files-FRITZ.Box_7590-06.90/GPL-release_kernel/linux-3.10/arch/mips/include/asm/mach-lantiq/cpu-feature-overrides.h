/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 */
#ifndef __ASM_MACH_LANTIQ_GRX500_CPU_FEATURE_OVERRIDES_H
#define __ASM_MACH_LANTIQ_GRX500_CPU_FEATURE_OVERRIDES_H

#define cpu_has_tlb		        1
#define cpu_has_tlbinv          1
#define cpu_has_3k_cache	    0
#define cpu_has_6k_cache	    0
#define cpu_has_8k_cache	    0
#define cpu_has_4k_cache	    1
#define cpu_has_tx39_cache	    0
#define cpu_has_octeon_cache	0

#define cpu_has_fpu		        0
#define cpu_has_32fpr		    0

#define cpu_has_mips_2          1
#define cpu_has_mips_3          0
#define cpu_has_mips_4          0
#define cpu_has_mips_5          0

#define cpu_has_mips32r1        1
#define cpu_has_mips32r2        1

#define cpu_has_counter		    1
#define cpu_has_watch		    1

#define cpu_has_llsc		    1

#define cpu_has_mips16		    0
#define cpu_has_smartmips       0
#define cpu_has_rixi		    0
#define cpu_has_mmips		    0
#define cpu_has_userlocal       1

#define cpu_has_vtag_icache	            0
#define cpu_has_vtag_dcache             0
#define cpu_has_ic_aliases              0
#define cpu_has_dc_aliases	            0
#define cpu_has_ic_fills_f_dc	        0
#define cpu_icache_snoops_remote_store	0

#define cpu_has_cm2             1
#define cpu_has_cm2_l2sync      1
#define cpu_has_eva             1

#define cpu_has_dsp             1
#define cpu_has_mipsmt          1


#endif /*--- #ifndef __ASM_MACH_LANTIQ_GRX500_CPU_FEATURE_OVERRIDES_H ---*/
