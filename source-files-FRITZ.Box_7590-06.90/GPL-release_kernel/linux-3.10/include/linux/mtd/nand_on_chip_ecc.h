#ifndef __MTD_NAND_ON_CHIP_ECC_H__
#define __MTD_NAND_ON_CHIP_ECC_H__


extern char* nand_oob_64_mx30lf1ge8ab_str;
extern char* nand_oob_64_mx30lf4ge8ab_str;
extern struct nand_ecclayout nand_oob_64_mx30lf1ge8ab ;
extern struct nand_ecclayout nand_oob_64_mx30lf4ge8ab ;

extern char* nand_oob_64_tc58bvg2s0hta_str;
extern struct nand_ecclayout nand_oob_128_tsh ;


int nand_read_page_nand_on_chip_ecc_mx(struct mtd_info *mtd, struct nand_chip *chip,
			  uint8_t *buf, int oob_required, int page);

int nand_read_subpage_nand_on_chip_ecc_mx(struct mtd_info *mtd, struct nand_chip *chip,
			uint32_t data_offs, uint32_t readlen, uint8_t *bufpoi);

int nand_read_page_nand_on_chip_ecc_tsh(struct mtd_info *mtd, struct nand_chip *chip,
			  uint8_t *buf, int oob_required, int page);

int nand_read_subpage_nand_on_chip_ecc_tsh(struct mtd_info *mtd, struct nand_chip *chip,
			uint32_t data_offs, uint32_t readlen, uint8_t *bufpoi);

#endif 
