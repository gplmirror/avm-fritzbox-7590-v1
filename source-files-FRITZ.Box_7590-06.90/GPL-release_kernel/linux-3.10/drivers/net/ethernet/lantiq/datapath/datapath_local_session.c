/* Reference: http://blog.csdn.net/shichaog/article/details/44572561 */
#include <linux/module.h>
#include <linux/types.h>	/* size_t */
#include <linux/timer.h>
#include <linux/list.h>
#include <linux/types.h>
#include <net/icmp.h>
#include <net/inet_hashtables.h>
#include <net/tcp.h>
#include <net/transp_v6.h>
#include <net/ipv6.h>

#include <net/datapath_api.h>
#include <net/datapath_proc_api.h>
#include "datapath.h"
#include "datapath_local_session.h"

#define TCP_LIST_HASH_SHIFT          8
#define TCP_LIST_HASH_BIT_LENGTH     12
#define TCP_LIST_HASH_MASK           ((1 << (TCP_LIST_HASH_BIT_LENGTH)) - 1)
#define TCP_LIST_NUM                 ((1 << (TCP_LIST_HASH_BIT_LENGTH)) + 1)

#define TCP_HASH_VALUE(ip_last_byte,port)   (((ip_last_byte << 8) | (port & 0x3f) ) & TCP_LIST_HASH_MASK)

typedef union {
	__be32 ip; /*!< the storage buffer for ipv4 */
#ifdef CONFIG_IPV6
	__be32 ip6[4];/*!< the storage buffer for ipv6 */
#endif
} DP_IPADDR;

struct tcp_item {
	struct hlist_node hlist;
	DP_IPADDR saddr, daddr;
	__be16 sport, dport;
	struct sock *sk;
	unsigned long _skb_refdst;
	struct net_device *dev;

};

spinlock_t dp_tcp_lock;
struct hlist_head g_sess_table[TCP_LIST_NUM]; 
struct kmem_cache *tcp_cache;

static inline void tcp_lock(void)
{
	if (unlikely(in_irq())) {
		PR_ERR("Not allowed to call dp tcp_lock in_irq mode\n");
		return ;
	} else
		raw_spin_lock_bh(&dp_tcp_lock.rlock);
}

static inline void tcp_unlock(void)
{
	raw_spin_unlock_bh(&dp_tcp_lock.rlock);
}

/*iph: input
  th   input
  hash: output */
struct tcp_item *_dp_tcp_lookup(struct iphdr *iph, struct tcphdr *th, int *hash)
{
	int index;
	struct tcp_item *p_item = NULL;

	if (iph->version == 4)
		index = TCP_HASH_VALUE((ntohl(iph->saddr)) & 0xF, th->source);
	else
		index = TCP_HASH_VALUE((ntohl(iph->saddr)) & 0xF, th->source); /*need change for futhre */
	PR_INFO("hash=%d (%d)\n", index, TCP_LIST_NUM);

	hlist_for_each_entry(p_item, &g_sess_table[index], hlist) {
		if ((iph->saddr == p_item->saddr.ip) &&
		    (iph->daddr == p_item->daddr.ip) &&
		    (th->source = p_item->sport) &&
		    (th->dest = p_item->dport)) {
			if (hash)
				*hash = index;
			PR_INFO("Found session\n");
			return p_item;
		}
	}
	if (hash)
		*hash = index;

	return NULL;

}

/* return 0: not handled yet.
   return 1: fast handled already and no need to process it any more
*/
int dp_tcp_fast_ok(int ep, struct sk_buff *skb, struct pmac_rx_hdr *pmac)
{
	struct iphdr *iph;
	struct tcphdr *th;

	struct tcp_item *p_item = NULL;

	if (ep) /*directpath-> don't handle it */
		return 0;

	if (!pmac->tcp_h_offset)
		return 0;

	if ((pmac->pkt_type != TCP_OVER_IPV4) &&
	    (pmac->pkt_type != TCP_OVER_IPV6))
		return 0;
	
	iph = (struct iphdr *)(skb->data + sizeof(struct pmac_rx_hdr) + pmac->ip_offset);
	th = (struct tcphdr *)(((void *)iph)  + pmac->tcp_h_offset * 4);

	tcp_lock();
	p_item = _dp_tcp_lookup(iph , th, NULL);
	if (!p_item) {
		tcp_unlock();
		return 0;
	}

	/*try to update skb if possible */
	if (p_item->sk && p_item->_skb_refdst) {
		/*set below TCP layer value */
		PR_INFO("TCP ACCEL...\n");
		skb->sk = p_item->sk;
		skb->_skb_refdst = p_item->_skb_refdst;
		skb->dev = p_item->dev;
		tcp_unlock();
	} else {
		tcp_unlock();
		return 0;
	}
	skb_pull(skb,
		 sizeof(struct pmac_rx_hdr) + pmac->ip_offset);
	skb_reset_network_header(skb);
	skb_pull(skb, pmac->tcp_h_offset * 4);
	skb_reset_transport_header(skb);
	skb->pkt_type = PACKET_HOST;
	skb->ip_summed = CHECKSUM_UNNECESSARY;
	tcp_v4_rcv(skb);
	return 1;
}

int dp_tcp_poll(void)
{
	return 0;
}

int dp_tcp_fast_learn(struct iphdr *iph, struct tcphdr *th, struct sk_buff *skb)
{
	int index = 0;
	struct tcp_item *p_item = NULL;

	tcp_lock();
	p_item = _dp_tcp_lookup(iph, th, &index);
	if (!p_item) {
		/* Not found */
		PR_INFO("New session for hash=%d\n", index);
		p_item = (struct tcp_item *)kmem_cache_alloc(tcp_cache, GFP_ATOMIC);
		if (!p_item) {
			PR_ERR("kmem_cache_alloc failed\n");
			tcp_unlock();
			return -1;
		}
		hlist_add_head((struct hlist_node *)p_item, &g_sess_table[index]);

		/*save session tuple */
		p_item->saddr.ip = iph->saddr;
		p_item->daddr.ip = iph->daddr;
		p_item->sport = th->source;
		p_item->dport = th->dest;
	}
	PR_INFO("update tcp session for session %x/%x -> %x/%x\n", 
		iph->saddr, th->source,
		iph->daddr, th->dest);
	

	/*save tcp lay param*/
	p_item->sk = skb->sk;
	p_item->_skb_refdst = skb->_skb_refdst;
	p_item->dev = skb->dev;
	tcp_unlock();
	return 0;
}

int dp_local_session_fast_init(uint32_t flag)
{
	u32 i;

	spin_lock_init(&dp_tcp_lock);

	PR_INFO("TCP_LIST_NUM=%d\n", TCP_LIST_NUM);

	for (i = 0; i < ARRAY_SIZE(g_sess_table); i++)
		INIT_HLIST_HEAD(&g_sess_table[i]);

	if ((tcp_cache = kmem_cache_create("dp_tcp", 
		sizeof(struct tcp_item), 
		0, 
		SLAB_HWCACHE_ALIGN, NULL)) == NULL) {
		PR_ERR("kmem_cache_create failed for datapath tcp session list.\n");
		return -1;
	}
	return 0;
}

int dp_tcp_fast_exit(uint32_t flag)
{
	return 0;
}
