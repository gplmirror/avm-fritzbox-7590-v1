#ifndef __PPA_IP_PROTOCOL_TYPES_
#define __PPA_IP_PROTOCOL_TYPES_

struct iphdr {
	uint8_t	 version:4,
  		 ihl:4;
	uint8_t	 tos;
	uint16_t tot_len;
	uint16_t id;
	uint16_t frag_off;
	uint8_t	 ttl;
	uint8_t	 protocol;
	uint16_t check;
	uint32_t saddr;
	uint32_t daddr;
};

struct tcphdr {
	uint16_t source;
	uint16_t dest;
	uint32_t seq;
	uint32_t ack_seq;
	uint16_t doff:4,
		 res1:4,
		 cwr:1,
		 ece:1,
		 urg:1,
		 ack:1,
		 psh:1,
		 rst:1,
		 syn:1,
		 fin:1;
	uint16_t window;
	uint16_t check;
	uint16_t urg_ptr;
};

struct udphdr {
	uint16_t source;
	uint16_t dest;
	uint16_t len;
	uint16_t check;
};

struct ip6_addr {
	uint32_t ip[4];
};

struct ipv6hdr {
  uint8_t   version:4,
            priority:4;
  uint8_t   flow_lbl[3];

  uint16_t  payload_len;
  uint8_t   nexthdr;
  uint8_t   hop_limit;

  struct  ip6_addr  saddr;
  struct  ip6_addr  daddr;
};

inline void swa_ipv6_addr_copy(struct ip6_addr* dst, struct ip6_addr* src)
{
	swa_memcpy((void*)dst, (void*)src, sizeof(struct ip6_addr) );
}

#endif
