#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/stat.h>
#include "dlrx_fw_def.h"
#include "dlrx_fw_internal_def.h"
#include "dlrx_dre_api.h"
#include "dlrx_fw_version.h"
#include "dlrx_fw_data_structure_macro.h"

//#include "cycle_counter.h"
#define DLRX_SRAM_PHY_ADDR      0x1F107400

#define DMA_BASE            0xBE104100
#define DMA_CS              (volatile u32*)(DMA_BASE + 0x18)
#define DMA_CCTRL           (volatile u32*)(DMA_BASE + 0x1C)
#define DMA_CDBA            (volatile u32*)(DMA_BASE + 0x20)
#define DMA_CDLEN           (volatile u32*)(DMA_BASE + 0x24)
#define DMA_CIS             (volatile u32*)(DMA_BASE + 0x28)
#define DMA_CIE             (volatile u32*)(DMA_BASE + 0x2C)
#define PPE_TX_CH_NO        3
// Global variables
unsigned int global_debug_flag;
//unsigned int global_debug_print;
unsigned int g_cfg_badr_ce5buf;
unsigned int g_cfg_badr_ce5des;
unsigned int g_cfg_badr_rxpb_ptr_ring;
unsigned int g_cfg_badr_ro_linklist;
unsigned int g_cfg_badr_ro_mainlist;

#if USE_GLOBAL_VARIABLES
unsigned int g_cfg_num_ce5buf;
unsigned int g_cfg_size_shift_ce5buf;
unsigned int g_cfg_num_rxpb_ptr_ring;
unsigned int g_cfg_size_rxpktdes;
unsigned int g_cfg_offset_atten;
#endif

unsigned int g_qca_hw;
unsigned int g_ce5_offset;
unsigned int g_qca_hw_sub_type;

unsigned int g_congestion_timeout_num;
unsigned int g_congestion_drop_flag;
unsigned int dl_kseg0;
unsigned int dl_kseg1;



extern int dlrx_main( void );

static int __init dlrx_fw_init(void)
{
//#if DLRX_GRX330_BOARD_CFG
    unsigned int loop_index;
    unsigned int write_index_address;
//#else
	unsigned int sram_phy_addr = 0;
//#endif

#if DLRX_GRX330_BOARD_CFG
#if defined( USE_CACHED_ADDR) && USE_CACHED_ADDR
    cfg_ctxt_base =(unsigned int*) CACHE_ADDR(DLRX_SRAM_PHY_ADDR);
#else
    cfg_ctxt_base =(unsigned int*) UNCACHE_ADDR(DLRX_SRAM_PHY_ADDR);
#endif
#else
	sram_phy_addr = ppa_dl_dre_get_sram_addr( );
	dl_kseg0 = ppa_dl_dre_get_kseg0( );
	dl_kseg1 = ppa_dl_dre_get_kseg1( );


#if defined( USE_CACHED_ADDR) && USE_CACHED_ADDR
    cfg_ctxt_base =(unsigned int*) CACHE_ADDR(sram_phy_addr);
#else
    cfg_ctxt_base =(unsigned int*)UNCACHE_ADDR(sram_phy_addr);
#endif
#endif

    DLRX_CFG_GLOBAL_fw_ver_id_set_indirect( DLRX_CFG_GLOBAL_BASE, 0 ,DRE_FW_VERSION );
    DLRX_CFG_GLOBAL_fw_feature_set_indirect( DLRX_CFG_GLOBAL_BASE, 0 ,DRE_FW_FEATURE );
    
        
    ppa_dl_dre_fn_register( DRE_MAIN_FN, dlrx_main );
    ppa_dl_dre_fn_register( DRE_GET_VERSION_FN, dlrx_drv_get_revision );
    ppa_dl_dre_fn_register( DRE_RESET_FN, dlrx_drv_reset );
    ppa_dl_dre_fn_register( DRE_GET_MIB_FN, dlrx_drv_get_mib_data );
    ppa_dl_dre_fn_register( DRE_GET_CURMSDU_FN, dlrx_drv_get_cur_rxpb_ptr );
    ppa_dl_dre_fn_register( DRE_SET_MEMBASE_FN ,dlrx_drv_set_mem_base );
    ppa_dl_dre_fn_register( DRE_SET_RXPN_FN ,dlrx_drv_set_wapi_rxpn );
#if DLRX_SUPPORT_UNLOAD
    ppa_dl_dre_fn_register( DRE_SET_DLRX_UNLOAD, dre_drv_set_dlrx_unload );
#endif
    
    printk("Hello, dlrx_fw\n=============\n");
    printk("DRE_Version is 0x%x, DRE_Feature is 0x%x \n", DRE_FW_VERSION,DRE_FW_FEATURE);

    global_debug_flag = 0;
    //global_debug_print = 1;
    g_cfg_badr_ce5buf = DLRX_CFG_CTXT_CE5BUF_cfg_badr_ce5buf_get_indirect(DLRX_CFG_CTXT_CE5BUF_BASE, 0);
    g_cfg_badr_ce5des = DLRX_CFG_CTXT_CE5DES_cfg_badr_ce5des_get_indirect(DLRX_CFG_CTXT_CE5DES_BASE,0);
	

	#if DLRX_GRX330_BOARD_CFG
		//TODO : debug 
		ddr_base =(unsigned int*)0x8f200000;
	#endif
	#if 0
    DLRX_CFG_GLOBAL_dlrx_congestion_bit_timeout_set_indirect(DLRX_CFG_GLOBAL_BASE,0,0x4000);
    DLRX_CFG_GLOBAL_dlrx_timout_count_th_set_indirect(DLRX_CFG_GLOBAL_BASE,0,0x0A);
	#endif
    g_congestion_timeout_num = 0;

    g_cfg_badr_rxpb_ptr_ring = DLRX_CFG_CTXT_RXPB_PTR_RING_cfg_badr_rxpb_ptr_ring_get_indirect( DLRX_CFG_CTXT_RXPB_PTR_RING_BASE, 0 );
    g_cfg_badr_ro_linklist = DLRX_CFG_CTXT_RO_LINKLIST_cfg_badr_ro_linklist_get_indirect(DLRX_CFG_CTXT_RO_LINKLIST_BASE, 0);
    g_cfg_badr_ro_mainlist = DLRX_CFG_CTXT_RO_MAINLIST_cfg_badr_ro_mainlist_get_indirect( DLRX_CFG_CTXT_RO_MAINLIST_BASE, 0);

#if USE_GLOBAL_VARIABLES 
    g_cfg_num_ce5buf = DLRX_CFG_CTXT_CE5BUF_cfg_num_ce5buf_get_indirect(DLRX_CFG_CTXT_CE5BUF_BASE, 0);
    g_cfg_size_shift_ce5buf = DLRX_CFG_CTXT_CE5BUF_cfg_size_shift_ce5buf_get_indirect(DLRX_CFG_CTXT_CE5BUF_BASE, 0);
    g_cfg_num_rxpb_ptr_ring = DLRX_CFG_CTXT_RXPB_PTR_RING_cfg_num_rxpb_ptr_ring_get_indirect( DLRX_CFG_CTXT_RXPB_PTR_RING_BASE, 0 );
    g_cfg_size_rxpktdes = DLRX_CFG_CTXT_RXPB_cfg_size_rxpktdes_get_indirect( DLRX_CFG_CTXT_RXPB_BASE, 0 );
    g_cfg_offset_atten = DLRX_CFG_CTXT_RXPB_cfg_offset_atten_get_indirect( DLRX_CFG_CTXT_RXPB_BASE, 0 );
#endif

    g_qca_hw = DLRX_CFG_GLOBAL_dlrx_qca_hw_get_indirect( DLRX_CFG_GLOBAL_BASE,0);
	g_qca_hw_sub_type = DLRX_CFG_GLOBAL_dlrx_qca_hw_sub_type_get_indirect(DLRX_CFG_GLOBAL_BASE,0);

    if( g_qca_hw == BEELINER_BOARD )
    {
        g_ce5_offset = DLRX_TARGET_CE5_BEELINER;
		if(g_qca_hw_sub_type == SUBTYPE_NONE_BOARD ){
			printk("DLRX FW: Beeliner board detected\n");
		}
		else{
			printk("DLRX FW: Beeliner Cascade board detected\n");
		}
    }
    else
    {
        g_ce5_offset = DLRX_TARGET_CE5_PEREGRINE;
        printk("DLRX FW: Peregrine board detected\n");
    }

//#if DLRX_GRX330_BOARD_CFG
    // Each entry in the rxpb ptr ring is one dword, Multiply GET_NUM_RXPB_PTR_RING by 4 to convert to bytes
    dre_dma_map( g_cfg_badr_rxpb_ptr_ring, (GET_NUM_RXPB_PTR_RING * 4) );
    // Each entry in the ce5 des ring is two dwords, Multiply GET_NUM_CE5BUF by 8 to convert to bytes
    dre_dma_map( g_cfg_badr_ce5des, (GET_NUM_CE5BUF * 2) *4 );
    // Each entry in the ce5 buffer is 512 bytes, Multiply GET_NUM_CE5BUF by 512 
    dre_dma_map( g_cfg_badr_ce5buf, (( GET_NUM_CE5BUF ) * ( 1 << GET_CE5BUF_SIZE_SHIFT )));

    for( loop_index = 0; loop_index < GET_NUM_RXPB_PTR_RING; loop_index++ )
    {
        unsigned int rxpb_ptr;
        rxpb_ptr = CACHE_ADDR( DLRX_RXPB_PTR_RING_rxpb_ptr_get_indirect( (unsigned int *)g_cfg_badr_rxpb_ptr_ring, loop_index ));
        dre_dma_map( rxpb_ptr, HTT_RX_BUF_SIZE );
    }
    write_index_address = (unsigned int)((unsigned int*)CACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE) + 2);  // 2 is offset for the write index
    
#if TEST_GRX350
         *((unsigned int *)UNCACHE_ADDR(write_index_address)) =  512;
#endif
    dre_dma_map( write_index_address, 4 );
	//function1_cycles  = CycleCounter_Create("Rx Ind : Overall");
#if DLRX_GRX330_BOARD_CFG
	*DMA_CS 	= 7;
	*DMA_CCTRL	= 0x30102;
	
	*DMA_CS 	= 7;
	*DMA_CCTRL	= 0x30101;
#endif
    return 0;
}

static void __exit dlrx_fw_exit(void)
{
#if DLRX_SUPPORT_UNLOAD
    // Find duplicate pointer values in the rxpb ring ans set to 0 if duplicate is found
    rxpb_find_duplicate_ptr_value( );
    // Release of the rxpb buffers between read and write pointer
    rxpb_free_buffers_on_unload( );
    // Clear the RO list
    ro_list_clear( );
    // Unregister functions that are already registered with the driver
    dlrx_unregister_driver_fns( );
#endif
    printk("Goodbye, dlrx_fw\n");
}

module_init(dlrx_fw_init);
module_exit(dlrx_fw_exit);
MODULE_AUTHOR("LANTIQ");
MODULE_DESCRIPTION("DRE FW");
MODULE_LICENSE("GPL");
