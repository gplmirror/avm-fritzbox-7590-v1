//*************************************************************************
//                       Lantiq Technologies AG                           *
//              Copyright (c) 2014 Lantiq Technologies AG                 *
//========================================================================*
//        Project: dlrx_fw                                                *
//        Module : dlrx_rxpb_fns                                          *
//------------------------------------------------------------------------*

//*************************************************************************
// History: Version 0.1
// Author : Pradeep Seshadri
//*************************************************************************

//*************************************************************************
// Fixes: None
//*************************************************************************

#include <linux/kernel.h>
#include <linux/module.h>
#include "dlrx_fw_def.h"
#include "dlrx_fw_data_structure.h"
#include "dlrx_fw_data_structure_macro.h"
#include "dlrx_fw_internal_def.h"
#include "dlrx_dre_api.h"
#include "dlrx_fw_macro_adaptor.h"


//**************************************************************************************
//      Function name: rxpb_write_free_buffer_to_rxpb_ptr_ring
//      Description  : Write a free buffer to the end of the rx packet buffer 
//                     pointer ring
//      Parameters   : ce5_buf_ptr : Message buffer pointer
//      Return value : None
//      Functions called by this module:
//                     dlrx_get_current_rxpb_buf_ptr 
//                     dlrx_get_ro_ll_rxpb_ptr    
//      Notes        : Make sure the cfg_badr_rxpb_ptr_ring, rxpb_ptr_write_index,
//                     cfg_badr_rel_msgbuf,pb_ptr_rel_num, cfg_num_rxpb_ptr_ring
//                     is set to the correct value in the context structure before 
//                     calling this function. Check the notes of functions called 
//                     for complete list to be set in the context.
//**************************************************************************************
/* == AVM/CMH 20170508 handle cbm alloc fail == */
#if 1
static int avm_refill_todo_cnt;
#endif

void rxpb_write_free_buffer_to_rxpb_ptr_ring( rxpb_free_ptr_type free_ptr_type )
{
    unsigned int rxpb_buf_ptr = 0;
    /* == AVM/CMH 20170508 handle cbm alloc fail == */
#if 1
    unsigned int rxpb_buf_ptr_vir = 0;
#endif
    unsigned int rxpb_ptr_write_index;
    unsigned int rxpb_ring_base_address;
    unsigned int va_rxpb_buf_ptr;
    unsigned int *tmp_buf_ptr;
    /* == AVM/CMH 20170508 handle cbm alloc fail == */
#if 1
    avm_refill_todo_cnt++;
#endif
	
    unsigned int  rxpb_ptr_read_index;
    rxpb_ring_base_address =UNCACHE_ADDR( DLRX_CFG_CTXT_RXPB_PTR_RING_cfg_badr_rxpb_ptr_ring_get_indirect( (unsigned int*)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE), 0 ));
    rxpb_ptr_write_index = DLRX_CFG_CTXT_RXPB_PTR_RING_rxpb_ptr_write_index_get_indirect( (unsigned int*)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE), 0 );
    switch( free_ptr_type )
    {
        case RECYCLE_RXPB_PTR:
            rxpb_buf_ptr = dlrx_get_current_rxpb_buf_ptr( );
#if TEST_GRX350
		ppa_dl_dre_rxpb_buf_free(	rxpb_buf_ptr);
                rxpb_buf_ptr = VIR_TO_PHY(dlrx_dl_dre_rxpb_buf_alloc());
#endif
        break;
        
        case NEW_RXPB_PTR:
            /* == AVM/CMH 20170508 handle cbm alloc fail == */
#if 0
            rxpb_buf_ptr = VIR_TO_PHY(dlrx_dl_dre_rxpb_buf_alloc());
#else
            rxpb_buf_ptr_vir = dlrx_dl_dre_rxpb_buf_alloc();
            rxpb_buf_ptr = VIR_TO_PHY(rxpb_buf_ptr_vir);
#endif
        break;

        case RECYCLE_LL_RXPB_PTR:
            rxpb_buf_ptr =VIR_TO_PHY(dlrx_get_ro_ll_rxpb_ptr());
#if TEST_GRX350
		ppa_dl_dre_rxpb_buf_free(	rxpb_buf_ptr);
                rxpb_buf_ptr = VIR_TO_PHY(dlrx_dl_dre_rxpb_buf_alloc());
#endif

        break;
        
        default:
            // Should not reach here
        break;
    }
    /* == AVM/CMH 20170508 handle cbm alloc fail == */
#if 0
	if (!rxpb_buf_ptr)
		return;
#else
    if ((free_ptr_type == NEW_RXPB_PTR) && (rxpb_buf_ptr_vir == 0)) {
        printk("AVM/CMH %s:%d cbm alloc failed, retry next time...\n", __func__, __LINE__);
        return;
    }

redo_refill:
    /* we are asserting the physical address not to be NULL */
    BUG_ON((rxpb_buf_ptr & 0x0FFFFFFF) == 0);
#endif

#ifdef USE_CACHED_ADDR
    va_rxpb_buf_ptr=CACHE_ADDR(rxpb_buf_ptr);
#else
	va_rxpb_buf_ptr=UNCACHE_ADDR(rxpb_buf_ptr);
#endif

//test only
rxpb_ptr_read_index = DLRX_CFG_CTXT_RXPB_PTR_RING_rxpb_ptr_read_index_get_indirect((unsigned int *)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE), 0 );

 
    DLRX_RXPB_HDR_msdu_load_status_clear_indirect( (unsigned int *)va_rxpb_buf_ptr, CFG_OFFSET_ATTEN_IN_DWORDS );
    // TODO: 
    dre_dma_map(CACHE_ADDR(rxpb_buf_ptr),MAX_INV_HEADER_LEN);

	tmp_buf_ptr=(unsigned int *)CACHE_ADDR(rxpb_buf_ptr);
//	printk("rxpb_ptr_write_index is 0x%x, reset 1st 256 DW to 0 from rxpb_buf_ptr 0x%x \n",rxpb_ptr_write_index,(unsigned int)tmp_buf_ptr);
//#if	 OPTIMIZE_PERF
#if 1
#else
	 for ( i=0; i<480 ;i++)
		*(tmp_buf_ptr+i)=0;

	dre_dma_map(CACHE_ADDR(rxpb_buf_ptr),HTT_RX_BUF_SIZE);
#endif
    //printk("phy rxpb_buf_ptr is 0x%x, va_rxpb_buf_ptr is 0x%x  uncached address is 0x%x ,  context is 0x%x , 0x%x, 0x%x ,0x%x \n" ,rxpb_buf_ptr,va_rxpb_buf_ptr ,tmp_buf_ptr,
  //	   *(tmp_buf_ptr),*(tmp_buf_ptr+1),*(tmp_buf_ptr+2) ,*(tmp_buf_ptr+3));

//    printk("rxpb_ptr_write_index 0x%x, read_idx 0x%x free_type %u,rxpb_buf_ptr 0x%x skb 0x%x\n",rxpb_ptr_write_index,rxpb_ptr_read_index,free_ptr_type,(unsigned int)rxpb_buf_ptr,*((unsigned int *)va_rxpb_buf_ptr-1));
    DLRX_RXPB_PTR_RING_rxpb_ptr_set_indirect( ( unsigned int *)rxpb_ring_base_address, rxpb_ptr_write_index, rxpb_buf_ptr );
    rxpb_ptr_write_index++;
    rxpb_ptr_write_index = ( rxpb_ptr_write_index % GET_NUM_RXPB_PTR_RING );  // Wrap around to zero if max number reached    

    tmp_buf_ptr =(unsigned int *)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE);
    DLRX_CFG_CTXT_RXPB_PTR_RING_rxpb_ptr_write_index_set_indirect(tmp_buf_ptr, 0, rxpb_ptr_write_index );    

    /* == AVM/CMH 20170508 handle cbm alloc fail == */
#if 1
    avm_refill_todo_cnt--;
    if (avm_refill_todo_cnt) {
        /* oh we need to allocate a new one */
        rxpb_buf_ptr_vir = dlrx_dl_dre_rxpb_buf_alloc();
        if (rxpb_buf_ptr_vir) {
            rxpb_buf_ptr = VIR_TO_PHY(rxpb_buf_ptr_vir);
            goto redo_refill;
        }
    }
#endif
}

#if DLRX_SUPPORT_UNLOAD

//**************************************************************************************
//      Function name: rxpb_find_duplicate_ptr_value
//      Description  : Find duplicate pointer values in the rxpb ring ans set to 0 if duplicate is found
//      Parameters   : None
//      Return value : None
//      Functions called by this module:
//      Notes        : Make sure the cfg_badr_rxpb_ptr_ring, rxpb_ptr_write_index,
//                         rxpb_ptr_read_index is set to the correct value in the context 
//                         structure before calling this function. Check the notes of functions 
//                         called for complete list to be set in the context.
//**************************************************************************************
void rxpb_find_duplicate_ptr_value( void )
{
    unsigned int loop_index_1;
    unsigned int loop_index_2;
    unsigned int rxpb_ptr;
    unsigned int temp_rxpb_ptr;
    unsigned int rxpb_ring_base_address;
    unsigned int rxpb_write_index;
    unsigned int rxpb_read_index;
    unsigned int loop_count;
    unsigned int rxpb_idx;
    unsigned int temp_rxpb_idx;

    rxpb_write_index = DLRX_CFG_CTXT_RXPB_PTR_RING_rxpb_ptr_write_index_get_indirect( (unsigned int*)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE), 0 );
    rxpb_read_index = DLRX_CFG_CTXT_RXPB_PTR_RING_rxpb_ptr_read_index_get_indirect((unsigned int *)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE), 0 );
    rxpb_ring_base_address = UNCACHE_ADDR( DLRX_CFG_CTXT_RXPB_PTR_RING_cfg_badr_rxpb_ptr_ring_get_indirect( (unsigned int*)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE), 0 ));
    //printk("dlrx_fw_exit : Read index %u  Write index %u\n",rxpb_read_index,rxpb_write_index);
    if( rxpb_write_index < rxpb_read_index )
    {
        loop_count = ( GET_NUM_RXPB_PTR_RING + rxpb_write_index ) - rxpb_read_index;
    }
    else
    {
        loop_count = rxpb_write_index - rxpb_read_index;
    }

    // Find duplicate pointer values in the rxpb ring ans set to 0 if duplicate is found
    for(loop_index_1=0; loop_index_1 < loop_count; loop_index_1++)
    {
        rxpb_idx = ( rxpb_read_index + loop_index_1 )% GET_NUM_RXPB_PTR_RING;
        rxpb_ptr = DLRX_RXPB_PTR_RING_rxpb_ptr_get_indirect( ( unsigned int *)rxpb_ring_base_address, rxpb_idx );

        for( loop_index_2=0; loop_index_2 < loop_count; loop_index_2++)
        {
            temp_rxpb_idx = ( rxpb_read_index + loop_index_2 )% GET_NUM_RXPB_PTR_RING;
            if( temp_rxpb_idx == rxpb_idx )
            {
                continue;
            }
            temp_rxpb_ptr = DLRX_RXPB_PTR_RING_rxpb_ptr_get_indirect( ( unsigned int *)rxpb_ring_base_address, temp_rxpb_idx );
            if( temp_rxpb_ptr == rxpb_ptr )
            {
                //printk("Duplicate ptr value found in index %u and %u . Ptr value = 0x%x\n", rxpb_idx,temp_rxpb_idx, rxpb_ptr);
                DLRX_RXPB_PTR_RING_rxpb_ptr_set_indirect( ( unsigned int *)rxpb_ring_base_address, loop_index_2, 0 );
            }
        }
    }

}
//**************************************************************************************
//      Function name: rxpb_free_buffers_on_unload
//      Description  : Release the rxpb buffers between the read pointer and write pointer
//      Parameters   : None
//      Return value : None
//      Functions called by this module:
//      Notes        : Make sure the cfg_badr_rxpb_ptr_ring, rxpb_ptr_write_index,
//                         rxpb_ptr_read_index is set to the correct value in the context 
//                         structure before calling this function. Check the notes of functions 
//                         called for complete list to be set in the context.
//**************************************************************************************
void rxpb_free_buffers_on_unload( void )
{
    unsigned int rxpb_write_index;
    unsigned int rxpb_read_index;
    unsigned int loop_count;
    unsigned int loop_index;
    unsigned int rxpb_ring_base_address;

    rxpb_write_index = DLRX_CFG_CTXT_RXPB_PTR_RING_rxpb_ptr_write_index_get_indirect( (unsigned int*)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE), 0 );
    rxpb_read_index = DLRX_CFG_CTXT_RXPB_PTR_RING_rxpb_ptr_read_index_get_indirect((unsigned int *)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE), 0 );
    rxpb_ring_base_address = UNCACHE_ADDR( DLRX_CFG_CTXT_RXPB_PTR_RING_cfg_badr_rxpb_ptr_ring_get_indirect( (unsigned int*)UNCACHE_ADDR(DLRX_CFG_CTXT_RXPB_PTR_RING_BASE), 0 ));

    if( rxpb_write_index < rxpb_read_index )
    {
        loop_count = ( GET_NUM_RXPB_PTR_RING + rxpb_write_index ) - rxpb_read_index;
    }
    else
    {
        loop_count = rxpb_write_index - rxpb_read_index;
    }

    // Release rxpb buffer
    for(loop_index=0; loop_index < loop_count; loop_index++)
    {
        unsigned int rxpb_ptr;
        unsigned int rxpb_idx;

        rxpb_idx = ( rxpb_read_index + loop_index )% GET_NUM_RXPB_PTR_RING;
        rxpb_ptr = DLRX_RXPB_PTR_RING_rxpb_ptr_get_indirect( ( unsigned int *)rxpb_ring_base_address, rxpb_idx );
        ppa_dl_dre_rxpb_buf_free( rxpb_ptr );
    }

}
#endif  // #if DLRX_SUPPORT_UNLOAD

