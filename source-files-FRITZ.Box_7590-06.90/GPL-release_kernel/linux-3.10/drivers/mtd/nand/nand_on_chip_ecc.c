/*
 */

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/bitops.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/nand_on_chip_ecc.h>

char* nand_oob_64_mx30lf1ge8ab_str = "avm,oob_64_mx30lf1ge8ab";
char* nand_oob_64_mx30lf4ge8ab_str = "avm,oob_64_mx30lf4ge8ab";
char* nand_oob_64_tc58bvg2s0hta_str = "avm,oob_64_tc58bvg2s0hta";

struct nand_ecclayout nand_oob_64_mx30lf1ge8ab = {
    .oobavail = 62,
   	.eccbytes = 0,
	.eccpos = {},
	.oobfree = {
		{.offset = 2, .length = 62}
	}
};

struct nand_ecclayout nand_oob_64_mx30lf4ge8ab = {
    .oobavail = 30,
    .oobfree = {
        {.offset = 0x02, .length = 6},
        {.offset = 0x10, .length = 8},
        {.offset = 0x20, .length = 8},
        {.offset = 0x30, .length = 8}
    }
};

struct nand_ecclayout nand_oob_128_tsh = {
    .oobavail = 126,
	.eccbytes = 0,
	.eccpos = {},
	.oobfree = {
		{.offset = 2, .length = 126}
	}
};

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int nand_on_chip_ecc_status_mx(struct mtd_info *mtd, struct nand_chip *chip)
{
	unsigned int max_bitflips = -1;
	u8 status;

	/* Check Read Status */
	chip->cmdfunc(mtd, NAND_CMD_STATUS, -1, -1);
	status = chip->read_byte(mtd);

	/* timeout */
	if (!(status & NAND_STATUS_READY)) {
		pr_debug("NAND : Time Out!\n");
		return -EIO;
	}

	/* uncorrectable */
	else if (status & NAND_STATUS_FAIL){
		mtd->ecc_stats.failed++;
        pr_err("NAND: Ecc Error Uncorrectable\n");
    }
	/* correctable */
    else {
        /*--- ein oder null Bitfehler - d.h. null Fehler werden immer als 1 Fehler gemeldet ---*/
        if((status & 0x18) == 0){
            //0 or 1 errors corrected
            return 0;
        }
        if((status & 0x18) == 0x10){
            //2 errors corrected
            max_bitflips = 2;
        }
        else if((status & 0x18) == 0x8){
            //2 errors corrected
            max_bitflips = 3;
        }
        else if((status & 0x18) == 0x18){
            //2 errors corrected
            max_bitflips = 4;
        }
        mtd->ecc_stats.corrected += max_bitflips;
        pr_err("NAND: Ecc %d Bits corrected\n", max_bitflips);
    }

	return max_bitflips;
}

/**
 * nand_read_page_raw - [Intern] read raw page data with nand_on_chip_ecc.
 * @mtd: mtd info structure
 * @chip: nand chip info structure
 * @buf: buffer to store read data
 * @oob_required: caller requires OOB data read to chip->oob_poi
 * @page: page number to read
 *
 */

int nand_read_page_nand_on_chip_ecc_mx(struct mtd_info *mtd, struct nand_chip *chip,
			  uint8_t *buf, int oob_required, int page)
{
	unsigned int max_bitflips;

	/* Check Read Status */
	max_bitflips = nand_on_chip_ecc_status_mx(mtd, chip);
	/* Command latch cycle */
    chip->cmd_ctrl(mtd, NAND_CMD_READ0, NAND_NCE | NAND_CLE | NAND_CTRL_CHANGE);
    chip->ecc.read_page_raw(mtd, chip, buf, oob_required, page);

	return max_bitflips;
}
EXPORT_SYMBOL(nand_read_page_nand_on_chip_ecc_mx);

/**
 * nand_read_subpage_subpage_mx - [Intern] read raw page data with nand_on_chip_ecc.
 * @mtd: mtd info structure
 * @chip: nand chip info structure
 * @buf: buffer to store read data
 * @oob_required: caller requires OOB data read to chip->oob_poi
 * @page: page number to read
 *
 */
int nand_read_subpage_nand_on_chip_ecc_mx(struct mtd_info *mtd, struct nand_chip *chip,
			uint32_t data_offs, uint32_t readlen, uint8_t *bufpoi)
{
	unsigned int max_bitflips = 0;
	uint8_t *p;

	/* Check Read Status */
	max_bitflips = nand_on_chip_ecc_status_mx(mtd, chip);

	/* Command latch cycle */
    chip->cmd_ctrl(mtd, NAND_CMD_READ0, NAND_NCE | NAND_CLE | NAND_CTRL_CHANGE);

	if (data_offs != 0)
		chip->cmdfunc(mtd, NAND_CMD_RNDOUT, data_offs, -1);

	p = bufpoi + data_offs;
	chip->read_buf(mtd, p, readlen);

	return max_bitflips;
}
EXPORT_SYMBOL(nand_read_subpage_nand_on_chip_ecc_mx);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int nand_on_chip_ecc_status_tsh(struct mtd_info *mtd, struct nand_chip *chip)
{
	unsigned int max_bitflips = 0;
	u8 status;

	/* Check Read Status */
    chip->cmd_ctrl(mtd, NAND_CMD_STATUS, NAND_NCE | NAND_CLE | NAND_CTRL_CHANGE);
	status = chip->read_byte(mtd);

	/* timeout */
	if (!(status & NAND_STATUS_READY)) {
		pr_debug("NAND : Time Out!\n");
		return -EIO;
	}

	/* uncorrectable */
	else if (status & NAND_STATUS_FAIL){
		mtd->ecc_stats.failed++;
        pr_err("NAND: Ecc Error Uncorrectable Status tsh 0x%x\n", status);
    }
	/* correctable */
    else {
        if (status & NAND_CHIP_READ_STATUS) {
            unsigned int i;
            int eccstatus;

            chip->cmd_ctrl(mtd, 0x7A, NAND_NCE | NAND_CLE | NAND_CTRL_CHANGE);        /*--- NAND_CMD_ECCSTATUS ---*/

            /*-------------------------------------------------------------*\
             * die FLASH-interne ECC wird für je 512 Byte berechnet,
             * wir schreiben aber eine 1Bit ECC für 256 Bytes
            \*-------------------------------------------------------------*/
            for (i = 0; i < (chip->ecc.steps >> 1); i++) {
                eccstatus = chip->read_byte(mtd) & 0xF;
                pr_err("NAND: Ecc Sector %d ecc 0x%x\n", i, eccstatus);

                if (eccstatus < 8) {
                    max_bitflips = max_t(unsigned int, max_bitflips, eccstatus);
                    mtd->ecc_stats.corrected += eccstatus;
                }
            }
            pr_err("NAND: Ecc Sector max_bitflips %d\n", max_bitflips);
        }
    }

	return max_bitflips;
}

/**
 * nand_read_page_nand_on_chip_ecc_tsh - [Intern] read raw page data with nand_on_chip_ecc.
 * @mtd: mtd info structure
 * @chip: nand chip info structure
 * @buf: buffer to store read data
 * @oob_required: caller requires OOB data read to chip->oob_poi
 * @page: page number to read
 *
 */
int nand_read_page_nand_on_chip_ecc_tsh(struct mtd_info *mtd, struct nand_chip *chip,
			  uint8_t *buf, int oob_required, int page)
{
	unsigned int max_bitflips;

	/* Check Read Status */
	max_bitflips = nand_on_chip_ecc_status_tsh(mtd, chip);
	/* Command latch cycle */
    chip->cmd_ctrl(mtd, NAND_CMD_READ0, NAND_NCE | NAND_CLE | NAND_CTRL_CHANGE);
    chip->ecc.read_page_raw(mtd, chip, buf, oob_required, page);
	return max_bitflips;
}
EXPORT_SYMBOL(nand_read_page_nand_on_chip_ecc_tsh);

/**
 * nand_read_subpage_subpage_tsh - [Intern] read raw page data with nand_on_chip_ecc.
 * @mtd: mtd info structure
 * @chip: nand chip info structure
 * @buf: buffer to store read data
 * @oob_required: caller requires OOB data read to chip->oob_poi
 * @page: page number to read
 *
 */
int nand_read_subpage_nand_on_chip_ecc_tsh(struct mtd_info *mtd, struct nand_chip *chip,
			uint32_t data_offs, uint32_t readlen, uint8_t *bufpoi)
{
	unsigned int max_bitflips = 0;
	uint8_t *p;

	/* Check Read Status */
	max_bitflips = nand_on_chip_ecc_status_tsh(mtd, chip);

	/* Command latch cycle */
    chip->cmd_ctrl(mtd, NAND_CMD_READ0, NAND_NCE | NAND_CLE | NAND_CTRL_CHANGE);

	if (data_offs != 0)
		chip->cmdfunc(mtd, NAND_CMD_RNDOUT, data_offs, -1);

	p = bufpoi + data_offs;
	chip->read_buf(mtd, p, readlen);

	return max_bitflips;
}
EXPORT_SYMBOL(nand_read_subpage_nand_on_chip_ecc_tsh);

