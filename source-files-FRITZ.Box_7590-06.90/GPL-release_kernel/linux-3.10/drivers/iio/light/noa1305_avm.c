/*------------------------------------------------------------------------------------------*\
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   author: mhackenberg@avm.de
 *	 description: Driver for Semiconductor® ambient light sensor NOA1305 by AVM
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation version 2 of the License.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
/*--- #include <linux/mutex.h> ---*/
#include <linux/delay.h>
/*--- #include <linux/err.h> ---*/
#include <linux/i2c.h>
#include <linux/iio/iio.h>
#include <linux/iio/sysfs.h>

#define TIMING_TESTS
#ifdef TIMING_TESTS
#include <linux/delay.h>  // XXX for testign
#include <linux/timex.h>  // XXX for testign
#include <asm/mach_avm.h> // XXX for testign
#endif


#define NOA1305_DRIVER_NAME         "avm_noa1305"

#define NOA1305_POWER_CONTROL       0x00
#define NOA1305_RESET               0x01
#define NOA1305_INTEGRATION_TIME    0x02
#define NOA1305_INT_SELECT          0x03
#define NOA1305_INT_THRESH_LSB      0x04  /* Interrupt threshold, least significant bits */
#define NOA1305_INT_THRESH_MSB      0x05  /* Interrupt threshold, most significant bits */
#define NOA1305_ALS_DATA_LSB        0x06  /* ALS measurement data, least significant bits (RO) */
#define NOA1305_ALS_DATA_MSB        0x07  /* ALS measurement data, most significant bits (RO) */
#define NOA1305_DEVICE_ID_LSB       0x08  /* Device ID value, least significant bits (1305 decimal, 0x0519 hex) (RO) */
#define NOA1305_DEVICE_ID_MSB       0x09  /* Device ID value, most significant bits (1305 decimal, 0x0519 hex) (RO) */

/*--- reg 0x00  NOA1305_POWER_CONTROL: ---*/
#define NOA1305_POWER_DOWN          0x00
#define NOA1305_POWER_ON            0x08  /* default */
#define NOA1305_TESTMODE1           0x09  /* reserved */
#define NOA1305_TESTMODE2           0x0A  /* fixed output 0x5555 */
#define NOA1305_TESTMODE3           0x0B  /* fixed output 0xAAAA */

/*--- reg 0x01 - NOA1305_RESET: ---*/
#define NOA1305_RESET_VAL           0x10

/*--- reg 0x02 - NOA1305_INTEGRATION_TIME: ---*/
#define NOA1305_INTEGR_TIME_800     0x00  /* 800 ms continuous measurement */
#define NOA1305_INTEGR_TIME_400     0x01  /* 400 ms continuous measurement */
#define NOA1305_INTEGR_TIME_200     0x02  /* 200 ms continuous measurement (default) */
#define NOA1305_INTEGR_TIME_100     0x03  /* 100 ms continuous measurement */
#define NOA1305_INTEGR_TIME_50      0x04  /* 50  ms continuous measurement */
#define NOA1305_INTEGR_TIME_25      0x05  /* 25  ms continuous measurement */
#define NOA1305_INTEGR_TIME_12_5    0x06  /* 12.5 ms continuous measurement */
#define NOA1305_INTEGR_TIME_6_25    0x07  /* 6.25 ms continuous measurement */

/*--- reg 0x03 - NOA1305_INT_SELECT: ---*/
#define NOA1305_INT_SELECT_RISING   0x01  /* L -> H */
#define NOA1305_INT_SELECT_FALLING  0x02  /* H -> L */
#define NOA1305_INT_SELECT_OFF      0x03  /* inactive (always H) */

/*--- for NOA1305_DEVICE_ID_LSB/MSB (reg 0x08/0x09): ---*/
#define NOA1305_DEVICE_ID           0x519 /* (1305 decimal) */


struct avm_noa1305_chip {
	struct device		*dev;
    struct i2c_client *client;
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- static int avm_noa1305_i2c_read() { ---*/
/*--- } ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- static int avm_noa1305_i2c_write() { ---*/
/*--- } ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int avm_noa1305_read_value(struct i2c_client *client, u8 reg)
{
	if (reg >= 0x10) {
		return -EINVAL;
    }
	return i2c_smbus_read_byte_data(client, reg);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int avm_noa1305_write_value(struct i2c_client *client, u8 reg, u16 value)
{
	if (reg >= 0x10) {
		return -EINVAL;
    }
	return i2c_smbus_write_byte_data(client, reg, value & 0xFF);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void avm_noa1305_reg_dump(struct avm_noa1305_chip *chip, const char *func) 
{
    printk(KERN_ERR "[%s] NOA1305_POWER_CONTROL    = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_POWER_CONTROL   ));
    printk(KERN_ERR "[%s] NOA1305_RESET            = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_RESET           ));
    printk(KERN_ERR "[%s] NOA1305_INTEGRATION_TIME = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_INTEGRATION_TIME));
    printk(KERN_ERR "[%s] NOA1305_INT_SELECT       = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_INT_SELECT      ));
    printk(KERN_ERR "[%s] NOA1305_INT_THRESH_MSB   = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_INT_THRESH_MSB  ));
    printk(KERN_ERR "[%s] NOA1305_INT_THRESH_LSB   = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_INT_THRESH_LSB  ));
    printk(KERN_ERR "[%s] NOA1305_ALS_DATA_MSB     = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_MSB    ));
    printk(KERN_ERR "[%s] NOA1305_ALS_DATA_LSB     = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_LSB    ));
    printk(KERN_ERR "[%s] NOA1305_DEVICE_ID_MSB    = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_DEVICE_ID_MSB   ));
    printk(KERN_ERR "[%s] NOA1305_DEVICE_ID_LSB    = 0x%02x\n", func, avm_noa1305_read_value(chip->client, NOA1305_DEVICE_ID_LSB   ));
}


#ifdef TIMING_TESTS
#define MAC_CYCLE_BUF 100000
    unsigned long avm_noa1305_cycle_buf[MAC_CYCLE_BUF];
    unsigned long avm_noa1305_cycle_buf_val[MAC_CYCLE_BUF];
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_led_test_busy(struct device *dev, struct device_attribute *attr, const char *buf, size_t len)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);
	unsigned long startcycles;
    unsigned long i, startval, value, step;
	if(strict_strtoul(buf, 10, &value)) {
		dev_err(dev, "value has represent the brightness decrease on LED off in percentage between 1%% and 99%%\n");
		return -EINVAL;
    }
    avm_gpio_out_bit(250, 1);
    msleep(1000);

    startval  = avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_LSB);
    startval |= avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_MSB) << 8;
    printk(KERN_ERR "initial brightness: %lu - threshold: %lu\n", startval, (startval * value) / 100);

    avm_gpio_out_bit(250, 0);
    startcycles = avm_get_cycles();

    for(i = 0; i < MAC_CYCLE_BUF; i++) {
        int val;
        val  = avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_LSB);
        val |= avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_MSB) << 8;
        avm_noa1305_cycle_buf[i] = avm_get_cycles();
        avm_noa1305_cycle_buf_val[i] = val;
        if(val < (startval * value) / 100) {
            avm_gpio_out_bit(250, 1);
            break;
        }
//        msleep(1);
    }
    if(i == MAC_CYCLE_BUF) {
        printk(KERN_ERR "did not detect LED off\n");
    } else {
        printk(KERN_ERR "%luus until LED off detected\n", (avm_noa1305_cycle_buf[i] - startcycles) / (avm_get_cyclefreq()/(1000*1000)) );
    }
    step = i > 100 ? i/100 : 1;
    while(i > 0) {
        printk(KERN_ERR "i: %lu cycles: %lu val: %lu\n", i, avm_noa1305_cycle_buf[i], avm_noa1305_cycle_buf_val[i]);
        i -= step;
    }

    return len;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_led_test(struct device *dev, struct device_attribute *attr, const char *buf, size_t len)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);
	unsigned long startjiffies;
    unsigned long i, startval, value;
#define MAX_JBUF 100
    unsigned long jbuf[MAX_JBUF];
    unsigned long jbuf_val[MAX_JBUF];
	if(strict_strtoul(buf, 10, &value)) {
		dev_err(dev, "value has represent the brightness decrease on LED off in percentage between 1%% and 99%%\n");
		return -EINVAL;
    }
    avm_gpio_out_bit(250, 1);
    msleep(1000);
    avm_gpio_out_bit(250, 0);
    msleep(1);
    avm_gpio_out_bit(250, 1);
    msleep(1000);

    startval  = avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_LSB);
    startval |= avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_MSB) << 8;
    printk(KERN_ERR "initial brightness: %lu - threshold: %lu\n", startval, (startval * value) / 100);

    avm_gpio_out_bit(250, 0);
    startjiffies = jiffies;

    for(i = 0; i < MAX_JBUF; i++) {
        int val;
        val  = avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_LSB);
        val |= avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_MSB) << 8;
        jbuf[i] = jiffies;
        jbuf_val[i] = val;
        if(val < (startval * value) / 100) {
            avm_gpio_out_bit(250, 1);
            break;
        }
        msleep(1);
    }
    if(i == MAX_JBUF) {
        printk(KERN_ERR "did not detect LED off\n");
    } else {
        printk(KERN_ERR "%lums until LED off detected\n", ((jbuf[i] - startjiffies)*1000)/HZ);
    }
    while(i > 0) {
        printk(KERN_ERR "jiffies: %lu val: %lu\n", jbuf[i], jbuf_val[i]);
        i--;
    }

    return len;
}
#endif /*--- #ifdef TIMING_TESTS ---*/


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_power_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);

    unsigned int val = avm_noa1305_read_value(chip->client, NOA1305_POWER_CONTROL);
	return sprintf(buf, "%d\n", val);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_power_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t len)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);
	unsigned long value;

	if(strict_strtoul(buf, 10, &value) || (value && ((value & 0xFC) != 0x8))) {
		dev_err(dev, "power has to be an index to of one of the following values:\n"
                     "\t0:  power down\n\t8:  power on\n\t9:  test mode 1\n\t10: test mode 2\n\t11: test mode 3\n");
		return -EINVAL;
    }
    if(value)
        avm_noa1305_write_value(chip->client, NOA1305_POWER_CONTROL, value);
    return len;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_reset_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t len)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);
	unsigned long value;

	if(strict_strtoul(buf, 10, &value)) {
		dev_err(dev, "a number != 0 will reset the ALS_DATA registers\n");
		return -EINVAL;
    }
    if(value)
        avm_noa1305_write_value(chip->client, NOA1305_RESET, NOA1305_RESET_VAL);

    return len;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_brightness_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);
    unsigned int val;
    val  = avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_LSB);
    val |= avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_MSB) << 8;
	return sprintf(buf, "%u\n", val);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_device_id_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);
    unsigned int val;
    val  = avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_LSB);
    val |= avm_noa1305_read_value(chip->client, NOA1305_ALS_DATA_MSB) << 8;
	return sprintf(buf, "%u\n", val);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_intr_thresh_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);

    unsigned int val;
    val  = avm_noa1305_read_value(chip->client, NOA1305_INT_THRESH_LSB);
    val |= avm_noa1305_read_value(chip->client, NOA1305_INT_THRESH_MSB) << 8;
	return sprintf(buf, "%d\n", val);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_intr_thresh_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t len)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);
	unsigned long value;

	if(strict_strtoul(buf, 10, &value) || (value > 0xFFFF)) {
		dev_err(dev, "interrupt threshold has to be in the range of [0 ... 65535]\n");
		return -EINVAL;
    }
    avm_noa1305_write_value(chip->client, NOA1305_INT_THRESH_LSB, value & 0xFF);
    avm_noa1305_write_value(chip->client, NOA1305_INT_THRESH_MSB, (value >> 8) & 0xFF);

    return len;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_intgr_time_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);

    unsigned int val = avm_noa1305_read_value(chip->client, NOA1305_INTEGRATION_TIME);
	return sprintf(buf, "%d\n", val);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_noa1305_intgr_time_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t len)
{
	struct iio_dev *iio_device = dev_to_iio_dev(dev);
	struct avm_noa1305_chip *chip = iio_priv(iio_device);
	unsigned long value;
//	unsigned int new_range;
//	int status;

	if(strict_strtoul(buf, 10, &value) || (value > 7)) {
		dev_err(dev, "integration time has to be an index to of one of the following timings:\n"
                     "\t0: 800 ms\n\t1: 400 ms\n\t2: 200 ms\n\t3: 100 ms\n\t4:  50 ms\n\t5:  25 ms\n\t6: 12.5 ms\n\t7: 6.25 ms\n");
		return -EINVAL;
    }
    avm_noa1305_write_value(chip->client, NOA1305_INTEGRATION_TIME, value);
#if 0
	/*--- if(strict_strtoul(buf, 10, &value) || (value > 0xFFFF)) { ---*/
		/*--- dev_err(dev, "integration value has do be below 65536\n"); ---*/
		/*--- return -EINVAL; ---*/
    /*--- } ---*/

	mutex_lock(&chip->lock);
	status = noa1305_set_range(chip, value, &new_range);
	if (status < 0) {
		mutex_unlock(&chip->lock);
		dev_err(dev,
			"Error in setting max range with err %d\n", status);
		return status;
	}
	chip->range = new_range;
	mutex_unlock(&chip->lock);
#endif
	return len;
}

static IIO_DEVICE_ATTR(reset,       S_IWUGO,           NULL,                         avm_noa1305_reset_store, 0);
static IIO_DEVICE_ATTR(power,       S_IRUGO | S_IWUSR, avm_noa1305_power_show,       avm_noa1305_power_store, 0);
static IIO_DEVICE_ATTR(intgr_time,  S_IRUGO | S_IWUSR, avm_noa1305_intgr_time_show,  avm_noa1305_intgr_time_store, 0);
static IIO_DEVICE_ATTR(intr_thresh, S_IRUGO | S_IWUSR, avm_noa1305_intr_thresh_show, avm_noa1305_intr_thresh_store, 0);
static IIO_DEVICE_ATTR(device_id,   S_IRUGO,           avm_noa1305_device_id_show,   NULL, 0);
static IIO_DEVICE_ATTR(brightness,  S_IRUGO,           avm_noa1305_brightness_show,  NULL, 0);
#ifdef TIMING_TESTS
static IIO_DEVICE_ATTR(led_test,    S_IWUGO,           NULL,                         avm_noa1305_led_test, 0);
static IIO_DEVICE_ATTR(led_test_busy,S_IWUGO,          NULL,                         avm_noa1305_led_test_busy, 0);
#endif

#define NOA1305_DEVICE_ATTR(name) (&iio_dev_attr_##name.dev_attr.attr)
static struct attribute *noa1305_attributes[] = {
	NOA1305_DEVICE_ATTR(power),
	NOA1305_DEVICE_ATTR(reset),
	NOA1305_DEVICE_ATTR(intgr_time),
	NOA1305_DEVICE_ATTR(intr_thresh),
	NOA1305_DEVICE_ATTR(device_id),
	NOA1305_DEVICE_ATTR(brightness),
#ifdef TIMING_TESTS
	NOA1305_DEVICE_ATTR(led_test),
	NOA1305_DEVICE_ATTR(led_test_busy),
#endif
	NULL
};
static const struct attribute_group noa1305_attribute_group = {
	.attrs = noa1305_attributes,
};

static const struct iio_info noa1305_info = {
	.driver_module = THIS_MODULE,
	.attrs = &noa1305_attribute_group,
	/*--- .read_raw = , ---*/
	/*--- .write_raw = , ---*/
};


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int avm_noa1305_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct iio_dev *iio_device;
	struct avm_noa1305_chip *chip;
    unsigned int val;
    int ret;

    printk(KERN_ERR "%s:%d\n", __FUNCTION__, __LINE__);

    iio_device = iio_device_alloc(sizeof(*chip));
	if (!iio_device) {
        printk(KERN_ERR "%s:%d\n", __FUNCTION__, __LINE__);
		return -ENOMEM;
    }

	chip = iio_priv(iio_device);
	chip->client = client;
	i2c_set_clientdata(client, iio_device);

	iio_device->name = NOA1305_DRIVER_NAME;
	iio_device->info = &noa1305_info;
	iio_device->modes = INDIO_DIRECT_MODE;
	iio_device->dev.parent = &client->dev;
	/*--- iio_device->channels = noa1305_channels; ---*/
	/*--- iio_device->num_channels = ARRAY_SIZE(noa1305_channels); ---*/

    avm_noa1305_write_value(client, NOA1305_POWER_CONTROL, NOA1305_POWER_ON);
    val  = avm_noa1305_read_value(chip->client, NOA1305_DEVICE_ID_LSB);
    val |= avm_noa1305_read_value(chip->client, NOA1305_DEVICE_ID_MSB) << 8;
    if(val != NOA1305_DEVICE_ID) {
        printk(KERN_ERR "%s:%d\n", __FUNCTION__, __LINE__);
		dev_err(&client->dev, "found device-id %u does not match expected id %d\n", val, NOA1305_DEVICE_ID);
    }

    ret = iio_device_register(iio_device);
	if(ret < 0) {
        printk(KERN_ERR "%s:%d\n", __FUNCTION__, __LINE__);
		dev_err(&client->dev, "registering %s failed\n", NOA1305_DRIVER_NAME);
        return ret;
    }

    avm_noa1305_write_value(client, NOA1305_RESET, NOA1305_RESET_VAL);
    avm_noa1305_write_value(client, NOA1305_INTEGRATION_TIME, NOA1305_INTEGR_TIME_6_25);
    avm_noa1305_write_value(client, NOA1305_INT_SELECT, NOA1305_INT_SELECT_RISING);
    avm_noa1305_write_value(client, NOA1305_INT_THRESH_LSB, 0xFF);
    avm_noa1305_write_value(client, NOA1305_INT_THRESH_MSB, 0x8F);

    avm_noa1305_reg_dump(chip, __FUNCTION__);

    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int avm_noa1305_remove(struct i2c_client *client)
{
	struct iio_dev *iio_device = i2c_get_clientdata(client);

	iio_device_unregister(iio_device);
	iio_device_free(iio_device);
	return 0;
}



/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static struct i2c_device_id avm_noa1305_id[] = {
	{ NOA1305_DRIVER_NAME, 0},
	{}
};

MODULE_DEVICE_TABLE(i2c, avm_noa1305_id);

static struct i2c_driver avm_noa1305_driver = {
	.driver	 = {
			.name = NOA1305_DRIVER_NAME,
			.owner = THIS_MODULE,
		    },
	.id_table = avm_noa1305_id,
	.probe	 = avm_noa1305_probe,
	.remove	 = avm_noa1305_remove,
};

//module_i2c_driver(avm_noa1305_driver);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __init avm_noa1305_init(void)
{
    int ret = i2c_add_driver(&avm_noa1305_driver);

    printk(KERN_ERR "[%s] i2c_add_driver(&avm_noa1305_driver) = %d\n", __FUNCTION__, ret);

    return ret;
}
subsys_initcall(avm_noa1305_init);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void __exit avm_noa1305_exit(void)
{
    i2c_del_driver(&avm_noa1305_driver);
    printk(KERN_ERR "[%s] i2c_del_driver(&avm_noa1305_driver)\n", __FUNCTION__);
}

// module_init(avm_noa1305_init);
module_exit(avm_noa1305_exit);





MODULE_DESCRIPTION("AVM ambient light driver for NOA1305");
MODULE_AUTHOR("AVM");
MODULE_LICENSE("GPL");


