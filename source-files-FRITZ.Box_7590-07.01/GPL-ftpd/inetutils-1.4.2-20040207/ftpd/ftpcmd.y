/*
 * Copyright (c) 1985, 1988, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)ftpcmd.y	8.3 (Berkeley) 4/6/94
 */

/*
 * Grammar for FTP commands.
 * See RFC 959.
 */

%{

#ifndef lint
static char sccsid[] = "@(#)ftpcmd.y	8.3 (Berkeley) 4/6/94";
#endif /* not lint */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include <netinet/in.h>
#include <arpa/ftp.h>

#include <ctype.h>
#include <errno.h>
#include <pwd.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#ifdef TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#else
# ifdef HAVE_SYS_TIME_H
#  include <sys/time.h>
# else
#  include <time.h>
# endif
#endif
#include <unistd.h>
#include <limits.h>
#ifdef HAVE_SYS_UTSNAME_H
#include <sys/utsname.h>
#endif

#include "charencoding.h"
#include <dirent.h>
#include "virtual.h" /* virtual_chmod virtual_stat virtual_fopen */

#include "extern.h"


/* Include glob.h last, because it may define "const" which breaks
   system headers on some platforms. */
#include <glob.h>



#if ! defined (NBBY) && defined (CHAR_BIT)
#define NBBY CHAR_BIT
#endif

off_t restart_point;

static char cbuf[512];           /* Command Buffer.  */
static char *fromname;
static int cmd_type;
static int cmd_form;
static int cmd_bytesz;

struct tab
{
  const char	*name;
  short	token;
  short	state;
  short	implemented;	/* 1 if command is implemented */
  const char	*help;
};

extern struct tab cmdtab[];
extern struct tab sitetab[];
static char *copy         __P ((char *));
static void help          __P ((struct tab *, char *));
static struct tab *lookup __P ((struct tab *, char *));
static void sizecmd2      __P ((char *));
static int yylex          __P ((void));
static void yyerror       __P ((const char *s));

%}

%union {
	int	i;
	char   *s;
	off_t   o;
}

%token
	letter_A	letter_B	letter_C	letter_E	letter_F	letter_I
	letter_L	letter_N	letter_P	letter_R	letter_S	letter_T

	SP	CRLF	COMMA

	USER	PASS	ACCT	REIN	QUIT	PORT
	PASV	TYPE	STRU	MODE	RETR	STOR
	APPE	MLFL	MAIL	MSND	MSOM	MSAM
	MRSQ	MRCP	ALLO	REST	RNFR	RNTO
	ABOR	DELE	CWD	LIST	NLST	SITE
	STAT	HELP	NOOP	MKD	RMD	PWD
	CDUP	STOU	SMNT	SYST	SIZE	MDTM

	UMASK	IDLE	CHMOD	EPRT	EPSV	QUOTA
	FEAT	OPTS	AUTH	PBSZ	PROT	RRMD

	LEXERR
	

%token	<s> STRING
%token	<i> NUMBER

%type	<i> check_login octal_number byte_size
%type	<i> struct_code mode_code type_code form_code
%type	<s> pathstring pathname password username address_spec param

%start	cmd_list

%%

cmd_list
	: /* empty */
	| cmd_list cmd
		{
			if (fromname != NULL)
				free (fromname);
			fromname = (char *) 0;
			restart_point = (off_t) 0;
		}
	| cmd_list rcmd
	;

cmd
	: USER SP username CRLF
		{
			user($3);
			free($3);
		}
	| PASS SP password CRLF
		{
			pass($3);
			memset ($3, 0, strlen ($3));
			free($3);
		}
	| PORT check_login SP host_port CRLF
		{
			usedefault = 0;
			if (pdata >= 0) {
				(void) close(pdata);
				pdata = -1;
				passive_firewall_close();
			}
			if ($2 && 0 == g_extended_passive_all) {
				if (is_same_addr(&his_addr, &data_dest) &&
					sockaddr_port(&data_dest) > IPPORT_RESERVED) {
					reply (200, "PORT command sucessful.");
				}
				else {
					memset (&data_dest, 0,
						sizeof (data_dest));
					reply(500, "Illegal PORT Command");
				}
			}
		}

	| EPRT check_login SP address_spec CRLF
		{
		usedefault = 0;
		if (pdata >= 0) {
			(void) close(pdata);
			pdata = -1;
			passive_firewall_close();
		}
		if ($2 && 0 == g_extended_passive_all) {
			extended_port($4);
		}
		}

		
	| EPSV check_login CRLF
		{
			if ($2) {
				extended_passive(0);
			}
		}

	| EPSV check_login SP param CRLF
		{
		if ($2) {
			if ($4 && $4[0]) {
				if (!strcasecmp($4, "ALL")) {
					g_extended_passive_all = 1;
					extended_passive(0);
				} else {
					unsigned n;
					if (1 == sscanf($4, "%u", &n)) {
						extended_passive(n);
					} else { 
						reply(500, "Illegal EPSV Command");
					}
				}
			} else {
				extended_passive(0);
			}
		}
		if ($4 != NULL) free($4);
		}


	| PASV check_login CRLF
		{
			if ($2 && 0 == g_extended_passive_all)
				passive();
		}
	| TYPE SP type_code CRLF
		{
			switch (cmd_type) {

			case TYPE_A:
				if (cmd_form == FORM_N) {
					reply(200, "Type set to A.");
					type = cmd_type;
					form = cmd_form;
				} else
					reply(504, "Form must be N.");
				break;

			case TYPE_E:
				reply(504, "Type E not implemented.");
				break;

			case TYPE_I:
				reply(200, "Type set to I.");
				type = cmd_type;
				break;

			case TYPE_L:
#if defined (NBBY) && NBBY == 8
				if (cmd_bytesz == 8) {
					reply(200,
					    "Type set to L (byte size 8).");
					type = cmd_type;
				} else
					reply(504, "Byte size must be 8.");
#else /* NBBY == 8 */
				UNIMPLEMENTED for NBBY != 8
#endif /* NBBY == 8 */
			}
		}
	| STRU SP struct_code CRLF
		{
			switch ($3) {

			case STRU_F:
				reply(200, "STRU F ok.");
				break;

			default:
				reply(504, "Unimplemented STRU type.");
			}
		}
	| MODE SP mode_code CRLF
		{
			switch ($3) {

			case MODE_S:
				reply(200, "MODE S ok.");
				break;

			default:
				reply(502, "Unimplemented MODE type.");
			}
		}
	| ALLO SP NUMBER CRLF
		{
			reply(202, "ALLO command ignored.");
		}
	| ALLO SP NUMBER SP letter_R SP NUMBER CRLF
		{
			reply(202, "ALLO command ignored.");
		}
	| RETR check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				retrieve((char *) 0, $4);
			if ($4 != NULL)
				free($4);
		}
	| STOR check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				store($4, "w", 0);
			if ($4 != NULL)
				free($4);
		}
	| APPE check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				store($4, "a", 0);
			if ($4 != NULL)
				free($4);
		}
	| NLST check_login CRLF
		{
			if ($2)
				send_file_list(".");
		}
	| NLST check_login SP STRING CRLF
		{
			if ($2 && $4 != NULL)
				send_file_list($4);
			if ($4 != NULL)
				free($4);
		}
	| LIST check_login CRLF
		{
			if ($2)
				retrieve("/bin/ls -lgA", "");
		}
	| LIST check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				retrieve("/bin/ls -lgA %s", $4);
			if ($4 != NULL)
				free($4);
		}
	| STAT check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				statfilecmd($4);
			if ($4 != NULL)
				free($4);
		}
	| STAT CRLF
		{
			statcmd();
		}
	| DELE check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				delete($4);
			if ($4 != NULL)
				free($4);
		}
	| RNTO check_login SP pathname CRLF
		{
		    if ($2) {
			if (fromname) {
				renamecmd(fromname, $4);
				free(fromname);
				fromname = (char *) 0;
			} else {
				reply(503, "Bad sequence of commands.");
			}
		    }
		    free ($4);
		}
	| ABOR CRLF
		{
			reply(225, "ABOR command successful.");
		}
	| CWD check_login CRLF
		{
			if ($2)
				cwd(cred.homedir);
		}
	| CWD check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				cwd($4);
			if ($4 != NULL)
				free($4);
		}
	| HELP CRLF
		{
			help(cmdtab, (char *) 0);
		}
	| HELP SP STRING CRLF
		{
			char *cp = $3;

			if (strncasecmp(cp, "SITE", 4) == 0) {
				cp = $3 + 4;
				if (*cp == ' ')
					cp++;
				if (*cp)
					help(sitetab, cp);
				else
					help(sitetab, (char *) 0);
			} else
				help(cmdtab, $3);
			if ($3 != NULL)
			    free ($3);
		}
	| NOOP CRLF
		{
			reply(200, "NOOP command successful.");
		}
	| MKD check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				makedir($4);
			if ($4 != NULL)
				free($4);
		}
	| RMD check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				removedir($4);
			if ($4 != NULL)
				free($4);
		}
	| RRMD check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				removedir_recursive($4);
			if ($4 != NULL)
				free($4);
		}
	| PWD check_login CRLF
		{
			if ($2)
				pwd();
		}
	| CDUP check_login CRLF
		{
			if ($2)
				cwd("..");
		}
	| SITE SP HELP CRLF
		{
			help(sitetab, (char *) 0);
		}
	| SITE SP HELP SP STRING CRLF
		{
			help(sitetab, $5);
			if ($5 != NULL)
			    free ($5);
		}
	| SITE SP UMASK check_login CRLF
		{
			int oldmask;

			if ($4) {
				oldmask = umask(0);
				(void) umask(oldmask);
				reply(200, "Current UMASK is %03o", oldmask);
			}
		}
	| SITE SP UMASK check_login SP octal_number CRLF
		{
			int oldmask;

			if ($4) {
				if (($6 == -1) || ($6 > 0777)) {
					reply(501, "Bad UMASK value");
				} else {
					oldmask = umask($6);
					reply(200,
					    "UMASK set to %03o (was %03o)",
					    $6, oldmask);
				}
			}
		}
	| SITE SP CHMOD check_login SP octal_number SP pathname CRLF
		{
			if ($4 && ($8 != NULL)) {
				if ($6 > 0777)
					reply(501,
				"CHMOD: Mode value must be between 0 and 0777");
				else if (virtual_chmod($8, $6) < 0)
					perror_reply(550, $8);
				else
					reply(200, "CHMOD command successful.");
			}
			if ($8 != NULL)
				free($8);
		}
	| SITE SP IDLE CRLF
		{
			reply(200,
			    "Current IDLE time limit is %d seconds; max %d",
				timeout, maxtimeout);
		}
	| SITE SP IDLE check_login SP NUMBER CRLF
		{
			if ($4) {
			    if ($6 < 30 || $6 > maxtimeout) {
				reply (501,
			"Maximum IDLE time must be between 30 and %d seconds",
					maxtimeout);
			    } else {
				timeout = $6;
				(void) alarm((unsigned) timeout);
				reply(200,
					"Maximum IDLE time set to %d seconds",
					timeout);
			    }
			}
		}
	| SITE SP QUOTA CRLF
		{
			quota();
		}
	| STOU check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				store($4, "w", 1);
			if ($4 != NULL)
				free($4);
		}
	| SYST CRLF
		{
		        const char *sys_type; /* Official rfc-defined os type.  */
			char *version = 0; /* A more specific type. */

#ifdef HAVE_UNAME
			struct utsname u;
			if (uname (&u) == 0) {
				version =
				  malloc (strlen (u.sysname)
					  + 1 + strlen (u.release) + 1);
				if (version)
					sprintf (version, "%s %s",
						 u.sysname, u.release);
		        }
#else
#ifdef BSD
			version = "BSD";
#endif
#endif

#ifdef unix
			sys_type = "UNIX";
#else
			sys_type = "UNKNOWN";
#endif

			if (version)
				reply(215, "%s Type: L%d Version: %s",
				      sys_type, NBBY, version);
			else
				reply(215, "%s Type: L%d", sys_type, NBBY);

#ifdef HAVE_UNAME
			if (version)
				free (version);
#endif
		}

		/*
		 * SIZE is not in RFC959, but Postel has blessed it and
		 * it will be in the updated RFC.
		 *
		 * Return size of file in a format suitable for
		 * using with RESTART (we just count bytes).
		 */
	| SIZE check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL)
				sizecmd2($4);
			if ($4 != NULL)
				free($4);
		}

		/*
		 * MDTM is not in RFC959, but Postel has blessed it and
		 * it will be in the updated RFC.
		 *
		 * Return modification time of file as an ISO 3307
		 * style time. E.g. YYYYMMDDHHMMSS or YYYYMMDDHHMMSS.xxx
		 * where xxx is the fractional second (of any precision,
		 * not necessarily 3 digits)
		 */
	| MDTM check_login SP pathname CRLF
		{
			if ($2 && $4 != NULL) {
				struct stat stbuf;
				if (stat($4, &stbuf) < 0)
					reply(550, "%s: %s",
					    $4, strerror(errno));
				else if (!S_ISREG(stbuf.st_mode)) {
					reply(550, "%s: not a plain file.", $4);
				} else {
					struct tm *t;
					t = gmtime(&stbuf.st_mtime);
					reply(213,
					    "%04d%02d%02d%02d%02d%02d",
					    1900 + t->tm_year, t->tm_mon+1,
					    t->tm_mday, t->tm_hour, t->tm_min,
					    t->tm_sec);
				}
			}
			if ($4 != NULL)
				free($4);
		}
	| QUIT CRLF
		{
			reply(221, "Goodbye.");
			dologout(0);
		}
	| FEAT CRLF
		{
			control_printf("211- Extensions supported:\r\n");
#ifndef NO_RFC2640_SUPPORT
			control_printf(" UTF8\r\n");
#endif
			control_printf(" MDTM\r\n");
			// control_printf(" REST\r\n");
			control_printf(" SIZE\r\n");
#ifndef NO_FTPS_SUPPORT
			control_printf(" AUTH TLS\r\n");
			control_printf(" PBSZ\r\n");
			control_printf(" PROT\r\n");
#endif
			control_printf("211 end\r\n");
			control_flush();
		}
	| OPTS check_login SP param CRLF
		{
		if ($2 && $4) {
		if (!strcmp($4, "UTF8 ON")) {
#ifndef NO_RFC2640_SUPPORT
			reply(200, "UTF8 ON");
			g_utf8 = 1;
#else
			reply(501, "UTF8 not supported");
#endif
		} else if (!strcmp($4, "UTF8 OFF")) {
#ifndef NO_RFC2640_SUPPORT
			reply(200, "UTF8 OFF");
			g_utf8 = 0;
#else
			reply(501, "UTF8 not supported");
#endif
		} else {
			reply(501, "Feature %s not supported", $4 ? $4 : "?");
		}
		}
		}

	| AUTH SP param CRLF
		{
#ifdef NO_FTPS_SUPPORT
  		reply(500, "FTPS not supported.");
#else
		if ($3 == NULL || strcasecmp($3, "TLS")) {
  			reply(501, "Use AUTH TLS for secure control connection.");
		} else {
			SetControlConnectionSecurity();
		}
#endif
		free($3);
		}

	| PBSZ SP NUMBER CRLF
		{
#ifdef NO_FTPS_SUPPORT
  		reply(500, "FTPS not supported.");
#else
		if ($3 != 0) {
  			reply(501, "Only PBSZ 0 supported for FTPS.");
		} else {
			if (!g_pControlConnectionSecurity) {
				reply(503, "Use AUTH TLS before PBSZ.");
			} else {
				g_PBSZ_Received = 1;
				reply(200, "ok");
			}
		}
#endif
		}

	| PROT SP letter_C CRLF
		{
#ifdef NO_FTPS_SUPPORT
  		reply(500, "FTPS not supported.");
#else
		if (!g_PBSZ_Received) {
			reply(503, "Send PBSZ before PROT.");
		} else { 
			reply(534, "Clear data channel request denied for policy reasons");
		}
#endif
		}

	| PROT SP letter_P CRLF
		{
#ifdef NO_FTPS_SUPPORT
  		reply(500, "FTPS not supported.");
#else
		if (!g_PBSZ_Received) {
			reply(503, "Send PBSZ 0 before PROT P.");
		} else {
			SetDataConnectionSecurity();
		}
#endif
		}

	| error CRLF
		{
			yyerrok;
		}
	;
rcmd
	: RNFR check_login SP pathname CRLF
		{
			restart_point = (off_t) 0;
			if ($2 && $4) {
			    if (fromname != NULL)
				free (fromname);
			    fromname = renamefrom($4);
			}
			if (fromname == (char *) 0 && $4)
			    free($4);
		}
	| REST SP byte_size CRLF
		{
			off_t off;
		    	if (fromname != NULL)
				free (fromname);
			fromname = (char *) 0;
			
			off = yyvsp[-1].o;	/* XXX $3 is only "int" */

			if (off == (off_t)-1) { /* AVM Nessus Check (Nessus ID 11701) */
				reply(500, "REST not understood");
			} else {
			restart_point = off;
			reply(350,
			      (sizeof(restart_point) > sizeof(long)
			       ? "Restarting at %qd. %s"
			       : "Restarting at %ld. %s"), restart_point,
			    "Send STORE or RETRIEVE to initiate transfer.");
			}		
		}
		
	;

username
	: STRING
	;

password
	: /* empty */
		{
			$$ = (char *)calloc(1, sizeof(char));
		}
	| STRING
	;

byte_size
	: NUMBER
	;

address_spec
	: STRING
	;

host_port
	: NUMBER COMMA NUMBER COMMA NUMBER COMMA NUMBER COMMA
		NUMBER COMMA NUMBER
		{
			char *a, *p;

			a = (char *)&((struct sockaddr_in *)&data_dest)->sin_addr;
			a[0] = $1; a[1] = $3; a[2] = $5; a[3] = $7;
			p = (char *)&((struct sockaddr_in *)&data_dest)->sin_port;
			p[0] = $9; p[1] = $11;
			data_dest.ss_family = AF_INET;
		}
	;

form_code
	: letter_N
		{
			$$ = FORM_N;
		}
	| letter_T
		{
			$$ = FORM_T;
		}
	| letter_C
		{
			$$ = FORM_C;
		}
	;

type_code
	: letter_A
		{
			cmd_type = TYPE_A;
			cmd_form = FORM_N;
		}
	| letter_A SP form_code
		{
			cmd_type = TYPE_A;
			cmd_form = $3;
		}
	| letter_E
		{
			cmd_type = TYPE_E;
			cmd_form = FORM_N;
		}
	| letter_E SP form_code
		{
			cmd_type = TYPE_E;
			cmd_form = $3;
		}
	| letter_I
		{
			cmd_type = TYPE_I;
		}
	| letter_L
		{
			cmd_type = TYPE_L;
			cmd_bytesz = NBBY;
		}
	| letter_L SP byte_size
		{
			cmd_type = TYPE_L;
			cmd_bytesz = $3;
		}
		/* this is for a bug in the BBN ftp */
	| letter_L byte_size
		{
			cmd_type = TYPE_L;
			cmd_bytesz = $2;
		}
	;

struct_code
	: letter_F
		{
			$$ = STRU_F;
		}
	| letter_R
		{
			$$ = STRU_R;
		}
	| letter_P
		{
			$$ = STRU_P;
		}
	;

mode_code
	: letter_S
		{
			$$ = MODE_S;
		}
	| letter_B
		{
			$$ = MODE_B;
		}
	| letter_C
		{
			$$ = MODE_C;
		}
	;

pathname
	: pathstring
		{
#ifndef NO_RFC2640_SUPPORT
			if ($1) {
#ifdef FULL_UTF8
				// filesystem charset is UTF-8
				if (!g_utf8) {
					char *iso = $1;
					$1 = ConvertStringFromISO8859_1ToUTF8_WithAlloc(iso);
					if ($1) free(iso);
					else $1 = iso; // error fallback
				}
#else
				// filesystem charset is ISO
				if (g_utf8) {
					ConvertStringFromUTF8ToISO8859_1_With_Fallback($1, '.');
				}
#endif
			}
#else
#ifdef FULL_UTF8
			{
			// filesystem charset is UTF-8
			char *iso = $1;
			$1 = ConvertStringFromISO8859_1ToUTF8_WithAlloc(iso);
			if ($1) free(iso);
			else $1 = iso; // error fallback
			}
#endif
#endif
			/*
			 * Problem: this production is used for all pathname
			 * processing, but only gives a 550 error reply.
			 * This is a valid reply in some cases but not in others.
			 */
			if (cred.logged_in && $1 && *$1 == '~') {
				glob_t gl;
				int flags = GLOB_NOCHECK;

#ifdef GLOB_BRACE
				flags |= GLOB_BRACE;
#endif
#ifdef GLOB_QUOTE
				flags |= GLOB_QUOTE;
#endif
#if 0 && defined(GLOB_TILDE)
				flags |= GLOB_TILDE;
#endif

				memset(&gl, 0, sizeof(gl));
				if (pattern_too_complex($1) || glob($1, flags, NULL, &gl) ||
				    gl.gl_pathc == 0) {
					reply(550, "not found");
					$$ = NULL;
				} else {
					$$ = strdup(gl.gl_pathv[0]);
				}
				globfree(&gl);
				free($1);
			} else
				$$ = $1;
		}
	;

pathstring
	: STRING
	;

param
	: STRING
	;

octal_number
	: NUMBER
		{
			int ret, dec, multby, digit;

			/*
			 * Convert a number that was read as decimal number
			 * to what it would be if it had been read as octal.
			 */
			dec = $1;
			multby = 1;
			ret = 0;
			while (dec) {
				digit = dec%10;
				if (digit > 7) {
					ret = -1;
					break;
				}
				ret += digit * multby;
				multby *= 8;
				dec /= 10;
			}
			$$ = ret;
		}
	;


check_login
	: /* empty */
		{
#ifndef NO_FTPS_SUPPORT
			if (cred.access_from_internet && g_bInternetFTPSOnly && !g_pControlConnectionSecurity) {
				reply (530, "Must use AUTH TLS");
				$$ = 0;
			} else {
#endif
				if (cred.logged_in)
					$$ = 1;
				else {
					reply(530, "Please login with USER and PASS.");
					$$ = 0;
				}
#ifndef NO_FTPS_SUPPORT
			}
#endif
		}
	;

%%

#define	CMD	0	/* beginning of command */
#define	ARGS	1	/* expect miscellaneous arguments */
#define	STR1	2	/* expect SP followed by STRING */
#define	STR2	3	/* expect STRING */
#define	OSTR	4	/* optional SP then STRING */
#define	ZSTR1	5	/* SP then optional STRING */
#define	ZSTR2	6	/* optional STRING after SP */
#define	SITECMD	7	/* SITE command */
#define	NSTR	8	/* Number followed by a string */
#define	OFFS    9 	/* File offset (may be 64bit) */

struct tab cmdtab[] = {		/* In order defined in RFC 765 */
	{ "USER", USER, STR1, 1,	"<sp> username" },
	{ "PASS", PASS, ZSTR1, 1,	"<sp> password" },
	{ "ACCT", ACCT, STR1, 0,	"(specify account)" },
	{ "SMNT", SMNT, ARGS, 0,	"(structure mount)" },
	{ "REIN", REIN, ARGS, 0,	"(reinitialize server state)" },
	{ "QUIT", QUIT, ARGS, 1,	"(terminate service)", },
	{ "PORT", PORT, ARGS, 1,	"<sp> b0, b1, b2, b3, b4" },
	{ "PASV", PASV, ARGS, 1,	"(set server in passive mode)" },
	{ "TYPE", TYPE, ARGS, 1,	"<sp> [ A | E | I | L ]" },
	{ "STRU", STRU, ARGS, 1,	"(specify file structure)" },
	{ "MODE", MODE, ARGS, 1,	"(specify transfer mode)" },
	{ "RETR", RETR, STR1, 1,	"<sp> file-name" },
	{ "STOR", STOR, STR1, 1,	"<sp> file-name" },
	{ "APPE", APPE, STR1, 1,	"<sp> file-name" },
	{ "MLFL", MLFL, OSTR, 0,	"(mail file)" },
	{ "MAIL", MAIL, OSTR, 0,	"(mail to user)" },
	{ "MSND", MSND, OSTR, 0,	"(mail send to terminal)" },
	{ "MSOM", MSOM, OSTR, 0,	"(mail send to terminal or mailbox)" },
	{ "MSAM", MSAM, OSTR, 0,	"(mail send to terminal and mailbox)" },
	{ "MRSQ", MRSQ, OSTR, 0,	"(mail recipient scheme question)" },
	{ "MRCP", MRCP, STR1, 0,	"(mail recipient)" },
	{ "ALLO", ALLO, ARGS, 1,	"allocate storage (vacuously)" },
/*	{ "REST", REST, ARGS, 1,	"<sp> offset (restart command)" }, */
	{ "REST", REST, OFFS, 1,	"<sp> offset (restart command)" },
	{ "RNFR", RNFR, STR1, 1,	"<sp> file-name" },
	{ "RNTO", RNTO, STR1, 1,	"<sp> file-name" },
	{ "ABOR", ABOR, ARGS, 1,	"(abort operation)" },
	{ "DELE", DELE, STR1, 1,	"<sp> file-name" },
	{ "CWD",  CWD,  OSTR, 1,	"[ <sp> directory-name ]" },
	{ "XCWD", CWD,	OSTR, 1,	"[ <sp> directory-name ]" },
	{ "LIST", LIST, OSTR, 1,	"[ <sp> path-name ]" },
	{ "NLST", NLST, OSTR, 1,	"[ <sp> path-name ]" },
	{ "SITE", SITE, SITECMD, 1,	"site-cmd [ <sp> arguments ]" },
	{ "SYST", SYST, ARGS, 1,	"(get type of operating system)" },
	{ "STAT", STAT, OSTR, 1,	"[ <sp> path-name ]" },
	{ "HELP", HELP, OSTR, 1,	"[ <sp> <string> ]" },
	{ "NOOP", NOOP, ARGS, 1,	"" },
	{ "MKD",  MKD,  STR1, 1,	"<sp> path-name" },
	{ "XMKD", MKD,  STR1, 1,	"<sp> path-name" },
	{ "RMD",  RMD,  STR1, 1,	"<sp> path-name" },
	{ "XRMD", RMD,  STR1, 1,	"<sp> path-name" },
	{ "RRMD", RRMD, STR1, 1,	"<sp> path-name" },
	{ "PWD",  PWD,  ARGS, 1,	"(return current directory)" },
	{ "XPWD", PWD,  ARGS, 1,	"(return current directory)" },
	{ "CDUP", CDUP, ARGS, 1,	"(change to parent directory)" },
	{ "XCUP", CDUP, ARGS, 1,	"(change to parent directory)" },
	{ "STOU", STOU, STR1, 1,	"<sp> file-name" },
	{ "SIZE", SIZE, OSTR, 1,	"<sp> path-name" },
	{ "MDTM", MDTM, OSTR, 1,	"<sp> path-name" },
	{ "EPSV", EPSV, OSTR, 1,	"(set server in extended passive mode)" },
	{ "EPRT", EPRT, STR1, 1,	"<sp> |prot|addr|port|" },
	{ "FEAT", FEAT, ARGS, 1,	"(list features)", },
	{ "OPTS", OPTS, OSTR, 1,	"<sp> cmd <sp> option", },
// #ifndef NO_FTPS_SUPPORT
	{ "AUTH", AUTH, STR1, 1,	"<sp> TLS", },
	{ "PBSZ", PBSZ, ARGS, 1,	"<sp> 0", },
	{ "PROT", PROT, ARGS, 1,	"<sp> C | P", },
	// { "CCC",  CCC,  ARGS, 1,	"", },
// #endif
	{ NULL,   0,    0,    0,	0 }
};

struct tab sitetab[] = {
	{ "UMASK", UMASK, ARGS, 1,	"[ <sp> umask ]" },
	{ "IDLE", IDLE, ARGS, 1,	"[ <sp> maximum-idle-time ]" },
	{ "CHMOD", CHMOD, NSTR, 1,	"<sp> mode <sp> file-name" },
	{ "QUOTA", QUOTA, ARGS, 1,	"" },
	{ "HELP", HELP, OSTR, 1,	"[ <sp> <string> ]" },
	{ NULL,   0,    0,    0,	0 }
};

static struct tab *
lookup(p, cmd)
	struct tab *p;
	char *cmd;
{

	for (; p->name != NULL; p++)
		if (strcmp(cmd, p->name) == 0)
			return (p);
	return (0);
}

static int get_control_input_char(void)
{
#ifndef NO_FTPS_SUPPORT
	if (g_pControlConnectionSecurity) {
// Log(("get_control_input_char"));
		unsigned char c;
		clear_ssl_errors();
		int ret = SSL_read(g_pControlConnectionSecurity->ssl, (void *)&c, 1);
		if (1 != ret) {
Log(("get_control_input_char SSL_read failed ret=%d", ret));
			Log_SSL_Errors();
			return EOF;
		}
// Log(("get_control_input_char SSL_read c=0x%x", c));

		return (int)c;
	} else
#endif
		return getc(stdin);
}

#include <arpa/telnet.h>


/*
 * getline - a hacked up version of fgets to ignore TELNET escape codes.
 */
char *
get_controlconnection_line(char *s, int n)
{
	int c;
	register char *cs;

	cs = s;
/* tmpline may contain saved command from urgent mode interruption */
	for (c = 0; tmpline[c] != '\0' && --n > 0; ++c) {
		*cs++ = tmpline[c];
		if (tmpline[c] == '\n') {
			*cs++ = '\0';
			if (debug)
				syslog(LOG_DEBUG, "command: %s", s);
			tmpline[0] = '\0';
			return(s);
		}
		if (c == 0)
			tmpline[0] = '\0';
	}
	while ((c = get_control_input_char()) != EOF) {
		c &= 0377;
		if (c == IAC) {
		    if ((c = get_control_input_char()) != EOF) {
			c &= 0377;
			switch (c) {
			case WILL:
			case WONT:
				c = get_control_input_char();
				control_printf("%c%c%c", IAC, DONT, 0377&c);
				control_flush();
				continue;
			case DO:
			case DONT:
				c = get_control_input_char();
				control_printf("%c%c%c", IAC, WONT, 0377&c);
				control_flush();
				continue;
			case IAC:
				break;
			default:
				continue;	/* ignore command */
			}
		    }
		}
		*cs++ = c;
		if (--n <= 0 || c == '\n')
			break;
	}
	if (c == EOF && cs == s)
	    return (NULL);
	*cs++ = '\0';
	if (debug) {
		if (!cred.guest && strncasecmp("pass ", s, 5) == 0) {
			/* Don't syslog passwords */
			syslog(LOG_DEBUG, "command: %.5s ???", s);
		} else {
			register char *cp;
			register int len;

			/* Don't syslog trailing CR-LF */
			len = strlen(s);
			cp = s + len - 1;
			while (cp >= s && (*cp == '\n' || *cp == '\r')) {
				--cp;
				--len;
			}
			syslog(LOG_DEBUG, "command: %.*s", len, s);
		}
	}
	return (s);
}

void
toolong(int signo)
{
  (void)signo;
	reply(421,
	    "Timeout (%d seconds): closing control connection.", timeout);
	if (logging)
		syslog(LOG_INFO, "User %s timed out after %d seconds",
		    (cred.name ? cred.name : "unknown"), timeout);
	dologout(1);
}

static int
yylex()
{
	static int cpos, state;
	char *cp, *cp2;
	struct tab *p;
	int n;
	char c;

	for (;;) {
		switch (state) {

		case CMD:
			(void) signal(SIGALRM, toolong);
			(void) alarm((unsigned) timeout);
			if (get_controlconnection_line(cbuf, sizeof(cbuf)-1) == NULL) {
				reply(221, "You could at least say goodbye.");
				dologout(0);
			}
			(void) alarm(0);
#ifdef HAVE_SETPROCTITLE
			if (strncasecmp(cbuf, "PASS", 4) != NULL)
				setproctitle("%s: %s", proctitle, cbuf);
#endif /* HAVE_SETPROCTITLE */
			if ((cp = strchr(cbuf, '\r'))) {
				*cp++ = '\n';
				*cp = '\0';
			}
			if ((cp = strpbrk(cbuf, " \n")))
				cpos = cp - cbuf;
			if (cpos == 0)
				cpos = 4;
			c = cbuf[cpos];
			cbuf[cpos] = '\0';
			upper(cbuf);
			p = lookup(cmdtab, cbuf);
			cbuf[cpos] = c;
			if (p != 0) {
				if (p->implemented == 0) {
					nack(p->name);
					longjmp(errcatch,0);
					/* NOTREACHED */
				}
				state = p->state;
				yylval.s = p->name;
				return (p->token);
			}
			break;

		case SITECMD:
			if (cbuf[cpos] == ' ') {
				cpos++;
				return (SP);
			}
			cp = &cbuf[cpos];
			if ((cp2 = strpbrk(cp, " \n")))
				cpos = cp2 - cbuf;
			c = cbuf[cpos];
			cbuf[cpos] = '\0';
			upper(cp);
			p = lookup(sitetab, cp);
			cbuf[cpos] = c;
			if (p != 0) {
				if (p->implemented == 0) {
					state = CMD;
					nack(p->name);
					longjmp(errcatch,0);
					/* NOTREACHED */
				}
				state = p->state;
				yylval.s = p->name;
				return (p->token);
			}
			state = CMD;
			break;

		case OSTR:
			if (cbuf[cpos] == '\n') {
				state = CMD;
				return (CRLF);
			}
			/* FALLTHROUGH */

		case STR1:
		case ZSTR1:
		dostr1:
			if (cbuf[cpos] == ' ') {
				cpos++;
				state = state == OSTR ? STR2 : ++state;
				return (SP);
			}
			break;

		case ZSTR2:
			if (cbuf[cpos] == '\n') {
				state = CMD;
				return (CRLF);
			}
			/* FALLTHROUGH */

		case STR2:
			cp = &cbuf[cpos];
			n = strlen(cp);
			cpos += n - 1;
			/*
			 * Make sure the string is nonempty and \n terminated.
			 */
			if (n > 1 && cbuf[cpos] == '\n') {
				cbuf[cpos] = '\0';
				yylval.s = copy(cp);
				cbuf[cpos] = '\n';
				state = ARGS;
				return (STRING);
			}
			break;

		case NSTR:
			if (cbuf[cpos] == ' ') {
				cpos++;
				return (SP);
			}
			if (isdigit(cbuf[cpos])) {
				cp = &cbuf[cpos];
				while (isdigit(cbuf[++cpos]))
					;
				c = cbuf[cpos];
				cbuf[cpos] = '\0';
				yylval.i = atoi(cp);
				cbuf[cpos] = c;
				state = STR1;
				return (NUMBER);
			}
			state = STR1;
			goto dostr1;

		case ARGS:
		case OFFS:
			if (isdigit(cbuf[cpos])) {
				cp = &cbuf[cpos];
				while (isdigit(cbuf[++cpos]))
					;
				c = cbuf[cpos];
				cbuf[cpos] = '\0';
				if (state == ARGS) {
					yylval.i = atoi(cp);
				} else {
					/* parse off_t */
					if (sizeof(off_t) > sizeof(long)) {
						if (1 != sscanf(cp, "%qd", &yylval.o)) yylval.o = (off_t)-1;
					} else {
						if (1 != sscanf(cp, "%ld", &yylval.o)) yylval.o = (off_t)-1;
					}
				}
				cbuf[cpos] = c;
				return (NUMBER);
			}
			switch (cbuf[cpos++]) {

			case '\n':
				state = CMD;
				return (CRLF);

			case ' ':
				return (SP);

			case ',':
				return (COMMA);

			case 'A':
			case 'a':
				return (letter_A);

			case 'B':
			case 'b':
				return (letter_B);

			case 'C':
			case 'c':
				return (letter_C);

			case 'E':
			case 'e':
				return (letter_E);

			case 'F':
			case 'f':
				return (letter_F);

			case 'I':
			case 'i':
				return (letter_I);

			case 'L':
			case 'l':
				return (letter_L);

			case 'N':
			case 'n':
				return (letter_N);

			case 'P':
			case 'p':
				return (letter_P);

			case 'R':
			case 'r':
				return (letter_R);

			case 'S':
			case 's':
				return (letter_S);

			case 'T':
			case 't':
				return (letter_T);

			}
			break;

		default:
			fatal("Unknown state in scanner.");
		}
		yyerror((char *) 0);
		state = CMD;
		longjmp(errcatch,0);
	}
}

void
upper(char *s)
{
	while (*s != '\0') {
		if (islower(*s))
			*s = toupper(*s);
		s++;
	}
}

static char *
copy(char *s)
{
	char *p;

	p = malloc((unsigned) strlen(s) + 1);
	if (p == NULL)
		fatal("Ran out of memory.");
	(void) strcpy(p, s);
	return (p);
}

static void
help(struct tab *ctab, char *s)
{
	struct tab *c;
	int width, NCMDS;
	const char *help_type;

	if (ctab == sitetab)
		help_type = "SITE ";
	else
		help_type = "";
	width = 0, NCMDS = 0;
	for (c = ctab; c->name != NULL; c++) {
		int len = strlen(c->name);

		if (len > width)
			width = len;
		NCMDS++;
	}
	width = (width + 8) &~ 7;
	if (s == 0) {
		int i, j, w;
		int columns, lines;

		lreply(214, "The following %scommands are recognized %s.",
		    help_type, "(* =>'s unimplemented)");
		columns = 76 / width;
		if (columns == 0)
			columns = 1;
		lines = (NCMDS + columns - 1) / columns;
		for (i = 0; i < lines; i++) {
			control_printf("   ");
			for (j = 0; j < columns; j++) {
				c = ctab + j * lines + i;
				control_printf("%s%c", c->name,
					c->implemented ? ' ' : '*');
				if (c + lines >= &ctab[NCMDS])
					break;
				w = strlen(c->name) + 1;
				while (w < width) {
					control_putchar(' ');
					w++;
				}
			}
			control_printf("\r\n");
		}
		control_flush();
		reply(214, "Direct comments to support@avm.de.");
		return;
	}
	upper(s);
	c = lookup(ctab, s);
	if (c == (struct tab *)0) {
		reply(502, "Unknown command %s.", s);
		return;
	}
	if (c->implemented)
		reply(214, "Syntax: %s%s %s", help_type, c->name, c->help);
	else
		reply(214, "%s%-*s\t%s; unimplemented.", help_type, width,
		    c->name, c->help);
}

static void
sizecmd2(char *filename)
{
	switch (type) {
	case TYPE_L:
	case TYPE_I: {
		struct stat stbuf;
		if (virtual_stat(filename, &stbuf) < 0 || !S_ISREG(stbuf.st_mode))
			reply(550, "%s: not a plain file.", filename);
		else
			reply(213,
			      (sizeof (stbuf.st_size) > sizeof(long)
			       ? "%qu" : "%lu"), stbuf.st_size);
		break; }
	case TYPE_A: {
		FILE *fin;
		int c;
		off_t count;
		struct stat stbuf;
		fin = virtual_fopen(filename, "r");
		if (fin == NULL) {
			perror_reply(550, filename);
			return;
		}
		if (virtual_stat(filename, &stbuf) < 0 || !S_ISREG(stbuf.st_mode)) {
			reply(550, "%s: not a plain file.", filename);
			(void) fclose(fin);
			return;
		}

		count = 0;
		while((c=getc(fin)) != EOF) {
			if (c == '\n')	/* will get expanded to \r\n */
				count++;
			count++;
		}
		(void) fclose(fin);

		reply(213, sizeof(count) > sizeof(long) ? "%qd" : "%ld",
		      count);
		break; }
	default:
		reply(504, "SIZE not implemented for Type %c.", "?AEIL"[type]);
	}
}

/* ARGSUSED */
static void
yyerror(const char *s)
{
  char *cp;

  (void)s;
  cp = strchr(cbuf,'\n');
  if (cp != NULL)
    *cp = '\0';
  reply(500, "'%s': command not understood.", cbuf);
}
