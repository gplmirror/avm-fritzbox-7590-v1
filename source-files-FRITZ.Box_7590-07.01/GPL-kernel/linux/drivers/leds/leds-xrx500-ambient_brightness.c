#include <asm/mach_avm.h>
#include <linux/avm_hw_config.h>
#include <linux/avm_event.h>
#include <linux/types.h>
#include <linux/of_platform.h>
#include <linux/of_gpio.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/err.h>
#include <linux/klogging.h>
#include <lantiq_soc.h>

#define MAX_BRIGHTNESS 0x3FFF

/*--- #define TEST_AMBIENT_EVENT ---*/ 

/*** The following code is taken from dect_iflantiq.h and dect_ifasc.h from Piglet_noemif ***/

#define DECT_UART_BASE (unsigned int *)KSEG1ADDR(0x16700000) /*--- ASC-UART1 ---*/
#define DECT_UART_INT 86 

typedef struct {        /* ifx_asc_reg_t */
    volatile unsigned int  asc_clc;                            /*0x0000*/
    volatile unsigned int  asc_pisel;                          /*0x0004*/
    volatile unsigned int  asc_id;                             /*0x0008*/
    volatile unsigned int  asc_rsvd1[1];   /* for mapping */   /*0x000C*/
    volatile unsigned int  asc_mcon;                           /*0x0010*/
    volatile unsigned int  asc_state;                          /*0x0014*/
    volatile unsigned int  asc_whbstate;                       /*0x0018*/
    volatile unsigned int  asc_rsvd2[1];   /* for mapping */   /*0x001C*/
    volatile unsigned int  asc_tbuf;                           /*0x0020*/
    volatile unsigned int  asc_rbuf;                           /*0x0024*/
    volatile unsigned int  asc_rsvd3[2];   /* for mapping */   /*0x0028*/
    volatile unsigned int  asc_abcon;                          /*0x0030*/
    volatile unsigned int  asc_abstat;     /* not used */      /*0x0034*/
    volatile unsigned int  asc_whbabcon;                       /*0x0038*/
    volatile unsigned int  asc_whbabstat;  /* not used */      /*0x003C*/
    volatile unsigned int  asc_rxfcon;                         /*0x0040*/
    volatile unsigned int  asc_txfcon;                         /*0x0044*/
    volatile unsigned int  asc_fstat;                          /*0x0048*/
    volatile unsigned int  asc_rsvd4[1];   /* for mapping */   /*0x004C*/
    volatile unsigned int  asc_bg;                             /*0x0050*/
    volatile unsigned int  asc_bg_timer;                       /*0x0054*/
    volatile unsigned int  asc_fdv;                            /*0x0058*/
    volatile unsigned int  asc_pmw;                            /*0x005C*/
    volatile unsigned int  asc_modcon;                         /*0x0060*/
    volatile unsigned int  asc_modstat;                        /*0x0064*/
    volatile unsigned int  asc_rsvd5[2];   /* for mapping */   /*0x0068*/
    volatile unsigned int  asc_sfcc;                           /*0x0070*/
    volatile unsigned int  asc_rsvd6[3];   /* for mapping */   /*0x0074*/
    volatile unsigned int  asc_eomcon;                         /*0x0080*/
    volatile unsigned int  asc_rsvd7[26];   /* for mapping */  /*0x0084*/
    volatile unsigned int  asc_dmacon;                         /*0x00EC*/
    volatile unsigned int  asc_rsvd8[1];   /* for mapping */   /*0x00F0*/
    volatile unsigned int  asc_irnen;                          /*0x00F4*/
    volatile unsigned int  asc_irncr;                          /*0x00F8*/
    volatile unsigned int  asc_irnicr;                         /*0x00FC*/
} ifx_asc_reg_t;

#define ASCID_TX32                      0x80000000
#define ASCFSTAT_TXFFLMASK              0x1F00
#define ASCFSTAT_RXFFLMASK              0x001F

/* WHBSTATE register's bits and bitfields */
#define ASCWHBSTATE_CLRREN              0x00000001
#define ASCWHBSTATE_SETREN              0x00000002
#define ASCWHBSTATE_CLRPE               0x00000004
#define ASCWHBSTATE_CLRFE               0x00000008
#define ASCWHBSTATE_CLRRUE              0x00000010
#define ASCWHBSTATE_CLRROE              0x00000020
#define ASCWHBSTATE_CLRTOE              0x00000040
#define ASCWHBSTATE_CLRBE               0x00000080
#define ASCWHBSTATE_SETPE               0x00000100
#define ASCWHBSTATE_SETFE               0x00000200
#define ASCWHBSTATE_SETRUE              0x00000400
#define ASCWHBSTATE_SETROE              0x00000800
#define ASCWHBSTATE_SETTOE              0x00001000
#define ASCWHBSTATE_SETBE               0x00002000

/* RXFCON register's bits and bitfields */
#define ASCRXFCON_RXFEN                 0x0001
#define ASCRXFCON_RXFFLU                0x0002
#define ASCRXFCON_RXFITLMASK            0x3F00
#define ASCRXFCON_RXFITLOFF             8

/* TXFCON register's bits and bitfields */
#define ASCTXFCON_TXFEN                 0x0001
#define ASCTXFCON_TXFFLU                0x0002
#define ASCTXFCON_TXFITLMASK            0x3F00
#define ASCTXFCON_TXFITLOFF             8

/* STATE register's bits and bitfields */
#define ASCSTATE_REN                    0x00000001
#define ASCSTATE_PE                     0x00010000
#define ASCSTATE_FE                     0x00020000
#define ASCSTATE_RUE                    0x00040000
#define ASCSTATE_ROE                    0x00080000
#define ASCSTATE_TOE                    0x00100000
#define ASCSTATE_BE                     0x00200000
#define ASCSTATE_TXBVMASK               0x07000000
#define ASCSTATE_TXBVOFFSET             24
#define ASCSTATE_TXEOM                  0x08000000
#define ASCSTATE_RXBVMASK               0x70000000
#define ASCSTATE_RXBVOFFSET             28
#define ASCSTATE_RXEOM                  0x80000000
#define ASCSTATE_ANY                    (ASCSTATE_PE|ASCSTATE_FE|ASCSTATE_ROE)

/* CON register's bits and bitfields */
#define ASCMCON_MODEMASK                0x0000000f
#define ASCMCON_M_8ASYNC                0x0
#define ASCMCON_M_8IRDA                 0x1
#define ASCMCON_M_7ASYNC                0x2
#define ASCMCON_M_7IRDA                 0x3
#define ASCMCON_WLSMASK                 0x0000000c
#define ASCMCON_WLSOFFSET               2
#define ASCMCON_WLS_8BIT                0x0
#define ASCMCON_WLS_7BIT                0x1
#define ASCMCON_PEN                     0x00000010
#define ASCMCON_ODD                     0x00000020
#define ASCMCON_SP                      0x00000040
#define ASCMCON_STP                     0x00000080
#define ASCMCON_BRS                     0x00000100
#define ASCMCON_FDE                     0x00000200
#define ASCMCON_ERRCLK                  0x00000400
#define ASCMCON_EMMASK                  0x00001800
#define ASCMCON_EMOFFSET                11
#define ASCMCON_EM_ECHO_OFF             0x0
#define ASCMCON_EM_ECHO_AB              0x1
#define ASCMCON_EM_ECHO_ON              0x2
#define ASCMCON_LB                      0x00002000
#define ASCMCON_ACO                     0x00004000
#define ASCMCON_R                       0x00008000
#define ASCMCON_PAL                     0x00010000
#define ASCMCON_FEN                     0x00020000
#define ASCMCON_RUEN                    0x00040000
#define ASCMCON_ROEN                    0x00080000
#define ASCMCON_TOEN                    0x00100000
#define ASCMCON_BEN                     0x00200000
#define ASCMCON_TXINV                   0x01000000
#define ASCMCON_RXINV                   0x02000000
#define ASCMCON_TXMSB                   0x04000000
#define ASCMCON_RXMSB                   0x08000000

/* CLC register's bits and bitfields */
#define ASCCLC_DISR                     0x00000001
#define ASCCLC_DISS                     0x00000002
#define ASCCLC_RMCMASK                  0x0000FF00
#define ASCCLC_RMCOFFSET                8

/* interrupt lines masks for the ASC device interrupts*/
/* change these macroses if it's necessary */
#define IFX_ASC_IRQ_LINE_ALL            0x000000FF      /* all IRQs */
#define IFX_ASC_IRQ_LINE_MASK_ALL       0x00000000      /* disable/mask all IRQs */

#define IFX_ASC_IRQ_LINE_TIR            0x00000001      /* Tx Int */
#define IFX_ASC_IRQ_LINE_RIR            0x00000002      /* Rx Int */
#define IFX_ASC_IRQ_LINE_EIR            0x00000004      /* Error Int */
#define IFX_ASC_IRQ_LINE_TBIR           0x00000008      /* Tx Buffer Int */
#define IFX_ASC_IRQ_LINE_ABSTIR         0x00000010      /* Autobaud Start Int */
#define IFX_ASC_IRQ_LINE_ABDETIP        0x00000020      /* Autobaud Detection Int */
#define IFX_ASC_IRQ_LINE_MSIR           0x00000040      /* Modem Status Int */
#define IFX_ASC_IRQ_LINE_SFCIR          0x00000080      /* Software Flow Control Int */

#define IFX_ASC_TXFIFO_FL               1
#define IFX_ASC_RXFIFO_FL               1
#define IFX_ASC_TXFIFO_FULL             16

/* TXFCON register's bits and bitfields */
#define ASCTXFCON_TXFEN                 0x0001
#define ASCTXFCON_TXFFLU                0x0002
#define ASCTXFCON_TXFITLMASK            0x3F00
#define ASCTXFCON_TXFITLOFF             8

#define HW_UARTREGBASESIZE  sizeof(ifx_asc_reg_t)


/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void get_fdv_and_reload_value(unsigned int baudrate, unsigned int *fdv, unsigned int *reload) {
    struct clk *pfpiclk;
	unsigned int fpi_clk;
	unsigned long long baudrate1 = (unsigned long long)baudrate * 8192;
	unsigned long long baudrate2 = (unsigned long long)baudrate * 1000;
	unsigned long long fdv_over_bg_fpi;
	unsigned long long fdv_over_bg;
	unsigned long long difference;
	unsigned long long min_difference;
	unsigned int bg;

	pfpiclk = clk_get_sys("fpi", "fpi");
	if(!pfpiclk || IS_ERR(pfpiclk)) {
		printk(KERN_ERR"failed to get fpi clk\n");
        clk_put(pfpiclk);
		return;
	}
	fpi_clk = clk_get_rate(pfpiclk);
	/* Sanity check first */
	if (baudrate >= (fpi_clk >> 4)) {
		printk(KERN_ERR"%s current fpi clock %u can't provide baudrate %u\n", __func__, fpi_clk, baudrate);
        clk_put(pfpiclk);
		return;
	}
/*--- 	printk(KERN_ERR"%s current fpi clock %u baudrate %u\n", __func__, fpi_clk, baudrate); ---*/
	min_difference = UINT_MAX;
	fdv_over_bg_fpi = baudrate1;
	for (bg = 1; bg <= 8192; bg++, fdv_over_bg_fpi += baudrate1) {
		fdv_over_bg = fdv_over_bg_fpi + fpi_clk / 2;
		do_div(fdv_over_bg, fpi_clk);
		if (fdv_over_bg <= 512) {
			difference = fdv_over_bg * fpi_clk * 1000;
			do_div(difference, 8192 * bg);
			if (difference < baudrate2)
				difference = baudrate2 - difference;
			else
				difference -= baudrate2;
			if (difference < min_difference) {
				*fdv = (unsigned int)fdv_over_bg & 511;
				*reload = bg - 1;
				min_difference = difference;
			}
			/* Perfect one found */
			if (min_difference == 0)
				break;
		}
	}
    clk_put(pfpiclk);
/*--- 	printk(KERN_ERR"%s baud=%u *fdv=%u *reload=%u\n", __func__, baudrate, *fdv, *reload); ---*/
}


/*--------------------------------------------------------------------------------*\
 * option: != 0 FIFO an und Rx-Irq an
\*--------------------------------------------------------------------------------*/
static inline void ascambientuart_init(unsigned int baudrate, int option) {
    ifx_asc_reg_t *asc_reg = (ifx_asc_reg_t *)DECT_UART_BASE;
    unsigned int fdv = 0, reload = 0;
    /*--- printk("%s baud %d option %d asc_reg %p\n", __func__, baudrate, option, asc_reg); ---*/

#if defined(GPIO_IFX_MODULE_REGISTER)
    ifx_gpio_deregister(IFX_GPIO_MODULE_ASC0);
    ifx_gpio_register(IFX_GPIO_MODULE_ASC0);
#endif/*--- #if !defined(GPIO_IFX_MODULE_REGISTER) ---*/

    asc_reg->asc_clc      =  0x1 <<  ASCCLC_RMCOFFSET; /* Set CLC register*/
    asc_reg->asc_mcon     =  ASCMCON_M_8ASYNC | 0x00c00000;  /* Initialy we are in async mode */
    
    asc_reg->asc_irnen = IFX_ASC_IRQ_LINE_MASK_ALL; /* disable ASC interrupts in module */
    asc_reg->asc_irncr = 0xFF;

    asc_reg->asc_rxfcon |= ASCRXFCON_RXFEN; /* enable receiver */
    asc_reg->asc_txfcon |= ASCTXFCON_TXFEN; /* enable tranmsitter */
    asc_reg->asc_mcon   |= ASCMCON_FEN;     /* enable error signals */

    /* Clear all error interrupts and disable receiver */
    asc_reg->asc_whbstate = ASCWHBSTATE_CLRPE | ASCWHBSTATE_CLRFE | ASCWHBSTATE_CLRRUE | ASCWHBSTATE_CLRROE | ASCWHBSTATE_CLRTOE | ASCWHBSTATE_CLRBE;
    asc_reg->asc_eomcon   = 0x00010300;

    asc_reg->asc_mcon    |= ASCMCON_FDE; /* set up to use divisor of 2 */
    /* now we can write the new baudrate into the register */

    /*--- ifx_asc_get_fdv_and_reload_value(baudrate, &fdv, &reload); ---*/
	get_fdv_and_reload_value(baudrate, &fdv, &reload);

    asc_reg->asc_fdv = fdv;
    asc_reg->asc_bg  = reload;

    asc_reg->asc_mcon  = (asc_reg->asc_mcon & ~ASCMCON_WLSMASK) | (ASCMCON_WLS_8BIT << ASCMCON_WLSOFFSET);  /*--- 8 bit ---*/
    asc_reg->asc_mcon &= ~ASCMCON_PEN;     /*--- no parity ---*/
    asc_reg->asc_mcon &= ~ASCMCON_STP;     /*--- number of stopp-bits ---*/

    if(option) {
        /* flush fifo */
        asc_reg->asc_rxfcon = (IFX_ASC_RXFIFO_FL << ASCRXFCON_RXFITLOFF) | ASCRXFCON_RXFFLU;
    }
    if(option) {
        /*--- EIR muss enabled sein, sonst gibt es keine RIR's !!!! ---*/
        asc_reg->asc_irnen  = IFX_ASC_IRQ_LINE_RIR | IFX_ASC_IRQ_LINE_EIR;
        asc_reg->asc_irnicr = IFX_ASC_IRQ_LINE_RIR | IFX_ASC_IRQ_LINE_EIR;
        asc_reg->asc_irncr = 0xFF;

       asc_reg->asc_rxfcon = (0x1 << ASCRXFCON_RXFITLOFF) | ASCRXFCON_RXFEN;
        if((asc_reg->asc_id & ASCID_TX32)) {
            /* setup fifos and timeout */
            asc_reg->asc_eomcon = 0x1040300;
        } else {
            asc_reg->asc_eomcon = 0x1040600;
        }
    }
    asc_reg->asc_mcon |= ASCMCON_R;             /* turn on baudrate generator */
    asc_reg->asc_whbstate = ASCWHBSTATE_SETREN; /* enable receiver */
    /*--- print_asc(asc_reg); ---*/
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline int ascambientuart_rxirqstatus(void) {
    ifx_asc_reg_t *asc_reg = (ifx_asc_reg_t *)DECT_UART_BASE;
    if(asc_reg->asc_irnicr & IFX_ASC_IRQ_LINE_RIR) {
        asc_reg->asc_irnicr &= ~IFX_ASC_IRQ_LINE_RIR;
        return 1;
    }
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void ascambientuart_rxirqenable(void) {
    ifx_asc_reg_t *asc_reg = (ifx_asc_reg_t *)DECT_UART_BASE;
    asc_reg->asc_irnen |= IFX_ASC_IRQ_LINE_RIR;
}


/*** The code above is taken from from Piglet_noemif ***/

static void  *ambient_brightness_eventhandle;
struct ambient_work{ 
    struct work_struct work;
    uint32_t value;
};

static void ambient_work(struct work_struct *work)
{
    struct ambient_work	*ambient_dat =
        container_of(work, struct ambient_work, work);
    struct _avm_event_ambient_brightness *event;
    int handled;
    event = (struct _avm_event_ambient_brightness *)kmalloc(sizeof(struct _avm_event_ambient_brightness), GFP_ATOMIC);
    if(event == NULL) {
        printk(KERN_WARNING "[%s]can't alloc event: %d\n", __func__, avm_event_id_ambient_brightness);
        return;
    }
    event->event_header.id = avm_event_id_ambient_brightness;
    event->value     = ambient_dat->value;
    event->maxvalue      = MAX_BRIGHTNESS;
    kfree(ambient_dat);
    handled = avm_event_source_trigger(ambient_brightness_eventhandle, avm_event_id_ambient_brightness, sizeof(struct _avm_event_ambient_brightness), event);
    if(handled == 0) {
        printk(KERN_WARNING "[%s]event: %d not handled\n", __func__, avm_event_id_ambient_brightness);
    }

}

static const struct of_device_id xrx500_ambient_match[] = {
	{ .compatible = "avm,ambient_brightness-xrx500" },
	{},
};
uint8_t low_byte = 0;
uint8_t event_sink_registered = 0;
static irqreturn_t lambientuart_irqfunc(int irq __maybe_unused, void *devid __maybe_unused) {
    unsigned int data;
    ifx_asc_reg_t *asc_reg = (ifx_asc_reg_t *)DECT_UART_BASE;
    if(ascambientuart_rxirqstatus()) {
        while((asc_reg->asc_state >> 28) & 0x3){
            data = asc_reg->asc_rbuf & 0xFF;
            if(data & 0x80){
                low_byte = data;
            }
            else{
                if(low_byte & 0x80){
                    data = ((data & 0x7f) << 7) | (low_byte & 0x7f) ;
#if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE) 
                    if(event_sink_registered){
                        struct ambient_work * ambient = kmalloc(sizeof(struct ambient_work), GFP_ATOMIC);
                        if(ambient){
                            INIT_WORK(&ambient->work, ambient_work);
                            ambient->value = data;
                            schedule_work(&ambient->work);
                        }
                        else{
                            pr_err("Could not allocate memory for work struct\n");
                        }
                    }
#endif/*--- #if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE)  ---*/
                }
                else{
                    pr_err("%s high byte received without low_byte\n", __func__);
                }
            }
        }
        return IRQ_HANDLED;
    }
    return IRQ_NONE;
}

static int xrx500_ambient_init_uart(void){
    static int uart_init = 0;
    int ret = 0;
    if(!uart_init){
        uart_init = 1;
        ascambientuart_init(115200,1);
        ret = request_irq( DECT_UART_INT, lambientuart_irqfunc, IRQF_DISABLED, "UARTDECT_IRQ" , 0);
        ascambientuart_rxirqenable();
         if(ret < 0) {
            printk(KERN_ERR"can't start irq: %d \n", ret);
            return ret;
        }
    }
    return 0;
}

#ifdef TEST_AMBIENT_EVENT
static void avm_event_ambient_test(void *handle, unsigned char *buf, unsigned int len){
#if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE) 
    struct _avm_event_ambient_brightness *pevent = (struct _avm_event_ambient_brightness *)buf;

	if(len < sizeof(struct _avm_event_ambient_brightness)) {
        printk(KERN_ERR"%s: error len=%u\n", __func__, len); 
		return;
	}
    pr_err("Received %d\n", pevent->value);
#endif/*--- #if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE)  ---*/
}
#endif

static void avm_event_piglet_ready_cb(void *handle, unsigned char *buf, unsigned int len){
#if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE) 
    struct _avm_event_piglet *pevent = (struct _avm_event_piglet *)buf;

	if(len < sizeof(struct _avm_event_piglet)) {
        printk(KERN_ERR"%s: error len=%u\n", __func__, len); 
		return;
	}
    if(pevent->type == piglet_tdm_ready) {
        xrx500_ambient_init_uart();
    }
#endif/*--- #if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE)  ---*/
}


#if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE) 
static void avmevent_ambient_notify(void *context __attribute__((unused)), enum _avm_event_id id) {
	if(id != avm_event_id_ambient_brightness){
        printk(KERN_WARNING "[%s]unknown event: %d\n", __func__, id);
        return;
    }
    event_sink_registered = 1;
}
#endif/*--- #if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE)  ---*/



static int xrx500_ambient_probe(struct platform_device *pdev)
{
    int ret = 0;
    void *sink_handle;
#if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE) 
    struct _avm_event_id_mask id_mask;
    //Register notifier to dect loader
    sink_handle = avm_event_sink_register("piglet_ready_sink",
            avm_event_build_id_mask(&id_mask, 1, avm_event_id_piglet),
            avm_event_piglet_ready_cb,
            0
            );
    if(sink_handle == NULL) {
        pr_err("%s not registered\n", __func__);
        return -ENOMEM;
    }
    ambient_brightness_eventhandle = avm_event_source_register( "ambient_brightness",
												   avm_event_build_id_mask(&id_mask, 1, avm_event_id_ambient_brightness),
                                                   avmevent_ambient_notify,
                                                   NULL
                                                   );

    if(ambient_brightness_eventhandle == NULL) {
        printk(KERN_ERR"[%s] avm event ambient brightness register failed !\n", __func__);
        return -1;
	}


#ifdef TEST_AMBIENT_EVENT
    ret = avm_event_sink_register("ambient_brightness",
            avm_event_build_id_mask(&id_mask, 1, avm_event_id_ambient_brightness),
            avm_event_ambient_test,
            0
            );
    if(ret == NULL) {
        pr_err("%s not registered\n", __func__);
        return -ENOMEM;
    }
#endif
#endif/*--- #if defined(CONFIG_AVM_EVENT) || defined(CONFIG_AVM_EVENT_MODULE)  ---*/
    return ret;
}


static struct platform_driver xrx500_ambient_driver = {
	.probe = xrx500_ambient_probe,
	.driver = {
		.name = "avm-ambient-xrx500",
		.owner = THIS_MODULE,
		.of_match_table = xrx500_ambient_match,
	},
};

int __init xrx500_ambient_init(void)
{
	return platform_driver_register(&xrx500_ambient_driver);
}


module_platform_driver(xrx500_ambient_driver);
