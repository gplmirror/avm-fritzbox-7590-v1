#
# AVM general functions 
#
#
menu "AVM Sammeltreiber"

config AVM_SAMMEL
	bool "AVM general support (Event, Watchdog, Debug, Profiling)"
	default y
	help
	  This driver provides general functions for AVMs own drivers.

	  To compile this driver as a module, choose M here. The module
	  will be called avm.

config AVM_EVENT
	bool "AVM event support"
	depends on AVM_SAMMEL
	default y
	help
	  This driver provides support for event handling.

config AVM_EVENT_20
	bool "AVM event support Version 2.0"
	depends on AVM_SAMMEL && AVM_EVENT
	default y
	help
	  This driver provides support for event handling.

config AVM_PWM
	bool "AVM pwm support"
	depends on AVM_SAMMEL
	default n
	help
	  This driver enables PWM support for LEDs.

config AVM_EVENTNODE_PUMA6
	bool "Remote AVM events on Puma6"
	depends on AVM_EVENT_20 && (MACH_PUMA6 || ARCH_GEN3)
	default n
	help
		This driver enables distribution of AVM events between Atom and ARM core on Puma6.

config AVM_EVENTNODE_PUMA7
	bool "Remote AVM events on Puma7"
	depends on AVM_EVENT_20 && (MACH_PUMA7) 
	default n
	help
		This driver enables distribution of AVM events between Atom and ARM core on Puma7.

config AVM_WATCHDOG
	bool "Watchdog support"
	depends on AVM_SAMMEL
	default n
	help
	  This driver provides the watchdog support.

config AVM_DEBUG
	bool "support for AVM Debug"
	depends on AVM_SAMMEL
	default y

config AVM_DEBUG_BUF_SHIFT
    int "debug log buffer size (16 => 64KB, 17 => 128KB)"
    range 16 25
    default 19
    depends on AVM_DEBUG
    help
      Selects the debug buffer size

config AVM_LED_EVENTS
	bool "support for AVM Led event based version"
	depends on AVM_SAMMEL
	default n

config AVM_SIMPLE_PROFILING
	bool "Simple Kernel-Profiler"
	depends on AVM_SAMMEL
	select PERF_EVENTS
	select AVM_PROM_ENVIRONMENT
	default y
	help
      Use cat cat /proc/avm/profile/help to get more information.

config ENABLE_SKB_PROFILING_HOOKS
	bool "Enable SKB-Profiling"
	depends on AVM_SIMPLE_PROFILING
	default n

config ENABLE_SPINLOCK_PROFILING_HOOKS
	bool "Enable Spinlock-Profiling"
	depends on AVM_SIMPLE_PROFILING
	default n

config AVM_PAGE_TRACE
	bool "Enhanced Page-Tracing"
	depends on AVM_SAMMEL
	default y
	help
      get summary of information about page-using

config AVM_NET_EVENT
        bool "AVM Network Event Source"
	depends on AVM_EVENT || AVM_LED_EVENTS || AVM_POWER
	depends on AVM_KERNEL
	select AVM_PROM_ENVIRONMENT
	default n
	help
            generic event source (hardware independent) for AVM_EVENT, AVM_LED and AVM_POWER
            generic ethernet port handler for AVM_POWER. This driver relays on ethtool_ops.

endmenu
