#ifndef __arch_avm_reboot_status_grx_h__
#define __arch_avm_reboot_status_grx_h__

#define UPDATE_REBOOT_STATUS_TEXT            "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                             "(c) AVM 2015, Reboot Status is: Firmware-Update" \
                                             "(c) AVM 2015, Reboot Status is: Firmware-Update"
#define NMI_REBOOT_STATUS_TEXT               "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: NMI-Watchdog"
#define SOFTWATCHDOG_REBOOT_STATUS_TEXT      "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: Software-Watchdog" \
                                             "(c) AVM 2015, Reboot Status is: Software-Watchdog"
#define POWERON_REBOOT_STATUS_TEXT           "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Power-On-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Power-On-Reboot"
#define TEMP_REBOOT_STATUS_TEXT              "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Temperature-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Temperature-Reboot"
#define SOFT_REBOOT_STATUS_TEXT_PANIC        "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(PANIC)"
#define SOFT_REBOOT_STATUS_TEXT_OOM          "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(OOM)"
#define SOFT_REBOOT_STATUS_TEXT_OOPS         "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(OOPS)"
#define SOFT_REBOOT_STATUS_TEXT_UPDATE       "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot\0(REBOOT-FOR-UPDATE)"
/*--- Achtung! Untermenge von obigen Eintraegen: ---*/
#define SOFT_REBOOT_STATUS_TEXT              "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot" \
                                             "(c) AVM 2015, Reboot Status is: Software-Reboot"

#include <linux/avm_kernel_config.h>
#include <asm/reboot.h>
#include <asm/bootinfo.h>

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static char *arch_get_mailbox(void){
    static char *mailbox = 0;
    const struct boot_mem_map_entry *mailbox_resource;


    if(mailbox) {
        return mailbox;
    }

    mailbox_resource = find_reserved_memory("avm_reboot_string");
    if (mailbox_resource)
        mailbox = (void *)KSEG1ADDR(mailbox_resource->addr);

    if (!mailbox) {
        mailbox = (char *)  AVM_REBOOT_STRING_LOCATION;
        pr_err("Didn't found avm_reboot_string, setting mailbox to fallback 0x%08x \n",(uint32_t)  mailbox);
    }

    return mailbox;
}

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static int arch_grx_die_notifier(struct notifier_block *self __maybe_unused, unsigned long cmd __maybe_unused, 
                                 void *ptr __maybe_unused) {
	int this_cpu;
	cpumask_t cpu_mask;

	if(cmd == DIE_OOPS) {
		struct die_args *args = (struct die_args *)ptr;
		struct pt_regs *regs = args->regs;
		static int die_counter;

		oops_enter();

		this_cpu = get_cpu();
		cpumask_setall(&cpu_mask);
		cpumask_clear_cpu(this_cpu, &cpu_mask);
		put_cpu();

		console_verbose();
		bust_spinlocks(1);

		printk("%s[#%d]:\n", args->str, ++die_counter);
		avm_stack_check(current);
		show_registers(regs);
		add_taint(TAINT_DIE, LOCKDEP_NOW_UNRELIABLE);
		arch_trigger_all_cpu_backtrace(&cpu_mask);

		avm_oom_show_memstat(0x1); /*--- oops_exit triggers kmsg_dump() -> last chance to get infos for panic-log ---*/
		oops_exit();
		panic("Fatal exception %s", in_interrupt() ? "in interrupt" : "");
	}
	return NOTIFY_OK;
}
#define arch_die_notifier arch_grx_die_notifier
#endif /*--- #ifndef __arch_avm_reboot_status_grx_h__ ---*/
