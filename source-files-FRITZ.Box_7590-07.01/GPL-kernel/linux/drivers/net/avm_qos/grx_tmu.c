#include <linux/ppa_api.h>
#include <net/lantiq_cbm_api.h>
#include <net/lantiq_cbm.h>
#include <net/pkt_sched.h>
#include <net/avm_qos.h>
#include <linux/if_arp.h>

struct hw_queue {
	struct hlist_node list;
	uint32_t tc;
	uint32_t ep;
	uint32_t priority; /* tc class priority */
	PPA_CMD_QOS_QUEUE_INFO q_info;
	struct kref refcount;
};

/* TODO consider to move bookkeeping to qdisc_priv */
static struct hlist_head q_prio_map[AVM_QOS_PORT_NUM][AVM_QOS_TC_NUM];
static uint32_t default_classid[AVM_QOS_PORT_NUM];

extern void (*ltq_vectoring_priority_hook)(uint32_t priority);

extern PPA_FILE_OPERATIONS *ppadev_fops;
static uint32_t ppacmd(uint32_t cmd, void *arg)
{
	uint32_t ppa_rv;
	mm_segment_t oldseg;

	oldseg = get_fs();
	set_fs(get_ds());

	ppa_rv = ppadev_fops->unlocked_ioctl(NULL, cmd, (unsigned long)arg);

	set_fs(oldseg);

	return ppa_rv;
}

static void oneshot_init(void)
{
	static atomic_t initialized = ATOMIC_INIT(-1);

	if(atomic_inc_and_test(&initialized)) {
		ppacmd(PPA_CMD_ENG_QUEUE_INIT, NULL);
	} else {
		/* roll back to avoid overflow */
		atomic_dec(&initialized);
	}
}

bool avm_qos_netdev_supported(struct net_device *netdev)
{
	dp_subif_t dp_subif = { 0 };
	PPA_VCC *vcc = NULL;

	/* netdev needs to be registered with datapath_api */
	ppa_br2684_get_vcc(netdev, &vcc);
	if(0 != dp_get_netif_subifid(netdev, NULL, vcc, 0, &dp_subif, 0)) {
		return false;
	}

	/* TMU filters can't handle ATM cells */
	if(netdev->type == ARPHRD_ATM) {
		return false;
	}

	return true;
}
EXPORT_SYMBOL(avm_qos_netdev_supported);

/* TODO
 * Remove constraint: default queue has to be set in advance.
 */
void avm_qos_set_default_queue(struct net_device *netdev, uint32_t classid)
{
	dp_subif_t dp_subif = { 0 };
	PPA_VCC *vcc = NULL;
	int ep;
	ppa_br2684_get_vcc(netdev, &vcc);
	dp_get_netif_subifid(netdev, NULL, vcc, 0, &dp_subif, 0);
	ep = dp_subif.port_id;

	pr_debug("%s(0x%x)", __func__, classid);

	default_classid[ep] = classid;
}
EXPORT_SYMBOL(avm_qos_set_default_queue);

static struct hw_queue *hw_queue_lookup(struct net_device *netdev,
                                        uint32_t classid)
{
	int i;
	int ep;
	dp_subif_t dp_subif = { 0 };
	PPA_VCC *vcc = NULL;
	ppa_br2684_get_vcc(netdev, &vcc);
	dp_get_netif_subifid(netdev, NULL, vcc, 0, &dp_subif, 0);
	ep = dp_subif.port_id;

	pr_debug("lookup for classid=%u by %pF\n",
	         classid,
	         __builtin_return_address(0));

	for(i = 0; i < AVM_QOS_TC_NUM; i++) {
		if(!hlist_empty(&q_prio_map[ep][i])) {
			struct hw_queue *hw_q;
			hlist_for_each_entry(hw_q, &q_prio_map[ep][i], list)
			{
				if(hw_q->ep == (uint32_t)dp_subif.port_id &&
				   TC_H_MIN(hw_q->tc) == TC_H_MIN(classid)) {
					pr_debug("found queue_num %u\n", hw_q->q_info.queue_num);
					return hw_q;
				}
			}
		}
	}
	return NULL;
}

static int prio_map_insert(struct hlist_head *prio_map, struct hw_queue *hw_q)
{
	int i;
	pr_debug("insert of classid=%u by %pF\n",
	         hw_q->tc,
	         __builtin_return_address(0));
	for(i = 0; i < AVM_QOS_TC_NUM; i++) {
		if(!hlist_empty(&prio_map[i])) {
			struct hw_queue *existing_q;
			existing_q = hlist_entry(prio_map[i].first, struct hw_queue, list);
			if(existing_q->priority == hw_q->priority) {
				break;
			} else if(existing_q->priority > hw_q->priority) {
				int j;
				pr_warn("%s: untested code path\n", __func__);
				/* make room for a new head */
				for(j = AVM_QOS_TC_NUM - 1; j > i; j--) {
					hlist_move_list(&prio_map[j - 1], &prio_map[j]);
				}
				break;
			}
		} else
			break;
	}
	BUG_ON(i >= AVM_QOS_TC_NUM);
	hlist_add_head(&hw_q->list, &prio_map[i]);
	return i;
}

static void sanitize_prio_map(struct hlist_head *prio_map)
{
	int i;
	for(i = 0; i < AVM_QOS_TC_NUM; i++) {
		struct hw_queue *hw_q;

		if(hlist_empty(&prio_map[i])) continue;

		hlist_for_each_entry(hw_q, &prio_map[i], list)
		{
			if(hw_q->q_info.priority != i + 1) {
				int ppa_rv;

				pr_debug("need to change priority for class 0x%x\n", hw_q->tc);
				ppa_rv = ppacmd(PPA_CMD_DEL_QOS_QUEUE, &hw_q->q_info);
				if(ppa_rv != PPA_SUCCESS) {
					pr_err("[%s] error removing queue. (ret=%d)\n",
					       __func__,
					       ppa_rv);
				}
				hw_q->q_info.priority += 1;
				ppa_rv = ppacmd(PPA_CMD_ADD_QOS_QUEUE, &hw_q->q_info);
				if(ppa_rv != PPA_SUCCESS) {
					pr_err("[%s] error removing queue. (ret=%d)\n",
					       __func__,
					       ppa_rv);
				}
			}
		}
	}
}

int avm_qos_add_hw_queue(struct net_device *netdev,
                         uint32_t classid,
                         uint32_t priority,
                         uint32_t weight)
{
	dp_subif_t dp_subif = { 0 };
	PPA_VCC *vcc = NULL;
	struct hw_queue *new_hw_q;
	int32_t ppa_rv;
	static int32_t next_queue_num = 1;

	pr_debug("called with netdev=%s classid=%x prio=%u weight=%u\n",
	         netdev->name,
	         classid,
	         priority,
	         weight);

	oneshot_init();

	ppa_br2684_get_vcc(netdev, &vcc);
	dp_get_netif_subifid(netdev, NULL, vcc, 0, &dp_subif, 0);

	if(hw_queue_lookup(netdev, classid)) {
		pr_err("queue already exists!\n");
		return -EINVAL;
	}

	pr_debug("allocate queue for port=%d subif=%d\n",
	         dp_subif.port_id,
	         dp_subif.subif);
	new_hw_q = ppa_malloc(sizeof(*new_hw_q));
	BUG_ON(!new_hw_q);
	INIT_HLIST_NODE(&new_hw_q->list);
	new_hw_q->ep = dp_subif.port_id;
	new_hw_q->tc = classid;
	new_hw_q->priority = priority;
	kref_init(&new_hw_q->refcount);

	memset(&new_hw_q->q_info, 0, sizeof(new_hw_q->q_info));
	strlcpy(new_hw_q->q_info.ifname,
	        netdev->name,
	        sizeof(new_hw_q->q_info.ifname));

	pr_debug("enqueue queue info\n");
	new_hw_q->q_info.priority =
	 prio_map_insert(q_prio_map[new_hw_q->ep], new_hw_q) + 1;
	sanitize_prio_map(q_prio_map[new_hw_q->ep]);

	/* learn prio for vectoring management */
	{
		/* highest prio means lowest value */
		static uint16_t highest_prio = 0xffff;

		/* [TODO] learnt prio should be reset at some point */
		if(ltq_vectoring_priority_hook &&
		   highest_prio > new_hw_q->q_info.priority) {
			highest_prio = new_hw_q->q_info.priority;
			ltq_vectoring_priority_hook(classid);
		}
	}

	new_hw_q->q_info.queue_num = next_queue_num;
	new_hw_q->q_info.shaper_num = next_queue_num++;
	new_hw_q->q_info.shaper.enable = 1; /* enable shaping capability */
	new_hw_q->q_info.weight = weight;
	new_hw_q->q_info.sched = weight ? PPA_QOS_SCHED_WFQ : PPA_QOS_SCHED_SP;
	new_hw_q->q_info.tc_no = 1;
	new_hw_q->q_info.tc_map[0] = TC_H_MIN(classid);
	if(TC_H_MIN(classid) == default_classid[new_hw_q->ep]) {
		/* PHU: tmu_hal sets a default priority instead of a default
		 * class internally, so setting PPA_QOS_Q_F_DEFAULT messes
		 * with the TC to Q mapping for WFQ queues that have the same
		 * priority as the default queue. To work around this, we just
		 * assume TC 0 as the default class and map this to our
		 * default queue */
		//new_hw_q->q_info.flags |= PPA_QOS_Q_F_DEFAULT;
		new_hw_q->q_info.tc_no = 2;
		new_hw_q->q_info.tc_map[1] = 0;
	}
	new_hw_q->q_info.enable = 1; /* TODO necessary? */
	pr_debug("add queue to ppa (prio: %u weight: %u)\n",
	         new_hw_q->q_info.priority,
	         weight);
	ppa_rv = ppacmd(PPA_CMD_ADD_QOS_QUEUE, &new_hw_q->q_info);
	if(ppa_rv != PPA_SUCCESS) {
		pr_err("[%s] error adding egress queue for %s (ret=%d)\n",
		       __func__,
		       netdev->name,
		       ppa_rv);
	}
	pr_debug("queue_num=%d\n", new_hw_q->q_info.queue_num);
	new_hw_q->q_info.flags = 0;

	return 0;
}
EXPORT_SYMBOL(avm_qos_add_hw_queue);

static void release_hw_queue(struct kref *refcount)
{
	struct hw_queue *q;
	q = container_of(refcount, typeof(*q), refcount);
	hlist_del(&q->list);
	kfree(q);
}

int avm_qos_set_queue_len(struct net_device *netdev,
                          uint32_t classid,
                          uint32_t len)
{
	struct hw_queue *hw_q;
	int32_t ppa_rv;
	enum { bytes_per_packet = 340 }; /* average of "simple IMIX" */

	if(len == 0) return -EINVAL;

	hw_q = hw_queue_lookup(netdev, classid);
	if(!hw_q) {
		pr_err("[%s] No queue found for classid %u\n", __func__, classid);
		return -ENOENT;
	}

	/*
	 * clamp to a min HW-value of 1 and some PPA default as max
	 * TODO: choose lower limit depending on the TBF (refill) rate
	 */
	len = clamp(len / bytes_per_packet, 8u, 280u);

	hw_q->q_info.drop.mode = PPA_QOS_DROP_WRED;
	hw_q->q_info.drop.wred.min_th0 = len/8;
	hw_q->q_info.drop.wred.max_th0 = (len/8)+1;
	hw_q->q_info.drop.wred.min_th1 = len/8;
	hw_q->q_info.drop.wred.max_th1 = (len/8)+1;

	ppa_rv = ppacmd(PPA_CMD_MOD_QOS_QUEUE, &hw_q->q_info);
	if(ppa_rv != PPA_SUCCESS) {
		pr_err("[%s] error modifying egress queue for %s (ret=%d)\n",
		       __func__,
		       netdev->name,
		       ppa_rv);
	}

	return 0;
}
EXPORT_SYMBOL(avm_qos_set_queue_len);

int avm_qos_remove_hw_queue(struct net_device *netdev,
                            uint32_t classid,
                            uint32_t priority,
                            uint32_t weight)
{
	struct hw_queue *hw_q;
	int32_t ppa_rv;

	pr_debug(
	 "TMU_QOS remove called with netdev=%s classid=%x prio=%u weight=%u\n",
	 netdev->name,
	 classid,
	 priority,
	 weight);

	hw_q = hw_queue_lookup(netdev, classid);

	if(hw_q) {
		if(classid == default_classid[hw_q->ep]) {
			default_classid[hw_q->ep] = 0;
		}
		ppa_rv = ppacmd(PPA_CMD_DEL_QOS_QUEUE, &hw_q->q_info);
		if(ppa_rv != PPA_SUCCESS) {
			pr_err("[%s] error removing egress queue for %s (ret=%d)\n",
			       __func__,
			       netdev->name,
			       ppa_rv);
		}
		kref_put(&hw_q->refcount, release_hw_queue);
	}

	return 0;
}
EXPORT_SYMBOL(avm_qos_remove_hw_queue);

int avm_qos_flush_hw_queues(struct net_device *netdev)
{
	int i;
	dp_subif_t dp_subif = { 0 };
	PPA_VCC *vcc = NULL;
	int ep, ppa_rv;

	BUG_ON(!netdev);

	ppa_br2684_get_vcc(netdev, &vcc);
	dp_get_netif_subifid(netdev, NULL, vcc, 0, &dp_subif, 0);
	ep = dp_subif.port_id;

	default_classid[ep] = 0;
	for(i = 0; i < 16; i++) {
		if(!hlist_empty(&q_prio_map[ep][i])) {
			struct hw_queue *hw_q;
			struct hlist_node *tmp;
			hlist_for_each_entry_safe(hw_q, tmp, &q_prio_map[ep][i], list)
			{
				BUG_ON(hw_q->ep != (uint32_t)ep);
				pr_debug("remove queue for classid %d ep %d\n",
				         hw_q->tc,
				         hw_q->ep);
				ppa_rv = ppacmd(PPA_CMD_DEL_QOS_QUEUE, &hw_q->q_info);
				if(ppa_rv != PPA_SUCCESS) {
					pr_err("[%s] error removing egress queue for %s (ret=%d)\n",
					       __func__,
					       netdev->name,
					       ppa_rv);
				}
				kref_put(&hw_q->refcount, release_hw_queue);
			}
		}
	}


	return 0;
}
EXPORT_SYMBOL(avm_qos_flush_hw_queues);

static int avm_qos_reset_shaper(struct net_device *netdev, uint32_t classid)
{
	PPA_CMD_RATE_INFO rate_info = { 0 };
	struct hw_queue *hw_q = NULL;

	pr_debug("reset for \"%s\"\n", netdev->name);

	hw_q = hw_queue_lookup(netdev, classid);

	strlcpy(rate_info.ifname, netdev->name, sizeof(rate_info.ifname));

	if(hw_q) {
		pr_debug("reset shaper %u of queue %u\n",
		         rate_info.queueid,
		         rate_info.shaperid);
		rate_info.queueid = hw_q->q_info.queue_num;
		rate_info.shaperid = hw_q->q_info.shaper_num;
		kref_put(&hw_q->refcount, release_hw_queue);
	} else {
		pr_debug("reset port shaper\n");
		rate_info.shaperid = -1;
		rate_info.queueid = -1;
	}
	ppacmd(PPA_CMD_RESET_QOS_RATE, &rate_info);

	return 0;
}

int avm_qos_reset_prio_shaper(struct net_device *netdev, uint32_t classid)
{
	return avm_qos_reset_shaper(netdev, classid);
}
EXPORT_SYMBOL(avm_qos_reset_prio_shaper);

int avm_qos_reset_port_shaper(struct net_device *netdev)
{
	return avm_qos_reset_shaper(netdev, -1);
}
EXPORT_SYMBOL(avm_qos_reset_port_shaper);

static int avm_qos_set_shaper(struct net_device *netdev,
                              uint32_t classid,
                              uint32_t pir,
                              uint32_t cir,
                              uint32_t pbs,
                              uint32_t cbs,
                              bool init)
{
	PPA_CMD_RATE_INFO rate_info = { 0 };
	struct hw_queue *hw_q;

	hw_q = hw_queue_lookup(netdev, classid);
	pr_debug("set pir=%u cir=%u pbs=%u cbs=%u init=%sfor \"%s\"\n",
	         pir,
	         cir,
	         pbs,
	         cbs,
             init?"yes":"no",
	         netdev->name);

	oneshot_init();
	strlcpy(rate_info.ifname, netdev->name, sizeof(rate_info.ifname));

	if(hw_q) {
		rate_info.queueid = hw_q->q_info.queue_num;
		rate_info.shaperid = hw_q->q_info.shaper_num;
		pr_debug("queue_num=%u shaper_num=%u\n",
		         hw_q->q_info.queue_num,
		         hw_q->q_info.queue_num);
		if(init)
			kref_get(&hw_q->refcount);
	} else {
		rate_info.shaperid = -1;
		rate_info.queueid = -1;
		if(!init && (classid != -1)){
			pr_err("[%s] tc change should find a valid hw_q\n", __func__);
		}
 	}
	

	rate_info.shaper.pir = pir / 125; /* kbit/s */
	rate_info.shaper.cir = cir / 125; /* kbit/s */
	rate_info.shaper.pbs = pbs;
	rate_info.shaper.cbs = cbs;
	rate_info.shaper.enable = 1;
	rate_info.flags = init ? 0 : AVM_NO_Q_FLUSH;

	// rate_info.shaper.mode = PPA_QOS_SHAPER_COLOR_BLIND;

	pr_debug("set shaper\n");
	ppacmd(PPA_CMD_SET_QOS_SHAPER,
	       &rate_info); /* allocate shaperid, store settings in tmu_hal */

	pr_debug("set rate for shaper %d on queue %d\n",
	         rate_info.shaper.phys_shaperid,
	         rate_info.queueid);
	ppacmd(PPA_CMD_SET_QOS_RATE, &rate_info); /* cmd_info->qos_rate_info */

	pr_debug("exit\n");
	return 0;
}

int avm_qos_set_port_shaper(struct net_device *netdev,
                            uint32_t pir,
                            uint32_t cir,
                            uint32_t pbs,
                            uint32_t cbs,
                            int8_t overhead,
                            bool init)
{
	dp_subif_t dp_subif = { 0 };
	PPA_VCC *vcc = NULL;
	int ep;
	ppa_br2684_get_vcc(netdev, &vcc);
	dp_get_netif_subifid(netdev, NULL, vcc, 0, &dp_subif, 0);
	ep = dp_subif.port_id;

	/* consider pmac header overhead unless it's DSL */
	cbm_enqueue_port_overhead_set(ep, ep == 7 ? overhead : overhead - 8);

	return avm_qos_set_shaper(netdev, -1, pir, cir, pbs, cbs, init);
}
EXPORT_SYMBOL(avm_qos_set_port_shaper);

int avm_qos_get_prio_stats(struct net_device *netdev, uint32_t classid, struct avm_qos_stats *stats)
{
	struct hw_queue *q;
	PPA_CMD_QOS_MIB_INFO qos_mib_info = { 0 };

	q = hw_queue_lookup(netdev, classid);
	if(!q) return -ENOENT;

	strlcpy(qos_mib_info.ifname, netdev->name, sizeof(qos_mib_info.ifname));
	qos_mib_info.queue_num = q->q_info.queue_num;

	if(ppacmd(PPA_CMD_GET_QOS_MIB, &qos_mib_info) != PPA_SUCCESS) {
		pr_err("[%s] could not obtain queue stats for queue_num %d\n",
		       __func__,
		       q->q_info.queue_num);
		return -1;
	}

	stats->valid_bytes = stats->valid_packets = 1;
	/* those are the per queue counters of /proc/dp/rmon */
	stats->packets = qos_mib_info.mib.total_rx_pkt; /* [TODO] need to add qocc here */
	stats->bytes = qos_mib_info.mib.total_tx_pkt;

	return 0;
}
EXPORT_SYMBOL(avm_qos_get_prio_stats);

int avm_qos_set_prio_shaper(struct net_device *netdev,
                            uint32_t classid,
                            uint32_t pir,
                            uint32_t cir,
                            uint32_t pbs,
                            uint32_t cbs,
                            bool init)
{
	return avm_qos_set_shaper(netdev, classid, pir, cir, pbs, cbs, init);
}
EXPORT_SYMBOL(avm_qos_set_prio_shaper);
