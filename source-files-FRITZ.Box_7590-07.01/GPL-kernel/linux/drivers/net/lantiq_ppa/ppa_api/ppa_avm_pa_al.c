#include <linux/ip.h>
#include <net/ip.h>
#include <net/ipv6.h>
#include <net/ip6_tunnel.h>
#include <linux/if_vlan.h>
#include <linux/netdevice.h>
#include <net/ppa_stack_al.h>
#include <net/ppa_api.h>
#include <net/ppa_hook.h>
#include <linux/types.h>
#include <linux/avm_pa.h>
#include <linux/avm_pa_hw.h>
#include <net/ppa_avm_pa_al.h>
#include <linux/workqueue.h>
#include <linux/pkt_sched.h>
#include <linux/time.h>
#include <linux/simple_proc.h>
#include <net/ppa_avm_pa_al.h>
#include <lantiq.h>
#include <lantiq_soc.h>
#include <net/ltq_mpe_api.h>
#include <net/datapath_api.h>
#include <asm/mach-lantiq/cpufreq/ltq_cpufreq.h>
#include "ppa_api_misc.h"
#include "ppa_api_netif.h"
#include "ppa_stack_tnl_al.h"
#define FENTRY_BUF(ppa_buf)                                               \
	pr_debug("called sh=%d, by %pF\n", ((ppa_buf) && (ppa_buf)->pa_session) ?     \
	                            ((ppa_buf)->pa_session->session_handle) : \
	                            0, __builtin_return_address(0))
#define FENTRY_NO_BUF pr_debug("called\n");

#define RT_EXTID_TCP	    0
#define RT_EXTID_UDP	    100

#define TX_CHAN_GRACE_PERIOD 100

#define GC_DEBUG_THRESHOLD 500
#define GC_DEBUG_DISABLED UINT_MAX
static size_t gc_failed = 0;
static size_t gc_ok = 0;

atomic_t active_sessions = ATOMIC_INIT(0);
struct net_device *avm_ppa_localif;
EXPORT_SYMBOL(avm_ppa_localif);

/*
 * Part of the subif_id that's reserved for a VAP id.
 * It is actually meant to be a 'Virtual AP' for Wifi, but we adopt the more
 * generic usage of the term from datapath_api, where it describes per-flow private
 * data determined by the registered driver.
 */
#define VAP_T_MAX 0xff

/*
 * Map vap to a avm pa session
 * Map to the actual session instead of a handle to get access to the associated
 * PIDs later on.
 */
static const struct avm_pa_session *vap_to_session[VAP_T_MAX + 1];

static bool find_vap(uint8_t *vap, const struct avm_pa_session *session)
{
	unsigned int i;
	BUG_ON(vap == NULL);

	for (i = 0; i < ARRAY_SIZE(vap_to_session); i++) {
		if (vap_to_session[i] == session) {
			*vap = i;
			return true;
		}
	}
	return false;
}

static bool _alloc_vap(uint8_t *vap)
{
	return find_vap(vap, NULL);
}

static bool assoc_vap(const struct avm_pa_session *session)
{
	uint8_t vap;

	if (_alloc_vap(&vap)) {
		vap_to_session[vap] = session;
		return true;
	}

	return false;
}

static bool disassoc_vap(const struct avm_pa_session *session)
{
	uint8_t vap;

	if (find_vap(&vap, session)) {
		vap_to_session[vap] = NULL;
		return true;
	}

	return false;
}

/* used to inhibit CPU downscaling e.g. for CPU-bound real-time sessions */
static bool low_cpu_latency_required;

struct session_work_item {
	struct avm_pa_session *session;
	struct work_struct work;
};

PPA_FILE_OPERATIONS *ppadev_fops;
EXPORT_SYMBOL(ppadev_fops);
static struct workqueue_struct *sessions_workqueue;
static int get_brif(PPA_NETIF *netif, PPA_NETIF **brif);

static LIST_HEAD(dpipe_netdevs_alive);
static LIST_HEAD(dpipe_netdevs_dead);
static DEFINE_SPINLOCK(avm_dpipe_lock);

static uint64_t hw_bytes_last[CONFIG_AVM_PA_MAX_SESSION];
static struct avm_pa_session *handle_to_session[CONFIG_AVM_PA_MAX_SESSION];

static bool pid_is_local(uint32_t pid_handle);
static bool pid_is_valid(uint32_t pid_handle);
static void *get_hdr_from_pa_match(const struct avm_pa_pkt_match *match,
                                   unsigned char type);
static void release_hash_item(struct kref *r);

int32_t (*build_netif_tree_hook)(PPA_BUF *) = NULL;
EXPORT_SYMBOL(build_netif_tree_hook);

void (*reset_netif_tree_hook)(void) = NULL;
EXPORT_SYMBOL(reset_netif_tree_hook);

/*
 * AVM PA hardware session handler
 */

static uint32_t ppacmd(uint32_t cmd, void *arg)
{
	uint32_t ppa_rv;
	mm_segment_t oldseg;

	/* adjust segment boundaries in order to pass access_ok() checks
	 * during ioctl call */
	oldseg = get_fs();
	set_fs(get_ds());

	ppa_rv = ppadev_fops->unlocked_ioctl(NULL, cmd, (unsigned long)arg);

	set_fs(oldseg);

	return ppa_rv;
}

static int32_t
get_routing_session_by_handle(PPA_CMD_SESSIONS_INFO *sessions_info,
                              uint32_t max_count,
                              uint32_t session_handle)
{
	uint32_t ppa_rv;

	memset(&sessions_info->count_info, 0, sizeof(sessions_info->count_info));

	sessions_info->count_info.count = max_count;
	sessions_info->count_info.stamp_flag = 0;
	sessions_info->count_info.hash_index = session_handle + 1;
	sessions_info->count_info.flag = SESSION_ADDED_IN_HW;

	ppa_rv = ppacmd(PPA_CMD_GET_LAN_WAN_SESSIONS, sessions_info);

	return ppa_rv;
}

static int collect_bridging_count(struct avm_pa_session *session,
                                  struct avm_pa_session_stats *ingress)
{
	struct avm_pa_pid_hwinfo *hwinfo;
	uint32_t last_hit_time;
	PPA_NETIF *brif;

	ingress->validflags = 0;
	if(session->bsession) {

		hwinfo = avm_pa_pid_get_hwinfo(avm_pa_first_egress(session)->pid_handle);
		BUG_ON(!hwinfo);

		brif = get_brif(hwinfo->ppa.netdev, &brif) ? NULL : brif;
		if(ppa_hook_bridge_entry_hit_time_fn(session->bsession->ethh.h_dest,
		                                     brif,
		                                     &last_hit_time) == PPA_HIT) {
			uint8_t is_static;
			uint32_t now, delta;

			/* PPA_HIT just means that the session isn't
			 * timed out yet, so we have to consider the
			 * actual hit time */
			now = ppa_get_time_in_sec();
			is_static = now < last_hit_time; /* false -> not sure */
			/* DEFAULT_BRIDGING_TIMEOUT_IN_SEC */
			if(is_static) last_hit_time += UINT_MAX - 300;
			delta = now - last_hit_time;

			pr_debug("session %d hit last at %u (delta=%u).\n",
				 session->session_handle, last_hit_time, delta);
			if(delta <= CONFIG_LTQ_PPA_AVM_PA_COUNTER_INTERVAL/1000) {
				pr_debug("report.\n");
				ingress->validflags = AVM_PA_SESSION_STATS_VALID_HIT;
			} else {
				ingress->validflags = 0;
			}
		} else {
			pr_debug("bridging session %d not (yet) registered with PPA.\n",
				 session->session_handle);
		}
	}

	return 0;
}

static int collect_routing_count(struct avm_pa_session *pa_session,
                                 struct avm_pa_session_stats *ingress)
{
	PPA_CMD_SESSION_ENTRY *session_entry;
	uint64_t delta;
	uint64_t hw_bytes;
	PPA_CMD_SESSIONS_INFO sessions_info;
	uint32_t ppa_rv;
	const uint32_t max_count = 1;
	void *hw_session;

	FENTRY_NO_BUF;

	/* only fetch session count if session was added successfully */
	hw_session = avm_pa_get_hw_session(pa_session);
	if(hw_session == NULL || ((char *)hw_session)[0] != 'y') return -ENOENT;

	ppa_rv = get_routing_session_by_handle(&sessions_info,
	                                       max_count,
	                                       pa_session->session_handle);

	if(ppa_rv != PPA_SUCCESS || sessions_info.count_info.count != 1) {
		pr_debug(
		 "could not get ppa session info for avm_pa session %d (results: %d)\n",
		 pa_session->session_handle,
		 sessions_info.count_info.count);
		return -ENOENT;
	}

	/* aliases */
	session_entry = &sessions_info.session_list[0];
	hw_bytes = session_entry->hw_bytes;

	if(unlikely(hw_bytes < hw_bytes_last[pa_session->session_handle])) {
		/* counter wraparound behandeln, falls eine session
		 * mal 4679 Jahre mit 1Gbps läuft. ;) */
		delta =
		 hw_bytes + ULLONG_MAX - hw_bytes_last[pa_session->session_handle];
	} else {
		delta = hw_bytes - hw_bytes_last[pa_session->session_handle];
	}

	pr_debug(
	 "pa_session %d (hash: %u) delta=%llu hw_bytes=%llu hw_bytes_last=%llu\n",
	 pa_session->session_handle,
	 session_entry->hash,
	 delta,
	 hw_bytes,
	 hw_bytes_last[pa_session->session_handle]);

	if (delta){
		ingress->validflags = AVM_PA_SESSION_STATS_VALID_HIT;
		ingress->validflags |= AVM_PA_SESSION_STATS_VALID_BYTES;
		ingress->tx_bytes = delta;
	} else  {
		ingress->validflags = 0;
	}

	hw_bytes_last[pa_session->session_handle] = hw_bytes;

	return 0;
}

static void _map_session_netdevs(struct avm_pa_session *session,
                                 void (*fn)(struct net_device *))
{
	struct avm_pa_pid_hwinfo *eg_hwinfo, *ig_hwinfo;
	eg_hwinfo = avm_pa_pid_get_hwinfo(avm_pa_first_egress(session)->pid_handle);
	ig_hwinfo = avm_pa_pid_get_hwinfo(session->ingress_pid_handle);
	BUG_ON(!(eg_hwinfo && ig_hwinfo));

	if(eg_hwinfo->ppa.netdev) fn(eg_hwinfo->ppa.netdev);
	if(ig_hwinfo->ppa.netdev) fn(ig_hwinfo->ppa.netdev);
}

static void hold_netdevs(struct avm_pa_session *session)
{
	_map_session_netdevs(session, dev_hold);
}

static void put_netdevs(struct avm_pa_session *session)
{
	_map_session_netdevs(session, dev_put);
}

/* get bridge from a bridge port via linux stack */
static int _get_brif(PPA_NETIF *netif, PPA_NETIF **brif, bool wait)
{
	if(!netif || !brif) return -EINVAL;
	if((netif->priv_flags & IFF_BRIDGE_PORT) == 0) {
		return -ENOENT;
	}

	if(wait) {
		rtnl_lock();
	} else if(rtnl_trylock() != 1) {
		return -EAGAIN;
	}

	*brif = ppa_netdev_master_upper_dev_get(netif);
	if(!*brif || !ppa_if_is_br_if(*brif, (*brif)->name)) {
		rtnl_unlock();
		return -ENOENT;
	}

	rtnl_unlock();
	return 0;
}

static int get_brif(PPA_NETIF *netif, PPA_NETIF **brif)
{
	return _get_brif(netif, brif, 0);
}

static int get_brif_bh(PPA_NETIF *netif, PPA_NETIF **brif)
{
	BUG_ON(in_atomic());
	return _get_brif(netif, brif, 1);
}

static int add_br_session(PPA_BUF *ppa_buf)
{
	struct avm_pa_pid_hwinfo *avm_hwinfo;
	PPA_NETIF *brif, *portif;
	int ppa_rv = PPA_FAILURE;
	struct avm_pa_session *session;

	BUG_ON(!ppa_buf);
	session = ppa_buf->pa_session;

	avm_hwinfo = avm_pa_pid_get_hwinfo(avm_pa_first_egress(session)->pid_handle);
	if(!avm_hwinfo) {
		avm_pa_set_hw_session( session, "no hwinfo" );
		return AVM_PA_TX_ERROR_SESSION;
	}
	portif = avm_hwinfo->ppa.netdev;

	if(get_brif_bh(portif, &brif)){
		avm_pa_set_hw_session( session, "no bridging if" );
	       	return AVM_PA_TX_ERROR_SESSION;
	}

	if(ppa_hook_bridge_entry_add_fn != NULL) {
		ppa_rv =
		 ppa_hook_bridge_entry_add_fn(session->bsession->ethh.h_dest,
		                              // brif, portif, PPA_F_STATIC_ENTRY);
		                              brif, portif, 0);
	}

	if(ppa_rv == PPA_SESSION_ADDED) {
		return AVM_PA_TX_SESSION_ADDED;
	} else {
		/* this happens when the GSWIP-R MAC table lock couldn't be
		 * claimed for too long */
		pr_warn("[%s] failed\n", __func__);
		avm_pa_set_hw_session( session, "failed" );
		return AVM_PA_TX_ERROR_SESSION;
	}
}

static int remove_br_session(PPA_BUF *ppa_buf)
{
	struct avm_pa_pid_hwinfo *avm_hwinfo;
	PPA_NETIF *brif, *portif;
	struct avm_pa_session *session;

	BUG_ON(!ppa_buf);
	session = ppa_buf->pa_session;

	avm_hwinfo = avm_pa_pid_get_hwinfo(avm_pa_first_egress(session)->pid_handle);
	if(!avm_hwinfo) return AVM_PA_TX_ERROR_SESSION;
	portif = avm_hwinfo->ppa.netdev;

	if(get_brif_bh(portif, &brif)) return AVM_PA_TX_ERROR_SESSION;

	if(ppa_hook_bridge_entry_delete_fn != NULL) {
		ppa_hook_bridge_entry_delete_fn(session->bsession->ethh.h_dest, brif,
		                                0);
		return AVM_PA_TX_OK;
	} else {
		return AVM_PA_TX_ERROR_SESSION;
	}
}

int add_rt_session(PPA_BUF *ppa_buf)
{
	int32_t ppa_rv;
	struct avm_pa_session *session;

	BUG_ON(!ppa_buf);
	session = ppa_buf->pa_session;
	FENTRY_BUF(ppa_buf);

	if (pid_is_local(avm_pa_first_egress(session)->pid_handle)) {
		if (!assoc_vap(session)) {
			/*
			 * We have to keep the session alive, as we might be
			 * racing with the synchronous part of remove_session
			 * at this point in time.
			 */
			avm_pa_set_hw_session(session, "no vap mappings left");
			return AVM_PA_TX_ERROR_SESSION;
		}
	}

	pr_debug("add from PREROUTING (%d)\n", ppa_buf->pa_session->session_handle);
	ppa_buf->state = PPA_BUF_PREROUTING;
	ppa_rv =
	 ppa_hook_session_add_fn(ppa_buf, session, PPA_F_BEFORE_NAT_TRANSFORM);
	if(ppa_rv != PPA_SESSION_FILTED) {
		pr_err("[%s] session not filtered as expected (rv=%d)\n", __func__,
		       ppa_rv);
		avm_pa_set_hw_session( session, "session not filtered as expected" );
		goto fail;
	}

	pr_debug("add from POSTROUTING (%d)\n",
	         ppa_buf->pa_session->session_handle);
	ppa_buf->state = PPA_BUF_POSTROUTING;

	if (possible_lro_session(session))
		ppa_buf->post_routing_flags |= PPA_F_SESSION_LOCAL_IN;

	ppa_rv = ppa_hook_session_add_fn(ppa_buf, session, ppa_buf->post_routing_flags);
	if(ppa_rv != PPA_SESSION_ADDED) {
		pr_debug("[%s] session not added (rv=%d)\n", __func__, ppa_rv);
		avm_pa_set_hw_session( session, "failed" );
		goto fail;
	}

	hw_bytes_last[session->session_handle] = 0;

	pr_debug("success\n");
	return AVM_PA_TX_SESSION_ADDED;

fail:
	disassoc_vap(session);
	return AVM_PA_TX_ERROR_SESSION;
}

void add_session_worker(struct work_struct *work)
{
	struct session_work_item *session_work;
	struct avm_pa_session *session;
	PPA_BUF ppa_buf;
	int rv;

	BUG_ON(!work);

	session_work = container_of(work, typeof(*session_work), work);
	session = session_work->session;
	memset(&ppa_buf, 0, sizeof(ppa_buf));
	ppa_buf.pa_session = session;


	if( build_netif_tree_hook &&
	    build_netif_tree_hook(&ppa_buf) != PPA_SUCCESS){
		avm_pa_set_hw_session( session, "vdevice not supported" );
		kfree(session_work);
		put_netdevs(session);
		return;
	}

	if(session->bsession) {
		rv = add_br_session(&ppa_buf);
	} else {
		rv = add_rt_session(&ppa_buf);
	}

	if(rv == AVM_PA_TX_SESSION_ADDED) {
		avm_pa_set_hw_session( session, "yes" );
	}

	atomic_inc(&active_sessions);

	kfree(session_work);
}

static bool pid_is_local(uint32_t pid_handle)
{
	struct avm_pa_pid_hwinfo *hwinfo;

	hwinfo = avm_pa_pid_get_hwinfo(pid_handle);

	return (hwinfo && (hwinfo->ppa.local_stack));
}

uint32_t ppa_avm_get_vap(PPA_BUF *ppa_buf)
{
	uint8_t vap;

	if (pid_is_local(avm_pa_first_egress(ppa_buf->pa_session)->pid_handle)) {
		if (find_vap(&vap, ppa_buf->pa_session)) {
			/* vap may be 0 */
			return vap;
		} else {
			/*
			 * session creation should have been prevented in the
			 * first place.
			 */
			BUG();
		}
	} else {
		return 0;
	}
}
EXPORT_SYMBOL(ppa_avm_get_vap);

static int session_proto_udp(const struct avm_pa_session *sess)
{
	const struct avm_pa_pkt_match *info = &sess->ingress;

	if(info->pkttype & AVM_PA_PKTTYPE_IP_MASK)
		return (info->pkttype & AVM_PA_PKTTYPE_PROTO_MASK) == IPPROTO_UDP;

	return false;
}

static int session_proto_tcp(const struct avm_pa_session *sess)
{
	const struct avm_pa_pkt_match *info = &sess->ingress;

	if(info->pkttype & AVM_PA_PKTTYPE_IP_MASK)
		return (info->pkttype & AVM_PA_PKTTYPE_PROTO_MASK) == IPPROTO_TCP;

	return false;
}


bool possible_lro_session(struct avm_pa_session *session){
	bool res = false;

	if(!avm_toe_lro_enabled()) {
		pr_debug("No TOE support for GRX350 600MHz\n");
		return false;
	}

	if (pid_is_local(avm_pa_first_egress(session)->pid_handle)) {
		pr_debug("pid is local\n");
		if (session_proto_tcp(session)){
			pr_debug("session is tcp\n");
			res = true;
		}
	}
	pr_debug("do lro check: %s\n", res?"is_lro":"no_lro");
	return res;
}
EXPORT_SYMBOL(possible_lro_session);

static bool pid_is_valid(uint32_t pid_handle)
{
	struct avm_pa_pid_hwinfo *hwinfo;

	hwinfo = avm_pa_pid_get_hwinfo(pid_handle);

	if(hwinfo) {
		pr_debug("atmvcc=%p netdev=%p\n", hwinfo->atmvcc, hwinfo->ppa.netdev);
	}

	return (hwinfo && (!hwinfo->atmvcc != !hwinfo->ppa.netdev));
}

int add_session(struct avm_pa_session *session)
{
	struct session_work_item *session_work;
	struct avm_pa_egress    *egress = avm_pa_first_egress(session);
	struct avm_pa_pkt_match *imatch = &session->ingress;
	struct avm_pa_pkt_match *ematch = &egress->match;

	pr_debug("avm_pa sh=%d\n", session->session_handle);

	if(session->negress > 1) {
		pr_debug("constraint failure: multiple egress not supported\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if(!session->bsession && !session_proto_tcp(session) &&
	   !session_proto_udp(session)) {
		pr_debug("routing session is neither UDP nor TCP.\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if(!session->bsession &&
	   (((imatch->pkttype & AVM_PA_PKTTYPE_IPENCAP_MASK) ==
	     AVM_PA_PKTTYPE_IPV4ENCAP) ||
	    ((ematch->pkttype & AVM_PA_PKTTYPE_IPENCAP_MASK) ==
	     AVM_PA_PKTTYPE_IPV4ENCAP))) {
		pr_debug("routing session uses IPv4 encapsulation.\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if(imatch->casttype == AVM_PA_IS_MULTICAST ||
	   ematch->casttype == AVM_PA_IS_MULTICAST) {
		pr_debug("constraint failure: multicast not yet supported\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if(!pid_is_valid(session->ingress_pid_handle)) {
		pr_debug("constraint failure: no ingress device\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	if (pid_is_local(egress->pid_handle)) {
		pr_debug("add_session: local session(maybe lro)\n");
	} else if(!pid_is_valid(egress->pid_handle)) {
		pr_debug("constraint failure: no egress device\n");
		return AVM_PA_TX_ERROR_SESSION;
	}

	session_work = kmalloc(sizeof(*session_work), GFP_ATOMIC);
	if(!session_work) {
		pr_err("[%s] oom\n", __func__);
		return AVM_PA_TX_ERROR_SESSION;
	}
	INIT_WORK(&session_work->work, add_session_worker);
	session_work->session = session;

	avm_pa_set_hw_session( session, "adding" );
	hold_netdevs(session);
	handle_to_session[session->session_handle] = session;
	queue_work(sessions_workqueue, &session_work->work);

	return AVM_PA_TX_SESSION_ADDED;
}

int remove_rt_session(PPA_BUF *ppa_buf)
{
	struct avm_pa_session *session;

	BUG_ON(!ppa_buf);
	session = ppa_buf->pa_session;

	ppa_hook_session_del_fn(session, 0);

	disassoc_vap(session);

	/* TODO report remaining byte count */
	return AVM_PA_TX_OK;
}

void remove_session_worker(struct work_struct *work)
{
	struct session_work_item *session_work;
	struct avm_pa_session *session;
	PPA_BUF ppa_buf;
	int rv;

	BUG_ON(!work);

	session_work = container_of(work, typeof(*session_work), work);
	session = session_work->session;
	memset(&ppa_buf, 0, sizeof(ppa_buf));
	ppa_buf.pa_session = session;

	if(session->bsession) {
		rv = remove_br_session(&ppa_buf);
	} else {
		rv = remove_rt_session(&ppa_buf);
	}

	put_netdevs(session);

	if(rv == AVM_PA_TX_OK) {
		avm_pa_set_hw_session( session, NULL );
	}

	if(reset_netif_tree_hook &&
	   atomic_dec_and_test(&active_sessions))
	   reset_netif_tree_hook();

	kfree(session_work);
}

int remove_session(struct avm_pa_session *session)
{
	struct session_work_item *session_work;

	pr_debug("avm_pa sh=%d\n", session->session_handle);
	avm_pa_set_hw_session( session, "removing" );

	session_work = kmalloc(sizeof(*session_work), GFP_ATOMIC);
	if(!session_work) {
		pr_err("[%s] oom\n", __func__);
		return AVM_PA_TX_ERROR_SESSION;
	}
	INIT_WORK(&session_work->work, remove_session_worker);
	session_work->session = session;

	queue_work(sessions_workqueue, &session_work->work);

	return 0;
}

const char *session_state(struct avm_pa_session *session)
{
	BUG_ON(!session);

	return avm_pa_get_hw_session(session);
}

int32_t directpath_local_rx_fn(PPA_NETIF *rxif,
                         PPA_NETIF *txif,
                         PPA_BUF *ppabuf,
                         int32_t len __attribute__((unused)))
{
	struct sk_buff *skb;
	uint32_t session_handle;
	const struct avm_pa_session *session;
	uint32_t pid_handle;
	struct dma_rx_desc_0 *desc_0;

	skb = (struct sk_buff *)ppabuf;
	desc_0 = (struct dma_rx_desc_0 *)&skb->DW0;

	if(unlikely(!txif)) {
		pr_warn("%s: attempt to receive on local pid\n", __func__);
		goto drop;
	}
	skb->dev = txif;

	/* Some PPA session addressed this port, so further
	 * modifications shouldn't be required */
	avm_pa_mark_routed(skb);
	avm_pa_do_not_accelerate(skb);

	session = vap_to_session[desc_0->field.dest_sub_if_id & VAP_T_MAX];
	if (session == NULL) {
		goto drop;
	}
	session_handle = session->session_handle;
	pid_handle = avm_pa_first_egress(session)->pid_handle;
	BUG_ON(!pid_is_local(pid_handle));

	avm_pa_tx_channel_accelerated_packet(pid_handle,
		                             session_handle,
		                             skb);

	return DP_SUCCESS;

drop:
	dev_kfree_skb_any(skb);
	return PPA_FAILURE;
}

/* Called by the directpath driver, this will return the skb to AVM_PA after
 * looping it through PAE/MPE for manipulation (rxif set) OR forward packets
 * belonging to the opposite direction of an accelerated connection (txif
 * set). */
int32_t directpath_rx_fn(PPA_NETIF *rxif,
                         PPA_NETIF *txif,
                         PPA_BUF *ppabuf,
                         int32_t len __attribute__((unused)))
{
	struct sk_buff *skb = (struct sk_buff *)ppabuf;
	avm_pid_handle pid_handle;
	// pr_debug("called\n");
	if(unlikely(!rxif && !txif)) return PPA_FAILURE;
	if(unlikely(rxif)) {
		// pr_debug("rxif set\n");
		// print_hex_dump_bytes("", DUMP_PREFIX_OFFSET, skb->data, 60);
		pid_handle = rxif->avm_pa.devinfo.pid_handle;
		skb->dev = rxif;
		avm_pa_rx_channel_packet_not_accelerated(pid_handle, skb);
	} else {
		struct pmac_rx_hdr *pmac =
		 (void *)skb->data - sizeof(struct pmac_rx_hdr);

		// pr_debug("txif set\n");
		// print_hex_dump_bytes("", DUMP_PREFIX_OFFSET, skb->data, 60);
		skb->dev = txif;
		pid_handle = txif->avm_pa.devinfo.pid_handle;

		/* Some PPA session addressed this port, so further
		 * modifications shouldn't be required */
		avm_pa_mark_routed(skb);
		avm_pa_do_not_accelerate(skb);

		if(pmac->ip_offset) {
			skb_set_network_header(skb, pmac->ip_offset);
		} else {
			pr_debug_ratelimited("no pmac->ip_offset set\n");
			skb_reset_network_header(skb);
		}
		skb_reset_mac_header(skb);
		skb->protocol = eth_hdr(skb)->h_proto;
		//dev_queue_xmit(skb);
		txif->netdev_ops->ndo_start_xmit(skb, txif);
	}
	return PPA_SUCCESS; /* TODO validate rv */
}

static int32_t directpath_stop_tx_fn(PPA_NETIF *dev)
{
	/* TODO this rather seems to discard the try_to_accelerate route
	 * instead of queuing the packets for deferred transmission. Not sure
	 * if that's the expected thing to do. -AVM:PHU */
	if(dev) avm_pa_rx_channel_suspend(dev->avm_pa.devinfo.pid_handle);
	return 0;
}

static int32_t directpath_start_tx_fn(PPA_NETIF *dev)
{
	if(dev) avm_pa_rx_channel_resume(dev->avm_pa.devinfo.pid_handle);
	return 0;
}

PPA_DIRECTPATH_CB dp_callbacks = {
	.stop_tx_fn = directpath_stop_tx_fn,
	.start_tx_fn = directpath_start_tx_fn,
	.rx_fn = directpath_rx_fn,
};
EXPORT_SYMBOL(dp_callbacks);

PPA_DIRECTPATH_CB dp_local_callbacks = {
	.stop_tx_fn = directpath_stop_tx_fn,
	.start_tx_fn = directpath_start_tx_fn,
	.rx_fn = directpath_local_rx_fn,
};
EXPORT_SYMBOL(dp_local_callbacks);

static int alloc_rx_channel(avm_pid_handle pid_handle)
{
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	PPA_SUBIF dp_port;
	PPA_NETIF *netdev, *superdev; /* e.g. wifi0:ath0 */
	uint32_t flags;

	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);
	pr_debug("[%s] setup PID %d \n", __func__, pid_handle);

	if( !pa_hwinfo ) {
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__, pid_handle);
		return -1; /* failure */
	}

	netdev = pa_hwinfo->ppa.netdev;
	superdev = pa_hwinfo->ppa.superdev;

	if( pa_hwinfo->ppa.local_stack ) {
		pr_debug("[%s] setup PID %d for local_stack\n", __func__, pid_handle);
		return 0;
	}

	if( !netdev ) {
		/* hwinfo has been specifically set before, so full
		 * acceleration via PAE is expected */
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__, pid_handle);
		return -2; /* failure */
	}

	if(pa_hwinfo->ppa.mode == AVM_PA_HWINFO_SLOWPATH) {
		pr_debug("directpath not enabled for PID %d\n", pid_handle);
		return -2;
	}
	if(pa_hwinfo->ppa.mode != AVM_PA_HWINFO_DIRECTPATH) {
		/* catch unsupported or illegal values */
		BUG();
	}

	memset(&dp_port, 0, sizeof(dp_port));
	BUG_ON(pa_hwinfo->ppa.pmac_port != 0);
	dp_port.port_id = pa_hwinfo->ppa.pmac_port;
	dp_port.subif = pa_hwinfo->ppa.subif_id;


	/* register or lookup wifi first */
	dp_port.port_id = -1;
	flags = PPA_F_DIRECTPATH_REGISTER | PPA_F_DIRECTPATH_ETH_IF |
	        (!superdev ? PPE_DIRECTPATH_LEGACY : 0);

	if(PPA_SUCCESS != ppa_hook_directpath_ex_register_dev_fn(&dp_port,
	                                                         superdev ?: netdev,
	                                                         &dp_callbacks,
	                                                         flags)) {
		return -1;
	}

	pr_debug("netdev port_id == %d subif_id == %d\n", dp_port.port_id, dp_port.subif);
	if(dp_port.subif == -1) {
		/* subif id untouched since last subif registration */
		pr_debug("%s has been registered before\n", netdev->name);
	}
	pa_hwinfo->ppa.pmac_port = dp_port.port_id;

	if(dp_port.port_id < 0 || dp_port.subif > 0) {
		pr_err("port_id=%d subif=%d\n", dp_port.port_id, dp_port.subif);
		BUG();
	}

	if(superdev) {
		/* register vap (port_id != -1) */
		dp_port.subif = -1;
		if(PPA_SUCCESS !=
		   ppa_hook_directpath_ex_register_dev_fn(&dp_port,
		                                          netdev,
		                                          &dp_callbacks,
		                                          PPA_F_DIRECTPATH_REGISTER |
		                                           PPA_F_DIRECTPATH_ETH_IF)) {
			return -1;
		}
		pr_debug("sub netdev port_id == %d subif_id == %d\n",
		         dp_port.port_id,
		         dp_port.subif);
		pa_hwinfo->ppa.subif_id = dp_port.subif;
	} else {
		pa_hwinfo->ppa.subif_id = 0;
	}

	return 0; /* success */
}

static int free_rx_channel(avm_pid_handle pid_handle)
{
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	PPA_SUBIF dp_port;
	PPA_NETIF *netdev, *superdev; /* e.g. wifi0:ath0 */
	uint32_t flags;

	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);
	if(!pa_hwinfo) {
		/* hwinfo has been specifically set before, so full
		 * acceleration via PAE is expected */
		pr_warn("[%s] failed: missing hwinfo for PID %d.\n",
		        __func__,
		        pid_handle);
		return -1; /* failure */
	}

	netdev = pa_hwinfo->ppa.netdev;
	superdev = pa_hwinfo->ppa.superdev;

	if(!netdev) {
		pr_warn("[%s] failed: inconsistent hwinfo for PID %d.\n",
		        __func__,
		        pid_handle);
		return -1; /* failure */
	}

	if(pa_hwinfo->ppa.mode == AVM_PA_HWINFO_SLOWPATH) {
		pr_debug("directpath not enabled for PID %d\n", pid_handle);
		return -2;
	}
	if(pa_hwinfo->ppa.mode != AVM_PA_HWINFO_DIRECTPATH) {
		/* catch unsupported or illegal values */
		BUG();
	}

	dp_port.port_id = pa_hwinfo->ppa.pmac_port;
	dp_port.subif = pa_hwinfo->ppa.subif_id;
	pr_debug("port_id == %d subif_id == %d\n", dp_port.port_id, dp_port.subif);

	if(superdev) {
		/* Removing subif from DP/DL. This should never fail. */
		if(PPA_SUCCESS !=
		   ppa_hook_directpath_ex_register_dev_fn(&dp_port,
		                                          netdev,
		                                          &dp_callbacks,
		                                          PPA_F_DIRECTPATH_ETH_IF)) {
			return -1;
		}
	}
	pr_debug("port_id == %d subif_id == %d\n", dp_port.port_id, dp_port.subif);

	/* Removing major net-device from DP/DL. This may fail. */
	dp_port.subif = -1;
	flags = PPA_F_DIRECTPATH_ETH_IF | (!superdev ? PPE_DIRECTPATH_LEGACY : 0);
	if(PPA_SUCCESS != ppa_hook_directpath_ex_register_dev_fn(&dp_port,
	                                                         superdev ?: netdev,
	                                                         &dp_callbacks,
	                                                         flags)) {
		pr_debug("Port device could not be removed. This probably means "
		         "that it's still being used by a subif\n");
	}
	pr_debug("port_id == %d subif_id == %d\n", dp_port.port_id, dp_port.subif);

	/* reset hwinfo to some sane defaults just in case */
	pa_hwinfo->ppa.mode = AVM_PA_HWINFO_SLOWPATH;
	pa_hwinfo->ppa.pmac_port = 0;
	pa_hwinfo->ppa.subif_id = 0;

	pr_debug("success\n");
	return 0; /* success */
}

static int try_to_accelerate(avm_pid_handle pid_handle, struct sk_buff *skb)
{
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	uint32_t rv;
	PPA_SUBIF dp_port;
	uint32_t flags = 0;

	pr_debug("called\n");
	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);
	if(!pa_hwinfo || !pa_hwinfo->ppa.netdev) {
		/* hwinfo has been specifically set before, so full
		 * acceleration via PAE is expected */
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__, pid_handle);
		return AVM_PA_RX_BYPASS; /* failure */
	}

	if(pa_hwinfo->ppa.mode == AVM_PA_HWINFO_SLOWPATH) {
		pr_debug("directpath not enabled for PID %d\n", pid_handle);
		return AVM_PA_RX_BYPASS;
	}

	dp_port.port_id = pa_hwinfo->ppa.pmac_port;
	if(!pa_hwinfo->ppa.superdev) {
		dp_port.subif = -1;
		flags |= PPE_DIRECTPATH_LEGACY;
	} else {
		dp_port.subif = pa_hwinfo->ppa.subif_id;
	}

	pr_debug("calling ppa_hook_directpath_send_fn with port=%d subifid=%d.\n",
	         dp_port.port_id,
	         dp_port.subif);
	rv =
	 ppa_hook_directpath_ex_send_fn(&dp_port, skb, skb->len, flags);

	pr_debug("-> %d\n", rv);

	/* be clear about consumption */
	return AVM_PA_RX_STOLEN;
}

int  alloc_tx_channel(avm_pid_handle pid_handle){
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);
	pr_debug("[%s] setup PID %d \n", __func__, pid_handle);

	if( !pa_hwinfo ) {
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__, pid_handle);
		return -1; /* failure */
	}

	if( pa_hwinfo->ppa.local_stack ) {
		PPA_SUBIF *new_subif;
		const PPA_SUBIF  *subif;
		struct net_device *localif;
		int32_t rv;

		localif = pa_hwinfo->ppa.netdev = alloc_etherdev(sizeof(PPA_SUBIF));
		sprintf(localif->name, "pid%d", pid_handle);
		localif->netdev_ops = avm_ppa_localif->netdev_ops;
		new_subif = (PPA_SUBIF *)netdev_priv(localif);

		memset(new_subif, 0, sizeof(*new_subif));
		subif = (PPA_SUBIF *)netdev_priv(avm_ppa_localif);
		new_subif->port_id = subif->port_id;
		new_subif->subif = -1;
		ppa_add_avm_dpipe_vitf(localif);
		rv = ppa_hook_directpath_ex_register_dev_fn(new_subif,
		                                            localif,
		                                            &dp_local_callbacks,
		                                            PPA_F_DIRECTPATH_REGISTER |
		                                             PPA_F_DIRECTPATH_ETH_IF);
		pr_debug("%s: rv=%d subif=%d:%d\n", localif->name, rv, new_subif->port_id, new_subif->subif);
		pr_debug("[%s] setup PID %d for local_stack\n", __func__, pid_handle);
		return 0;
	}
	return -2; /* failure */
}

int free_tx_channel(avm_pid_handle pid_handle)
{
	struct avm_pa_pid_hwinfo *pa_hwinfo;
	struct net_device *netif;
	PPA_SUBIF *subif;

	pr_debug("[%s] free PID %d \n", __func__, pid_handle);

	pa_hwinfo = avm_pa_pid_get_hwinfo(pid_handle);

	if(!pa_hwinfo) {
		pr_warn("[%s] failed: missing/inconsistent hwinfo for PID %d.\n",
		        __func__,
		        pid_handle);
		return -1; /* failure */
	}

	if(pa_hwinfo->ppa.local_stack && pa_hwinfo->ppa.netdev) {
		netif = pa_hwinfo->ppa.netdev;
		subif = (PPA_SUBIF *)netdev_priv(netif);
		if(ppa_hook_directpath_ex_register_dev_fn(subif,
		                                          netif,
		                                          NULL,
		                                          PPA_F_DIRECTPATH_ETH_IF) !=
		   PPA_SUCCESS) {
			pr_err("Could not remove DirectPath interface for %s\n",
			       netif->name);
		}
		ppa_del_avm_dpipe_vitf(pa_hwinfo->ppa.netdev);
	}
	return 0;
}

void telephony_state(int active)
{
	pr_debug("telephony is %sactive\n", &(active<<1)["in"]);
	low_cpu_latency_required = !!active;
}

static int hw_pa_cpufreq_notifier(struct notifier_block *nb,
				   unsigned long val,  void *data)
{
	struct cpufreq_freqs *freq = data;
	enum ltq_cpufreq_state new_state, old_state;

	new_state = ltq_cpufreq_get_ps_from_khz(freq->new);
	old_state = ltq_cpufreq_get_ps_from_khz(freq->old);

	if(val == CPUFREQ_PRECHANGE) {
		pr_debug("prechange from %d to %d: ", old_state, new_state);

		if(low_cpu_latency_required && new_state > old_state) {
			pr_debug("rejected\n");
			return NOTIFY_STOP_MASK | (LTQ_CPUFREQ_MODULE_DP << 4);
		}
	}
	pr_debug("accepted\n");
	return NOTIFY_OK | (LTQ_CPUFREQ_MODULE_DP << 4);
}

static struct notifier_block hw_pa_cpufreq_notifier_block = {
	.notifier_call = hw_pa_cpufreq_notifier
};

int session_stats(struct avm_pa_session *avm_session,
                  struct avm_pa_session_stats *ingress)
{
	if(avm_session->bsession) {
		return collect_bridging_count(avm_session, ingress);
	} else {
		return collect_routing_count(avm_session, ingress);
	}
}

/*
    int  (*alloc_rx_channel)(avm_pid_handle pid_handle);
    int  (*alloc_tx_channel)(avm_pid_handle pid_handle);
    int  (*free_rx_channel)(avm_pid_handle pid_handle);
    int  (*free_tx_channel)(avm_pid_handle pid_handle);
    int  (*try_to_accelerate)(avm_pid_handle pid_handle, struct sk_buff *skb);
*/

static struct avm_hardware_pa hw_pa = {
	.add_session = add_session,
	.remove_session = remove_session,
	.session_state = session_state,
	.alloc_rx_channel = alloc_rx_channel,
	.alloc_tx_channel = alloc_tx_channel,
	.free_rx_channel = free_rx_channel,
	.free_tx_channel = free_tx_channel,
	.try_to_accelerate = try_to_accelerate,
	.telephony_state = telephony_state,
	.session_stats = session_stats,
};

void avm_ppa_register_hardware_pa(void)
{
	FENTRY_NO_BUF
	/* new ordered workqueue */
	sessions_workqueue = alloc_ordered_workqueue("hwpa_sessions", 0);
	avm_pa_register_hardware_pa(&hw_pa);

	if (cpufreq_register_notifier(&hw_pa_cpufreq_notifier_block,
				      CPUFREQ_TRANSITION_NOTIFIER)) {
		pr_err("hw_pa_cpufreq_init failed:cpufreq_register_notifier failed ?\n");
	}
	
}
EXPORT_SYMBOL(avm_ppa_register_hardware_pa);

static void do_gc(void);

struct avm_dpipe_vitf {
	struct list_head list;
	struct net_device *dev;
	ktime_t insert_time;
	size_t remove_retries;
};

static struct avm_dpipe_vitf *lookup_vitf_name(PPA_IFNAME *ifname,
					       struct list_head *list)
{
	struct avm_dpipe_vitf *vitf;
	
	list_for_each_entry(vitf, list, list) {
		if(strncmp(vitf->dev->name, ifname, PPA_IF_NAME_SIZE) == 0) {
			return vitf;
		}
	}
	return NULL;

}

static struct avm_dpipe_vitf *lookup_vitf(struct net_device *dev,
					       struct list_head *list)
{
	struct avm_dpipe_vitf *vitf;
	
	list_for_each_entry(vitf, list, list) {
		if (vitf->dev == dev){
			return vitf;
		}
	}
	return NULL;
}


void insert_vitf(struct avm_dpipe_vitf *vitf, struct list_head *list)
{
	struct timespec now;

	BUG_ON(lookup_vitf_name(vitf->dev->name,list));
	getrawmonotonic(&now);
	vitf->insert_time = timespec_to_ktime(now);
	list_add_tail(&vitf->list, list);
}


void ppa_add_avm_dpipe_vitf(PPA_NETIF *dev)
{
	struct avm_dpipe_vitf *vitf;

	do_gc();
	pr_debug("[%s] dev=%p/%s rc=%d\n", __func__, dev, dev->name,
		 netdev_refcnt_read(dev));

	BUG_ON(ppa_hold_avm_dpipe_vitf(dev->name));

	vitf = kzalloc(sizeof(struct avm_dpipe_vitf), GFP_KERNEL);
	BUG_ON(!vitf);
	vitf->dev = dev;

	spin_lock_bh(&avm_dpipe_lock);
	insert_vitf(vitf, &dpipe_netdevs_alive);
	spin_unlock_bh(&avm_dpipe_lock);
}

/*
 * if (netdev->refcnt == 0)
 * 	1) free_netdev
 * 	2) delete vitf from its current list (alive/dead)
 * 	3) free vitf
 */
static int try_free_avm_dpipe_vitf(struct avm_dpipe_vitf *vitf)
{
	int refcnt;
	struct net_device *dev = vitf->dev;

	BUG_ON(dev->reg_state != NETREG_UNINITIALIZED);

	refcnt = netdev_refcnt_read(dev);
	if (refcnt == 0) {
		free_netdev(dev);
		list_del_init(&vitf->list);
		kfree(vitf);
	} else {
		BUG_ON(refcnt < 0);
		if (vitf->remove_retries != GC_DEBUG_DISABLED) {
			vitf->remove_retries++;
		}
	}
	return refcnt;
}

void ppa_del_avm_dpipe_vitf(PPA_NETIF *dev)
{
	struct avm_dpipe_vitf *vitf;
	spin_lock_bh(&avm_dpipe_lock);

	vitf = lookup_vitf(dev, &dpipe_netdevs_alive);
	if (vitf) {
		dev_put(dev);

		if (try_free_avm_dpipe_vitf(vitf) == 0) {
			spin_unlock_bh(&avm_dpipe_lock);
			return;
		} else {
			pr_debug("%s still referenced, keep it for gc\n",
				 dev->name);
			insert_vitf(vitf, &dpipe_netdevs_dead);
		}
	}

	spin_unlock_bh(&avm_dpipe_lock);
	/* dev not in dpipe_netdevs_alive! */
	BUG();
}

PPA_NETIF *ppa_hold_avm_dpipe_vitf(PPA_IFNAME *ifname)
{
	struct avm_dpipe_vitf *vitf;

	if (!ifname)
		return 0;

	spin_lock_bh(&avm_dpipe_lock);
	vitf = lookup_vitf_name(ifname, &dpipe_netdevs_alive);

	if (vitf) {
		dev_hold(vitf->dev);
		spin_unlock_bh(&avm_dpipe_lock);
		return vitf->dev;
	}
	pr_debug("[%s] no vdev found: %s\n", __func__, ifname);
	spin_unlock_bh(&avm_dpipe_lock);
	return 0;
}

static void gc_debug(struct avm_dpipe_vitf *vitf, int refcnt)
{
	if (vitf->remove_retries == GC_DEBUG_THRESHOLD) {
		pr_err("[%s] %s still referenced(%d), keep it for gc\n",
		       __func__, vitf->dev->name, refcnt);
		vitf->remove_retries = GC_DEBUG_DISABLED;
	}
}

static void do_gc(void)
{
	struct avm_dpipe_vitf *vitf, *tmp;
	spin_lock_bh(&avm_dpipe_lock);
	list_for_each_entry_safe (vitf, tmp, &dpipe_netdevs_dead, list) {
		int refcnt = try_free_avm_dpipe_vitf(vitf);
		if (refcnt != 0) {
			gc_debug(vitf, refcnt);
			gc_failed++;
		} else {
			gc_ok++;
		}
	}
	spin_unlock_bh(&avm_dpipe_lock);
}

static void proc_dpipe_netdevs_read(struct seq_file *seq, void *priv)
{
	struct list_head *list = priv;
	struct avm_dpipe_vitf *vitf;
	struct timespec now;
	ktime_t now_kt;

	seq_printf(seq, "gc stats:\n");
	seq_printf(seq, "     ok: %zu\n", gc_ok);
	seq_printf(seq, "   fail: %zu\n", gc_failed);

	getrawmonotonic(&now);
	now_kt = timespec_to_ktime(now);
	spin_lock_bh(&avm_dpipe_lock);
	list_for_each_entry(vitf, list, list) {
		ktime_t diff_kt = ktime_sub(now_kt , vitf->insert_time);
		seq_printf(seq, "%.10s: refs=%d, lifetime=%ld:%ld\n",
			   vitf->dev->name,
			   netdev_refcnt_read(vitf->dev),
			   ktime_to_timespec(diff_kt).tv_sec,
			   ktime_to_timespec(diff_kt).tv_nsec);
	}

	spin_unlock_bh(&avm_dpipe_lock);

}

static size_t panic_dpipe_list(struct list_head *list, const char *list_name,
			       size_t *space_left)
{
	size_t space_used = 0;
	size_t print_no_more = 0;
	struct avm_dpipe_vitf *vitf;
	struct timespec now;
	ktime_t now_kt;

	getrawmonotonic(&now);
	now_kt = timespec_to_ktime(now);

	list_for_each_entry (vitf, list, list) {
		if (space_used == 0) {
			pr_emerg("  %s:\n", list_name);
		}
		if (space_used < *space_left) {
			ktime_t diff_kt = ktime_sub(now_kt, vitf->insert_time);
			pr_emerg("    %.10s: refs=%d, lifetime=%ld:%ld\n",
				 vitf->dev->name, netdev_refcnt_read(vitf->dev),
				 ktime_to_timespec(diff_kt).tv_sec,
				 ktime_to_timespec(diff_kt).tv_nsec);
			space_used++;
		} else {
			print_no_more++;
		}
	}

	if (print_no_more) {
		pr_emerg("    [...] %zu more ...\n", print_no_more);
	}

	*space_left -= space_used;
	return space_used;
}

static int panic_dpipe_stats(struct notifier_block *nb __maybe_unused,
			     unsigned long event __maybe_unused,
			     void *cause_string __maybe_unused)
{
	size_t space_left = 100;

	pr_emerg("[%s]\n", __func__);

	if (gc_ok || gc_failed) {
		pr_emerg("gc stats:\n");
		pr_emerg("     ok: %zu\n", gc_ok);
		pr_emerg("   fail: %zu\n", gc_failed);
	}

	panic_dpipe_list(&dpipe_netdevs_alive, "alive", &space_left);
	panic_dpipe_list(&dpipe_netdevs_dead, "dead", &space_left);

	return NOTIFY_DONE;
}

static struct notifier_block panic_notifier = {
	.notifier_call = panic_dpipe_stats,
	.next = NULL,
	.priority = 1,
};

void setup_dpipe_netdev_dbg(void)
{
	add_simple_proc_file("dpipe_netdevs_alive", NULL,
			     proc_dpipe_netdevs_read, &dpipe_netdevs_alive);

	add_simple_proc_file("dpipe_netdevs_dead", NULL,
			     proc_dpipe_netdevs_read, &dpipe_netdevs_dead);

	atomic_notifier_chain_register(&panic_notifier_list, &panic_notifier);
}

EXPORT_SYMBOL(ppa_hold_avm_dpipe_vitf);
EXPORT_SYMBOL(ppa_add_avm_dpipe_vitf);
EXPORT_SYMBOL(ppa_del_avm_dpipe_vitf);
EXPORT_SYMBOL(setup_dpipe_netdev_dbg);

/*
 * PPA <> PA Adaption Layer
 *
 *
 * [XXX] für ipv6 wird nicht zwischen PRE- und POSTROUTING unterschieden
 */

#define HDRCOPY(info) ((info)->hdrcopy + (info)->hdroff)
static void *get_hdr_from_pa_match(const struct avm_pa_pkt_match *match,
                                   unsigned char type)
{
	unsigned int i;
	FENTRY_NO_BUF
	if(!match) return NULL;

	for(i = 0; i < match->nmatch; i++) {
		const struct avm_pa_match_info *info = &match->match[i];
		if(info && info->type == type) {
			return (void *)(HDRCOPY(match) + info->offset);
		}
	}

	return NULL;
}

static void *get_hdr_from_ppa_buf(const PPA_BUF *ppa_buf,
                                  unsigned char type,
                                  HDR_DIRECTION hdr_dir)
{
	unsigned int i;
	void *hdr = NULL;
	const struct avm_pa_pkt_match *match;
	struct avm_pa_egress *egress;
	FENTRY_BUF(ppa_buf);

	if(!ppa_buf) return NULL;

	if(hdr_dir == HDR_EGRESS) {
		avm_pa_for_each_egress(egress, ppa_buf->pa_session) {
			match = &egress->match;
			hdr = get_hdr_from_pa_match(match, type);
			pr_debug("found header in egress match #%d\n", i);
			if(hdr) return hdr;
		}
	}

	if(hdr_dir == HDR_INGRESS) {
		hdr = get_hdr_from_pa_match(&ppa_buf->pa_session->ingress, type);
		if(hdr) pr_debug("found header in ingress\n");
	}

	return hdr;
}

static struct ipv6hdr *ipv6_hdr_from_ppa_buf(const PPA_BUF *ppa_buf)
{
	HDR_DIRECTION dir;
	FENTRY_BUF(ppa_buf);

	dir = (ppa_buf->state == PPA_BUF_PREROUTING) ? HDR_INGRESS : HDR_EGRESS;
	return get_hdr_from_ppa_buf(ppa_buf, AVM_PA_IPV6, dir);
}

static struct iphdr *ip_hdr_from_ppa_buf(const PPA_BUF *ppa_buf)
{
	HDR_DIRECTION dir;
	FENTRY_BUF(ppa_buf);

	dir = (ppa_buf->state == PPA_BUF_PREROUTING) ? HDR_INGRESS : HDR_EGRESS;
	return get_hdr_from_ppa_buf(ppa_buf, AVM_PA_IPV4, dir);
}

PPA_SESSION *ppa_get_session(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	return ppa_buf->pa_session;
}

uint8_t ppa_is_pkt_ipv6(const PPA_BUF *ppa_buf)
{
	uint16_t pkttype = ppa_buf->pa_session->ingress.pkttype;
	FENTRY_BUF(ppa_buf);
	return ((pkttype & AVM_PA_PKTTYPE_IP_MASK) == AVM_PA_PKTTYPE_IPV6);
}

uint8_t ppa_get_ip_tos(PPA_BUF *ppa_buf)
{
	struct iphdr *hdr;
	uint8_t tos;
	struct avm_pa_v4_mod_rec *mod;

	FENTRY_BUF(ppa_buf);
	mod = &ppa_buf->pa_session->mod.v4_mod;
	if(ppa_buf->state == PPA_BUF_POSTROUTING &&
	   mod->flags & AVM_PA_V4_MOD_TOS) {
		tos = mod->tos;
	} else {
		hdr = ip_hdr_from_ppa_buf(ppa_buf);
		tos = hdr ? hdr->tos : 0;
	}

	return tos;
}

uint8_t ppa_get_ipv6_tos(PPA_BUF *ppa_buf)
{
	struct ipv6hdr *hdr;
	uint8_t tc;
	FENTRY_BUF(ppa_buf);

	hdr = ipv6_hdr_from_ppa_buf(ppa_buf);
	if(!hdr) return 0;
	tc = hdr->priority << 4;
	tc |= hdr->flow_lbl[0] >> 4;

	return tc;
}

uint8_t ppa_get_pkt_ip_tos(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	if(ppa_is_pkt_ipv6(ppa_buf)) {
		return ppa_get_ipv6_tos(ppa_buf);
	}

	return ppa_get_ip_tos(ppa_buf);
}

uint32_t ppa_get_pkt_ip_len(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	if(ppa_is_pkt_ipv6(ppa_buf)) {
		return sizeof(struct in6_addr);
	}
	return sizeof(uint32_t);
}

/* [TODO] get_ip_{s,d}addr zusammenlegen */
PPA_IPADDR ppa_get_ip_saddr(PPA_BUF *ppa_buf)
{
	struct iphdr *hdr;
	PPA_IPADDR saddr;
	struct avm_pa_v4_mod_rec *mod;

	FENTRY_BUF(ppa_buf);
	pr_debug("(%s: %d)\n",
	         (ppa_buf->state == PPA_BUF_POSTROUTING) ? "post" : "pre",
	         ppa_buf->pa_session->session_handle);

	mod = &ppa_buf->pa_session->mod.v4_mod;
	if((ppa_buf->state == PPA_BUF_POSTROUTING) &&
	   (mod->flags & AVM_PA_V4_MOD_SADDR)) {
		pr_debug("return snat ip addr\n");
		saddr.ip = mod->saddr;
	} else {
		pr_debug("return ip-header saddr\n");
		hdr = ip_hdr_from_ppa_buf(ppa_buf);
		saddr.ip = hdr ? hdr->saddr : 0;
	}

	return saddr;
}

PPA_IPADDR ppa_get_ipv6_saddr(PPA_BUF *ppa_buf)
{
	struct ipv6hdr *hdr;
	PPA_IPADDR addr;

	FENTRY_BUF(ppa_buf);
	hdr = ipv6_hdr_from_ppa_buf(ppa_buf);
	memcpy(&addr.ip6, &hdr->saddr, sizeof(addr.ip6));

	return addr;
}

void ppa_get_pkt_src_ip(PPA_IPADDR *ip, PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	if(ppa_is_pkt_ipv6(ppa_buf)) {
		*ip = ppa_get_ipv6_saddr(ppa_buf);
	} else {
		*ip = ppa_get_ip_saddr(ppa_buf);
	}
}


PPA_IPADDR ppa_get_ip_daddr(PPA_BUF *ppa_buf)
{
	struct iphdr *hdr;
	PPA_IPADDR daddr;
	struct avm_pa_v4_mod_rec *mod;
	FENTRY_BUF(ppa_buf);
	pr_debug("(%s: %d)\n",
	         (ppa_buf->state == PPA_BUF_POSTROUTING) ? "post" : "pre",
	         ppa_buf->pa_session->session_handle);

	mod = &ppa_buf->pa_session->mod.v4_mod;
	if((ppa_buf->state == PPA_BUF_POSTROUTING) &&
	   (mod->flags & AVM_PA_V4_MOD_DADDR)) {
		daddr.ip = mod->daddr;
		pr_debug("return dnat ip addr\n");
	} else {
		hdr = ip_hdr_from_ppa_buf(ppa_buf);
		daddr.ip = hdr ? hdr->daddr : 0;
		pr_debug("return ip-header\n");
	}

	return daddr;
}

PPA_IPADDR ppa_get_ipv6_daddr(PPA_BUF *ppa_buf)
{
	struct ipv6hdr *hdr;
	PPA_IPADDR addr;
	FENTRY_BUF(ppa_buf);

	hdr = ipv6_hdr_from_ppa_buf(ppa_buf);
	memcpy(&addr.ip6, &hdr->daddr, sizeof(addr.ip6));

	return addr;
}

void ppa_get_pkt_dst_ip(PPA_IPADDR *ip, PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	if(ppa_is_pkt_ipv6(ppa_buf)) {
		*ip = ppa_get_ipv6_daddr(ppa_buf);
	} else {
		*ip = ppa_get_ip_daddr(ppa_buf);
	}
}


static uint16_t get_port(PPA_BUF *ppa_buf, bool is_src)
{
	uint16_t *ports, modflag;
	struct avm_pa_v4_mod_rec *mod;
	FENTRY_BUF(ppa_buf);
	mod = &ppa_buf->pa_session->mod.v4_mod;

	modflag = is_src ? AVM_PA_V4_MOD_SPORT : AVM_PA_V4_MOD_DPORT;
	if(ppa_buf->state == PPA_BUF_POSTROUTING && mod->flags & modflag) {
		return is_src ? mod->sport : mod->dport;
	} else {
		ports =
		 get_hdr_from_pa_match(&ppa_buf->pa_session->ingress, AVM_PA_PORTS);
		return ports ? ports[is_src ? 0 : 1] : 0;
	}
}

uint16_t ppa_get_pkt_src_port(PPA_BUF *ppa_buf)
{
	const bool is_src = 1;
	FENTRY_BUF(ppa_buf);
	return get_port(ppa_buf, is_src);
}

uint16_t ppa_get_pkt_dst_port(PPA_BUF *ppa_buf)
{
	const bool is_src = 0;
	FENTRY_BUF(ppa_buf);
	return get_port(ppa_buf, is_src);
}

uint32_t ppa_is_ip_fragment(PPA_BUF *ppa_buf)
{
	struct iphdr *hdr = ip_hdr_from_ppa_buf(ppa_buf);
	FENTRY_BUF(ppa_buf);
	return (hdr && hdr->frag_off & htons(IP_MF | IP_OFFSET)) == 0 ? 0 : 1;
}

uint8_t ppa_get_pkt_ip_proto(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	return (uint8_t)(ppa_buf->pa_session->ingress.pkttype &
	                 AVM_PA_PKTTYPE_PROTO_MASK);
}

static void get_tuple_from_packet(PPA_BUF *ppa_buf, PPA_TUPLE *tuple)
{
	PPA_IPADDR src_ip, dst_ip;
	FENTRY_BUF(ppa_buf);
	ppa_get_pkt_src_ip(&src_ip, ppa_buf);
	ppa_get_pkt_dst_ip(&dst_ip, ppa_buf);

	/* have to keep this in sync with ppa_compare_with_tuple */
	tuple->src.l3num = ppa_is_pkt_ipv6(ppa_buf) ? AF_INET6 : AF_INET;
	tuple->dst.protonum = ppa_get_pkt_ip_proto(ppa_buf);
	tuple->src.u.all = ppa_get_pkt_src_port(ppa_buf);
	tuple->src.u3.all[0] = src_ip.ip6[0];
	tuple->src.u3.all[1] = src_ip.ip6[1];
	tuple->src.u3.all[2] = src_ip.ip6[2];
	tuple->src.u3.all[3] = src_ip.ip6[3];
	tuple->dst.u.all = ppa_get_pkt_dst_port(ppa_buf);
	tuple->dst.u3.all[0] = dst_ip.ip6[0];
	tuple->dst.u3.all[1] = dst_ip.ip6[1];
	tuple->dst.u3.all[2] = dst_ip.ip6[2];
	tuple->dst.u3.all[3] = dst_ip.ip6[3];
}

int ppa_get_hash_from_packet(PPA_BUF *ppa_buf,
                             unsigned char pf __attribute__((unused)),
                             uint32_t *u32_hash,
                             PPA_TUPLE *tuple)
{
	FENTRY_BUF(ppa_buf);
	*u32_hash = (uint32_t)ppa_buf->pa_session->session_handle;
	get_tuple_from_packet(ppa_buf, tuple);

	return 0;
}
EXPORT_SYMBOL(ppa_get_hash_from_packet);

uint32_t ppa_get_hash_from_ct(const PPA_SESSION *pa_session,
                              uint8_t dir __attribute__((unused)),
                              PPA_TUPLE *tuple)
{
	PPA_BUF ppa_buf;
	FENTRY_NO_BUF

	ppa_buf.pa_session = (PPA_SESSION *)pa_session;
	ppa_buf.state = PPA_BUF_PREROUTING; /* [TODO] richtig? */
	get_tuple_from_packet(&ppa_buf, tuple);

	return pa_session->session_handle;
}
EXPORT_SYMBOL(ppa_get_hash_from_ct);

uint32_t ppa_is_pkt_fragment(PPA_BUF *ppa_buf __attribute__((unused)))
{
	/* AVM PA would only request a session if it were accelerable */
	return false;
}

PPA_NETIF *ppa_get_pkt_src_if(PPA_BUF *ppa_buf)
{
	struct avm_pa_pid_hwinfo *hwinfo;
	FENTRY_BUF(ppa_buf);

	if(ppa_buf->ingress_vitf) {
		return ppa_buf->ingress_vitf->netif;
	}

	hwinfo = avm_pa_pid_get_hwinfo(ppa_buf->pa_session->ingress_pid_handle);
	BUG_ON(!hwinfo);
	return hwinfo->ppa.netdev;
}

PPA_NETIF *ppa_get_pkt_dst_if(PPA_BUF *ppa_buf)
{
	struct avm_pa_pid_hwinfo *hwinfo;
	FENTRY_BUF(ppa_buf);
	/* [TODO] consider multiple egress pids */

	if(ppa_buf->egress_vitf) {
		return ppa_buf->egress_vitf->netif;
	}

	hwinfo = avm_pa_pid_get_hwinfo(avm_pa_first_egress(ppa_buf->pa_session)->pid_handle);
	BUG_ON(!hwinfo);
	return hwinfo->ppa.netdev;
}

uint32_t ppa_check_is_special_session(PPA_BUF *ppa_buf, PPA_SESSION *p_session)
{
	FENTRY_BUF(ppa_buf);
	/* AVM PA would only request a session if it were accelerable */
	return false;
}

int32_t ppa_is_pkt_local(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	BUG(); /* not implemented */
	return 0;
}

int32_t ppa_is_pkt_routing(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	return ppa_buf->pa_session->routed;
}

int32_t ppa_is_pkt_mc_routing(PPA_BUF *ppa_buf __attribute__((unused)))
{
	FENTRY_BUF(ppa_buf);
	BUG(); /* unused */
}

void ppa_get_pkt_rx_src_mac_addr(PPA_BUF *ppa_buf, uint8_t mac[PPA_ETH_ALEN])
{
	int8_t strbuf[64];
	struct ethhdr *ethh;
	HDR_DIRECTION dir;

	FENTRY_BUF(ppa_buf);

	dir = (ppa_buf->state == PPA_BUF_PREROUTING) ? HDR_INGRESS : HDR_EGRESS;
	ethh = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_ETH, dir);

	memcpy(mac, ethh->h_source, PPA_ETH_ALEN);
	pr_debug("src_mac: %s\n", ppa_get_pkt_mac_string(mac, strbuf));
}

void ppa_get_pkt_rx_dst_mac_addr(PPA_BUF *ppa_buf, uint8_t mac[PPA_ETH_ALEN])
{
	struct ethhdr *ethh;
	HDR_DIRECTION dir;
	FENTRY_BUF(ppa_buf);

	dir = (ppa_buf->state == PPA_BUF_PREROUTING) ? HDR_INGRESS : HDR_EGRESS;
	ethh = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_ETH, dir);
	memcpy(mac, ethh->h_dest, PPA_ETH_ALEN);
}

int ppa_get_multicast_pkt_ip(PPA_BUF *ppa_buf __attribute__((unused)),
                             void *dst_ip __attribute__((unused)),
                             void *src_ip __attribute__((unused)))
{
	FENTRY_BUF(ppa_buf);
	/* [TODO] we don't get those anyway. or do we?! */
	return PPA_FAILURE;
}

uint32_t ppa_get_pkt_priority(PPA_BUF *ppa_buf)
{
	uint32_t prio;
	struct avm_pa_session *session;
	FENTRY_BUF(ppa_buf);

	session = ppa_buf->pa_session;
	BUG_ON(!session);

	switch(avm_pa_first_egress(session)->type) {
	case avm_pa_egresstype_local:
	case avm_pa_egresstype_rtp:
		/* map rt ingress to a dedicated high prio queue */
		prio = session->realtime ? PPA_REALTIME_CLASSID : 0;
		break;
	case avm_pa_egresstype_output:
	default:
		prio = avm_pa_first_egress(session)->output.priority;
		break;
	}

	if(prio) {
		/* ugw 7.2: no more offset calculation required
		 * pae tc_remark is directly mapped to tc prio  */
		prio = TC_H_MIN(prio); 
	}

	pr_debug("TC: %u -> %u (%u)\n",
	         TC_H_MIN(avm_pa_first_egress(session)->output.priority),
	         prio,
	         avm_pa_first_egress(session)->output.priority);
	return prio;
}
EXPORT_SYMBOL(ppa_get_pkt_priority);

uint32_t ppa_get_low_prio_thresh(uint32_t flags __attribute__((unused)))
{
	FENTRY_NO_BUF
	return 0; /* [TODO] lowest? */
}

uint32_t ppa_get_skb_mark(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	/* haben wir nicht */
	return 0;
}
EXPORT_SYMBOL(ppa_get_skb_mark);

uint32_t ppa_get_skb_extmark(PPA_BUF *ppa_buf)
{
	uint32_t prio = avm_pa_first_egress(ppa_buf->pa_session)->output.priority;
	FENTRY_BUF(ppa_buf);

	/* 
	 * only apply classes derived from TC priorities or real-time sessions
	 */
	if(TC_H_MAJ(prio) || ppa_buf->pa_session->realtime) {
		return SESSION_FLAG_TC_REMARK;
	} else {
		return 0;
	}
}
EXPORT_SYMBOL(ppa_get_skb_extmark);

PPA_BUF *ppa_buf_clone(PPA_BUF *ppa_buf, uint32_t flags)
{
	FENTRY_BUF(ppa_buf);
	BUG();
	return NULL;
}

int32_t ppa_buf_cloned(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	BUG();
	return false;
}

PPA_BUF *ppa_buf_get_prev(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	pr_warn("%s not implemented\n", __func__);
	return NULL;
}

PPA_BUF *ppa_buf_get_next(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	pr_warn("%s not implemented\n", __func__);
	return NULL;
}

void ppa_buf_free(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	pr_warn("%s not implemented\n", __func__);
}

uint16_t ppa_get_vlan_type(PPA_NETIF *netif)
{
	pr_warn("%s not implemented\n", __func__);
	return PPA_VLAN_PROTO_8021Q;
}
EXPORT_SYMBOL(ppa_get_vlan_type);

uint16_t ppa_vlan_dev_get_egress_qos_mask(PPA_NETIF *dev, PPA_BUF *ppa_buf)
{
	struct vlan_hdr *vlanhdr;
	uint16_t vlan_qos = 0;
	FENTRY_BUF(ppa_buf);

	vlanhdr = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_VLAN, HDR_EGRESS);
	if(vlanhdr) vlan_qos = vlanhdr->h_vlan_TCI & ((8 - 1) << 13);

	pr_debug("vlan_qos: 0x%04hx (hdr: %p)\n", vlan_qos, vlanhdr);
	return vlan_qos;
}

int32_t ppa_is_pkt_multicast(PPA_BUF *ppa_buf)
{
	FENTRY_BUF(ppa_buf);
	return ppa_buf->pa_session->ingress.casttype == AVM_PA_IS_MULTICAST;
}

int32_t ppa_get_dst_mac(PPA_BUF *ppa_buf,
                        PPA_SESSION *p_session,
                        uint8_t mac[PPA_ETH_ALEN],
                        uint32_t daddr)
{
	struct ethhdr *ethh = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_ETH, HDR_EGRESS);
	int8_t macstr[18];
	FENTRY_BUF(ppa_buf);
	if(!ethh) {
		if(pid_is_local(avm_pa_first_egress(ppa_buf->pa_session)->pid_handle)) {
			ethh = get_hdr_from_ppa_buf(ppa_buf, AVM_PA_ETH, HDR_INGRESS);
		} else {
			return PPA_FAILURE;
		}
	}
	memcpy(mac, ethh->h_dest, PPA_ETH_ALEN);
	pr_debug("found mac %s\n", ppa_get_pkt_mac_string(ethh->h_dest, macstr));
	pr_debug("found mac %s\n", ppa_get_pkt_mac_string(mac, macstr));
	if(mac[0] | mac[1] | mac[2] | mac[3] | mac[4] | mac[5]) {
		return PPA_SUCCESS;
	} else {
		return PPA_FAILURE;
	}
}

uint32_t ppa_set_pkt_priority(PPA_BUF *ppa_buf, uint32_t new_pri)
{
	FENTRY_BUF(ppa_buf);
	avm_pa_first_egress(ppa_buf->pa_session)->output.priority = new_pri;
	return new_pri;
}
EXPORT_SYMBOL(ppa_set_pkt_priority);

uint32_t ppa_get_session_helper(PPA_SESSION *p_session)
{
	FENTRY_NO_BUF
	pr_warn("%s not implemented\n", __func__);
	return 0;
}

int32_t ppa_register_chrdev(int32_t major,
                            const uint8_t *name,
                            PPA_FILE_OPERATIONS *fops)
{
	ppadev_fops = fops;
	return register_chrdev(major, (char *)name, fops);
}

void ppa_unregister_chrdev(int32_t major, const uint8_t *name)
{
	ppadev_fops = NULL;
	unregister_chrdev(major, (char *)name);
	return;
}

int32_t ppa_pppoe_get_pppoe_addr(PPA_NETIF *netif, struct pppoe_addr *pa)
{
	BUG();
	return PPA_EPERM;
}

__u16 ppa_pppoe_get_pppoe_session_id(PPA_NETIF *netif)
{
	struct avm_dpipe_vitf_pppoe *priv;

	if(!(netif->priv_flags & IFF_PPA_DUMMY) ||
	   !(netif->flags & IFF_POINTOPOINT)) {
		pr_err("[%s] no ppoedev\n", __func__);
		return 0;
	}

	priv = netdev_priv(netif);
	return priv->pppoe_session_id;
}

__u16 ppa_get_pkt_pppoe_session_id(PPA_BUF *ppa_buf)
{
	BUG();
	return 0;
}

int32_t ppa_pppoe_get_eth_netif(PPA_NETIF *netif,
                                PPA_IFNAME pppoe_eth_ifname[PPA_IF_NAME_SIZE])
{
	struct avm_dpipe_vitf_pppoe *priv;

	if(!(netif->priv_flags & IFF_PPA_DUMMY) ||
	   !(netif->flags & IFF_POINTOPOINT)) {
		pr_err("[%s] no ppoedev\n", __func__);
		return PPA_FAILURE;
	}

	priv = netdev_priv(netif);
	strlcpy(pppoe_eth_ifname, priv->lower_dev->name, PPA_IF_NAME_SIZE);
	return PPA_SUCCESS;
}


uint32_t ppa_check_is_pppoe_netif(PPA_NETIF *netif)
{
	return (netif->type == ARPHRD_PPP && (netif->flags & IFF_POINTOPOINT));
}

int32_t ppa_pppoe_get_dst_mac(PPA_NETIF *netif, uint8_t mac[PPA_ETH_ALEN])
{
	struct avm_dpipe_vitf_pppoe *priv;

	if(!(netif->priv_flags & IFF_PPA_DUMMY) ||
	   !(netif->flags & IFF_POINTOPOINT)) {
		pr_err("[%s] no ppoedev\n", __func__);
		return PPA_FAILURE;
	}

	priv = netdev_priv(netif);
	memcpy(mac, priv->remote_mac, PPA_ETH_ALEN);
	return PPA_SUCCESS;
}

int32_t ppa_pppoe_get_physical_if(PPA_NETIF *netif,
                                  PPA_IFNAME *ifname,
                                  PPA_IFNAME phy_ifname[PPA_IF_NAME_SIZE])
{
	if(!netif) netif = ppa_get_netif(ifname);

	if(!netif) return PPA_EINVAL;

	if(!ppa_check_is_pppoe_netif(netif)) return PPA_EINVAL;

	return ppa_pppoe_get_eth_netif(netif, phy_ifname);
}

uint32_t ppa_check_is_ppp_netif(PPA_NETIF *netif)
{
	return (netif->type == ARPHRD_PPP && (netif->flags & IFF_POINTOPOINT));
}

extern int vlan_dev_get_vid(const char *dev_name, unsigned short *result);
uint32_t ppa_get_vlan_id(PPA_NETIF *netif)
{
	unsigned short vid = ~0;

	if(!netif) return ~0;
	if((netif->priv_flags & IFF_802_1Q_VLAN) &&
	   ((netif->priv_flags & IFF_PPA_DUMMY))) {
		struct avm_dpipe_vitf_vlan *vlan_priv = netdev_priv(netif);
		return vlan_priv->vid;
	}
#if defined(CONFIG_VLAN_8021Q) || defined(CONFIG_VLAN_8021Q_MODULE)
	if(vlan_dev_get_vid(netif->name, &vid) == 0) return (uint32_t)vid;
#endif
#if defined(CONFIG_WAN_VLAN_SUPPORT)
	if(br2684_vlan_dev_get_vid(netif, &vid) == 0) return (uint32_t)vid;
#endif

	return ~0;
}
EXPORT_SYMBOL(ppa_get_vlan_id);

int32_t ppa_br2684_get_vcc(PPA_NETIF *netif, PPA_VCC **pvcc)
{
	pr_debug("name=%s priv_flags=0x%x flags=0x%x\n",
	         netif->name,
	         netif->priv_flags,
	         netif->flags);
	if(netif->priv_flags & IFF_PPA_DUMMY && netif->flags & IFF_BR2684) {
		struct avm_dpipe_vitf_vcc *vcc_priv = netdev_priv(netif);
		*pvcc = vcc_priv->vcc;

		return PPA_SUCCESS;
	}
	pr_debug("nope\n");
	return PPA_EINVAL;
}

int32_t ppa_if_is_br2684(PPA_NETIF *netif, PPA_IFNAME *ifname)
{
	pr_debug("name=%s priv_flags=0x%x flags=0x%x\n",
	         ifname,
	         netif->priv_flags,
	         netif->flags);
	return (netif->priv_flags & IFF_PPA_DUMMY && netif->flags & IFF_BR2684);
}

int32_t ppa_if_is_ipoa(PPA_NETIF * netif, PPA_IFNAME * ifname) { return 0; }

void ppa_if_force_remove(char *ifname)
{
	PPA_IFINFO lan_ifinfo, wan_ifinfo;

	pr_debug("called\n");

	lan_ifinfo.ifname = ifname;
	lan_ifinfo.if_flags = PPA_F_LAN_IF;

	wan_ifinfo.ifname = ifname;
	wan_ifinfo.if_flags = 0;

	/* attempt to remove the device from both, LAN and WAN */
	ppa_hook_del_if_fn(&lan_ifinfo, 0);
	ppa_hook_del_if_fn(&wan_ifinfo, 0);
}

 EXPORT_SYMBOL(ppa_br2684_get_vcc);
 EXPORT_SYMBOL(ppa_if_is_br2684);
 EXPORT_SYMBOL(ppa_if_is_ipoa);
 EXPORT_SYMBOL(ppa_if_force_remove);

struct net_device* ppa_get_dslite_phyif(struct net_device *netif)
{
	struct ip6_tnl *tip6 = netdev_priv(netif);
	return tip6->dev;
}
EXPORT_SYMBOL(ppa_get_dslite_phyif);

int32_t ppa_msec_timer_add(PPA_TIMER *p_timer, uint32_t timeout_in_msec)
{
    p_timer->expires = jiffies + msecs_to_jiffies(timeout_in_msec);
    add_timer(p_timer);

    return PPA_SUCCESS;
}
EXPORT_SYMBOL(ppa_msec_timer_add);
