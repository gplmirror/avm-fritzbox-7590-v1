#include <crypto/ltq_ipsec_ins.h>
#include <net/avm_eip97.h>
#include <crypto/aead.h>
#include <crypto/authenc.h>
#include <linux/err.h>
#include <linux/module.h>
#include <net/ip.h>
#include <net/xfrm.h>
#include <net/esp.h>
#include <linux/scatterlist.h>
#include <linux/kernel.h>
#include <linux/pfkeyv2.h>
#include <linux/rtnetlink.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/in6.h>
#include <net/icmp.h>
#include <net/protocol.h>
#include <net/udp.h>

//dummys for when crypto acceleration is not enabled
#ifndef CONFIG_LTQ_CRYPTO

void avm_eip97_add_sa(struct xfrm_state *xfrm_sa, int direction, unsigned int packetlength) {};
void avm_eip97_del_sa(struct xfrm_state *xfrm_sa) {};
int esp_input_accel(struct xfrm_state *x, struct sk_buff *skb) { return -ENOSYS; }
int esp_output_accel(struct xfrm_state *x, struct sk_buff *skb) { return -ENOSYS; }

#else

int32_t mpe_hal_get_auth_algo(char *algo)
{
    if(!strcmp(algo, "digest_null"))
        return LTQ_SAB_AUTH_NULL;
    if(!strcmp(algo, "hash(md5)"))
        return LTQ_SAB_AUTH_HASH_MD5;
    if(!strcmp(algo, "hmac(md5)"))
        return LTQ_SAB_AUTH_HMAC_MD5;
    if(!strcmp(algo, "hmac(sha1)"))
        return LTQ_SAB_AUTH_HMAC_SHA1;
    if(!strcmp(algo, "hmac(sha256)"))
        return LTQ_SAB_AUTH_HMAC_SHA2_256;
    if(!strcmp(algo, "hmac(sha384)"))
        return LTQ_SAB_AUTH_HMAC_SHA2_384;
    if(!strcmp(algo, "hmac(sha512)"))
        return LTQ_SAB_AUTH_HMAC_SHA2_512;
    if(!strcmp(algo, "xcbc(aes)"))
        return LTQ_SAB_AUTH_AES_XCBC_MAC;
    if(!strcmp(algo, "gcm"))
        return LTQ_SAB_AUTH_AES_GCM;
    if(!strcmp(algo, "gmac"))
        return LTQ_SAB_AUTH_AES_GMAC;

    return -1;
}
int32_t mpe_hal_get_crypto_algo(char *algo)
{

    if(!strcmp(algo, "cipher_null"))
        return LTQ_SAB_CRYPTO_NULL;
    if(!strcmp(algo, "des"))
        return LTQ_SAB_CRYPTO_DES;
    if(!strcmp(algo, "des3_ede"))
        return LTQ_SAB_CRYPTO_3DES;
    if(!strcmp(algo, "3des"))
        return LTQ_SAB_CRYPTO_3DES;
    if(!strcmp(algo, "aes"))
        return LTQ_SAB_CRYPTO_AES;
    if(!strcmp(algo, "arcfour"))
        return LTQ_SAB_CRYPTO_ARCFOUR;

    return -1;
}
int32_t mpe_hal_get_crypto_mode(char *mode)
{

    if(!strcmp(mode, "ecb"))
        return LTQ_SAB_CRYPTO_MODE_ECB;
    if(!strcmp(mode, "cbc"))
        return LTQ_SAB_CRYPTO_MODE_CBC;
    if(!strcmp(mode, "ofb"))
        return LTQ_SAB_CRYPTO_MODE_OFB;
    if(!strcmp(mode, "cfb"))
        return LTQ_SAB_CRYPTO_MODE_CFB;
    if(!strcmp(mode, "cfb1"))
        return LTQ_SAB_CRYPTO_MODE_CFB1;
    if(!strcmp(mode, "cfb8"))
        return LTQ_SAB_CRYPTO_MODE_CFB8;
    if(!strcmp(mode, "ctr"))
        return LTQ_SAB_CRYPTO_MODE_CTR;
    if(!strcmp(mode, "icm"))
        return LTQ_SAB_CRYPTO_MODE_ICM;
    if(!strcmp(mode, "ccm"))
        return LTQ_SAB_CRYPTO_MODE_CCM;
    if(!strcmp(mode, "gcm"))
        return LTQ_SAB_CRYPTO_MODE_GCM;
    if(!strcmp(mode, "gmac"))
        return LTQ_SAB_CRYPTO_MODE_GMAC;

    return -1;

}

void parse_alg( const char *alg_name, char *mode, unsigned int mode_size, char *algo, unsigned int algo_size ) {
    char *mode_tmp, *algo_tmp, *algo_tmp_end;

    if( ( alg_name == 0 ) || ( mode == 0 ) || ( algo == 0 ) || ( mode_size <= 0 ) || ( algo_size <= 0 ) ) {
        return;
    }

    *mode=0;
    *algo=0;

    mode_tmp = kstrdup(alg_name, GFP_KERNEL);
    if( mode_tmp == 0 ) {
        goto parse_alg_early_error;
    }

    if( strncmp( mode_tmp, "rfc", 3 ) == 0 ) {
        mode_tmp = strchr( mode_tmp, '(' );
        if( mode_tmp == 0 ) {
            goto parse_alg_error; 
        }
        mode_tmp++;
        if( *mode_tmp == '\0' ) {
            goto parse_alg_error; 
        }
    }

    algo_tmp = strchr( mode_tmp, '(' );
    if( algo_tmp == 0 ) {
        goto parse_alg_error; 
    }
    *algo_tmp = '\0';
    algo_tmp++;
    if( *algo_tmp == '\0' ) {
        goto parse_alg_error; 
    }
    
    algo_tmp_end = strchr( algo_tmp, ')' );
    if( algo_tmp_end == 0 ) {
        goto parse_alg_error; 
    }
    *algo_tmp_end = '\0';

    strncpy( mode, mode_tmp, mode_size );
    mode[mode_size-1] = '\0';

    strncpy( algo, algo_tmp, algo_size );
    algo[mode_size-1] = '\0';

    return;

parse_alg_error:
    kfree( mode_tmp );

parse_alg_early_error:

    *mode=0;
    *algo=0;
    return;

}



unsigned int guesstimate_packetsize(int crypto_algo, int crypto_mode, int auth_algo) {

    if( crypto_algo == 1 && crypto_mode == 1 && auth_algo == 9 ) {
        return 28 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 1 && crypto_mode == 1 && auth_algo == 10 ) {
        return 28 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 1 && crypto_mode == 1 && auth_algo == 11 ) {
        return 32 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 1 && crypto_mode == 1 && auth_algo == 12 ) {
        return 32 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 1 && crypto_mode == 1 && auth_algo == 13 ) {
        return 40 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 1 && crypto_mode == 1 && auth_algo == 14 ) {
        return 48 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 2 && crypto_mode == 1 && auth_algo == 9 ) {
        return 28 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 2 && crypto_mode == 1 && auth_algo == 10 ) {
        return 28 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 2 && crypto_mode == 1 && auth_algo == 11 ) {
        return 32 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 2 && crypto_mode == 1 && auth_algo == 12 ) {
        return 32 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 2 && crypto_mode == 1 && auth_algo == 13 ) {
        return 40 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 2 && crypto_mode == 1 && auth_algo == 14 ) {
        return 48 + 10 * 8; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 1 && auth_algo == 9 ) {
        return 36 + 10 * 16; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 1 && auth_algo == 10 ) {
        return 36 + 10 * 16; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 1 && auth_algo == 11 ) {
        return 40 + 10 * 16; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 1 && auth_algo == 12 ) {
        return 40 + 10 * 16; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 1 && auth_algo == 13 ) {
        return 48 + 10 * 16; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 1 && auth_algo == 14 ) {
        return 56 + 10 * 16; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 1 && auth_algo == 15 ) {
        return 36 + 10 * 16; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 6 && auth_algo == 9 ) {
        return 28 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 6 && auth_algo == 10 ) {
        return 28 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 6 && auth_algo == 11 ) {
        return 32 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 6 && auth_algo == 12 ) {
        return 32 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 6 && auth_algo == 13 ) {
        return 40 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 6 && auth_algo == 14 ) {
        return 48 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 6 && auth_algo == 15 ) {
        return 28 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 8 && auth_algo == 16 ) {
        return 32 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 9 && auth_algo == 17 ) {
        return 32 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }
    if( crypto_algo == 3 && crypto_mode == 10 && auth_algo == 18 ) {
        return 32 + 10 * 4; // (8+IVLength+ICVLength+Bypasslength) + 10 * blocksize
    }

    printk( KERN_WARNING "Unknown IPSec Configuration to guesstimate packet length for: crypto_algo=%d, crypto_mode=%d, auth_algo=%d\n",crypto_algo,crypto_mode,auth_algo);
    //return invalid packet length
    return 0;
}



void fill_ipsec_info(struct ipsec_info *ii, struct xfrm_state *xfrm_sa, int direction) {
    if(!ii || !xfrm_sa) {
        return;
    }

    /*AVM specific*/
    ii->spi = xfrm_sa->id.spi;
    
    if(direction == LTQ_SAB_DIRECTION_INBOUND ) {
        ii->dir=INBOUND;
    } else { //direction == LTQ_SAB_DIRECTION_OUTBOUND
        ii->dir=OUTBOUND;
    }

    if(xfrm_sa->encap) {
        ii->encap_sport = xfrm_sa->encap->encap_sport;
        ii->encap_dport = xfrm_sa->encap->encap_dport;
        if( xfrm_sa->encap->encap_type == UDP_ENCAP_ESPINUDP ) {
            ii->encap_type = ESPINUDP;
        } else if( xfrm_sa->encap->encap_type == UDP_ENCAP_ESPINUDP_NON_IKE ) {
            ii->encap_type = ESPINUDP_NONIKE;
        } else {
            ii->encap_type = NONE;
        }
    } else {
        ii->encap_type = NONE;
    }

    ii->src = xfrm_sa->props.saddr.a4;
    ii->dst = xfrm_sa->id.daddr.a4;

    /*EIP97 specific*/
    ii->cd_info.dw0.acd_size = xfrm_sa->ipsec_eip97_params->tokenwords;
    ii->cd_info.dw0.first_seg = 1;
    ii->cd_info.dw0.last_seg = 1;
    ii->cd_info.dw0.buf_size = 0;

    ii->cd_info.dw2.acd_ptr = (uint32_t)xfrm_sa->ipsec_eip97_params->token_buffer;

    memcpy(&ii->cd_info.dw3 ,&xfrm_sa->ipsec_eip97_params->token_headerword, sizeof(uint32_t));

    ii->cd_info.dw4.res = 0;
    ii->cd_info.dw4.app_id = 0;

    ii->cd_info.dw5.ctx_ptr = (uint32_t)xfrm_sa->ipsec_eip97_params->SA_buffer;

    ii->pad_instr_offset = (xfrm_sa->ipsec_eip97_params->total_pad_offs * 4);
    ii->pad_en = 1;
    ii->crypto_instr_offset = (xfrm_sa->ipsec_eip97_params->crypto_offs * 4);

    ii->hash_pad_instr_offset = xfrm_sa->ipsec_eip97_params->hash_pad_offs ;
    ii->msg_len_instr_offset = xfrm_sa->ipsec_eip97_params->msg_len_offs;

    ii->cd_size = 6; //Command Descriptor Size is 6 DWORDS

    ii->iv_len = xfrm_sa->ipsec_eip97_params->ivsize;
    ii->icv_len = xfrm_sa->ipsec_eip97_params->ICV_length;
    ii->blk_size = xfrm_sa->ipsec_eip97_params->pad_blksize;

}



void avm_eip97_add_sa(struct xfrm_state *xfrm_sa, int direction, unsigned int packetlength) {
    char emode[64]="", ealgo[64]="";
    int32_t tmp;
    
    xfrm_sa->ipsec_eip97_params = kmalloc( sizeof( struct ltq_crypto_ipsec_params), GFP_KERNEL);
    if( xfrm_sa->ipsec_eip97_params == 0 ) {
        printk(KERN_ERR "<%s> Out of memory, cannot activate hardware IPSec acceleration for SPI 0x%08x\n",__FUNCTION__,xfrm_sa->id.spi);
        return;
    }

    memset(xfrm_sa->ipsec_eip97_params, 0, sizeof(struct ltq_crypto_ipsec_params));

    if( xfrm_sa->id.proto != IPPROTO_ESP ) {
        printk( KERN_ERR "<%s> Only ESP allowed as protocol for IPSec, could not activate hardware IPSec acceleration for SPI 0x%08x.\n",__FUNCTION__,xfrm_sa->id.spi);
        return;
    }
    xfrm_sa->ipsec_eip97_params->ipsec_mode = LTQ_SAB_IPSEC_ESP;
    xfrm_sa->ipsec_eip97_params->ipsec_tunnel = LTQ_SAB_IPSEC_TUNNEL;
    xfrm_sa->ipsec_eip97_params->direction = direction;
    xfrm_sa->ipsec_eip97_params->spi = xfrm_sa->id.spi;
    xfrm_sa->ipsec_eip97_params->ip_type = LTQ_SAB_IPSEC_IPV4;
    xfrm_sa->ipsec_eip97_params->ring_no = 1;

    if(xfrm_sa->aead->alg_name != NULL) {
        parse_alg( xfrm_sa->aead->alg_name, emode, sizeof(emode), ealgo, sizeof(ealgo) );
        tmp = mpe_hal_get_crypto_mode(emode);
        if( tmp < 0 ) {
            printk( KERN_ERR "<%s> Unknown crypto mode '%s'. IPSec hardware acceleration setup failed for SPI 0x%08x.\n", __FUNCTION__, emode,xfrm_sa->id.spi);
            goto error;
        }
        xfrm_sa->ipsec_eip97_params->crypto_mode = tmp;
        tmp = mpe_hal_get_crypto_algo(ealgo);
        if( tmp < 0 ) {
            printk( KERN_ERR "<%s> Unknown crypto algorithm '%s'. IPSec hardware acceleration setup failed for SPI 0x%08x.\n", __FUNCTION__, ealgo,xfrm_sa->id.spi);
            goto error;
        }
        xfrm_sa->ipsec_eip97_params->crypto_algo = tmp;
        xfrm_sa->ipsec_eip97_params->crypto_keysize = (xfrm_sa->aead->alg_key_len + 7)/8; //bits to bytes
        xfrm_sa->ipsec_eip97_params->crypto_key = &xfrm_sa->aead->alg_key[0];

        xfrm_sa->ipsec_eip97_params->auth_algo = LTQ_SAB_AUTH_AES_GCM; //NOTE: AES-GCM weil das die einzige unterstuetzte Verschluesselung im Galois/Counter Mode (GCM) ist
        xfrm_sa->ipsec_eip97_params->authsize = (xfrm_sa->aead->alg_key_len + 7)/8;
        xfrm_sa->ipsec_eip97_params->auth_keysize = (xfrm_sa->aead->alg_key_len + 7)/8;
        xfrm_sa->ipsec_eip97_params->auth_key = &xfrm_sa->aead->alg_key[0];

    }
    if(xfrm_sa->ealg->alg_name != NULL) {
        parse_alg( xfrm_sa->ealg->alg_name, emode, sizeof(emode), ealgo, sizeof(ealgo) );
        tmp = mpe_hal_get_crypto_mode(emode);
        if( tmp < 0 ) {
            printk( KERN_ERR "<%s> Unknown crypto mode '%s'. IPSec hardware acceleration setup failed for SPI 0x%08x.\n", __FUNCTION__, emode,xfrm_sa->id.spi);
            goto error;
        }
        xfrm_sa->ipsec_eip97_params->crypto_mode = tmp;
        tmp = mpe_hal_get_crypto_algo(ealgo);
        if( tmp < 0 ) {
            printk( KERN_ERR "<%s> Unknown crypto algorithm '%s'. IPSec hardware acceleration setup failed for SPI 0x%08x.\n", __FUNCTION__, ealgo,xfrm_sa->id.spi);
            goto error;
        }
        xfrm_sa->ipsec_eip97_params->crypto_algo = tmp;
        xfrm_sa->ipsec_eip97_params->crypto_keysize = (xfrm_sa->ealg->alg_key_len + 7)/8;
        xfrm_sa->ipsec_eip97_params->crypto_key = &xfrm_sa->ealg->alg_key[0];
    }

    if(xfrm_sa->aalg->alg_name != NULL){
        tmp = mpe_hal_get_auth_algo(xfrm_sa->aalg->alg_name);
        if( tmp < 0 ) {
            printk( KERN_ERR "<%s> Unknown auth algorithm '%s'. IPSec hardware acceleration setup failed for SPI 0x%08x.\n", __FUNCTION__, xfrm_sa->aalg->alg_name,xfrm_sa->id.spi);
            goto error;
        }
        xfrm_sa->ipsec_eip97_params->auth_algo = tmp;
        xfrm_sa->ipsec_eip97_params->authsize =(xfrm_sa->aalg->alg_key_len + 7)/8;
        xfrm_sa->ipsec_eip97_params->auth_keysize = (xfrm_sa->aalg->alg_key_len + 7)/8;
        xfrm_sa->ipsec_eip97_params->auth_key = &xfrm_sa->aalg->alg_key[0];
    }

    if(direction == LTQ_SAB_DIRECTION_INBOUND ) {

        xfrm_sa->ipsec_eip97_params->cryptosize = guesstimate_packetsize(
                                                        xfrm_sa->ipsec_eip97_params->crypto_algo,
                                                        xfrm_sa->ipsec_eip97_params->crypto_mode,
                                                        xfrm_sa->ipsec_eip97_params->auth_algo
                                                    );
        xfrm_sa->ipsec_eip97_params->cryptosize -= xfrm_sa->ipsec_eip97_params->authsize; /* because
            for whatever reason the Lantiq driver adds the authsize in ltq_build_token() to
            the packetsize (cryptosize). Either cryptosize is meant to be really just the size of the
            crypto part in the ESP => ltq_build_token() would also need to add 8 bytes for SPI and
            sequence number at least, OR cryptosize is meant to be the whole packet size so it should not
            subtract authsize from it. */
        xfrm_sa->ipsec_eip97_params->data_len = xfrm_sa->ipsec_eip97_params->cryptosize;

    } else if(direction == LTQ_SAB_DIRECTION_OUTBOUND ) {

        //NOTE: for outbound connections, any non-zero packet
        //size is valid as that data is the data to be encrypted
        xfrm_sa->ipsec_eip97_params->cryptosize = 16;
        xfrm_sa->ipsec_eip97_params->data_len = 16;
    }

    if(ltq_ipsec_setkey(xfrm_sa->ipsec_eip97_params) != 0 ) {
        printk( KERN_ERR "<%s> IPSec hardware acceleration setup failed for SPI 0x%08x due to ltq_ipsec_setkey(). Compile EIP97 driver with -DDEBUG for more info.\n",__FUNCTION__,xfrm_sa->id.spi);
        goto error;
    }

    if(ltq_get_ipsec_token(xfrm_sa->ipsec_eip97_params) != 0 ) {

        printk( KERN_INFO "<%s> IPSec hardware acceleration setup failed for SPI 0x%08x using packet length lookup function guesstimate_packetsize. Trying with actual packet length.\n",__FUNCTION__,xfrm_sa->id.spi);

        xfrm_sa->ipsec_eip97_params->cryptosize = packetlength - xfrm_sa->ipsec_eip97_params->authsize;
        xfrm_sa->ipsec_eip97_params->data_len = packetlength;

        if(ltq_get_ipsec_token(xfrm_sa->ipsec_eip97_params) != 0 ) {
            printk( KERN_ERR "<%s> IPSec hardware acceleration setup failed for SPI 0x%08x due to ltq_get_ipsec_token(). Compile EIP97 driver with -DDEBUG for more info.\n",__FUNCTION__,xfrm_sa->id.spi);
            goto error;
        }
    }

    //set flag in xfrm state so within other components we know how to treat this session
    if(direction == LTQ_SAB_DIRECTION_INBOUND ) {
        xfrm_sa->props.extra_flags |= XFRM_SA_XFLAG_EIP97_ACCEL_IN;
        printk( KERN_INFO "Enabled inbound IPSec hardware acceleration for SPI 0x%08x\n", xfrm_sa->ipsec_eip97_params->spi);
    } else {
        xfrm_sa->props.extra_flags |= XFRM_SA_XFLAG_EIP97_ACCEL_OUT;
        printk( KERN_INFO "Enabled outbound IPSec hardware acceleration for SPI 0x%08x\n", xfrm_sa->ipsec_eip97_params->spi);
    }

    //hand ipsec parameters down to yield thread
    struct ipsec_info iis; //TODO: change to structure shared with yield thread!
    fill_ipsec_info(&iis,xfrm_sa,direction);

    return;
error:
    kfree(xfrm_sa->ipsec_eip97_params);
    xfrm_sa->ipsec_eip97_params = 0;
    return;
}

void avm_eip97_del_sa(struct xfrm_state *xfrm_sa) {
    if( xfrm_sa->ipsec_eip97_params ) {
        ltq_destroy_ipsec_sa( xfrm_sa->ipsec_eip97_params );
        kfree( xfrm_sa->ipsec_eip97_params );
        xfrm_sa->ipsec_eip97_params = 0;
    }
    xfrm_sa->props.extra_flags &= ~XFRM_SA_XFLAG_EIP97_ACCEL_IN;
    xfrm_sa->props.extra_flags &= ~XFRM_SA_XFLAG_EIP97_ACCEL_IN;
};

static void esp_output_accel_done(struct ltq_ipsec_complete *enc_done)
{
    struct udphdr *uh;
    struct sk_buff *skb_done = (struct sk_buff *)(enc_done->data);
    struct xfrm_state *x;
    int udph_len = 0;
    __u16 protocol = 0;

    if( enc_done->err != 0 ) {
        //drop
        kfree_skb(skb_done);
        return;
    }

    x = *((struct xfrm_state **) skb_done->cb);

    skb_done->data = skb_transport_header(skb_done);

    udph_len = 0;
    protocol = IPPROTO_ESP;
    if( x->encap ) {

        uh = ((struct udphdr *)skb_done->data);

        if( x->encap->encap_type == UDP_ENCAP_ESPINUDP ) {
            udph_len = sizeof(struct udphdr);
            protocol = IPPROTO_UDP;
            uh->len = enc_done->ret_pkt_len + udph_len;
        } else if( x->encap->encap_type == UDP_ENCAP_ESPINUDP_NON_IKE ) {
            udph_len = sizeof(struct udphdr) + 8;
            protocol = IPPROTO_UDP;
            uh->len = enc_done->ret_pkt_len + udph_len;
        }
    }

    skb_done->len = enc_done->ret_pkt_len + udph_len;
    skb_done->tail = skb_done->data + skb_done->len;
    skb_push(skb_done, -skb_network_offset(skb_done));
    /* for whatever reason, *skb_mac_header(skb_done) points to the IP protocol field.
     * Probably because of too few pointers within struct sk_buff.
     */
    *skb_mac_header(skb_done) = protocol; 

    xfrm_output_resume(skb_done, enc_done->err);
}

int esp_output_accel(struct xfrm_state *x, struct sk_buff *skb)
{
    int32_t err;
    int32_t trailer_len; 
    uint32_t iv_len, icv_len, blk_size;
    struct sk_buff *trailer;
    int encap_offset = 0;

    ltq_get_length_params(x->id.spi, &iv_len, &icv_len, &blk_size);
    trailer_len = icv_len + blk_size ;

    err = skb_cow_data(skb, trailer_len, &trailer);
    if (err < 0)
        goto error;

    skb_linearize(skb);

    encap_offset = 0;

    //if necessary add udp header
    if( x->encap ) {
        struct udphdr *uh = (struct udphdr *)skb_transport_header(skb);
        int encap_type;
        struct xfrm_encap_tmpl *encap; 
        __be32 *udpdata32;

        //NOTE: skb->data points to the packet to be encrypted (i.e. IP-packet, ICMP-packet, ...),
        //skb_transport_header(skb) to the end of the outer unencrypted IP header which is
        //already present and which will embrace the ESP later.
        //check wether there is space for an udp header at all between skb_data and skb_transport_header(skb)

        if( skb->data - skb_transport_header(skb) < sizeof(struct udphdr) ) {
            static int udph_reported = 0;
            if( !udph_reported ) {
                printk( KERN_ERR "%s: Reported only once: No space left in skb for UDP header encapsulation!\n",__FUNCTION__);
                udph_reported = 1;
            }
            return -EINVAL;
        }

        //there is enough space, so lets do it
        encap = x->encap;

        //get port numbers and encapsulation type
        spin_lock_bh( &x->lock );
        uh->source = encap->encap_sport;
        uh->dest = encap->encap_dport;
        encap_type = encap->encap_type;
        spin_unlock_bh( &x->lock );

        uh->len = 0; //will be set in callback as we dont know the size of the final encrypted packet yet
        uh->check = 0;  //no checksum used because the tunneled packet itself contains checksums and
                        //the IPSec encryption might also feature an Integrity Check Value (ICV)
        
        encap_offset += sizeof(struct udphdr);

        //see wether we also need to add a non-IKE marker
        if( encap_type == UDP_ENCAP_ESPINUDP_NON_IKE ) {

            //check wether we do have space for the non-IKE marker
            if( skb->data - skb_transport_header(skb) < encap_offset + 8 ) {
                static int espm_reported = 0;
                if( !espm_reported ) {
                    printk( KERN_ERR "%s: Reported only once: No space left in skb for non-IKE marker!\n",__FUNCTION__);
                    espm_reported = 1;
                }
                return -EINVAL;
            }

            udpdata32 = (__be32 *)(uh + 1);
            udpdata32[0] = udpdata32[1] = 0;
            encap_offset += 8;

        }
    }
    struct ltq_crypto_ipsec_params *p;
    unsigned int enc_delta;

    //memorize xfrm state for callback
    *((struct xfrm_state **) skb->cb) = x;

    //check wether encrypted version can fit into skb at all
    p = x->ipsec_eip97_params;
    enc_delta = 8 +                     //4 byte SPI, 4 byte sequence number
                p->ivsize +             //Initialization Vector for the encryption
                p->ICV_length +         //Integrity Check Value, only if integrity service like an HMAC was selected
                p->pad_blksize +        //block size of the encryption, since cleartext is padded until it is a multiple of this
                8;                      //just some additional space in case we missed something in this calculation

    if( ( skb->data - skb_transport_header(skb) - encap_offset ) + skb_tailroom(skb) < enc_delta ) {
        static int encd_reported=0;
        if( !encd_reported ) {
            printk( KERN_ERR "%s: Reported only once: Not enough space in sk_buff to support encryption enlargement of IPSec packet. Dropping packets like that.\n",__FUNCTION__);
            encd_reported = 1;
        }
        return -EINVAL;
    }
    
    //printk( KERN_ERR " %d / %d\n", skb_transport_header(skb), skb->data);
    //printk( KERN_ERR "preenc skb: %*ph (%d)\n",32,skb_transport_header(skb),skb->len);

    err = ltq_ipsec_enc(x->id.spi, skb->data, skb_transport_header(skb) + encap_offset, esp_output_accel_done,
            skb->len, skb);

    if (err == -EINPROGRESS)
        goto error;

    if (err == -EBUSY)
        err = NET_XMIT_DROP;

    if(err > 0) {
        skb->data = skb_transport_header(skb);
        skb->len = err;
        skb->tail = skb->data + skb->len;
        skb_push(skb, -skb_network_offset(skb));
        *skb_mac_header(skb) = IPPROTO_ESP;
        return 0;

    }

error:
    return err;
}


void dump_skb(struct sk_buff *skb)
{
    int elt;
    struct sk_buff *skb1, **skb_p;

    printk("============= \n");
    printk("<%s> skb:%p\n",__FUNCTION__,skb);
    printk("<%s> skb->data_len:%d  headlen:%d skb->len:%d \n",
        __FUNCTION__, skb->data_len, skb->hdr_len, skb->len);

    printk("<%s> nr_frags:%d\n",__FUNCTION__, skb_shinfo(skb)->nr_frags);
    printk("<%s> Head:%\ tail:%p end:%p \n",__FUNCTION__, skb->head, skb->tail, skb->end);
    printk("<%s> Headroom:%d tailroom:%d \n",__FUNCTION__, skb_headroom(skb), skb_tailroom(skb));
    printk("<%s> Data pointer:%p network_header:%p transport_header:%p\n",__FUNCTION__, skb->data, skb_network_header(skb),skb_transport_header(skb));
    printk("<%s> network_offset:%x transport_offset:%x\n",__FUNCTION__,skb_network_offset(skb),skb_transport_offset(skb));

    elt = 1;
    skb_p = &skb_shinfo(skb)->frag_list;
    while ((skb1 = *skb_p) != NULL) {

        printk("<%s> ++++ Frags:[%d] +++++\n",__FUNCTION__, elt);
        printk("<%s> skb1->data_len:%d  headlen:%d  skb1->len:%d \n",
            __FUNCTION__, skb1->data_len, skb1->hdr_len, skb1->len);
        printk("<%s> Head:%p tail:%p \n",__FUNCTION__, skb1->head, skb1->tail);
        printk("<%s> Data pointer:%p network_header:%p transport_header:%p\n",__FUNCTION__,skb1->data, skb_network_header(skb1),skb_transport_header(skb1));
        printk("<%s> network_offset:%x transport_offset:%x\n",__FUNCTION__,skb_network_offset(skb1),skb_transport_offset(skb1));
        elt++;
        skb_p = &skb1->next;

    }

    printk("<%s> Number of Frags: %d\n",__FUNCTION__, elt);
    printk("============= \n");
}


static void esp_input_accel_done(struct ltq_ipsec_complete *dec_done)
{
    struct sk_buff *skb_done = (struct sk_buff *)(dec_done->data);

    if( dec_done->err != 0 ) { //TODO: assumption. check specific error code once we do have the EIP97 documentation
        //drop
        kfree_skb(skb_done);
        return;
    }

    skb_done->len = dec_done->ret_pkt_len;
    skb_done->tail = skb_done->data  + skb_done->len;

    xfrm_input_resume(skb_done,4);
}

int esp_input_accel(struct xfrm_state *x, struct sk_buff *skb)
{
    int32_t err;
    struct sk_buff *trailer;
    
    err = skb_cow_data(skb, 0, &trailer);
    if (err < 0)
        goto error;

    skb_linearize(skb);    
        //dump_skb(skb); printk( "pre-dec skb: %*ph (%d in total)\n",84,skb->data,skb->len);

    err = ltq_ipsec_dec(x->id.spi, skb->data, skb->data, esp_input_accel_done,
            skb->len, skb);

    if (err == -EINPROGRESS)
        goto error;

    if (err == -EBUSY)
        err = NET_XMIT_DROP;
        
    if(err > 0)
    {
        skb->len = err;
        skb->tail = skb->data  + skb->len;
    return 0;
    }

error:
    return err;
}

#endif


