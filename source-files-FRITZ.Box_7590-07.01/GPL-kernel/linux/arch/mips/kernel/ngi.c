#include <linux/bug.h>
#include <linux/kernel.h>
#include <linux/debugfs.h>
#include <linux/init.h>
#include <linux/notifier.h>
#include <linux/kdebug.h>
#include <linux/string.h>
#include <linux/gfp.h>

#include <asm/signal.h>
#include <asm/barrier.h>

#include <asm/mach-lantiq/grx500/ssx0_ssx.h>

struct agent{
	uint32_t ssx_id;
	int32_t link;
	void *addr;
	char *name;
	enum {
		IA_AGENT,
		TA_AGENT
	} type;
};

struct agent agents[] =
{
	{6, -1, (void *) CKSEG1ADDR(0x13f10000u) ,"l2", IA_AGENT},
	{6,  0, (void *) CKSEG1ADDR(0x13f10800u) ,"ln06", IA_AGENT},
	{6, -1, (void *) CKSEG1ADDR(0x13f10400u) ,"tep", IA_AGENT},
	{6, -1, (void *) CKSEG1ADDR(0x13f01400u) ,"ddr", TA_AGENT},
	{6, -1, (void *) CKSEG1ADDR(0x13f01000u) ,"ic", TA_AGENT},
	{6,  0, (void *) CKSEG1ADDR(0x13f02000u) ,"ln60", TA_AGENT},
	{6, -1, (void *) CKSEG1ADDR(0x13f01800u) ,"pctl", TA_AGENT},
	{6, -1, (void *) CKSEG1ADDR(0x13f01c00u) ,"pub", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff11000u) ,"dm0", IA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff11400u) ,"dm3", IA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff11800u) ,"dm4", IA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff10c00u) ,"e123r", IA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff10800u) ,"e123w", IA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff10400u) ,"e97r", IA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff10000u) ,"e97w", IA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff12800u) ,"ex50", IA_AGENT},
	{0,  1, (void *) CKSEG1ADDR(0x1ff11c00u) ,"ln10", IA_AGENT},
	{0,  2, (void *) CKSEG1ADDR(0x1ff12000u) ,"ln20", IA_AGENT},
	{0,  3, (void *) CKSEG1ADDR(0x1ff12400u) ,"ln30", IA_AGENT},
	{0,  6, (void *) CKSEG1ADDR(0x1ff12c00u) ,"ln60", IA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff13000u) ,"toe", IA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff01c00u) ,"cbm1", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff02000u) ,"cbm2", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff02c00u) ,"dm3", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff03000u) ,"dm4", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff02800u) ,"e123", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff02400u) ,"e97", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff04000u) ,"ex04", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff04400u) ,"ex05", TA_AGENT},
	{0,  1, (void *) CKSEG1ADDR(0x1ff03400u) ,"ln01", TA_AGENT},
	{0,  2, (void *) CKSEG1ADDR(0x1ff03800u) ,"ln02", TA_AGENT},
	{0,  3, (void *) CKSEG1ADDR(0x1ff03c00u) ,"ln03", TA_AGENT},
	{0,  6, (void *) CKSEG1ADDR(0x1ff04800u) ,"ln06", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff01800u) ,"otp", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff01400u) ,"rom", TA_AGENT},
	{0, -1, (void *) CKSEG1ADDR(0x1ff01000u) ,"ssb", TA_AGENT},
	{1, -1, (void *) CKSEG1ADDR(0x1df10000u) ,"dm2r", IA_AGENT},
	{1, -1, (void *) CKSEG1ADDR(0x1df10400u) ,"dm2t", IA_AGENT},
	{1,  0, (void *) CKSEG1ADDR(0x1df11800u) ,"ln01", IA_AGENT},
	{1, -1, (void *) CKSEG1ADDR(0x1df10800u) ,"xcm2", IA_AGENT},
	{1, -1, (void *) CKSEG1ADDR(0x1df10c00u) ,"xcm3", IA_AGENT},
	{1, -1, (void *) CKSEG1ADDR(0x1df11000u) ,"xcm4", IA_AGENT},
	{1, -1, (void *) CKSEG1ADDR(0x1df11400u) ,"xcm5", IA_AGENT},
	{1, -1, (void *) CKSEG1ADDR(0x1df01000u) ,"dm2r", TA_AGENT},
	{1, -1, (void *) CKSEG1ADDR(0x1df01400u) ,"dm2t", TA_AGENT},
	{1, -1, (void *) CKSEG1ADDR(0x1df01800u) ,"gwpl", TA_AGENT},
	{1,  0, (void *) CKSEG1ADDR(0x1df01c00u) ,"ln10", TA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf11000u) ,"dm1r", IA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf11400u) ,"dm1t", IA_AGENT},
	{2,  0, (void *) CKSEG1ADDR(0x1bf11c00u) ,"ln02", IA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf10400u) ,"us0r", IA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf10000u) ,"us0w", IA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf10c00u) ,"us1r", IA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf10800u) ,"us1w", IA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf11800u) ,"xcmf", IA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf02000u) ,"dm1r", TA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf02400u) ,"dm1t", TA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf02800u) ,"gwpr", TA_AGENT},
	{2,  0, (void *) CKSEG1ADDR(0x1bf02c00u) ,"ln20", TA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf01400u) ,"us0c", TA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf01000u) ,"us0", TA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf01c00u) ,"us1c", TA_AGENT},
	{2, -1, (void *) CKSEG1ADDR(0x1bf01800u) ,"us1", TA_AGENT},
	{3,  0, (void *) CKSEG1ADDR(0x19f11800u) ,"ln03", IA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f10c00u) ,"pc1r", IA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f10800u) ,"pc1w", IA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f11400u) ,"pc2r", IA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f11000u) ,"pc2w", IA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f10400u) ,"pc3r", IA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f10000u) ,"pc3w", IA_AGENT},
	{3,  0, (void *) CKSEG1ADDR(0x19f04000u) ,"ln30", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f02c00u) ,"msi1", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f03c00u) ,"msi2", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f01c00u) ,"msi3", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f02400u) ,"pc1a", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f02800u) ,"pc1c", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f02000u) ,"pc1", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f03400u) ,"pc2a", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f03800u) ,"pc2c", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f03000u) ,"pc2", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f01400u) ,"pc3a", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f01800u) ,"pc3c", TA_AGENT},
	{3, -1, (void *) CKSEG1ADDR(0x19f01000u) ,"pc3", TA_AGENT},
};
	
static volatile uint32_t *agent_status_flags_addr(char *agent_base)
{
	return (uint32_t *)(agent_base + 0x28);
}

static volatile uint32_t *agent_control_addr(char *agent_base)
{
	return (uint32_t *)(agent_base + 0x20);
}

#define SERROR_MASK TSSB_TA_AGENT_STATUS_SERROR_MASK
#define REQ_TIMEOUT_MASK TSSB_TA_AGENT_STATUS_REQ_TIMEOUT_MASK

void ssx_status_print(void)
{
	unsigned int i;

	for(i = 0; i < ARRAY_SIZE(agents); i++) {
		uint32_t val = *agent_status_flags_addr(agents[i].addr);

		if(val & (SERROR_MASK | REQ_TIMEOUT_MASK)) {
			pr_err("ssx%u %s %-8s: 0x%08x %s%s\n",
			       agents[i].ssx_id,
			       agents[i].type == IA_AGENT ? "ia" : "ta",
			       agents[i].name,
			       val,
			       val & SERROR_MASK ? "serror/merror " : "",
			       val & REQ_TIMEOUT_MASK ? "req_timeout/resp_timeout " : "");
		}

		if(val & (SERROR_MASK | REQ_TIMEOUT_MASK)) {
			pr_err("try to reset %s\n", agents[i].name);
			*agent_control_addr(agents[i].addr) |= 1;
			smp_wmb();
			*agent_control_addr(agents[i].addr) = 0;
		}
	}
}
EXPORT_SYMBOL(ssx_status_print);

int die_handler(struct notifier_block *nb,
                unsigned long action __maybe_unused,
                void *data)
{
	struct die_args *args = data;
	static bool oneshot=1;

	if(args->signr == SIGBUS && oneshot) {

		/* one-shot to rule out recursion */
		oneshot = 0;

		ssx_status_print();

		return NOTIFY_OK;
	} else {
		return NOTIFY_DONE;
	}
}

static struct notifier_block nb = {
	.notifier_call = die_handler,
	.priority = 0,
};

static int __init ngi_debug_init(void)
{
	struct dentry *dir;
	uint32_t i;
//	static uint32_t addr = 0xbe7d4100;

	register_die_notifier(&nb);

	dir = debugfs_create_dir("ngi", NULL);
	if(!dir) return -1;
	for(i = 0; i < ARRAY_SIZE(agents); i++) {
		char name[20];
		snprintf(name,
		         sizeof(name),
		         "ssx%u_%s_%s",
		         agents[i].ssx_id,
		         agents[i].type == IA_AGENT ? "ia" : "ta",
		         agents[i].name);
		debugfs_create_x32(kstrdup(name, GFP_KERNEL),
		                   0600,
		                   dir,
		                   (void *)agent_status_flags_addr(agents[i].addr));
	}

#if 0
	debugfs_create_x32("addr", 0600, dir, &addr);
	debugfs_create_x32("access", 0600, dir, (void *)addr);
#endif

	return 0;
}

arch_initcall(ngi_debug_init);
