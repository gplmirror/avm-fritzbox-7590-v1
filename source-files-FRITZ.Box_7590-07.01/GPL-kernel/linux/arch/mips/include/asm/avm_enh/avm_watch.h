#ifndef __avm_watch_h__
#define __avm_watch_h__

#if defined(CONFIG_AVM_WP)
/**
 * ret: 1 -> handled
 *      0 -> use ptrace-watchpoint interface
 */
int avm_wp_dispatcher(struct pt_regs *regs);

/**
 */
enum _wp_type {
    wp_type_write        = (1 << 0),
    wp_type_read         = (1 << 1),
    wp_type_instruction  = (1 << 2)
};
/**
 * ref:      ref for wp_handler
 * watch_nr: interaptiv 0-3 (0-1 instruction, 2-3 data)
 * addr:     watchpoint-addr (double-word)
 * mask:     Bit 11 - 3: bits which ignored on addr (watch-area)
 * type:     interaptiv: 0/1 wp_type_read/wp_type_write
 *                       3/4 instruction
 * cpu_mask: mask of cpu-settings
 *
 * ret-param of wp_handler: ret= 1: retrigger watchpoint
 */
int set_watchpoint_handler(void *ref, int watch_nr, unsigned long addr, unsigned long mask, enum _wp_type type,
                           int (*wp_handler)(void *ref, enum _wp_type status, int deferred, struct pt_regs *),
                           const cpumask_t *cpu_mask);
/**
 * use default-watchpointhandler
 * watch_nr: interaptiv 0-3 (0-1 instruction, 2-3 data)
 * addr:     watchpoint-addr (double-word)
 * mask:     Bit 11 - 3: bits which ignored on addr (watch-area)
 * type:     interaptiv: 0/1 wp_type_read/wp_type_write
 *                       3/4 instruction
 * cpu_id:    >= NR_CPUS - all cpus
*/
int set_watchpoint(int watch_nr, unsigned long addr, int mask, enum _wp_type type, int cpu_id);
/**
 * use default-watchpointhandler
 * and trigger panic if watchpoint occur
 * watch_nr: interaptiv 0-3 (0-1 instruction, 2-3 data)
 * addr:     watchpoint-addr (double-word)
 * mask:     Bit 11 - 3: bits which ignored on addr (watch-area)
 * type:     interaptiv: 0/1 wp_type_read/wp_type_write
 *                       3/4 instruction
 * cpu_id:    >= NR_CPUS - all cpus
 */
int set_watchpoint_fatal(int watch_nr, unsigned long addr, int mask, enum _wp_type type, int cpu_id);

/**
 * delete watchpoint
 * watch_nr: interaptiv 0-3 (0-1 instruction, 2-3 data)
 * cpu_id:    >= NR_CPUS - all cpus
 */
void del_watchpoint(int watch_nr, int cpu_id);


/**
 * only current cpu!
 * watch_nr: interaptiv 0-3 (0-1 instruction, 2-3 data)
 */
int watchpoint_busy(int watch_nr);
#endif
#endif/*--- #ifndef __avm_watch_h__ ---*/
