/*
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 *
 *  Copyright (C) 2010 John Crispin <blogic@openwrt.org>
 *  Copyright (C) 2013 Lei Chuanhua <chuanhua.lei@lantiq.com>
 */

#include <linux/init.h>
#include <linux/io.h>
#include <linux/ioport.h>
#include <linux/pm.h>
#include <linux/export.h>
#include <linux/delay.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <linux/reset-controller.h>

#include <asm/reboot.h>

#include <lantiq_soc.h>

#ifdef CONFIG_LTQ_ETHSW_API
#include <xway/switch-api/lantiq_gsw_api.h>
#endif

#include "prom.h"

#if defined(CONFIG_AVM_ENHANCED)
#include <asm/mach_avm.h>
#endif/*--- #if defined(CONFIG_AVM_ENHANCED) ---*/
/* reset request register */
#define RCU_RST_REQ		0x0010
/* reset status register */
#define RCU_RST_STAT		0x0014

#define RCU_RST_STAT2		0x0024
#define RCU_RST_REQ2		0x0048

#define RCU_AHB_ENDIAN		0x8004C
#define RCU_IFMUX_CFG		0x80120

#define RCU_USB0_CFG		0x80018
#define RCU_USB1_CFG		0x80034

/* reboot bit */
#define RCU_RD_SRST		BIT(30)

/* reset cause */
#define RCU_STAT_SHIFT		26
/* boot selection */
#define RCU_BOOT_SEL(x)		((x >> 18) & 0x7)
#define RCU_BOOT_SEL_XRX200(x)	(((x >> 17) & 0xf) | ((x >> 8) & 0x10))

/* remapped base addr of the reset control unit */

/* Status */
static u32 rcu_stat[] = {
	RCU_RST_STAT,
	RCU_RST_STAT2,
};

/* request */
static u32 rcu_req[] = {
	RCU_RST_REQ,
	RCU_RST_REQ2,
};

#define RCU_STAT_REG(x)		(rcu_stat[(x)])
#define RCU_REQ_REG(x)		(rcu_req[(x)])

static void __iomem *ltq_rcu_membase;
static struct device_node *ltq_rcu_np;
static DEFINE_SPINLOCK(ltq_rcu_lock);

/* This function is used by the watchdog driver */
int ltq_reset_cause(void)
{
	u32 val = ltq_rcu_r32(RCU_RST_STAT);
	return val >> RCU_STAT_SHIFT;
}
EXPORT_SYMBOL_GPL(ltq_reset_cause);

/* This function is used by the watchdog driver */
int ltq_reset_get_status (void)
{
	u32 val = ltq_rcu_r32(RCU_RST_REQ);
	return val;
}
EXPORT_SYMBOL_GPL(ltq_reset_get_status);

/* allow platform code to find out what source we booted from */
unsigned char ltq_boot_select(void)
{
	u32 val = ltq_rcu_r32(RCU_RST_STAT);

	if (of_device_is_compatible(ltq_rcu_np, "lantiq,rcu-xrx200"))
		return RCU_BOOT_SEL_XRX200(val);

	return RCU_BOOT_SEL(val);
}


/* reset a io domain for u micro seconds */
void ltq_reset_once(unsigned int module, ulong u)
{
	ltq_rcu_w32_mask(0, module, RCU_RST_REQ);
	udelay(u);
	ltq_rcu_w32_mask(module, 0, RCU_RST_REQ);
}
EXPORT_SYMBOL_GPL(ltq_reset_once);

/* reset a io domain by hardware control instead of software control */
void ltq_hw_reset(unsigned int module)
{
	ltq_rcu_w32(module, RCU_RST_REQ);
	while (!(ltq_rcu_r32(RCU_RST_STAT) & module))
		;
}
EXPORT_SYMBOL_GPL(ltq_hw_reset);

void ltq_rcu_w32(uint32_t val, uint32_t reg_off)
{
	ltq_w32(val, ltq_rcu_membase + reg_off);
}
EXPORT_SYMBOL_GPL(ltq_rcu_w32);

uint32_t ltq_rcu_r32(uint32_t reg_off)
{
	return ltq_r32(ltq_rcu_membase + reg_off);
}
EXPORT_SYMBOL_GPL(ltq_rcu_r32);

void ltq_rcu_w32_mask(uint32_t clr, uint32_t set, uint32_t reg_off)
{
	unsigned long flags;

	spin_lock_irqsave(&ltq_rcu_lock, flags);
	ltq_rcu_w32((ltq_rcu_r32(reg_off) & ~(clr)) | (set), reg_off);
	spin_unlock_irqrestore(&ltq_rcu_lock, flags);
}
EXPORT_SYMBOL_GPL(ltq_rcu_w32_mask);

void ltq_endian_set(int bitn)
{
	ltq_rcu_w32_mask(0, BIT(bitn), RCU_AHB_ENDIAN);
}
EXPORT_SYMBOL_GPL(ltq_endian_set);

void ltq_pcie_clk_out(int bitn)
{
	ltq_rcu_w32_mask(BIT(bitn), 0, RCU_IFMUX_CFG);
}
EXPORT_SYMBOL_GPL(ltq_pcie_clk_out);

void ltq_endian_clr(int bitn)
{
	ltq_rcu_w32_mask(BIT(bitn), 0, RCU_AHB_ENDIAN);
}
EXPORT_SYMBOL_GPL(ltq_endian_clr);

void ltq_usb_phy_reset(unsigned int nbit)
{
	ltq_rcu_w32_mask(nbit, 0, RCU_RST_REQ2);
}
EXPORT_SYMBOL_GPL(ltq_usb_phy_reset);

void ltq_usb0_port_endian_set(unsigned int clr, unsigned int set)
{
	ltq_rcu_w32_mask(clr, set, RCU_USB0_CFG);
}
EXPORT_SYMBOL_GPL(ltq_usb0_port_endian_set);

void ltq_usb1_port_endian_set(unsigned int clr, unsigned int set)
{
	ltq_rcu_w32_mask(clr, set, RCU_USB1_CFG);
}
EXPORT_SYMBOL_GPL(ltq_usb1_port_endian_set);

static int ltq_assert_device(struct reset_controller_dev *rcdev,
				unsigned long id)
{
	u32 val;
	int regidx = id >> 5;
	int regbit = id & 0x1F;

	val = ltq_rcu_r32(RCU_REQ_REG(regidx));
	val |= BIT(regbit);
	ltq_rcu_w32(val, RCU_REQ_REG(regidx));

	return 0;
}

static int ltq_deassert_device(struct reset_controller_dev *rcdev,
				  unsigned long id)
{
	u32 val;
	int regidx = id >> 5;
	int regbit = id & 0x1F;

	val = ltq_rcu_r32(RCU_REQ_REG(regidx));
	val &= ~BIT(regbit);
	ltq_rcu_w32(val, RCU_REQ_REG(regidx));

	return 0;
}

static int ltq_reset_device(struct reset_controller_dev *rcdev,
			       unsigned long id)
{
	int regidx = id >> 5;
	int regbit = id & 0x1F;
	
	ltq_rcu_w32(BIT(regbit), RCU_REQ_REG(regidx));
	while (!(ltq_rcu_r32(RCU_STAT_REG(regidx)) & (BIT(regbit))))
		;
	return 0;
}

static struct reset_control_ops reset_ops = {
	.reset = ltq_reset_device,
	.assert = ltq_assert_device,
	.deassert = ltq_deassert_device,
};

static struct reset_controller_dev reset_dev = {
	.ops			= &reset_ops,
	.owner			= THIS_MODULE,
	.nr_resets		= 64,
	.of_reset_n_cells	= 1,
};

#if 0
/* NB, put it into reset state until GPHY take it out of reset 
 * it tries to fix half-way packet issues
 */
static void gphy_rst_assert(void)
{
	ltq_rcu_w32_mask(0, BIT(31), RCU_RST_REQ); /* GPHY6F */
	ltq_rcu_w32_mask(0, BIT(29), RCU_RST_REQ); /* GPHY2 */
	ltq_rcu_w32_mask(0, BIT(28), RCU_RST_REQ); /* GPHY3 */
	ltq_rcu_w32_mask(0, BIT(26), RCU_RST_REQ); /* GPHY4 */
	ltq_rcu_w32_mask(0, BIT(25), RCU_RST_REQ); /* GPHY5 */
}
#endif

void ltq_rst_init(void)
{
	reset_dev.of_node = of_find_compatible_node(NULL, NULL,
				"lantiq,rcu-grx500");
	if (!reset_dev.of_node)
		pr_err("Failed to find reset controller node");
	if (IS_ENABLED(CONFIG_RESET_CONTROLLER))
		reset_controller_register(&reset_dev);
}


static void ltq_machine_restart(char *command)
{
	local_irq_disable();
#if defined(CONFIG_AVM_ENHANCED)
    avm_set_reset_status(RS_REBOOT);
#endif
	ltq_rcu_w32(ltq_rcu_r32(RCU_RST_REQ) | RCU_RD_SRST, RCU_RST_REQ);
	unreachable();
}

static void ltq_machine_halt(void)
{
	local_irq_disable();
	unreachable();
}

static void ltq_machine_power_off(void)
{
	local_irq_disable();
	unreachable();
}

static int __init mips_reboot_setup(void)
{
	struct resource res;


	ltq_rcu_np = of_find_compatible_node(NULL, NULL,
				"lantiq,rcu-grx500");

	/* check if all the reset register range is available */
	if (!ltq_rcu_np)
		panic("Failed to load reset resources from devicetree");

	if (of_address_to_resource(ltq_rcu_np, 0, &res))
		panic("Failed to get rcu memory range");

	if (!request_mem_region(res.start, resource_size(&res), res.name))
		pr_err("Failed to request rcu memory");

	ltq_rcu_membase = ioremap_nocache(res.start, resource_size(&res));
	if (!ltq_rcu_membase)
		panic("Failed to remap core memory");

	_machine_restart = ltq_machine_restart;
	_machine_halt = ltq_machine_halt;
	pm_power_off = ltq_machine_power_off;

	/*gphy_rst_assert();*/
#ifdef CONFIG_LTQ_ETHSW_API
	gsw_api_disable_switch_ports();
#endif
	return 0;
}

core_initcall(mips_reboot_setup);
