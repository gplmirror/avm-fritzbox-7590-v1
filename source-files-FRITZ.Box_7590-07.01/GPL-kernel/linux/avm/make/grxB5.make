KERNEL_IMG_NAME	:= .vmlinux

START_SCRIPT_CAPI := $(FILESYSTEM)/etc/init.d/E41-capi
START_SCRIPT_PIGLET := $(FILESYSTEM)/etc/init.d/S11-piglet
INITRAMFS_SCRIPTS := scripts/gen_initramfs_list.sh conf/initramfs-base-files.txt
LINUX_KERNEL_BUILDDIR := $(LINUX_KERNEL_SOURCE)

ADD_FILE_HELPER := $(wildcard add_file_helper/*)
ADD_FILE_HELPER := $(wildcard scripts/add-file-helper/*)

.lha.options: $(LINUX_KERNEL_BUILDDIR)/System.map
	echo -e "-l \c" >> $@
	grep 'T _text' $< | sed -e's/..\(.*\) T .*/0x80\1/g' | tr '\n' ' ' >> $@

	echo -e "-e \c" >> $@
	grep 'T kernel_entry' $< | sed -e's/..\(.*\) T .*/0x80\1/g' | tr '\n' ' ' >> $@    

$(FILESYSTEM)/$(KERNEL_IMG_NAME): $(LINUX_KERNEL_BUILDDIR)/vmlinux
	@echo "  OBJCOPY $(notdir $<) -> $@"
	$(CROSS_COMPILE)objcopy --remove-section .note.gnu.build-id -S -O binary $< $@
	# fuer dissassemble-helper in avmterm:
	echo `nm $< | grep "kallsyms_\(names\|token\|num_syms\|addresses\)"` >> $@
	echo `file -b -e elf $<` >>$@


.PHONY: install_initramfs_scripts
install: install_initramfs_scripts
install_initramfs_scripts:
	@echo "  INSTALL kernel scripts"
	mkdir -p $(FILESYSTEM)/Config
	$(foreach script,$(INITRAMFS_SCRIPTS),$(call cmd_install,$(script),$(FILESYSTEM)/Config/))
	$(call cmd_install,$(LINUX_KERNEL_BUILDDIR)/usr/gen_init_cpio,$(FILESYSTEM))

.PHONY: install_add_file_helper
install: install_add_file_helper 
install_add_file_helper: $(ADD_FILE_HELPER) 
	@echo "  INSTALL kernel scripts"
	mkdir -p $(FILESYSTEM)/Config
	$(foreach script,$(ADD_FILE_HELPER),$(call cmd_install,$(script),$(FILESYSTEM)/Config/))

dtbs = $(wildcard $(LINUX_KERNEL_BUILDDIR)/arch/$(ARCH)/boot/dts/Fritz_Box_*.dtb)
install: install_dtbs
install_dtbs: $(dtbs)
ifeq ($(dtbs),)
	$(error No device-tree blobs found!  Is CONFIG_OF_AVM_DT set?)
endif
	@echo "  INSTALL generated device tree blobs"
	$(foreach dtb,$(dtbs),$(call cmd_install,$(dtb),$(FILESYSTEM)/avm_dts/))

# vim: set ft=make noexpandtab ts=8:
