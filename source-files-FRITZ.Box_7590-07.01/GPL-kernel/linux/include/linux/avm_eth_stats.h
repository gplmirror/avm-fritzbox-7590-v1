#ifndef _AVM_ETH_STATS
#define _AVM_ETH_STATS

enum eth_tracepoint {
	ETH_TRACEPOINT_TCP_XMIT_PRE_SPLIT,
	ETH_TRACEPOINT_DP_XMIT,
	__ETH_TRACEPOINT_BOTTOM__
};


void avm_eth_stats_log(enum eth_tracepoint, size_t len);

#endif /*_AVM_ETH_STATS*/
