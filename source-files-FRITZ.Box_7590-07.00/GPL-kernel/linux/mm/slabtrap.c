#include <linux/init.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/timer.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <asm/ptrace.h>
#include <asm/avm_enh/avm_watch.h>

MODULE_AUTHOR("Paul Hüber, AVM GmbH");
MODULE_DESCRIPTION("slab debugging using watchpoints");
MODULE_LICENSE ("GPL");

static unsigned long wp_active;
static DEFINE_PER_CPU(struct timer_list, trap_timer);

static bool disable = false;
module_param(disable, bool, 00);

enum timeouts {
	slabtrap_timeout = 10, /* ms */
	slabtrap_slack = slabtrap_timeout / 2 /* ms */
};

static void wp_handler( int status, int deferred, struct pt_regs *regs) {

	extern void show_registers(const struct pt_regs *regs);
	printk("[%s] status=%#x, deferred=%d, epc=0x%lx \n", __FUNCTION__, status, deferred, regs->cp0_epc);
#if 0
	show_registers(regs);
	dump_stack();
#else
	/* fail hard */
	BUG();
#endif
}

/* XXX migrate allocation to avm_wd.c */
static int wp_alloc(void)
{
	int i;
	/* first two watchpoints can only watch for WP_TYPE_I */
	for(i = 2; i < NR_WATCHPOINTS; i++) {
		if(test_and_set_bit(i, &wp_active) == 0) return i;
	}
	return -1;
}
static void wp_free(int wp_nr)
{
	BUG_ON(wp_nr >= NR_WATCHPOINTS);
	BUG_ON(wp_nr < 0);
	if(test_and_clear_bit(wp_nr, &wp_active) == 0) {
		pr_err("%s: watchpoint allocator inconsistency\n", __func__);
	}
}

struct slabtrap {
	struct kmem_cache *cachep;
	int wp_nr;
	void *bait;
};

static void timer_func(unsigned long data)
{
	struct timer_list *t;
	struct slabtrap *trap = (void *) data;

	if(!trap) {
		pr_err("%s: trap is zero, give up\n", __func__);
		return;
	}

	if(trap->wp_nr < 0) {
		int mask, size, addr, offset, rv;
		const int type = WP_TYPE_W | WP_TYPE_R;

		trap->wp_nr = wp_alloc();
		if(trap->wp_nr < 0) goto reschedule;

		trap->bait = kmem_cache_alloc(trap->cachep, GFP_ATOMIC);
		if(trap->bait == NULL) {
			wp_free(trap->wp_nr);
			trap->wp_nr = -1;
			goto reschedule;
		}

		/*
		 * alignment may force us to use some offset into the buffer
		 */
		addr = ALIGN((int)trap->bait, 0x8);
		offset = addr - (int)trap->bait;

		/* 
		 * Ignore as many bits as possible while staying inside object
		 * limits. Leave gaps as needed.
		 */
		size = trap->cachep->object_size - offset;
		mask = BIT(fls(size)-1) - 1;
		mask &= ~(addr & 0xfff);

		/* watch-points have double-word resolution */
		mask >>= 3;

		rv = set_watchpoint_handler(trap->wp_nr, addr, mask, type, wp_handler);
#if 0
		printk("set wp %d on %p with mask %x -> %d (cache-size: %x , offset = %d)\n",
		       trap->wp_nr,
		       trap->bait,
		       mask,
		       rv,
		       trap->cachep->object_size,
		       offset
		       );
#endif
	       ((char *)trap->bait)[256]++;
	} else {
		del_watchpoint(trap->wp_nr);
		wp_free(trap->wp_nr);
		trap->wp_nr = -1;
		kmem_cache_free(trap->cachep, trap->bait);
	}

reschedule:
	t = &per_cpu(trap_timer, smp_processor_id());
	t->expires = jiffies + msecs_to_jiffies(slabtrap_timeout);
	add_timer_on(t, smp_processor_id());
}

void slabtrap_plant(struct kmem_cache *cachep) {
	int cpu;

	if(disable) return;
	if(cachep == NULL) return;

	for_each_online_cpu(cpu)  {
		struct timer_list *t = &per_cpu(trap_timer, cpu);
		struct slabtrap *trap;

		trap = kmalloc(sizeof(*trap), GFP_ATOMIC);
		if(!trap) continue;
		trap->cachep = cachep;
		trap->wp_nr = -1;

		t->slack = msecs_to_jiffies(slabtrap_slack);
		t->expires = jiffies + msecs_to_jiffies(slabtrap_timeout);
		t->data = (unsigned long)trap;
		t->function = timer_func;
		printk("%s: set trap for %s on CPU %d\n", __func__, cachep->name, cpu);
		add_timer_on(t, cpu);
	}
}
EXPORT_SYMBOL(slabtrap_plant);

static int __init slabtrap_init(void)
{
	int cpu;
	for_each_online_cpu(cpu)  {
		struct timer_list *t = &per_cpu(trap_timer, cpu);
		init_timer(t);
	}

	return 0;
}
arch_initcall(slabtrap_init);
