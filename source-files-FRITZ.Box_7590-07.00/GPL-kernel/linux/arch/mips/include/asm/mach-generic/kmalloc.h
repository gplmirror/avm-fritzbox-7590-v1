#ifndef __ASM_MACH_GENERIC_KMALLOC_H
#define __ASM_MACH_GENERIC_KMALLOC_H


#ifndef CONFIG_DMA_COHERENT
/*
 * Total overkill for most systems but need as a safe default.
 * Set this one if any device in the system might do non-coherent DMA.
 */
#if !defined(CONFIG_AVM_ENHANCED)
#define ARCH_DMA_MINALIGN	128
#else/*--- #if !defined(CONFIG_AVM_ENHANCED) ---*/
#define ARCH_DMA_MINALIGN	64	/*--- lantiq: internal bus-system based on 64 byte ---*/
#endif/*--- #else ---*//*--- #if !defined(CONFIG_AVM_ENHANCED) ---*/
#endif

#endif /* __ASM_MACH_GENERIC_KMALLOC_H */
