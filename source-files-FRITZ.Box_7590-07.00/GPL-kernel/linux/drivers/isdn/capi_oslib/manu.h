/*--------------------------------------------------------------------------------------*\

	$Id: manu.h 1.1 2001/07/25 11:54:05Z MPommerenke Exp $

	$Log: manu.h $
	Revision 1.1  2001/07/25 11:54:05Z  MPommerenke
	Initial revision
	Revision 1.2  2001/02/07 16:55:23Z  MPommerenke
	Revision 1.1  2001/02/05 17:08:10Z  ugillieron
	Initial revision

\*-------------------------------------------------------------------------------------*/

#ifndef _MANU_H_
#define _MANU_H_

void CM_Manu_Task(void);
void CAPI_DebugPuts(char *buf, int Len);

#endif
