/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_AVM_WATCHDOG)

#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/platform_device.h>
#include <linux/reboot.h>
#include <linux/watchdog.h>
#include <asm/mach_avm.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <asm/cacheflush.h>
#include <linux/nmi.h>
#include <linux/version.h>
#include <linux/ar7wdt.h>

#if (LINUX_VERSION_CODE > KERNEL_VERSION(4,4,0))
#include <linux/qcom_scm.h>
#else
#include <soc/qcom/scm.h>
#endif

#ifdef CONFIG_AVM_FASTIRQ
#ifdef CONFIG_AVM_FASTIRQ_ARCH_ARM_COMMON
#include <asm/avm_enh/avm_tz.h>
#include <asm/avm_enh/avm_fiq.h>
#include <asm/avm_enh/avm_fiq_os.h>
#include <asm/avm_enh/avm_gic_fiq.h>
#include <asm/avm_enh/avm_fiq_groups.h>
#else
#include <mach/avm_tz.h>
#include <mach/avm_fiq.h>
#include <mach/avm_fiq_os.h>
#include <mach/avm_gic_smc.h>
#include <mach/avm_gic_fiq.h>
#endif
#endif
#include "avm_sammel.h"

#define SCM_CMD_SET_REGSAVE  0x2
static int in_panic;
struct qcom_wdt {
	struct clk		       *clk;
	unsigned long		    rate;
    unsigned long           timeout;
	struct notifier_block	restart_nb;
	void __iomem		   *base;
	void __iomem		   *wdt_reset;
	void __iomem		   *wdt_enable;
	void __iomem		   *wdt_bark_time;
	void __iomem		   *wdt_bite_time;
    unsigned int            act_wdt_cpu;
    atomic_t                wdt_busy;
};

static struct timer_list WDTimer[NR_CPUS];
static struct qcom_wdt *wdt;
static atomic_t wdt_active;

static RAW_NOTIFIER_HEAD(nmi_chain);
/**--------------------------------------------------------------------------------**\
 * der (pseudo-)nmi-handler ueber fastirq
\**--------------------------------------------------------------------------------**/
int register_nmi_notifier(struct notifier_block *nb) {
	return raw_notifier_chain_register(&nmi_chain, nb);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static long qcom_wdt_configure_bark_dump(void *arg __attribute__((unused)))
{
	long ret = -ENOMEM;
	struct {
		unsigned addr;
		int len;
	} cmd_buf;

	/* Area for context dump in secure mode */
	void *scm_regsave = (void *)__get_free_page(GFP_KERNEL);

	if (scm_regsave) {
		cmd_buf.addr = virt_to_phys(scm_regsave);
		cmd_buf.len  = PAGE_SIZE;

#if (LINUX_VERSION_CODE > KERNEL_VERSION(4,4,0))
		ret = qcom_scm_regsave(SCM_SVC_UTIL,SCM_CMD_SET_REGSAVE,
						scm_regsave, PAGE_SIZE);
#else
		ret = scm_call(SCM_SVC_UTIL, SCM_CMD_SET_REGSAVE,
						&cmd_buf, sizeof(cmd_buf), NULL, 0);
#endif

	}

	if (ret)
		pr_err("Setting register save address failed.\n"
				"Registers won't be dumped on a dog bite\n");
	return ret;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int qcom_wdt_start(struct qcom_wdt *wdt)
{

	writel(0, wdt->wdt_enable);
	writel(1, wdt->wdt_reset);
	writel(wdt->timeout * wdt->rate, wdt->wdt_bark_time);
	writel(wdt->timeout * 2 * wdt->rate, wdt->wdt_bite_time);
	writel(1, wdt->wdt_enable);
	return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#if 0
static int qcom_wdt_start_nonsecure(struct qcom_wdt *wdt)
{
	writel(0, wdt->wdt_enable);
	writel(1, wdt->wdt_reset);
	writel( wdt->rate * wdt->timeout, wdt->wdt_bite_time);
	writel(1, wdt->wdt_enable);
	return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void dump_wdt(void) {

    pr_err("{%s} enable 0x%x reset 0x%x bark_time 0x%x bite_time 0x%x", __func__, 
            readl(wdt->wdt_enable), readl(wdt->wdt_reset), readl(wdt->wdt_bark_time), readl(wdt->wdt_bite_time));
}
#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int qcom_wdt_restart(struct notifier_block *nb, unsigned long action __attribute__((unused)), void *data __attribute__((unused)))
{
	struct qcom_wdt *wdt = container_of(nb, struct qcom_wdt, restart_nb);
	u32 timeout;

	/*
	 * Trigger watchdog bite/bark:
	 *
	 * For regular reboot case: Trigger watchdog bite:
	 * Setup BITE_TIME to be lower than BARK_TIME, and enable WDT.
	 *
	 * For panic reboot case: Trigger WDT bark
	 * So that TZ can save CPU registers:
	 * Setup BARK_TIME to be lower than BITE_TIME, and enable WDT.
	 */
    pr_err("{%s} restart %s\n", __func__, in_panic ? "in_panic" : "");

	timeout = 128 * wdt->rate / 1000;

	writel(0, wdt->wdt_enable);
	writel(1, wdt->wdt_reset);
	if (in_panic) {
        avm_set_reset_status(RS_SOFTWATCHDOG);
		avm_stack_check(NULL);
		writel(timeout, wdt->wdt_bark_time);
		writel(2 * timeout, wdt->wdt_bite_time);
	} else {
        avm_set_reset_status(RS_REBOOT);
		avm_stack_check(NULL);
		writel(5 * timeout, wdt->wdt_bark_time);
		writel(timeout, wdt->wdt_bite_time);
	}
	writel(1, wdt->wdt_enable);

	/*
	 * Actually make sure the above sequence hits hardware before sleeping.
	 */
	wmb();

	mdelay(150);
	return NOTIFY_DONE;
}

/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
int ar7wdt_hw_is_wdt_running(void) {
    return atomic_read(&wdt_active);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_deinit(void) {
    /*--- pr_debug( "[qcom:watchdog] stop ...\n"); ---*/
	writel(0, wdt->wdt_enable);
    ar7wdt_hw_secure_wdt_disable();
    atomic_set(&wdt_active, 0);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_reboot(void) {
    panic("ar7wdt_hw_reboot: watchdog expired\n");
}
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static void wd_timer_function(unsigned long context __maybe_unused) {
    /*--- printk(KERN_INFO"%s\n", __func__); ---*/
    writel(1, wdt->wdt_reset);
    atomic_set(&wdt->wdt_busy, 0);
}
/**------------------------------------------------------------------------------------------*\
 * \brief: jede CPU per roundrobin triggern
\**------------------------------------------------------------------------------------------*/
void ar7wdt_hw_trigger(void) {
    unsigned int cpu, this_cpu, i;
    if(!ar7wdt_hw_is_wdt_running()) {
        return;
    }
    if(atomic_read(&wdt->wdt_busy)) {
        return;
    }
    this_cpu = get_cpu();
    /*--- printk(KERN_INFO"[%s] trigger cpu=%u\n", __func__, wdt->act_wdt_cpu); ---*/
    cpu = wdt->act_wdt_cpu;
#if defined(CONFIG_AVM_FASTIRQ)
    /* Auch Secure Config Watchdog triggern */
    if(avm_is_avm_tz())
        avm_secure_wdt_pet();
#endif

    for(i = 0; i < NR_CPUS; i++, cpu++) {
        if(cpu >= NR_CPUS) cpu = 0;
        if(!cpu_online(cpu)) {
            continue;
        }
        atomic_set(&wdt->wdt_busy, cpu + 1);
        if(cpu == this_cpu) {
            wd_timer_function(0);
            break;
        }
        WDTimer[cpu].expires = jiffies + 1;
        del_timer(&WDTimer[cpu]);
        add_timer_on(&WDTimer[cpu], cpu);
        break;
    }
    put_cpu();
    if(++cpu >= NR_CPUS) cpu = 0;
    wdt->act_wdt_cpu = cpu;
}
EXPORT_SYMBOL(ar7wdt_hw_trigger);
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static irqreturn_t wdt_bark_isr(int irq __attribute__((unused)), void *arg __attribute__((unused))) {
    struct pt_regs regs, *pregs;

#if defined(CONFIG_AVM_FASTIRQ)
    /* Secure Config Watchdog triggern, damit dieser nicht bei CR Erstellung zuschlägt */
    if(avm_is_avm_tz())
        avm_secure_wdt_pet();
    if(firq_is_linux_context()) {
        pregs = get_irq_regs();
    } else {
        avm_tick_jiffies_update(); /*--- auch im FASTIRQ-Fall jiffies korrigieren ---*/
        pregs = get_fiq_regs();
    }
    prepare_register_for_trap(&regs, &pregs);
#else
    pregs = get_irq_regs();
#endif
    avm_set_reset_status(RS_NMIWATCHDOG);
	avm_stack_check(NULL);
	raw_notifier_call_chain(&nmi_chain, 0, pregs);

    flush_cache_all();
	bust_spinlocks(1);
    die("HardwareWatchDog - NMI taken" , pregs, 0);
	return IRQ_HANDLED;
}

static irqreturn_t wdt_secure_bark_isr(int irq __attribute__((unused)), void *arg __attribute__((unused))) {
		/*--- struct pt_regs regs, *pregs; ---*/

		pr_err("Secure watchdog triggered. Waiting for secure watchdog bite for hw reset\n");

		return IRQ_HANDLED;
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void register_wdt_bark_irq(int irq, struct qcom_wdt *wdt) {
    uint32_t tz_version, modified;
	int ret;

#if defined(CONFIG_AVM_FASTIRQ)
    if( ! avm_get_tz_version(&tz_version, &modified)){
        ret  = avm_request_fiq_on((1 << NR_CPUS) - 1, irq, wdt_bark_isr, FIQ_CPUMASK, "watchdog", wdt);
        if( ! ret) {
            if (tz_version <= 10148) {
               // We need to alter the WDOG priority to the highest possible, as the TZ registers
               // all interrupts with it and there is a high chance that these interrupts prevent the wdog interrupt.
               // This is neccessary till we decide for a TZ update.
               avm_gic_fiq_setup(irq,  (1<< NR_CPUS) - 1, FIQ_PRIO_MONITOR, 0, 0);
               pr_err("[%s] watchdog as fastirq(%u) on all cpus registered with highest prio\n", __func__, irq);
               return;
            } else {
               // Set watchdog to its default priority
               avm_gic_fiq_setup(irq,  (1<< NR_CPUS) - 1, FIQ_PRIO_WATCHDOG, 0, 0);
               pr_err("[%s] watchdog as fastirq(%u) on all cpus registered with its default prio\n", __func__, irq);
               return;
            }
        }
    }
#endif
    ret = request_irq(irq, wdt_bark_isr, IRQF_TRIGGER_HIGH, "watchdog", wdt);
    if(!ret) {
        pr_err("[%s] watchdog as irq(%u) registered\n", __func__, irq);
        return;
    }
    pr_err("[%s] ERROR request_irq(irq(%d)) ret:%d\n", __func__, irq, ret);
}

void register_secure_wdt_bark_irq(int irq, struct qcom_wdt *wdt) {
		int ret = 0;

#if defined(CONFIG_AVM_FASTIRQ)
		ret = avm_request_fiq_on((1 << NR_CPUS) - 1, irq, wdt_secure_bark_isr, FIQ_CPUMASK, "secure-watchdog", wdt);
		if(!ret) {
				pr_err("[%s] secure watchdog as fastirq(%u) on all cpus registered\n", __func__, irq);
				return;
		}
#endif
}
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
void ar7wdt_hw_secure_wdt_disable(void) {
#if defined(CONFIG_AVM_FASTIRQ)
    /* Auch Secure Config Watchdog triggern */
    /* Disable Secure Watchdog */
    if(avm_is_avm_tz()) {
        avm_secure_wdt_config(0, 20000, 40000);
    }
#endif/*--- #if defined(CONFIG_AVM_FASTIRQ) ---*/
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_init(void) {

	struct device_node *np;
	int ret, cpu;
	uint32_t val;
	struct resource irqres;

    /* pr_err( "[qcom:watchdog] init ...\n"); */

    if ( ! wdt) {
        wdt = kzalloc(sizeof(*wdt), GFP_KERNEL);
        if ( ! wdt) {
            pr_err("[qcom:%s] ERROR no memory\n", __func__);
            return;
        }

        np = of_find_compatible_node(NULL, NULL, "qcom,kpss-wdt-ipq40xx");
        if ( ! np) {
            pr_err("[qcom:%s] ERROR dt-entry\n", __func__);
            goto err_out;
        }
        wdt->base = of_iomap(np, 0);
        if ( ! wdt->base) {
            pr_err("[qcom:%s] ERROR base\n", __func__);
            goto err_out;
        }

        if (of_property_read_u32(np, "wdt_res", &val)) {
            pr_err("[qcom:%s] ERROR wdt_res not set in dt\n", __func__);
            goto err_out;
        } else {
            wdt->wdt_reset = wdt->base + val;
        }

        if (of_property_read_u32(np, "wdt_en", &val)) {
            pr_err("[qcom:%s] ERROR wdt_en not set in dt\n", __func__);
            goto err_out;
        } else {
            wdt->wdt_enable = wdt->base + val;
        }

        if (of_property_read_u32(np, "wdt_bark_time", &val)) {
            pr_err("[qcom:%s] ERROR wdt_bark_time not set in dt\n", __func__);
            goto err_out;
        } else {
            wdt->wdt_bark_time = wdt->base + val;
        }

        if (of_property_read_u32(np, "wdt_bite_time", &val)) {
            pr_err("[qcom:%s] ERROR wdt_bite_time not set in dt\n", __func__);
            goto err_out;
        } else {
            wdt->wdt_bite_time = wdt->base + val;
        }

        wdt->clk = of_clk_get_by_name(np, NULL);
        if (IS_ERR(wdt->clk)) {
            pr_err("[qcom:%s] ERROR failed to get input clock\n", __func__);
            goto err_out;
        }

        ret = of_irq_to_resource_table(np, &irqres, 1);
        if (ret) {
		    register_wdt_bark_irq(irqres.start, wdt);
        } 

		if (of_property_read_u32(np, "wdt_secure_irq", &val)) {
				pr_info("[qcom:%s] ERROR wdt_secure_irq not set in dt\n", __func__);
		} else {
				register_secure_wdt_bark_irq(val, wdt);
		}


        ret = clk_prepare_enable(wdt->clk);
        if (ret) {
            pr_err("[qcom:%s] ERROR failed to setup clock\n", __func__);
            goto err_out;
        }

        /*
         * We use the clock rate to calculate the max timeout, so ensure it's
         * not zero to avoid a divide-by-zero exception.
         *
         * WATCHDOG_CORE assumes units of seconds, if the WDT is clocked such
         * that it would bite before a second elapses it's usefulness is
         * limited.  Bail if this is the case.
         */
        wdt->rate = clk_get_rate(wdt->clk);
        if (wdt->rate == 0 || wdt->rate > 0x10000000U) {
            pr_err("[qcom:%s] ERROR invalid clock rate\n", __func__);
            goto err_clk_unprepare;
        }

        /*
         * If 'timeout-sec' unspecified in devicetree, assume a 30 second
         * default, unless the max timeout is less than 30 seconds, then use
         * the max instead.
         */
        if (of_property_read_u32(np, "timeout-sec", &val))
            wdt->timeout = val;
        else 
            wdt->timeout = 10;

        /*
         * WDT restart notifier has priority 0 (use as a last resort)
         */
        wdt->restart_nb.notifier_call = qcom_wdt_restart;
        ret = register_restart_handler(&wdt->restart_nb);
        if (ret)
            pr_err("[qcom:%s] ERROR failed to setup restart handler\n", __func__);
    }

    /*--- enable watchdog ---*/
    work_on_cpu(0, qcom_wdt_configure_bark_dump, NULL);

    qcom_wdt_start(wdt);
    atomic_set(&wdt_active, 1);
#if defined(CONFIG_AVM_FASTIRQ)
    /* Reconfigure Secure Watchdog */
    if(avm_is_avm_tz())
        avm_secure_wdt_config(1, 20000, 40000);
#endif
    for(cpu = 0; cpu < NR_CPUS; cpu++) {
        init_timer(&WDTimer[cpu]);
        WDTimer[cpu].data     = (unsigned long)&WDTimer[cpu];
        WDTimer[cpu].function = wd_timer_function;
    }
	return;

err_clk_unprepare:
	clk_disable_unprepare(wdt->clk);
err_out:
    {
        struct qcom_wdt *tmp = wdt;
        wdt = NULL;
        kfree(tmp);
    }

	return;
}

#if 0
static const struct of_device_id qcom_wdt_of_table[] = {
	{ .compatible = "qcom,kpss-wdt-msm8960", },
	{ .compatible = "qcom,kpss-wdt-apq8064", },
	{ .compatible = "qcom,kpss-wdt-ipq8064", },
	{ .compatible = "qcom,kpss-wdt-ipq40xx", },
	{ },
};
MODULE_DEVICE_TABLE(of, qcom_wdt_of_table);
#endif

#endif
