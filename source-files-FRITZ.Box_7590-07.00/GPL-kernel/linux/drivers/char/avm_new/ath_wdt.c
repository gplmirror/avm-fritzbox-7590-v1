/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_AVM_WATCHDOG)
/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/fcntl.h>
#include <asm/ioctl.h>
#include <asm/errno.h>
#include <asm/uaccess.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,28)
#include <asm/mips-boards/prom.h>
#else
#include <asm/prom.h>
#endif
#include <linux/timer.h>
#include <linux/ar7wdt.h>
#include "avm_sammel.h"
#include <linux/hardirq.h>
#include <asm/mach_avm.h>
#include <asm/traps.h>

#ifdef CONFIG_MACH_AR7240
#include <ar7240.h>
#elif LINUX_VERSION_CODE < KERNEL_VERSION(4,4,46)
#include <atheros.h>
#else
#include <avm_atheros.h>
#endif

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
/*--- #define AVM_WATCHDOG_DEBUG ---*/

#if defined(AVM_WATCHDOG_DEBUG)
#define DBG_ERR(...)   printk(KERN_ERR __VA_ARGS__)
#define DBG_INFO(...)  printk(KERN_INFO __VA_ARGS__)
#else /*--- #if defined(AVM_WATCHDOG_DEBUG) ---*/
#define DBG_ERR(...)  
#define DBG_INFO(...)  
#endif /*--- #else ---*/ /*--- #if defined(AVM_WATCHDOG_DEBUG) ---*/

#if defined(CONFIG_MACH_AR7240)
extern uint32_t ar7240_ahb_freq;

static uint32_t get_ath_wdt_freq(void) {
    return ar7240_ahb_freq;
}
#elif LINUX_VERSION_CODE < KERNEL_VERSION(4,4,46)
extern uint32_t ath_ahb_freq;

static uint32_t get_ath_wdt_freq(void) {
    return ath_ahb_freq;
}
#else
static uint32_t get_ath_wdt_freq(void) {
    return ath79_get_clock(avm_clock_id_wdt);
}
#endif
static atomic_t wdt_active;

static int nmi_notify_first(struct notifier_block *self __maybe_unused, unsigned long dummy __maybe_unused, void *param);
static int nmi_notify_last(struct notifier_block *self, unsigned long dummy, void *param);
static atomic_t nmi_trigger_once;
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static struct notifier_block nmi_nb[2] = {
	{ .notifier_call = nmi_notify_first, .priority     = INT_MAX	},
	{ .notifier_call = nmi_notify_last,  .priority     = 0		},
};
#define TIME_OUT_SECS   10
/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
void ar7wdt_hw_init(void) {
	unsigned int i;
	DBG_ERR( "[ath:watchdog] start ...\n");
	atomic_set(&nmi_trigger_once, 0);
#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
	if(nmi_workaround_func.cb_ath_workaround_watchdog) {
	ath_workaround_watchdog(TIME_OUT_SECS);
	} else 
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/
	{
	ath_reg_wr(ATH_WATCHDOG_TMR, (TIME_OUT_SECS * get_ath_wdt_freq()));
#if defined(CONFIG_MACH_AR724x) || defined(CONFIG_ATH79_MACH_AVM_HW238)
	ath_reg_wr(ATH_WATCHDOG_TMR_CONTROL, ATH_WD_ACT_RESET); 
#else
	ath_reg_wr(ATH_WATCHDOG_TMR_CONTROL, ATH_WD_ACT_NMI);
#endif
	}
	DBG_ERR( "[ath:watchdog] action: %u (hardware reset) - ticks: %u (= %u seconds * %u hz)\n", 
		ath_reg_rd(ATH_WATCHDOG_TMR_CONTROL), 
		TIME_OUT_SECS * get_ath_wdt_freq(), 
		TIME_OUT_SECS, get_ath_wdt_freq());
	for(i = 0; i < ARRAY_SIZE(nmi_nb); i++) {
		register_nmi_notifier(&nmi_nb[i]);
	}
	atomic_set(&wdt_active, 1);
	return;
}
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
int ar7wdt_hw_is_wdt_running(void) {
    return atomic_read(&wdt_active);
}
/**--------------------------------------------------------------------------------**\
 * dummy
\**--------------------------------------------------------------------------------**/
void ar7wdt_hw_secure_wdt_disable() {
}
/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
void ar7wdt_hw_deinit(void) {
    DBG_ERR( "[ath:watchdog] stop ...\n");
#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
    if(nmi_workaround_func.cb_ath_workaround_watchdog) {
        ath_workaround_watchdog(0);
    } else 
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/
    {
        ath_reg_wr(ATH_WATCHDOG_TMR_CONTROL, ATH_WD_ACT_NONE);
    }
    atomic_set(&wdt_active, 0);
    return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_reboot(void) {
    DBG_ERR("ar7wdt_hw_reboot!!\n");
    panic("ar7wdt_hw_reboot: watchdog expired\n");
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_trigger(void) {

    if(!ar7wdt_hw_is_wdt_running()) {
        return;
    }
#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
    if(nmi_workaround_func.cb_ath_workaround_watchdog) {
        ath_workaround_watchdog(TIME_OUT_SECS);
    } else  
#endif/*--- #if defined(CONFIG_NMI_ARBITER_WORKAROUND) ---*/
    {
#ifdef AVM_WATCHDOG_DEBUG
        unsigned int ticks_left = ath_reg_rd(ATH_WATCHDOG_TMR);
        DBG_ERR( "[ath:watchdog] %x %x triggered %u.%u secs before reset\n", 
                ath_reg_rd(ATH_WATCHDOG_TMR_CONTROL), 
                ath_reg_rd(ATH_WATCHDOG_TMR), 
                ticks_left / get_ath_wdt_freq(), (((ticks_left%get_ath_wdt_freq())*10U) / get_ath_wdt_freq()) % 10);
#endif
        ath_reg_wr(ATH_WATCHDOG_TMR, (TIME_OUT_SECS * get_ath_wdt_freq()));
    }
}
EXPORT_SYMBOL(ar7wdt_hw_trigger);
extern void set_reboot_status_to_NMI(void);

/**
Atheros: Exception 0xbfc00380 wird auch auf nmi_exception_handler gelegt
	High-Prio!
 */
static int nmi_notify_first(struct notifier_block *self __maybe_unused, 
							unsigned long dummy __maybe_unused, void *param) {
    struct pt_regs *regs = (struct pt_regs *)param;
    u32 status;

	if(regs) regs->cp0_epc = read_c0_errorepc(); /*--- damit backtrace vernuenftig funktioniert ---*/
#if defined(CONFIG_NMI_ARBITER_WORKAROUND)
	ath_reg_wr(ATH_WATCHDOG_TMR_CONTROL, ATH_WD_ACT_NONE);
	wmb();
	memset(&nmi_workaround_func, 0, sizeof(struct _nmi_workaround_func));
#elif defined(CONFIG_MACH_ATHEROS) || defined(CONFIG_ATH79)
	ath_reg_wr(ATH_WATCHDOG_TMR, (10 /* s */ * get_ath_wdt_freq()));
	wmb();
	ath_reg_wr(ATH_WATCHDOG_TMR_CONTROL, ATH_WD_ACT_RESET);
	wmb();
    atomic_set(&wdt_active, 0);
#endif /*--- #if defined(CONFIG_MACH_ATHEROS) ---*/ 
	status = read_c0_status();
	status &= ~(1 << 0);  /* disable all interrupts */
	status &= ~(1 << 19); /* reset NMI status */
	status &= ~(1 << 22); /* bootstrap bit BEV zurücksetzen */
    /*--------------------------------------------------------------------------------*\
     * mbahr:
       Doku MIPS32 4KE Processor Cores Software User's Manual:
        Operation:
        // If StatusEXL is 1, all exceptions go through the general exception vector !!!
        // and neither EPC nor CauseBD nor SRSCtl are modified
            if StatusEXL = 1 then
                vectorOffset ← 16#180
            else
                if InstructionInBranchDelaySlot then
                EPC ← restartPC // PC of branch/jump
                CauseBD ← 1
            else
                EPC ← restartPC //PC of instruction
                CauseBD ← 0
            endif
            ....
            -> NMI setzt EXL!!!!!!
    \*--------------------------------------------------------------------------------*/
	status &= ~(1 << 1);  /* Superwichtig! EXL ruecksetzen - somit funktionieren nachfolgend auch TLB-Exceptions (Zugriff auf virtuellen Speicher)*/
	write_c0_status(status);
	set_reboot_status_to_NMI();
	return NOTIFY_OK;
}
/**
	Lowest-Prio -> trigger die !
 */
static int nmi_notify_last(struct notifier_block *self __maybe_unused, unsigned long dummy __maybe_unused,
						   void *param) {
	struct pt_regs *regs = (struct pt_regs *)param;
	char str[100];

	bust_spinlocks(1);
	console_verbose();
	printk_avm_console_bend(0); /* force serial-output */

	snprintf(str, sizeof(str), "CPU%d NMI taken (err)epc=%pF", smp_processor_id(), regs ? (void *)regs->cp0_epc : 
																			      (void *)read_c0_errorepc());
	die(str, regs);
	return NOTIFY_STOP;
}
#endif /*--- #if defined(CONFIG_AVM_WATCHDOG) ---*/


