#ifndef _ifxmips_wdt_h_
#define _ifxmips_wdt_h_

#define WDT_TIMEOUT_SEC 25

#define IFX_WDT_PW1 0x000000BE /**< First password for access */
#define IFX_WDT_PW2 0x000000DC /**< Second password for access */


#endif /*--- #ifndef _ifxmips_wdt_h_ ---*/
