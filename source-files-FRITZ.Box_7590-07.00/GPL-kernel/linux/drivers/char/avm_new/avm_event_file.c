/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <asm/fcntl.h>
#include <asm/ioctl.h>
/*--- #include <linux/devfs_fs_kernel.h> ---*/
#include <linux/fs.h>
#include <linux/semaphore.h>
#include <asm/errno.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/device.h>

#include <linux/avm_event.h>
#include "avm_sammel.h"
#include "avm_event.h"
#include "avm_event_intern.h"
#include "avm_event_remote.h"
#include "avm_dist_event/endian.h"
#include "avm_dist_event/avm_event_gen_types.h"

/*------------------------------------------------------------------------------------------*\
 *  2.6 Kernel H-Files
\*------------------------------------------------------------------------------------------*/
#if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE 
#include <linux/cdev.h>
#include <asm/mach_avm.h>
#endif 

#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 19)
#define AVM_EVENT_UDEV
#endif/*--- #if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 19) ---*/

/*------------------------------------------------------------------------------------------*\
 *  2.4 Kernel H-Files
\*------------------------------------------------------------------------------------------*/
#if KERNEL_VERSION(2, 6, 0) > LINUX_VERSION_CODE 
#include <asm/smplock.h>
#endif 

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int avm_event_open(struct inode *, struct file *);
static int avm_event_close(struct inode *, struct file *);
static int avm_event_fasync(int, struct file *, int);
static ssize_t avm_event_write(struct file *, const char *, size_t , loff_t *);
static ssize_t avm_event_read(struct file *, char *, size_t , loff_t *);
void avm_event_cleanup(void);
static unsigned int avm_event_poll(struct file *filp, poll_table *wait);

/*--- #define DEBUG_AVM_EVENT_FILE ---*/

#define DBG_ERR(args...)   printk(KERN_INFO "[avm_event_file]" args)
#if defined(DEBUG_AVM_EVENT_FILE)
#define DBG_TRC(args...)   printk(KERN_INFO "[avm_event_file]" args)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void dump_data(const char *prefix, const unsigned char *data, unsigned int len) {
    printk(KERN_ERR"%s: data len=%d:", prefix, len);
    while(len--) {
        printk(KERN_CONT"%02x,",*data++);
    }
    printk(KERN_ERR"\n");
}
#else/*--- #if defined(DEBUG_AVM_EVENT_FILE) ---*/
#define DBG_TRC(args...) 
#define dump_data(a, b, c)
#endif
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_event avm_event;
struct semaphore avm_event_sema;
#if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE 
DEFINE_SPINLOCK(avm_event_lock);
#endif /*--- #if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE ---*/ 

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct file_operations avm_event_fops = {
    owner:   THIS_MODULE,
    open:    avm_event_open,
    release: avm_event_close,
    read:    avm_event_read,
    write:   avm_event_write,
    /*--- ioctl:   avm_event_ioctl, ---*/
    fasync:  avm_event_fasync,
    poll: avm_event_poll,
};

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int __init avm_event_init(void) {

#if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE 
    int reason;

    DBG_TRC("%s: register_chrdev_region()\n", __func__);
#if (defined(AVM_EVENT_UDEV))
    reason = alloc_chrdev_region(&avm_event.device, 0, 1, "avm_event");
#else /*--- #if defined(AVM_EVENT_UDEV) ---*/
    avm_event.device = MKDEV(AVM_EVENT_MAJOR, 0);
    reason = register_chrdev_region(avm_event.device, 1, "avm_event");
#endif
    if(reason) {
        DBG_ERR("%s: register_chrdev_region failed: reason %d!\n", __func__, reason);
        return -ERESTARTSYS;
    }

	avm_event.cdev = cdev_alloc();
	if (!avm_event.cdev) {
        unregister_chrdev_region(avm_event.device, 1);
        DBG_ERR("%s: cdev_alloc failed!\n", __func__);
        return -ERESTARTSYS;
    }

	avm_event.cdev->owner = avm_event_fops.owner;
	avm_event.cdev->ops = &avm_event_fops;
	kobject_set_name(&(avm_event.cdev->kobj), "avm_event");
    spin_lock_init(&avm_event_lock);
    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
#else /*--- #if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE ---*/ 
    DBG_TRC("%s: register_chrdev()\n", __func__);
    avm_event.major = register_chrdev(0 /*--- dynamic major ---*/, "avm_event", &avm_event_fops);
    if(avm_event.major < 1) {
        DBG_ERR("%s: register_chrdrv failed: reason %d\n", __func__, avm_event.major);
        return -ERESTARTSYS;
    }
#endif /*--- #else ---*/ /*--- #if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE ---*/ 
    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/

    sema_init(&avm_event_sema, 1);

    avm_event_init2(512 /* max_items */, 512 /* max_datas */);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 0) 
	if(cdev_add(avm_event.cdev, avm_event.device, 1)) {
        kobject_put(&avm_event.cdev->kobj);
        unregister_chrdev_region(avm_event.device, 1);
        DBG_ERR("%s: cdev_add failed!\n", __func__);
        return -ERESTARTSYS;
    }
#if defined(AVM_EVENT_UDEV)
    /*--- Geraetedatei anlegen: ---*/
    avm_event.osclass = class_create(THIS_MODULE, "avm_event");
    device_create(avm_event.osclass, NULL, 1, NULL, "%s%d", "avm_event", 0);
#endif/*--- #if defined(AVM_EVENT_UDEV) ---*/
#else /*--- #if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 0) ---*/ 
    avm_event.minor   = 0;
	avm_event.devfs_handle = devfs_register(NULL, "avm_event", DEVFS_FL_DEFAULT, avm_event.major, avm_event.minor,
                                	   S_IFCHR | S_IRUGO | S_IWUSR, &avm_event_fops, NULL);

    DBG_TRC("%s: major %d (success)\n", __func__, avm_event.major);
#endif /*--- #else ---*/ /*--- #if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE ---*/ 
#if (LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 0)) && defined(CONFIG_AVM_PUSH_BUTTON)
    avm_event_push_button_init();
#endif /*--- #if (LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 0)) && defined(CONFIG_AVM_PUSH_BUTTON) ---*/

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 32)
    avm_event_proc_init();
#endif/*--- #if LINUX_VERSION_CODE == KERNEL_VERSION(2, 6, 32) ---*/
    return 0;
}

/*--- module_init(avm_event_init); ---*/

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
#if defined(AVM_EVENT_MODULE)
void avm_event_cleanup(void) {
    DBG_TRC("%s: unregister_chrdev(%u)\n", __func__, avm_event.major);
#if defined(CONFIG_AVM_PUSH_BUTTON)
    avm_event_push_button_deinit();
#endif /*--- #if defined(CONFIG_AVM_PUSH_BUTTON) ---*/
    avm_event_deinit2();
    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
#if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE 
#if defined(AVM_EVENT_UDEV)
    device_destroy(avm_event.osclass, 1);
    class_destroy(avm_event.osclass);
#endif/*--- #if defined(AVM_EVENT_UDEV) ---*/
    cdev_del(avm_event.cdev); /* Delete char device */
    avm_event_deinit2();
    unregister_chrdev_region(avm_event.device, 1);
    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
#else /*--- #if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE ---*/ 
    if(avm_event.devfs_handle)
	    devfs_unregister(avm_event.devfs_handle);
	devfs_unregister_chrdev(avm_event.major, "avm_event");
#endif /*--- #else ---*/ /*--- #if KERNEL_VERSION(2, 6, 0) < LINUX_VERSION_CODE ---*/ 
    return;
}

/*--- module_exit(avm_event_cleanup); ---*/
#endif /*--- #if defined(AVM_EVENT_MODULE) ---*/

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int avm_event_fasync(int fd, struct file *filp, int mode) {
    struct _avm_event_open_data *open_data = (struct _avm_event_open_data *)filp->private_data;
    DBG_TRC("%s: avm_event_fasync:\n", __func__);
    return fasync_helper(fd, filp, mode, &(open_data->fasync));
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static unsigned int avm_event_poll(struct file *filp, poll_table *wait) {
    struct _avm_event_open_data *open_data = (struct _avm_event_open_data *)filp->private_data;

	poll_wait (filp, &(open_data->wait_queue), wait);

    if(open_data->item) {
        DBG_TRC("%s: POLLIN (%s)\n", __func__, current->comm);
	    return POLLIN | POLLRDNORM;
    }
    DBG_TRC("%s: (%s)\n", __func__, current->comm);

    return 0;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int avm_event_open(struct inode *inode __attribute__((unused)), struct file *filp) {
    struct _avm_event_open_data *open_data;

    DBG_TRC("%s:\n", __func__);
    /*-------------------------------------------------------------------------------------------*\
    \*-------------------------------------------------------------------------------------------*/
    if(filp->f_flags & O_APPEND) {
        DBG_ERR("%s: open O_APPEND not supported\n", __func__);
        return -EFAULT;
    }

    /*-------------------------------------------------------------------------------------------*\
    \*-------------------------------------------------------------------------------------------*/
    if(down_interruptible(&avm_event_sema)) {
        DBG_ERR("%s: down_interruptible() failed\n", __func__);
        return -ERESTARTSYS;
    }

    /*-------------------------------------------------------------------------------------------*\
    \*-------------------------------------------------------------------------------------------*/
    open_data = (struct _avm_event_open_data *)kmalloc(sizeof(struct _avm_event_open_data), GFP_KERNEL);
    if(!open_data) {
        DBG_ERR("avm_event_open: open malloc failed\n");
        up(&avm_event_sema);
        return -EFAULT;
    }
    memset(open_data, 0, sizeof(*open_data));

    init_waitqueue_head (&(open_data->wait_queue));
    open_data->pf_owner = &(filp->f_owner);
    filp->private_data = (void *)open_data;

#if KERNEL_VERSION(2, 6, 0) > LINUX_VERSION_CODE 
    MOD_INC_USE_COUNT;
#endif
    up(&avm_event_sema);
    DBG_TRC("%s: open success flags=0x%x\n", __func__, filp->f_flags);
    return 0;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int avm_event_close(struct inode *inode __attribute__((unused)), struct file *filp) {

    DBG_TRC("%s:\n", __func__);

    if(down_interruptible(&avm_event_sema)) {
        DBG_ERR("%s down_interruptible() failed\n", __func__);
        return -ERESTARTSYS;
    }
    /*--- achtung auf ind wartende "gefreien" und warten bis alle fertig ---*/

    if(filp->private_data) {
        struct _avm_event_open_data *open_data = (struct _avm_event_open_data *)filp->private_data;
        struct _avm_event_cmd_param_release avm_event_cmd_param_release;
        if(open_data->registered) {
            if(open_data->event_source_handle)
                avm_event_source_release(open_data->event_source_handle);
            open_data->event_source_handle = NULL;

            strcpy(avm_event_cmd_param_release.Name, open_data->Name);
            (void)avm_event_release(open_data, &avm_event_cmd_param_release);
        }
        avm_event_fasync(-1, filp, 0);  /*--- remove this file from asynchonously notified filp ---*/
        kfree(filp->private_data);
        filp->private_data = NULL;
    }

#if KERNEL_VERSION(2, 6, 0) > LINUX_VERSION_CODE 
    MOD_DEC_USE_COUNT;
#endif
    up(&avm_event_sema);
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void avm_event_source_user_mode_notify(void *Context, enum _avm_event_id id) {
    struct _avm_event_open_data *O = (struct _avm_event_open_data *)Context;
    struct _avm_event_data *D;

    DBG_TRC("[%s]: avm_event_source_user_mode_notify:\n", __func__);

    if(check_id_mask_with_id(&O->event_mask_registered, avm_event_id_user_source_notify)) {
        struct _avm_event_user_mode_source_notify *N;
        N = (struct _avm_event_user_mode_source_notify *)kmalloc(sizeof(struct _avm_event_user_mode_source_notify), GFP_KERNEL);
        if(N == NULL) {
            DBG_ERR("[%s]: out of memory\n", __func__);
            return;
        }

        D = (struct _avm_event_data *)avm_event_alloc_data();
        if(D == NULL) {
            kfree(N);
            DBG_ERR("[%s]: out of memory\n", __func__);
            return;
        }
        N->header.id   = avm_event_id_user_source_notify;
        N->id          = id;
        atomic_set(&D->link_count, 0); 
        D->data        = N;
        D->data_length = sizeof(struct _avm_event_user_mode_source_notify);
        avm_event_source_trigger_one(O, D);
    }
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_event_write(struct file *filp, const char *write_buffer, size_t write_length, loff_t *write_pos __attribute__((unused))) {
    unsigned int status;
    unsigned char *data;
    unsigned int data_length;
    struct _avm_event_cmd Buffer;
    struct _avm_event_open_data *open_data = (struct _avm_event_open_data *)filp->private_data;

    DBG_TRC("%s: write_length = %u *write_pos = 0x%LX\n", __func__, write_length, *write_pos);

    if(write_length < sizeof(Buffer)) {
        DBG_ERR("%s: avm_event_write: write_lengh < %u\n", __func__, sizeof(Buffer));
        return -EINVAL;
    }
    if(copy_from_user(&Buffer, write_buffer, sizeof(Buffer))) {
        DBG_ERR("%s: avm_event_write: copy_from_user failed\n", __func__);
        return -EFAULT;
    }
	dump_data(__func__, (const char *)&Buffer, sizeof(Buffer));
    if((Buffer.cmd != avm_event_cmd_register) && (open_data->registered == 0)) {
        DBG_ERR("%s: avm_event_write: not registered\n", __func__);
        return -EFAULT;
    }
    if(down_interruptible(&avm_event_sema)) {
        DBG_ERR("%s down_interruptible() failed\n", __func__);
        return -ERESTARTSYS;
    }
    switch(Buffer.cmd) {
        case avm_event_cmd_register:
            DBG_TRC("%s: avm_event_cmd_register\n", __func__);
            status = avm_event_register(open_data, &Buffer.param.avm_event_cmd_param_register);
            if(status == 0)
                open_data->registered = 1;
            break;

        case avm_event_cmd_release:
            DBG_TRC("%s: avm_event_cmd_release\n", __func__);
            status = avm_event_release(open_data, &Buffer.param.avm_event_cmd_param_release);
            open_data->registered = 0;
            break;

        case avm_event_cmd_trigger:
            DBG_TRC("%s: avm_event_cmd_release\n", __func__);
            status = avm_event_trigger(open_data, &Buffer.param.avm_event_cmd_param_trigger);
            break;

        case avm_event_cmd_source_register:
            DBG_TRC("%s: avm_event_cmd_source_register(name='%s' mask[0]=%llx\n", __func__, Buffer.param.avm_event_cmd_param_source_register.Name, 
																   					     Buffer.param.avm_event_cmd_param_source_register.mask.mask[0]);
            if(open_data->event_source_handle) {
                status = -EACCES;
                break;
            }
            open_data->event_source_handle = avm_event_source_register( Buffer.param.avm_event_cmd_param_source_register.Name, 
																		&Buffer.param.avm_event_cmd_param_source_register.mask, 
																		avm_event_source_user_mode_notify, open_data);
            if(open_data->event_source_handle) {
                status = 0;
            } else {
                status = -EACCES;
            }
            break;

        case avm_event_cmd_source_release:
            DBG_TRC("%s: avm_event_cmd_source_release\n", __func__);
            status = 0;
            if(open_data->event_source_handle == NULL) {
                break;
            }
            avm_event_source_release(open_data->event_source_handle);
            open_data->event_source_handle = NULL;
            break;

        case avm_event_cmd_source_trigger:
            DBG_TRC("%s: avm_event_cmd_source_trigger id=%u data_length=%u\n", __func__, Buffer.param.avm_event_cmd_param_source_trigger.id,
																						   Buffer.param.avm_event_cmd_param_source_trigger.data_length);
            if(open_data->event_source_handle == NULL) {
                status = -EACCES;
                break;
            }
            data_length = Buffer.param.avm_event_cmd_param_source_trigger.data_length;
            data = kmalloc(data_length, GFP_KERNEL);
            if(data == NULL) {
                status = -ENOMEM;
                break;
            }
            if(copy_from_user(data, write_buffer + sizeof(struct _avm_event_cmd), data_length)) {
				kfree(data);
                DBG_ERR("%s: copy_from_user failed\n", __func__);
                return -EFAULT;
            }
            avm_event_source_trigger(open_data->event_source_handle, Buffer.param.avm_event_cmd_param_source_trigger.id, data_length, data);
            status = 0;
            break;

        default:
        case avm_event_cmd_undef:
            DBG_ERR("%s: avm_event_cmd_undef\n", __func__);
            status = -EINVAL;
    }
    up(&avm_event_sema);
    if(status)
        return status;
    return write_length;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static ssize_t avm_event_read(struct file *filp, char *read_buffer, size_t max_read_length, loff_t *read_pos) {
    unsigned int event_pos;
    unsigned int copy_length = 0;
    unsigned int rx_buffer_length = 0;
    unsigned char *rx_buffer;
    unsigned int commit = 0;
    struct _avm_event_open_data *open_data = (struct _avm_event_open_data *)filp->private_data;

    DBG_TRC("%s\n", __func__);

avm_event_read_retry:
    if(down_interruptible(&avm_event_sema)) {
        DBG_ERR("%s: down_interruptible() failed\n", __func__);
        return -ERESTARTSYS;
    }
    avm_event_get(open_data, &rx_buffer, &rx_buffer_length, &event_pos);
    /*--------------------------------------------------------------------------------------*\
     * sind ueberhaupt Daten vorhanden
    \*--------------------------------------------------------------------------------------*/
    if(rx_buffer_length) {
        DBG_TRC("%s: '%s' rx_buffer_length = %u *read_pos = %Lu (%s)\n", __func__, open_data->Name, rx_buffer_length, *read_pos, current->comm);
        copy_length = rx_buffer_length - *read_pos;
        if(copy_length <= max_read_length) {
            commit = 1;
        } else {
            copy_length = max_read_length;
        }
    /*--------------------------------------------------------------------------------------*\
     * sind wir blockierend, nein
    \*--------------------------------------------------------------------------------------*/
    } else if(filp->f_flags & O_NONBLOCK) {
        up(&avm_event_sema);
        DBG_TRC("%s: non block, empty\n", __func__);
        return -EAGAIN;
    /*--------------------------------------------------------------------------------------*\
     * sind wir blockierend, ja
    \*--------------------------------------------------------------------------------------*/
    } else {
        up(&avm_event_sema);
        DBG_TRC("%s: sleep on\n", __func__);
        if(wait_event_interruptible(open_data->wait_queue, open_data->item)) {
            DBG_TRC("%s: handle released\n", __func__);
            return -ERESTARTSYS;
        }
        DBG_TRC("%s: wake up\n", __func__);
        goto avm_event_read_retry;
    }

    if(copy_to_user(read_buffer, rx_buffer + *read_pos, copy_length)) {
        up(&avm_event_sema);
        DBG_ERR("%s: copy_to_user failed\n", __func__);
        return -EFAULT;
    }
    *read_pos += (loff_t)copy_length;
    if(commit) {
        *read_pos = (loff_t)0;
        avm_event_commit(open_data, event_pos);
    }
    up(&avm_event_sema);
    return copy_length;
}


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/

#if KERNEL_VERSION(2, 6, 0) > LINUX_VERSION_CODE
EXPORT_NO_SYMBOLS;
#endif /*--- #if KERNEL_VERSION(2, 6, 0) > LINUX_VERSION_CODE ---*/

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
