/*
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 *
 *  Copyright © 2012 John Crispin <blogic@openwrt.org>
 *  Copyright (c) 2013 Mohammad Firdaus B Alias Thani  <m.aliasthani@lantiq.com>
 *	Bug-fix to allow write to flash possible
 *	including minor hardware behaviour adaptation fixes
 */

#include <linux/mtd/nand.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <lantiq_soc.h>
#include <linux/mtd/nand_on_chip_ecc.h>
#include <linux/slab.h>

#if defined(CONFIG_TFFS_DEV_MTDNAND)
#include <linux/tffs.h>
#include <linux/mtd/partitions.h>

extern int nand_calculate_ecc(struct mtd_info *mtd, const unsigned char *buf, unsigned char *code);
extern int nand_correct_data(struct mtd_info *mtd, unsigned char *buf, unsigned char *read_ecc, unsigned char *calc_ecc);


static struct mtd_info *panic_reinit(struct mtd_info *mtd __maybe_unused);

static struct nand_chip *panic_chip = NULL;
static struct mtd_info *panic_mtd = NULL;

#define chip_lock(lock, flags)              \
    do{                                     \
        flags = 0;                          \
        if(likely(panic_mode == 0)){        \
            spin_lock_irqsave(lock, flags); \
        }                                   \
    }while(0)

#define chip_unlock(lock, flags)                 \
    do{                                          \
        if(likely(panic_mode == 0)){             \
            spin_unlock_irqrestore(lock, flags); \
        }                                        \
    }while(0)

#else

#define chip_lock(lock, flags)              \
    spin_lock_irqsave(lock, flags)

#define chip_unlock(lock, flags)            \
    spin_unlock_irqrestore(lock, flags)

#endif /* CONFIG_TFFS_DEV_MTDNAND */

static unsigned int panic_mode = 0;


/* nand registers */
#define EBU_ADDSEL0		0x20
#define EBU_ADDSEL1		0x24
#define EBU_NAND_CON		0xB0
#define EBU_NAND_WAIT		0xB4
#define EBU_NAND_ECC0		0xB8
#define EBU_NAND_ECC_AC		0xBC

/* nand commands */
#define NAND_CMD_ALE		(1 << 2)
#define NAND_CMD_CLE		(1 << 3)
#define NAND_CMD_CS		(1 << 4)
#define NAND_WRITE_CMD_RESET	0xff
#define NAND_WRITE_CMD		(NAND_CMD_CS | NAND_CMD_CLE)
#define NAND_WRITE_ADDR		(NAND_CMD_CS | NAND_CMD_ALE)
#define NAND_WRITE_DATA		(NAND_CMD_CS)
#define NAND_READ_DATA		(NAND_CMD_CS)
#define NAND_WAIT_WR_C		(1 << 3)
#define NAND_WAIT_RD		(0x1)

/* we need to tel the ebu which addr we mapped the nand to */
#define ADDSEL1_MASK(x)		(x << 4)
#define ADDSEL0_REGEN		1
#define ADDSEL1_REGEN		1

/* we need to tell the EBU that we have nand attached and set it up properly */
#define BUSCON1_SETUP		(1 << 22)
#define BUSCON1_BCGEN_RES	(0x3 << 12)
#define BUSCON1_WAITWRC2	(2 << 8)
#define BUSCON1_WAITRDC2	(2 << 6)
#define BUSCON1_HOLDC1		(1 << 4)
#define BUSCON1_RECOVC1		(1 << 2)
#define BUSCON1_CMULT4		1

#define BUSCON0_SETUP		(1 << 22)
#define BUSCON0_ALEC		(2 << 14)
#define BUSCON0_BCGEN_RES	(0x3 << 12)
#define BUSCON0_WAITWRC2	(7 << 8)
#define BUSCON0_WAITRDC2	(3 << 6)
#define BUSCON0_HOLDC1		(3 << 4)
#define BUSCON0_RECOVC1		(3 << 2)
#define BUSCON0_CMULT4		2

#define NAND_CON_CE		(1 << 20)
#define NAND_CON_OUT_CS1	(1 << 10)
#define NAND_CON_IN_CS1		(1 << 8)
#define NAND_CON_PRE_P		(1 << 7)
#define NAND_CON_WP_P		(1 << 6)
#define NAND_CON_SE_P		(1 << 5)
#define NAND_CON_CS_P		(1 << 4)
#define NAND_CON_CSMUX		(1 << 1)
#define NAND_CON_NANDM		1

#define NAND_ALE_SET		ltq_ebu_w32(ltq_ebu_r32(EBU_NAND_CON) | \
        (1 << 18), EBU_NAND_CON);
#define NAND_ALE_CLEAR		ltq_ebu_w32(ltq_ebu_r32(EBU_NAND_CON) & \
        ~(1 << 18), EBU_NAND_CON);

#ifndef CONFIG_EVA
#define NANDPHYSADDR(x)    CPHYSADDR(x)
#else
#define NANDPHYSADDR(x) RPHYSADDR(x)
#endif /* CONFIG_EVA */

#ifdef CONFIG_LTQ_BCH_4BITS
#define ECC_BYTES	7 
#else
#define ECC_BYTES	13
#endif /* CONFIG_LTQ_BCH_4BITS */

static u32 xway_latchcmd;

static void xway_reset_chip(struct nand_chip *chip)
{
    unsigned long nandaddr = (unsigned long) chip->IO_ADDR_W;
    unsigned long timeout;
    unsigned long flags;

    nandaddr &= ~NAND_WRITE_ADDR;
    nandaddr |= NAND_WRITE_CMD;

    /* finish with a reset */
    timeout = jiffies + msecs_to_jiffies(20);

    chip_lock(&ebu_lock, flags);
    writeb(NAND_WRITE_CMD_RESET, (void __iomem *) nandaddr);
    chip_unlock(&ebu_lock, flags);
    do {
        if ((ltq_ebu_r32(EBU_NAND_WAIT) & NAND_WAIT_WR_C) == 0){
            break;
        }

        if(panic_mode == 0){
            cond_resched();
        }
    } while (!time_after_eq(jiffies, timeout));
}

static void xway_select_chip(struct mtd_info *mtd, int chip)
{

    switch (chip) {
        case -1:
            ltq_ebu_w32_mask(NAND_CON_CE, 0, EBU_NAND_CON);
            ltq_ebu_w32_mask(NAND_CON_NANDM, 0, EBU_NAND_CON);
            break;
        case 0:
            ltq_ebu_w32_mask(0, NAND_CON_NANDM, EBU_NAND_CON);
            ltq_ebu_w32_mask(0, NAND_CON_CE, EBU_NAND_CON);
            break;
        default:
            BUG();
    }
}

static void xway_cmd_ctrl(struct mtd_info *mtd, int cmd, unsigned int ctrl)
{
    struct nand_chip *this = mtd->priv;
    unsigned long nandaddr = (unsigned long) this->IO_ADDR_W;
    unsigned long flags;

    if (ctrl & NAND_CTRL_CHANGE) {
        if (ctrl & NAND_CLE) {
            NAND_ALE_CLEAR;
            xway_latchcmd = NAND_WRITE_CMD;
        } else if (ctrl & NAND_ALE) {
            NAND_ALE_SET;
            xway_latchcmd = NAND_WRITE_ADDR;
        } else {
            if (xway_latchcmd == NAND_WRITE_ADDR) {
                NAND_ALE_CLEAR;
                xway_latchcmd = NAND_WRITE_DATA;
            }
        }
    }

    if (cmd != NAND_CMD_NONE) {
        chip_lock(&ebu_lock, flags);
        writeb(cmd, (void __iomem *) (nandaddr | xway_latchcmd));
        while ((ltq_ebu_r32(EBU_NAND_WAIT) & NAND_WAIT_WR_C) == 0)
            ;
        chip_unlock(&ebu_lock, flags);
    }
}

static int xway_dev_ready(struct mtd_info *mtd)
{
    return ltq_ebu_r32(EBU_NAND_WAIT) & NAND_WAIT_RD;
}

static unsigned char xway_read_byte(struct mtd_info *mtd)
{
    struct nand_chip *this = mtd->priv;
    unsigned long nandaddr = (unsigned long) this->IO_ADDR_R;
    unsigned long flags;
    int ret;

    chip_lock(&ebu_lock, flags);
    ret = ltq_r8((void __iomem *)(nandaddr | NAND_READ_DATA));
    while ((ltq_ebu_r32(EBU_NAND_WAIT) & NAND_WAIT_WR_C) == 0)
        ;
    chip_unlock(&ebu_lock, flags);

    return ret;
}

static void xway_read_buf(struct mtd_info *mtd, u_char *buf, int len)
{
    struct nand_chip *this = mtd->priv;
    unsigned long nandaddr = (unsigned long) this->IO_ADDR_R;
    unsigned long flags;
    int i;

    chip_lock(&ebu_lock, flags);
    for (i = 0; i < len; i++) {
        buf[i] = ltq_r8((void __iomem *)(nandaddr | NAND_READ_DATA));
        while ((ltq_ebu_r32(EBU_NAND_WAIT) & NAND_WAIT_WR_C) == 0)
            ;
    }
    chip_unlock(&ebu_lock, flags);
}

static void xway_write_buf(struct mtd_info *mtd, const u_char *buf, int len)
{
    struct nand_chip *this = mtd->priv;
    unsigned long nandaddr = (unsigned long) this->IO_ADDR_W;
    unsigned long flags;
    int i;

    chip_lock(&ebu_lock, flags);
    for (i = 0; i < len; i++) {
        ltq_w8(buf[i], (void __iomem *) (nandaddr | NAND_WRITE_DATA));
        while ((ltq_ebu_r32(EBU_NAND_WAIT) & NAND_WAIT_WR_C) == 0)
            ;
    }
    chip_unlock(&ebu_lock, flags);
}

extern int nand_write_page_raw(struct mtd_info *mtd, struct nand_chip *chip,
        const uint8_t *buf, int oob_required);
extern int nand_read_page_raw(struct mtd_info *mtd, struct nand_chip *chip,
        uint8_t *buf, int oob_required, int page);
extern void nand_command(struct mtd_info *mtd, unsigned int command,
        int column, int page_addr);
extern int nand_write_oob_std(struct mtd_info *mtd, struct nand_chip *chip,
        int page);
extern int nand_read_oob_std(struct mtd_info *mtd, struct nand_chip *chip,
        int page);
extern int nand_default_bbt(struct mtd_info *mtd);

#if ! defined(CONFIG_PRODTEST)
enum copy_mode {
    slow,
    fast
};

#define CONVERT_MAGIC 0x41564D43
#define CONVERT_DONE 1
#define CONVERT_ONGOING 2
#define CONVERT_RECOVER 3
struct _magic_convert {
    uint32_t magic; // 4 Byte
    uint8_t status; // 1 Byte
    loff_t page_number; // 64bit 8 Byte
};


int avm_write_page(struct mtd_info *mtd, loff_t to, int data_len, unsigned char *data) {
    int retlen = 0;

    if (mtd->_write(mtd, to, data_len, &retlen, data)) {
        pr_err("{%s} ERROR write data of Block %llx\n", __func__, to);
        return -1;
    }

    if (data_len != retlen) {
        pr_err("{%s} Input & written len differ [%d/%d]\n", __func__, data_len, retlen);
        return -1;
    }

    return 0;
}

/*--------------------------------------------------------------------------------*\
  \*--------------------------------------------------------------------------------*/
static void avm_fix_ecc_cmd_copyback(struct mtd_info *mtd, unsigned int command, unsigned int command1, int page_addr) {

    register struct nand_chip *chip = mtd->priv;
    unsigned int column = 0;

    /* Command latch cycle */
    chip->cmd_ctrl(mtd, command, NAND_NCE | NAND_CLE | NAND_CTRL_CHANGE);

    if (page_addr != -1) {
        int ctrl = NAND_CTRL_CHANGE | NAND_NCE | NAND_ALE;

        /* Serially input address */
        chip->cmd_ctrl(mtd, column, ctrl);
        ctrl &= ~NAND_CTRL_CHANGE;
        chip->cmd_ctrl(mtd, column, ctrl);
        chip->cmd_ctrl(mtd, page_addr, ctrl);
        chip->cmd_ctrl(mtd, page_addr >> 8, NAND_NCE | NAND_ALE);
        chip->cmd_ctrl(mtd, page_addr >> 16, NAND_NCE | NAND_ALE);
    }
    chip->cmd_ctrl(mtd, NAND_CMD_NONE, NAND_NCE | NAND_CTRL_CHANGE);
    chip->cmd_ctrl(mtd, command1, NAND_NCE | NAND_CLE | NAND_CTRL_CHANGE);
    chip->cmd_ctrl(mtd, NAND_CMD_NONE, NAND_NCE | NAND_CTRL_CHANGE);

    ndelay(100);
    nand_wait_ready(mtd);
}

/*--------------------------------------------------------------------------------*\
 * slow_mode_buffer == NULL - fast mode
 * slow_mode_buffer != NULL - slow mode
 \*--------------------------------------------------------------------------------*/
#define NAND_CMD_COPYBACK   0x35
static int avm_fix_ecc_copy_page(struct mtd_info *mtd, loff_t from, loff_t to, unsigned char *slow_mode_buffer, unsigned int save, struct _magic_convert *magic_convert) {

    int ret = 0, chipnr, page, realpage;
    unsigned char status;
    struct nand_chip *chip = mtd->priv;

    chipnr = (int)(from >> chip->chip_shift);
    chip->select_chip(mtd, chipnr);

    realpage = (int)(from >> chip->page_shift);
    page = realpage & chip->pagemask;

    /*--- read Page on copy-back ---*/
    if ( slow_mode_buffer ) 
        avm_fix_ecc_cmd_copyback(mtd, NAND_CMD_READ0, NAND_CMD_READSTART, page);
    else
        avm_fix_ecc_cmd_copyback(mtd, NAND_CMD_READ0, NAND_CMD_COPYBACK, page);

    /*--- read Status ---*/
    chip->cmdfunc(mtd, NAND_CMD_STATUS, -1, -1);
    status = chip->read_byte(mtd);

    /*--- read-mode ---*/
    chip->cmd_ctrl(mtd, NAND_CMD_READ0, NAND_NCE | NAND_CLE | NAND_CTRL_CHANGE);
    chip->cmd_ctrl(mtd, NAND_CMD_NONE, NAND_NCE | NAND_CTRL_CHANGE);

    /*--- wenn es einen Fehler beim Lesen gibt ist hier erstmal schluss ---*/
    /*--- wir schreiben den ganzen Block neu ---*/
    if (save == 0) {
        if ( status & (NAND_STATUS_FAIL | NAND_CHIP_READ_STATUS)) {
            pr_debug("{%s} Page 0x%llx readstatus 0x%x\n", __func__, from, status);
            return 1;
        }
        return 0;
    }

    //		pr_debug("{%s} Copy page 0x%llx\n", __func__, from);

    if ( slow_mode_buffer ) {
        int len = mtd->writesize;
        unsigned char *read_buf = slow_mode_buffer;
        while (len--) {
            *read_buf++ = chip->read_byte(mtd);
        }
        // write it back
        if (avm_write_page(mtd, to, mtd->writesize, read_buf) < 0 ) {
            pr_err("{%s} ERROR write Block %d\n", __func__, page);
            return -1;
        }

    } else {
        realpage = (int)(to >> chip->page_shift);
        page = realpage & chip->pagemask;
        /*--- write Page to ---*/
        chip->cmdfunc(mtd, NAND_CMD_RNDIN, 0x00, page);

        /*--- Page schreiben ---*/
        chip->cmdfunc(mtd, NAND_CMD_PAGEPROG, -1, -1);
        status = chip->waitfunc(mtd, chip);
        if (status & NAND_STATUS_FAIL) {
            pr_err("{%s} ERROR write status failed 0x%x\n", __func__, status);
            ret = -1;
        }
    }
    chip->select_chip(mtd, -1);
    return ret;
}

int avm_check_status(struct mtd_info *mtd, loff_t status_block, struct _magic_convert *magic_convert, int *recover) {
    int retlen = 0;

    if (!magic_convert || !recover) {
        pr_err("{%s} Invalid params\n", __func__);
        return -1;
    }
    memset(magic_convert, 0, sizeof(struct _magic_convert));
    (*recover) = 0;

    if (mtd->_read(mtd, status_block, sizeof(struct _magic_convert), &retlen, (uint8_t *)magic_convert)){
        pr_err("{%s} ERROR read status block %0llx\n", __func__, status_block);
        return -1;
    }

    if (retlen != sizeof(struct _magic_convert)) {
        pr_err("{%s} ERROR reading status. Start from beginning\n", __func__);
        memset(magic_convert, 0, sizeof(struct _magic_convert));
        magic_convert->magic = CONVERT_MAGIC;
        return 0;
    }
    if (magic_convert->magic != CONVERT_MAGIC) {
        pr_err("{%s} Convert magic not found\n", __func__);
        // first run, walk over the flash normally
        memset(magic_convert, 0, sizeof(struct _magic_convert));
        magic_convert->magic = CONVERT_MAGIC;
    } else {
        // magic is set, check status
        if (magic_convert->status == CONVERT_DONE) {
            pr_info("{%s} Convert already done.\n", __func__);
            return 0;
        } else if (magic_convert->status == CONVERT_ONGOING) {
            // convert ongoing -> always recover
            pr_info("{%s} Status block valid. Write in source block [0x%llx]\n", __func__, magic_convert->page_number);
            (*recover) = 1;
        } else {
            pr_err("{%s} ERROR invalid convert status[%d] Start from beginning.\n", __func__, magic_convert->status);
            memset(magic_convert, 0, sizeof(struct _magic_convert));
            magic_convert->magic = CONVERT_MAGIC;
        }
    }
    return 0;
}


/*--------------------------------------------------------------------------------*\
 * Subpagewrite mit NAND_ECC_SOFT verträgt sich nicht mit Onchip-ECC, wenn das
 * oob_layout von NAND_ECC_SOFT genutzt wird
 \*--------------------------------------------------------------------------------*/
static int avm_fix_ecc(struct mtd_info *mtd, enum copy_mode speed) {

    struct device_node *node = NULL, *child = NULL;
    char* comp = "lantiq,nand-xway";

    uint32_t prop_size;
    uint32_t ubi[2], kernel[2];
    const void* prop;
    loff_t from, to, freeblock[2], status_block;

    struct nand_chip *chip = mtd->priv;
    struct erase_info instr;

    unsigned char *pbuffer, *buffer = NULL;
    struct _magic_convert *magic_convert = NULL;
    int recover = 0;

    node = of_find_compatible_node(node, NULL, comp);
    if ( ! node ) {
        pr_err("{%s} no node found\n", __func__);
        return 1;
    }

    child = of_get_child_by_name(node, "ubi");
    if ( ! child) {
        pr_err("{%s} no child found\n", __func__);
        return 1;
    }

    prop = of_get_property(child, "reg", &prop_size);
    if( ! prop || (prop_size != 8)) {
        return -1;
    } else {
        if(of_property_read_u32_array(child, "reg", ubi, ARRAY_SIZE(ubi))) {
            pr_err("{%s} no Parameter found\n", __func__);
            return 1;
        }
        pr_debug("{%s} UBI 0x%x 0x%x\n", __func__, ubi[0], ubi[1]);
    } 

    child = of_get_child_by_name(node, "kernel");
    if ( ! child) {
        pr_err("{%s} no child found\n", __func__);
        return 1;
    }

    prop = of_get_property(child, "reg", &prop_size);
    if( ! prop || (prop_size != 8)) {
        return -1;
    } else {
        if(of_property_read_u32_array(child, "reg", kernel, ARRAY_SIZE(kernel))) {
            pr_err("{%s} no Parameter found\n", __func__);
            return 1;
        }
        pr_debug("{%s} Kernel 0x%x 0x%x\n", __func__, kernel[0], kernel[1]);
    } 

    if (speed == slow) {
        buffer = kmalloc(mtd->erasesize, GFP_KERNEL);
        if ( ! buffer) {
            pr_err("{%s} no Buffer available\n", __func__);
            return 1;
        }
    }

    instr.mtd = mtd;
    instr.len = mtd->erasesize;
    instr.callback = NULL;
    instr.priv = 0;
    instr.next = NULL;

    /*--------------------------------------------------------------------------------*\
     * am schnellsten geht das kopieren per CopyBack
     * 00h - 35h ... 85h - 10h - wir brauche die Daten nicht einzeln lesen
     * wir brauchen 2 leere Blocks - CopyBack funktioniert innerhalb eines District
     * also gerade Blockadresse zu gerader Blockadresse usw.
     \*--------------------------------------------------------------------------------*/
    /*--- wir brauchen 2 leeren Blocks zum schnellen umkopieren ---*/
    freeblock[1] = kernel[0] + kernel[1] - mtd->erasesize;
    freeblock[0] = freeblock[1] - mtd->erasesize;
    status_block = freeblock[0] - mtd->erasesize;

    if (chip->block_bad(mtd, freeblock[0], 1) || chip->block_bad(mtd, freeblock[1], 1)) {
        status_block = freeblock[1];
        freeblock[1] = freeblock[0] - mtd->erasesize;
        freeblock[0] = freeblock[1] - mtd->erasesize;
        if (chip->block_bad(mtd, freeblock[0], 1) || chip->block_bad(mtd, freeblock[1], 1)) {
            pr_err("{%s} bad freeblocks\n", __func__);
            if (buffer)
                kfree(buffer);
            return -1;
        }
    }

    if (chip->block_bad(mtd, status_block, 1)) {
        status_block -= mtd->erasesize;
        if (chip->block_bad(mtd, status_block, 1)) {
            pr_err("{%s} bad status_block\n", __func__);
            if (buffer)
                kfree(buffer);
            return -1;
        }
    }


    // we need to alloc for writesize in order to write to a page
    magic_convert = kmalloc(mtd->writesize, GFP_KERNEL);
    if (!magic_convert) {
        pr_err("{%s} ERROR oom\n", __func__);
        if (buffer)
            kfree(buffer);
        return -1;
    }
    memset(magic_convert, 0, mtd->writesize);
    recover = 0;


    if (avm_check_status(mtd, status_block, magic_convert, &recover)) {
        pr_err("{%s} ERROR reading buffer blocks [0x%llx] [0x%llx]\n", __func__, freeblock[0], freeblock[1]);
        if (buffer)
            kfree(buffer);
        kfree(magic_convert);
        return -1;
    }

    if (magic_convert->status == CONVERT_DONE) {
        pr_info("{%s} Convert done.\n", __func__);
        if (buffer)
            kfree(buffer);
        kfree(magic_convert);
        return 0;
    } else if (magic_convert->status == CONVERT_ONGOING) {
        from = magic_convert->page_number;
    } else {
        magic_convert->status = CONVERT_ONGOING;
        from = ubi[0];

        // erase blocks
        instr.addr = freeblock[0];
        if (mtd->_erase(mtd, &instr)) {
            pr_err("{%s} ERROR erase BufferBlock 0x%llx\n", __func__, instr.addr);
            if (buffer)
                kfree(buffer);
            kfree(magic_convert);
            return -1;
        }
        instr.addr = freeblock[1];
        if (mtd->_erase(mtd, &instr)) {
            pr_err("{%s} ERROR erase BufferBlock 0x%llx\n", __func__, instr.addr);
            if (buffer)
                kfree(buffer);
            kfree(magic_convert);
            return -1;
        }
        instr.addr = status_block;
        if (mtd->_erase(mtd, &instr)) {
            pr_err("{%s} ERROR erase status block 0x%llx\n", __func__, instr.addr);
            if (buffer)
                kfree(buffer);
            kfree(magic_convert);
            return -1;
        }
    }

    pr_info("{%s} Start convert at 0x%llx\n", __func__, from);
    /*--- alles hin- und herkopieren ---*/
    for (; from < (ubi[0] + ubi[1]); from += mtd->erasesize) { 
        loff_t page_to, page_from;
        unsigned int save;

        if (chip->block_bad(mtd, from, 1)) {
            pr_debug("{%s} bad Block 0x%llx\n", __func__, from);
            continue;
        }


        /*--- der Block in den gesichert wird muss im selben District sein ---*/
        to = freeblock[ (from >> chip->phys_erase_shift) & 1 ];


        /*--- save == 0 - erstmal nur lesen ---*/
        /*--- save == 1 - umkopieren ---*/
        save = 0;
retry_save:
        pr_debug("{%s} Analyze Block 0x%llx (saved [%d] to 0x%llx)\n", __func__, from, save, to);
        /*--- Page sichern, pbuffer ist 0 im fast-Mode ---*/
        if (!recover) {
            for (page_from = from, page_to = to, pbuffer = buffer; page_from < (from + mtd->erasesize); page_from += mtd->writesize, page_to += mtd->writesize) {
                // save current block
                magic_convert->page_number = from;
                if (avm_fix_ecc_copy_page(mtd, page_from, page_to, pbuffer, save, magic_convert) > 0) {
                    /*--- pr_debug("{%s} rewrite block 0x%llx\n", __func__, page_from); ---*/
                    /*--- beim lesen ist ein Fehler aufgetreten - alles noch einmal, den Block umkopieren ---*/
                    save = 1;
                    goto retry_save;
                }
                if ( speed == slow ) {
                    pbuffer += mtd->writesize;  /*--- pbuffer ist 0 im fast-Mode ---*/
                }
            }
            if ( save == 0 )    
                continue;   /*--- nichts gefunden - nächster Block ---*/

            // write status block
            if (avm_write_page(mtd, status_block, mtd->writesize, (uint8_t *)magic_convert) < 0) {
                pr_err("{%s} ERROR write status block\n", __func__);
                if (buffer)
                    kfree(buffer);
                kfree(magic_convert);
                return -1;
            }
            /*--- Block löschen ---*/
            instr.addr = from;
            if (mtd->_erase(mtd, &instr)) {
                pr_err("{%s} ERROR erase srcBlock 0x%llx\n", __func__, instr.addr);
                if (buffer)
                    kfree(buffer);
                kfree(magic_convert);
                return -1;
            }
            pr_debug("{%s} Block 0x%llx saved to 0x%llx\n", __func__, from, to);
        }


        /*--- zurückschreiben ---*/
        pr_debug("{%s} Write back block 0x%llx\n", __func__, from);
        if ( speed == fast ) {
            pr_debug("{%s} Use flash specific direct page copy command\n", __func__);
            for (page_from = from, page_to = to; page_from < (from + mtd->erasesize); page_from += mtd->writesize, page_to += mtd->writesize) {
                /*--- pr_debug("{%s} copy page_from 0x%llx page_to 0x%llx\n", __func__, page_to, page_from); ---*/
                avm_fix_ecc_copy_page(mtd, page_to, page_from, 0, save, magic_convert);
            }

        } else {
            pr_debug("{%s} Use ram buffer to copy data\n", __func__);
            for (page_from = from, page_to = to, pbuffer = buffer; page_from < (from + mtd->erasesize); page_from += mtd->writesize, page_to += mtd->writesize, pbuffer += mtd->writesize) {
                if (recover) {
                    size_t retlen = 0;
                    if (page_from == from)
                        pr_debug("{%s} Read back buffer block 0x%llx\n", __func__, to);
                    // read back from buffer block
                    mtd->_read(mtd, page_to, mtd->writesize, &retlen, pbuffer);
                }
                if (avm_write_page(mtd, page_from, mtd->writesize, pbuffer) < 0) {
                    pr_err("{%s} ERROR write saveBlock 0x%llx\n", __func__, instr.addr);
                    if (buffer)
                        kfree(buffer);
                    kfree(magic_convert);
                    return -1;
                }
            }
        }
        recover = 0;
        /*--- Sicherung löschen ---*/
        pr_debug("{%s} Clear buffer block 0x%llx and status block\n", __func__, to);
        instr.addr = to;
        if (mtd->_erase(mtd, &instr)) {
            pr_err("{%s} ERROR erase saveBlock 0x%llx\n", __func__, instr.addr);
            if (buffer)
                kfree(buffer);
            kfree(magic_convert);
            return -1;
        }
        instr.addr = status_block;
        if (mtd->_erase(mtd, &instr)) {
            pr_err("{%s} ERROR erase status block 0x%llx\n", __func__, instr.addr);
            if (buffer)
                kfree(buffer);
            kfree(magic_convert);
            return -1;
        }

    }

    // Buffer block is already deleted
    // Mark conversion as done
    pr_info("{%s} Flash convert done. Write done marker.\n", __func__);
    magic_convert->status = CONVERT_DONE;
    avm_write_page(mtd, status_block, mtd->writesize, (uint8_t *)magic_convert);

    if (buffer)
        kfree(buffer);
    kfree(magic_convert);

    return 0;
}

static unsigned int avm_fix_ecc_done = 0;
static int __init avm_set_fix_ecc(char *p) {

    if (p) {
        pr_info("[%s]\n", __func__);
        avm_fix_ecc_done = 1;
    }
    return 0;
}
__setup("chip_ecc", avm_set_fix_ecc);
#endif /*--- #if ! defined(CONFIG_PRODTEST) ---*/

/*
 * Dummy function for saving the associated mtd_info to use
 * during panic_reinit()
 */
static int xway_nand_bbt(struct mtd_info *mtd)
{
    int ret;
    struct nand_chip *chip = mtd->priv;

#if defined(CONFIG_TFFS_DEV_MTDNAND)
    if(panic_mtd == NULL){
        pr_info("[%s] Using MTD %s as panic mtd\n", __func__, mtd->name);
        panic_mtd = mtd;
    }
#endif
    ret = nand_default_bbt(mtd);
#if ! defined(CONFIG_PRODTEST)
    if ((chip->ecc.layout == &nand_oob_128_tsh) && (avm_fix_ecc_done == 0)) {
        pr_info("[avm_fix_ecc]\n");
        avm_fix_ecc(mtd, fast);
    }
    if ((chip->ecc.layout == &nand_oob_64_mx30lf1ge8ab) && (avm_fix_ecc_done == 0)) {
        pr_info("[avm_fix_ecc] slow\n");
        avm_fix_ecc(mtd, slow);
    }
#endif /*--- #if ! defined(CONFIG_PRODTEST) ---*/
    return ret;
}

static int xway_nand_probe(struct platform_device *pdev)
{
    struct nand_chip *this = platform_get_drvdata(pdev);
    unsigned long nandaddr = (unsigned long) this->IO_ADDR_W;
    const __be32 *cs = of_get_property(pdev->dev.of_node,
            "lantiq,cs", NULL);


    u32 cs_flag = 0;

    /* load our CS from the DT. Either we find a valid 1 or default to 0 */
    if (cs && (*cs == 1)) {
        cs_flag = NAND_CON_IN_CS1 | NAND_CON_OUT_CS1;
    }

#if defined(CONFIG_LTQ_BCH_MODE)
    this->ecc.mode = NAND_ECC_SOFT_BCH;
    this->ecc.size = 512;
    this->ecc.bytes = ECC_BYTES;
    this->options |= NAND_NO_SUBPAGE_WRITE;
    this->ecc.mode = NAND_ECC_SOFT;
#endif
    /* setup the EBU to run in NAND mode on our base addr for different CS */
    if (cs && (*cs  == 1)) {
        if (of_machine_is_compatible("lantiq,vr9")) {
            ltq_ebu_w32(NANDPHYSADDR(nandaddr)
                    | ADDSEL1_MASK(3) | ADDSEL1_REGEN, EBU_ADDSEL1);
        } else if (of_machine_is_compatible("lantiq,grx500")) {
            ltq_ebu_w32(NANDPHYSADDR(nandaddr)
                    | ADDSEL1_MASK(5) | ADDSEL1_REGEN, EBU_ADDSEL1);
        } else {
            ltq_ebu_w32(NANDPHYSADDR(nandaddr)
                    | ADDSEL1_MASK(2) | ADDSEL1_REGEN, EBU_ADDSEL1);
        }

        ltq_ebu_w32(BUSCON1_SETUP | BUSCON1_BCGEN_RES | BUSCON1_WAITWRC2
                | BUSCON1_WAITRDC2 | BUSCON1_HOLDC1 | BUSCON1_RECOVC1
                | BUSCON1_CMULT4, LTQ_EBU_BUSCON1);

        ltq_ebu_w32(NAND_CON_NANDM | NAND_CON_CSMUX | NAND_CON_CS_P
                | NAND_CON_SE_P | NAND_CON_WP_P | NAND_CON_PRE_P
                | cs_flag, EBU_NAND_CON);	
    } else if (cs && (*cs == 0)) {
        ltq_ebu_w32(NANDPHYSADDR(nandaddr)
                |  ADDSEL1_MASK(1) | ADDSEL0_REGEN, EBU_ADDSEL0);

        ltq_ebu_w32(BUSCON0_SETUP | BUSCON0_ALEC | BUSCON0_BCGEN_RES
                | BUSCON0_WAITWRC2 | BUSCON0_WAITRDC2
                | BUSCON0_HOLDC1 | BUSCON0_RECOVC1
                | BUSCON0_CMULT4, LTQ_EBU_BUSCON0);

        ltq_ebu_w32(NAND_CON_CSMUX | NAND_CON_CS_P
                | NAND_CON_SE_P | NAND_CON_WP_P
                | NAND_CON_PRE_P, EBU_NAND_CON);
    } else {
        pr_err("Platform does not support chip select %d\n", cs_flag);
    }

    /* finish with a reset */
    xway_reset_chip(this);

    /* check if we want to use the on chip ecc */
    /* available devices are stored in the device tree */
    if (of_have_populated_dt()){
        struct mtd_info dummy_mtd;
        struct device_node *node, *child;
        uint8_t id_data[8];
        int i;
        char* comp = "avm,nand_use_on_chip_ecc_devices";

        dummy_mtd.priv = this;
        this->cmd_ctrl	= xway_cmd_ctrl;
        xway_select_chip(NULL, 0);      /*--- select chip ---*/

        nand_command(&dummy_mtd, NAND_CMD_RESET, -1, -1);

        /* Send the command for reading device ID */
        nand_command(&dummy_mtd, NAND_CMD_READID, 0x00, -1);
        /* Read entire ID string */
        for (i = 0; i < 8; i++){
            id_data[i] = xway_read_byte(&dummy_mtd);
        }
        xway_select_chip(NULL, -1);     /*--- deselect chip ---*/

        node = NULL;
        node = of_find_compatible_node(node, NULL, comp);
        while(NULL != node){
            for_each_child_of_node(node, child){
                uint8_t of_id[8];
                uint32_t prop_size;
                const void* prop = of_get_property(child, "id", &prop_size);
                if(prop && prop_size <= 8){
                    if(! of_property_read_u8_array(child, "id", of_id, prop_size)){
                        if(! memcmp(of_id, id_data, prop_size)){
                            const char* oob_str =  of_get_property(child, "oob", &prop_size);
                            if(oob_str){
                                if(!strcmp(nand_oob_64_tc58bvg0s3hta_str, oob_str)){
                                    pr_err("{%s} use HW-ECC tc58bvg0s3hta\n", __func__);
                                    this->ecc.layout = &nand_oob_64_tc58bvg0s3hta;
                                    this->ecc.calculate = NULL;
                                    this->ecc.correct = NULL;

                                    this->ecc.read_page = nand_read_page_nand_on_chip_ecc_tsh;
                                    this->ecc.read_page_raw = nand_read_page_raw;

                                    this->ecc.write_page = nand_write_page_raw;
                                    this->ecc.write_page_raw = nand_write_page_raw;
                                    this->ecc.read_oob = nand_read_oob_std;
                                    this->ecc.write_oob = nand_write_oob_std;

                                    this->ecc.size = 512;
                                    this->ecc.strength = 8;
                                    this->ecc.mode = NAND_ECC_HW;
                                    this->options |= NAND_NO_SUBPAGE_WRITE;
                                }
                                if(!strcmp(nand_oob_64_tc58bvg2s0hta_str, oob_str)){
                                    pr_err("{%s} use HW-ECC tc58bvg2s0hta\n", __func__);
                                    this->ecc.layout = &nand_oob_128_tsh;

                                    this->ecc.calculate = NULL;
                                    this->ecc.correct = NULL;

                                    this->ecc.read_page = nand_read_page_nand_on_chip_ecc_tsh;
                                    this->ecc.read_page_raw = nand_read_page_raw;
                                    this->ecc.read_subpage = nand_read_subpage_nand_on_chip_ecc_tsh;

                                    this->ecc.write_page = nand_write_page_raw;
                                    this->ecc.write_page_raw = nand_write_page_raw;
                                    this->ecc.read_oob = nand_read_oob_std;
                                    this->ecc.write_oob = nand_write_oob_std;

                                    this->ecc.size = 512;
                                    this->ecc.strength = 8;
                                    this->ecc.mode = NAND_ECC_BENAND;
                                    this->options |= NAND_SUBPAGE_READ;
                                }
                                if(!strcmp(nand_oob_64_mx30lf1ge8ab_str, oob_str)){
                                    pr_err("{%s} use HW-ECC mx30fl1ge8ab\n", __func__);
                                    this->ecc.layout = &nand_oob_64_mx30lf1ge8ab;

                                    this->ecc.calculate = NULL;
                                    this->ecc.correct = NULL;

                                    this->ecc.read_page = nand_read_page_nand_on_chip_ecc_mx;
                                    this->ecc.read_page_raw = nand_read_page_raw;
                                    this->ecc.read_subpage = nand_read_subpage_nand_on_chip_ecc_mx;

                                    this->ecc.write_page = nand_write_page_raw;
                                    this->ecc.write_page_raw = nand_write_page_raw;
                                    this->ecc.read_oob = nand_read_oob_std;
                                    this->ecc.write_oob = nand_write_oob_std;

                                    this->ecc.size = 512;
                                    this->ecc.strength = 4;
                                    this->ecc.mode = NAND_ECC_BENAND;
                                    this->options |= NAND_SUBPAGE_READ;
                                }
                                if(!strcmp(nand_oob_64_mx30lf4ge8ab_str, oob_str)){
                                    pr_err("{%s} use HW-ECC mx30fl4ge8ab\n", __func__);
                                    this->ecc.layout = &nand_oob_64_mx30lf4ge8ab;
                                    this->ecc.calculate = NULL;
                                    this->ecc.correct = NULL;
                                    this->ecc.read_page = nand_read_page_nand_on_chip_ecc_mx;
                                    this->ecc.write_page = nand_write_page_raw;
                                    this->ecc.read_page_raw = nand_read_page_raw;
                                    this->ecc.write_page_raw = nand_write_page_raw;
                                    this->ecc.read_oob = nand_read_oob_std;
                                    this->ecc.write_oob = nand_write_oob_std;
                                    this->ecc.size = 512;
                                    this->ecc.strength = 4;
                                    this->ecc.mode = NAND_ECC_HW;
                                    this->options |= NAND_NO_SUBPAGE_WRITE;
                                }
                            }
                        }
                    }
                }
            }
            node = of_find_compatible_node(node, NULL, comp);
        }
    }

#if defined(CONFIG_TFFS_DEV_MTDNAND)
    /* ugly hack to get mtd_info */
    this->scan_bbt = xway_nand_bbt;
    panic_chip = this;
    (void) TFFS3_Register_Panic_CB(NULL, panic_reinit);
#endif /* CONFIG_TFFS_DEV_MTDNAND */

    return 0;
}

/* allow users to override the partition in DT using the cmdline */
static const char const *part_probes[] = { "cmdlinepart", "ofpart", NULL };

static struct platform_nand_data xway_nand_data = {
    .chip = {
        .nr_chips		= 1,
        .chip_delay		= 30,
        .part_probe_types	= part_probes,
    },
    .ctrl = {
        .probe		= xway_nand_probe,
        .cmd_ctrl	= xway_cmd_ctrl,
        .dev_ready	= xway_dev_ready,
        .select_chip	= xway_select_chip,
        .read_byte	= xway_read_byte,
        .read_buf	= xway_read_buf,
        .write_buf	= xway_write_buf,
    }
};

/*
 * Try to find the node inside the DT. If it is available attach out
 * platform_nand_data
 */
static int __init xway_register_nand(void)
{
    struct device_node *node;
    struct platform_device *pdev;

    node = of_find_compatible_node(NULL, NULL, "lantiq,nand-xway");
    if (!node) {
        pr_err("Cannot find compatible string\n");
        return -ENOENT;
    }
    pdev = of_find_device_by_node(node);
    if (!pdev) {
        pr_err("Cannot find device in tree\n");
        return -EINVAL;
    }
    pdev->dev.platform_data = &xway_nand_data;
    of_node_put(node);

    pr_err("Register xway nand done!\n");
    return 0;
}

subsys_initcall(xway_register_nand);

#if defined(CONFIG_TFFS_DEV_MTDNAND)
static struct mtd_info *panic_reinit(struct mtd_info *mtd)
{
    struct mtd_info *tmp, *master;

    pr_err("[%s] Called for xway_nand\n", __func__);

    if(panic_mode == 0 && panic_chip != NULL && panic_mtd != NULL){
        panic_mode = 1;
        smp_mb();

        tmp = panic_mtd;
        do {
            master = tmp;
            tmp = get_mtd_part_master(master);
        }while(tmp != NULL);

        if(master == NULL || master->priv != panic_chip){
            return NULL;
        }

        nand_base_panic_reinit(master);
        xway_reset_chip(panic_chip);
    }

    return mtd;
}
#endif /* CONFIG_TFFS_DEV_MTDNAND */


