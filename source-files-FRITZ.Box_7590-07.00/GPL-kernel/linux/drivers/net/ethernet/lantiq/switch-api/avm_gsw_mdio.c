#include <linux/jiffies.h>
#include <linux/timer.h>

#include <lantiq_soc.h>
#include "gsw_init.h"
#include "avm_gsw_mdio.h"
#include <linux/spinlock.h>

struct mdio_cmd{
    struct list_head list;
    unsigned char rw;
    unsigned char is_mmd;
    void *cdev;
    GSW_MDIO_data_t *parm;
    GSW_return_t ret;
    struct semaphore *start;
};

static struct task_struct   *mdio_thread;
static struct semaphore     mdio_sema;
static struct spinlock      list_lock;
struct list_head mdio_list;


int mdio_worker_thread(void *unused){
    unsigned long flags = 0;
    for(;;){
        /*--- wait for new cmd ---*/
        down(&mdio_sema);
        spin_lock_irqsave(&list_lock, flags);
        if(!list_empty(&mdio_list)){
            struct mdio_cmd *cmd = list_first_entry(&mdio_list, struct mdio_cmd, list);
            spin_unlock_irqrestore(&list_lock, flags);
            /*--- mdio or mmd? ---*/
            if(cmd->is_mmd == AVM_MDIO_MMD){
                cmd->ret = _GSW_MDIO_DataWrite(cmd->cdev, &cmd->parm[0]);
                cmd->ret = _GSW_MDIO_DataWrite(cmd->cdev, &cmd->parm[1]);
                cmd->ret = _GSW_MDIO_DataWrite(cmd->cdev, &cmd->parm[2]);
                if(cmd->rw == AVM_MDIO_READ){
                    cmd->ret = _GSW_MDIO_DataRead(cmd->cdev, &cmd->parm[3]);
                }
                else{
                    cmd->ret = _GSW_MDIO_DataWrite(cmd->cdev, &cmd->parm[3]);
                }
            }
            else{
                if(cmd->rw == AVM_MDIO_READ){
                    cmd->ret = _GSW_MDIO_DataRead(cmd->cdev, cmd->parm);
                }
                else{
                    cmd->ret = _GSW_MDIO_DataWrite(cmd->cdev, cmd->parm);
                }
            }
            /*--- remove cmd from list ---*/
            spin_lock_irqsave(&list_lock, flags);
            list_del(&cmd->list);
            spin_unlock_irqrestore(&list_lock, flags);
            /*--- signal that cmd is finished ---*/
            up(cmd->start);
        }
        else{
            spin_unlock_irqrestore(&list_lock, flags);
            pr_err("mdio_worker_thread running even though list is empty!\n");
        }
    }
    return 0;
}
GSW_return_t avm_GSW_MDIO_DataRW(void *cdev, GSW_MDIO_data_t *parm, uint8_t rw, uint8_t avm_mdio){
    struct mdio_cmd *cmd;
    unsigned long flags = 0;
    GSW_return_t ret = GSW_statusOk;    
    cmd = kmalloc(sizeof(struct mdio_cmd), GFP_KERNEL);
    if(!cmd){
        return GSW_statusErr;
    }
    /*--- setup cmd ---*/
    cmd->rw = rw;
    cmd->cdev = cdev;
    cmd->parm = parm; 
    cmd->is_mmd = avm_mdio;
    /*--- append cmd to list---*/
    spin_lock_irqsave(&list_lock, flags);
    list_add_tail(&cmd->list, &mdio_list);
    spin_unlock_irqrestore(&list_lock, flags);
    cmd->start = kmalloc(sizeof(struct semaphore), GFP_KERNEL);
    if(!cmd->start){
        spin_lock_irqsave(&list_lock, flags);
        list_del(&cmd->list);
        spin_unlock_irqrestore(&list_lock, flags);
        kfree(cmd);
        return GSW_statusErr;
    }
    sema_init(cmd->start, 0);
    /*--- kick of thread ---*/
    up(&mdio_sema);
    /*--- wait for cmd to finish ---*/
    down(cmd->start);

    ret = cmd->ret;
    kfree(cmd->start);
    kfree(cmd);
    return ret;
}

void avm_gsw_mdio_init(void){
    /*--- init list ---*/
    static int initialized = 0;
    if(!initialized){
        initialized = 1;
        INIT_LIST_HEAD(&mdio_list);
        spin_lock_init(&list_lock);
        /*--- start worker thread ---*/
        mdio_thread = kthread_create(mdio_worker_thread, NULL, "avm_mdio");
        sema_init(&mdio_sema, 0);
        if(mdio_thread){
            printk(KERN_INFO "[%s] Starting mdio worker thread\n", __func__);
            wake_up_process(mdio_thread);
        }
        else{
            pr_err("[%s] Mdio worker thread not started\n", __func__);
        }
    }
}
#if 0
void avm_gsw_mdio_exit(void){
 int ret;
 ret = kthread_stop(mdio_thread);
 if(!ret)
    printk(KERN_INFO "Thread stopped");
}
#endif
