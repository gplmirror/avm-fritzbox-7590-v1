#include <linux/seq_file.h>
#include <linux/interrupt.h>

#ifdef pr_fmt
#undef pr_fmt
#endif
#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

extern struct dentry *avm_cpunet_debug_dir;

/* Initialization */
int cpunet_hw_init(irq_handler_t clb);
void __exit cpunet_hw_exit(void);

/* Query hardware properties */
unsigned int cpunet_hw_buffer_size(void);
unsigned int cpunet_hw_ctrl_max_num(void);
unsigned int cpunet_hw_data_max_num(void);

/* Receiving messages */
int cpunet_hw_rx(const void **data, unsigned int *len);
void cpunet_hw_rx_done(void);
void *cpunet_hw_read_ctrl(void);

/* Sending messages */
void *cpunet_hw_tx_alloc(void);
int cpunet_hw_tx(void *addr, unsigned int length);
bool cpunet_hw_tx_full(void);

/* Interrupt configuration */
void cpunet_hw_ack_int(void);
void cpunet_hw_enable_int(void);
void cpunet_hw_disable_int(void);

