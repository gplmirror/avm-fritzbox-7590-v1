

export CC			:= $(CROSS_COMPILE)gcc
export CXX			:= $(CROSS_COMPILE)g++
export AR			:= $(CROSS_COMPILE)ar
export RANLIB 		:= $(CROSS_COMPILE)ranlib
export STRIP		:= $(CROSS_COMPILE)strip
export LD			:= $(CROSS_COMPILE)ld
export OBJDUMP	 	:= $(CROSS_COMPILE)objdump
export OBJCOPY	 	:= $(CROSS_COMPILE)objcopy

ifeq ($(TARGET_CPU),arm)
# export HOST			:= $(shell x=$${CROSS_COMPILE} ; echo $${x%-}) 
export TARGET		:= arm-linux-elf
# export TARGET		:= $(shell x=$${CROSS_COMPILE} ; echo $${x%-}) 
endif

ifeq ($(TARGET_CPU),mips)
# export HOST			:= $(shell x=$${CROSS_COMPILE} ; echo $${x%-}) 
export TARGET		:= mipsel-linux-elf
# export TARGET		:= $(shell x=$${CROSS_COMPILE} ; echo $${x%-}) 
endif

export BUILD		:= x86_64-linux
export PATH			:= $(shell echo $${PATH}:$${CROSS_COMPILE%/*})

TOPDIR				:= $(shell pwd)
FILESYSTEM	        := $(TOPDIR)/../filesystem_$(KERNEL_LAYOUT)
INSTALLDIR			:= $(TOPDIR)/filesystem

LIBZ_VERSION		:= 1.2.11


export EXTRA_LD_FLAGS	    := $(shell for i in $(FRITZ_BOX_LIB_PATH) ; do echo "-Wl,-rpath-link,$$i" ; done)
export EXTRA_LD_FLAGS	    += $(shell for i in $(FRITZ_BOX_LIB_PATH) ; do echo "-L$$i" ; done)
export EXTRA_INCLUDE_FLAGS	:= $(shell for i in $(FRITZ_BOX_INCLUDE_PATH) ; do echo "-I$$i" ; done)
export EXTRA_CPP_FLAGS 		:= $(EXTRA_INCLUDE_FLAGS) 

ifeq ($(PRODUKTE),)
CONFIG_FILE     := Config..$(KERNEL_LAYOUT)
TAR_FILE        := libz-$(KERNEL_LAYOUT).tar.gz
else
CONFIG_FILE     := Config.$(PRODUKTE).$(KERNEL_LAYOUT)
TAR_FILE        := libz-$(KERNEL_LAYOUT)-$(PRODUKTE).tar.gz
endif

export CFLAGS = $(DEFAULT_CFLAGS_LIB) $(EXTRA_CPP_FLAGS)
export LDFLAGS = $(DEFAULT_LDFLAGS_LIB) $(EXTRA_LD_FLAGS)


prepare:
	if [ ! -d $(INSTALLDIR) ] ; then mkdir -p $(INSTALLDIR) ; fi
	if [ ! -d zlib-$(LIBZ_VERSION) ]; then \
	    tar xzf zlib-$(LIBZ_VERSION).tar.gz; \
	    for p in *.patch; do \
            ( cd zlib-$(LIBZ_VERSION); patch -p1 < ../$${p} ); \
        done; \
    fi
	( cd zlib-$(LIBZ_VERSION); \
		./configure --prefix=$(INSTALLDIR) \
		; \
	)

all: 
	make -C zlib-$(LIBZ_VERSION) all MAKEFLAGS=  EXTRA_LD_FLAGS="$(EXTRA_LD_FLAGS)" EXTRA_CPP_FLAGS="$(EXTRA_CPP_FLAGS)"

distclean:
	rm -rf libz_static_build 
	rm -rf libz_shared_build
	rm -rf zlib-$(LIBZ_VERSION)
	rm -fr $(INSTALLDIR)
	rm -f add-file-libz

clean:  distclean
	
depend: prepare
	if  [ -d zlib-$(LIBZ_VERSION) ] && [ -d zlib-$(LIBZ_VERSION)/Makefile ]; then \
		make -C zlib-$(LIBZ_VERSION) depend || exit -1 ; \
	fi

install: 
	make -C zlib-$(LIBZ_VERSION) install
	make add-file
	make .strip
	( \
	  	if [ -z "$(PRODUKTE)" ] ; then \
			produkt= ; \
		else \
			produkt=-$(PRODUKTE) ; \
		fi ; \
	  	cd filesystem ; \
		cp ../add-file-libz . -v ; \
		tar czf ../libz-$(KERNEL_LAYOUT)$${produkt}.tar.gz * \
	)

.strip:
	( \
	for i in `find filesystem -type f` ; do \
		if file $$i | grep -q -i 'not stripped' ; then \
			strip_params="" ; strip_type="" ;\
			if file $$i | grep -q -i 'relocatable' ; then strip_type="relocatable" ; strip_params="$${strip_params} --strip-debug" ; fi ; \
			if file $$i | grep -q -i 'shared'      ; then strip_type="shared"      ; strip_params="$${strip_params} --discard-all --strip-debug --strip-unneeded " ; fi ; \
			if file $$i | grep -q -i 'executable'  ; then strip_type="executable"  ; strip_params="$${strip_params} -s" ; fi ; \
			before=`wc --bytes $$i | sed -e 's/\([0123456789]*\) .*/\1/g'` ; \
			$(STRIP) $${strip_params} $$i  || exit -1 ; \
			after=`wc --bytes $$i | sed -e 's/\([0123456789]*\) .*/\1/g'` ; \
			if [ ! "$$before" = "$$after" ] ; then \
				$(ECHO) "[install: strip] Datei $(ECHO_GRUEN)$$i ($${strip_type})$(ECHO_END) hat sich veraendert: zuvor $$before nachher $$after [differenz: $$(($$before - $$after)) Bytes]" ; \
			fi ; \
			continue ; \
		fi ; \
	done; \
	)

add-file:
	( \
	  	if [ -z "$(PRODUKTE)" ] ; then \
			produkt= ; \
		else \
			produkt=\\/$(PRODUKTE) ; \
		fi ; \
		find filesystem -type d | sed -e "s/filesystem\(.*\)/LIBZ    D    777    filesystem_libz_$(KERNEL_LAYOUT)$${produkt}\/.\1    .\1/g" | grep -v ' .$$' ; \
		find filesystem -type l -ls | tr -s ' ' | sed -e "s/.*filesystem/filesystem/g" | cut -d' ' -f1,3 | sed -e "s/filesystem\/\(.*\) \(.*\)/LIBZ    L    777    filesystem_libz_$(KERNEL_LAYOUT)$${produkt}\/.\/\1    \2/g" | grep -v ' .$$' ; \
		find filesystem -type f | grep -ve '.*\.[oa]' | sed -e "s/filesystem\(.*\)/LIBZ    F    777    filesystem_libz_$(KERNEL_LAYOUT)$${produkt}\/.\1    .\1/g" ; \
	) | grep -e '/lib/' | grep -v pkgconfig >add-file-libz
	
##########################################################################################
#
##########################################################################################
install_tar:  install
	if [ ! "$(TAR_FILE)" = "$(INSTALL_TAR_FILE_NAME)" ]; then \
		cp -v $(TAR_FILE) $(INSTALL_TAR_FILE_NAME) ; \
	fi

.PHONY: install clean distclean update
